package Workflows_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Assign_Department_Tab_tests extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_WorkflowTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	  driver.quit();			
	}
	
	@Test
	public void a_View_AssignDepartment_Tab()
	{
		view_AssignDepartmentTab();
	}

	@Test
	public void b_Click_AssignDepartment_Tab() throws InterruptedException
	{
		Thread.sleep(400);
		Click_Assign_Departmet_tab();
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
	@Test
	public void g_reset_test() throws InterruptedException
	{
		Thread.sleep(1000);
		reset();
	} 
	
	@Test
	public void i_View_Assigned_DepartmentTask() throws InterruptedException
	{
		Thread.sleep(500);
		View_Assigned_Department_Task();
	}
	
	@Test
	public void j_View_UnassignedDepartmentTask() throws InterruptedException
	{
		Thread.sleep(500);
		View_UnAssigned_Department_Task();
	}
	
	@Test
	public void k_Assign_Task_To_Department() throws InterruptedException
	{
		Thread.sleep(500);
		AssignTask_ToDepartment();
	}
	
	@Test
	public void l_flashBackTest() throws InterruptedException
	{
		Thread.sleep(1000);
		flashback_filter();
	}
	
	@Test
	public void x_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","unallocated_department_tasks.csv"));
	}
	
	@Test
	public void y_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","unallocated_department_tasks.htm"));
	}
	
	
	@Test
	public void z_emailReport() throws InterruptedException
	{
		Thread.sleep(500);
		email_report();
	}
	
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Assign Department tab ******************************
public void view_AssignDepartmentTab()
{
	WebElement AssignDepartment= driver.findElement(By.linkText("Assign Department"));
	
	Boolean assignDepartment= AssignDepartment.isDisplayed();
	
	Assert.assertTrue(assignDepartment);
}


//********************************* Method to click Assign department ******************************
public void Click_Assign_Departmet_tab() throws InterruptedException
{
	WebElement AssignDepartment= driver.findElement(By.linkText("Assign Department"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(AssignDepartment).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}


//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	col= "Type";
	expvalue= "Create Bill Run Workflow";
	
	view_filtered_report();
	
	// 	Verify report
	
	Thread.sleep(600);
	
	String verifyreport= driver.findElement(By.xpath("//div[@id='apexir_DATA_PANEL']//tr[2]//td[3]")).getText();
	
	Thread.sleep(300);
	
	Reset();
	
	Assert.assertEquals(verifyreport, expvalue);
	
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 

	save_report();
	
	//click apply
	
	Click_apply();
	
	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
	
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();
	
	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
	
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
	select_primary_report();
	
	Thread.sleep(300);
	
	try {
	
		WebElement primaryreport= driver.findElement(By.id("7228722074227012"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	
	}catch(Exception e) 	{
		
		WebElement primaryreport= driver.findElement(By.id("7228722074227012"));
	
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}
	
}
//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
{
	Thread.sleep(200);

	Reset();
	
	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("7228722074227012"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
	
}

//********************************* Method to view Assigned department task ****************
public void View_Assigned_Department_Task()
{
	//Click assigned department

	WebElement assigned= driver.findElement(By.id("P2_ALLOCATED_1"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(assigned).click().build().perform();
	
	//Verify
	
	Boolean verify= driver.findElement(By.id("P2_ALLOCATED_1")).isSelected();
	
	Assert.assertTrue(verify);

  }

//********************************* Method to view unassigned department task ****************
public void View_UnAssigned_Department_Task()
{
	//Click Unassigned department
	
	WebElement Unassigned= driver.findElement(By.id("P2_ALLOCATED_0"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Unassigned).click().build().perform();
	
	//Verify
	
	Boolean verify= driver.findElement(By.id("P2_ALLOCATED_0")).isSelected();
	
	Assert.assertTrue(verify);
}

//************************************ Method to assign task to department ********************
public void AssignTask_ToDepartment() throws InterruptedException
{
	// Save task id before assign

	String id= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[2]")).getText();
	
	System.out.println(id);
		
	//Select task
	
	WebElement task= driver.findElement(By.xpath("//div[@id='apexir_WORKSHEET_REGION']//tr[2]//td[1]//input[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(task).click().build().perform();
	
	Thread.sleep(300);	
		
	//Click Assign
	
	WebElement assign= driver.findElement(By.id("B79179906539236125"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(assign).click().build().perform();
	
	Thread.sleep(500);	
		
	// Select department
	
	Select department= new Select(driver.findElement(By.xpath("//select[@name='f02']")));
	
	department.selectByIndex(2);
		
	//Click Submit
	
	WebElement submit= driver.findElement(By.id("B7256932199206742"));
	
	Actions act0= new Actions(driver);
	
	act0.moveToElement(submit).click().build().perform();
	
	Thread.sleep(500);	
		
	//Verify in unassigned section
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	verify.clear();
	
	verify.sendKeys(id);
		
	//Click Go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
		
	//verify search
	
	Boolean text1= driver.findElement(By.xpath("//span[@id='apexir_NO_DATA_FOUND_MSG']")).isDisplayed();
	
	Assert.assertTrue(text1);
		
	// Verify in assigned section
	
	//Click assigned department
	
	WebElement assigned= driver.findElement(By.id("P2_ALLOCATED_1"));
	
	Actions act4= new Actions(driver);
	
	act4.moveToElement(assigned).click().build().perform();
	
	Thread.sleep(800);
				
	//verify search
	
	String text= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[2]")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, id);
	
   }

}
