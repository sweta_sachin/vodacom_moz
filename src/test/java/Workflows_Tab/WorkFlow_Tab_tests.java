package Workflows_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WorkFlow_Tab_tests extends common_methods.CommonMethods{
	
	public static String Status= "Completed";
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_WorkflowTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	  driver.quit();			
	}
	
	@Test
	public void a_View_Workflow_Tab()
	{
		view_WorkflowTab();
	}

	@Test
	public void b_Click_Workflow_Tab() throws InterruptedException
	{
		Thread.sleep(400);
		Click_workflow_tab();
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(1000); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
	@Test
	public void g_SearchWorkflow_By_Status() throws InterruptedException
	{
		Thread.sleep(400);
		SearchByStatus();
	}
	
	@Test
	public void la_reset_test() throws InterruptedException
	{
		Thread.sleep(1000);
		reset();
	} 
	
	
	@Test
	public void l_flashBackTest() throws InterruptedException
	{
		Thread.sleep(1000);
		flashback_filter();
	}
	
	@Test
	public void x_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","workflow.csv"));
	}
	
	@Test
	public void y_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","workflow.htm"));
	}
	
	
	@Test
	public void z_emailReport() throws InterruptedException
	{
		Thread.sleep(500);
		email_report();
	}
	
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Workflow tab ******************************
public void view_WorkflowTab()
{
	WebElement Workflow= driver.findElement(By.linkText("Workflow"));
	
	Boolean workflow= Workflow.isDisplayed();
	
	Assert.assertTrue(workflow);
}


//********************************* Method to click Workflow  ******************************
public void Click_workflow_tab() throws InterruptedException
{
	WebElement Workflow= driver.findElement(By.xpath("//div[@id='echo-standard-tabs']//a[contains(text(),'Workflow')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Workflow).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}


//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	col= "Workflow";
	expvalue= "Create Bill Run Workflow";
	
	view_filtered_report();
	
	// 	Verify report
	
	Thread.sleep(600);
	
	String verifyreport= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
	
	Thread.sleep(300);
	
	Reset();
	
	Assert.assertEquals(verifyreport, expvalue);
	
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 

	save_report();
	
	//click apply
	
	Click_apply();
	
	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
	
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();
	
	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
	
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
	select_primary_report();
	
	Thread.sleep(300);
	
	try {
	
		WebElement primaryreport= driver.findElement(By.id("7368421476166896"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}catch(Exception e) 	{
		
		WebElement primaryreport= driver.findElement(By.id("7368421476166896"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}
	
}
//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
{
	Thread.sleep(200);
	
	Reset();
	
	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("7368421476166896"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
	
}

//***************************************** Method to search workflow by status ****************
public void SearchByStatus() throws InterruptedException
{
	 //Select status
		
	Select status= new Select(driver.findElement(By.id("P17_SS_UID")));
	
	status.selectByVisibleText(Status);
	
	Thread.sleep(1000);
		
	//Verify
	
	String verify= driver.findElement(By.xpath("//tr[2]//td[4]")).getText();
	
	Assert.assertEquals(verify, Status);

	}
}
