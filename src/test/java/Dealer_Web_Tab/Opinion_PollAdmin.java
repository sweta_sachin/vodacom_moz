package Dealer_Web_Tab;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Opinion_PollAdmin extends common_methods.CommonMethods {
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_DealersWebTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		
	 Thread.sleep(1000);
	 driver.quit();
	 
	}

	
	@Test
	public void a_View_OpinionPollAdminTab()
	{
		view_OpinionPollAdminTab();
	}

	@Test
	public void b_Click_OpinionPollAdminTab() throws InterruptedException
	{
		Thread.sleep(500);
		click_OpinionPollAdminTab();
	}
	
	@Test
	public void c_CreateOpinionPollAdmin() throws InterruptedException
	{
		Thread.sleep(500);
		CreateOpinionPoll();
	}
	
	@Test
	public void e_Edit_OpinionPoll() throws InterruptedException
	{
		Thread.sleep(500);
		Edit_OpinionPoll();
	}
	
	@Test
	public void f_VerifyEdited() throws InterruptedException
	{
		Thread.sleep(500);
		Verify_edit();
	}
	
	@Test
	public void g_deleteOpinionPoll() throws InterruptedException
	{
		Thread.sleep(500);
		delete_OpinionPoll();
	}
	
	@Test
	public void h_VerifyDeleted() throws InterruptedException
	{
		Thread.sleep(500);
		verifyDeleted();
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Opinion Poll Admin tab ******************************
public void view_OpinionPollAdminTab()
	{
		WebElement cg= driver.findElement(By.linkText("Opinion Poll Admin"));
		
		Boolean CG= cg.isDisplayed();
		
		Assert.assertTrue(CG);
}

//********************************* Method to click Opinion Poll Admin tab ******************************
public void click_OpinionPollAdminTab()
	{

	WebElement CG =driver.findElement(By.linkText("Opinion Poll Admin"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(CG).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("B10826305240363073"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
	
	}
	
//**************************************** Method to create Opinion Poll *******************************
public void CreateOpinionPoll() throws InterruptedException
 {
	
	// Click create
	
	WebElement create = driver.findElement(By.id("B10826305240363073"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(create).click().build().perform();	
	
	Thread.sleep(500);
		
	// Enter question
		
	WebElement name= driver.findElement(By.id("P49_OP_QUESTION"));
	
	name.clear();
	
	name.sendKeys(Name);
	
	// Select display option
	
	WebElement display = driver.findElement(By.id("P49_OP_DISPLAY_0"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(display).click().build().perform();	
	
	Thread.sleep(500);
		
	//Select voting status
	
	WebElement voting = driver.findElement(By.id("P49_OP_VOTE_STATUS_0"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(voting).click().build().perform();	
	
	Thread.sleep(500);
		
	//Select display on home page option
	
	WebElement option = driver.findElement(By.id("P49_OP_DISPLAY_HOMEPAGE_0"));
	
	Actions act3= new Actions(driver);
	
	act3.moveToElement(option).click().build().perform();	
	
	Thread.sleep(500);
		
	//click create
	
	WebElement create1 = driver.findElement(By.id("B10827209036363084"));
	
	Actions act4= new Actions(driver);
	
	act4.moveToElement(create1).click().build().perform();	
	
	Thread.sleep(500);
	
	//check success message
	
	Success_Msg();
	
	}


//************************* Method to check success message *************************************************
public void Success_Msg()
{

	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
	
	String Expected_msg= "Action Processed.";
	
	Assert.assertEquals(Msg, Expected_msg);
}

//************************* Method to verify created Opinion Poll ************************************************
public boolean VerifyCreated() throws InterruptedException
  {
	
	//Verify name
	
	List<WebElement> text1= driver.findElements(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/table[1]/tbody[1]/tr/td[2]"));
	
	for(int i=0;i<=text1.size();i++){
		
		while(text1.get(i) != null)  {
			
			  String text= text1.get(i).getText();
			
			  if(text==Name) {
				  
					System.out.println(text);
					
					Assert.assertEquals(text, Name);
					
					return true;
				}else {
					
					return false;
				}
			}
		}
		
		return false;
					
		}

//*************************************** Method to edit Opinion Poll ****************************************
public void Edit_OpinionPoll() throws InterruptedException
	{
	    //click edit
			
	WebElement edit = driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(500);
	
	//change the question
	
	WebElement name= driver.findElement(By.id("P49_OP_QUESTION"));
	
	name.clear();
	
	name.sendKeys(Name1);
	
	//Click apply changes
	
	WebElement go = driver.findElement(By.id("B10827301331363084"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();	
	
	Thread.sleep(500);
	
	//check success message
		
	Success_Msg();
		 
	}

//****************************************** Method to verify edit *************************************
public void Verify_edit() throws InterruptedException
{
	//Verify name
	
	String text1= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
	
	Assert.assertEquals(text1, Name1);
	
}

//******************************************* Method to delete Opinion Poll***********************************
public void delete_OpinionPoll() throws InterruptedException
{
  //click edit

	WebElement edit = driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(500);
	
	//Click delete
	
	WebElement delete = driver.findElement(By.id("B10827404424363084"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(delete).click().build().perform();	
	
	Thread.sleep(300);
	
	//Accept alert
	
	Alert alert= driver.switchTo().alert();
	
	alert.accept();
	
	Thread.sleep(300);
	
	//check success message

	Success_Msg();
	
}

//******************************************** Method to verify deleted Opinion Poll ***************************
public void verifyDeleted()
	{
	//Verify name

	String text1= driver.findElement(By.xpath("//tr[2]//td[3]")).getText();
	
	Assert.assertNotEquals(text1, Name1);

	}

}
