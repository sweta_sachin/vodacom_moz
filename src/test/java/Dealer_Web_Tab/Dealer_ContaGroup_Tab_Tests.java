package Dealer_Web_Tab;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Dealer_ContaGroup_Tab_Tests  extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_DealersWebTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		
	 Thread.sleep(1000);
	 driver.quit();
	 
	}

	
	@Test
	public void a_View_Delear_ContactGroupTab()
	{
		view_ContactGroupTab();
	}

	@Test
	public void b_Click_ContactGroupTab() throws InterruptedException
	{
		Thread.sleep(500);
		click_ContactGroupTab();
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(1000);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(3000); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(1500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void g_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","contact_groups.csv"));
	}
	
	@Test
	public void h_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
		
	}
		
	@Test
	public void i_reset_test() throws InterruptedException
	{
		Thread.sleep(800);
		reset();
	}
	

	@Test
	public void z_flashBackTest() throws InterruptedException
	{
		Thread.sleep(500);
		flashback_filter();
	}
	
   @Test
   public void k_Create_ContactGroup() throws InterruptedException
   {
	   create_ContactGroup();
   }
   
   @Test
   public void l_verify_CreatedGroup() throws InterruptedException
   {
	   Thread.sleep(500);
	   VerifyCreated();
   }
   
   @Test
   public void m_Edit_ContactGroup() throws InterruptedException
   {
	   Thread.sleep(500);
	   Edit_ContactGroup();
   }
   
   @Test
   public void n_VerifyEdited() throws InterruptedException
   {
	   Verify_edit();
   }
   
   @Test
   public void o_DeleteGroup() throws InterruptedException
   {
	   Thread.sleep(500);
	   delete_ContactGroup();
   }
   
   @Test
   public void p_VerifyDeleted() throws InterruptedException
   {
	   Thread.sleep(500);
	   verifyDeleted();
   }
   
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Contact group tab ******************************
public void view_ContactGroupTab()
{
	WebElement cg= driver.findElement(By.linkText("Contact Groups"));
	
	Boolean CG= cg.isDisplayed();
	
	Assert.assertTrue(CG);
}

//********************************* Method to click Contact group tab ******************************
public void click_ContactGroupTab()
{
	WebElement CG =driver.findElement(By.linkText("Contact Groups"));

	Actions act= new Actions(driver);
	
	act.moveToElement(CG).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}	
//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	 col= "Group Name";
	 expvalue= "test";
	
	 view_filtered_report();
	
	// Verify report
	
	 Thread.sleep(300);
	
	 String verifyreport= driver.findElement(By.xpath("//div[@id='apexir_DATA_PANEL']//td[2]")).getText();
	
	 Thread.sleep(300);
	
	 Assert.assertEquals(verifyreport, expvalue);
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 
	
	save_report();
		
	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();	
	
	// verify 

	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
	{
		select_primary_report();

		Thread.sleep(300);
		
		try {

			WebElement primaryreport= driver.findElement(By.id("4946130720776080"));
			
			Boolean report= primaryreport.isDisplayed();
		
			Assert.assertTrue(report);
		}catch(Exception e) 	{
			
			WebElement primaryreport= driver.findElement(By.id("8590907286396848"));
			
			Boolean report= primaryreport.isDisplayed();
			
			Assert.assertTrue(report);
		}
}

//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
	{
		Reset();

		// Verify report

		WebElement primaryreport= driver.findElement(By.id("8590907286396848"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}

//**************************************** Method to create contact group *******************************
public void create_ContactGroup() throws InterruptedException
{
      // Click create

	WebElement create = driver.findElement(By.id("B8591517209396853"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(create).click().build().perform();	
	
	Thread.sleep(500);
		
	// Enter name
	
	WebElement name= driver.findElement(By.id("P22_DCG_NAME"));
	
	name.clear();
	
	name.sendKeys(Name);
		
	//click create
	
	WebElement create1 = driver.findElement(By.id("B8588017899396804"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(create1).click().build().perform();	
	
	Thread.sleep(500);
		
	//check success message
	
	Success_Msg();
}

//************************* Method to check success message *************************************************
public void Success_Msg()
{
	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();

	String Expected_msg= "Action Processed.";
	
	Assert.assertEquals(Msg, Expected_msg);
}

//************************* Method to verify created contact group ************************************************
public void VerifyCreated() throws InterruptedException
{
   //Enter contact name in the search box
	
	WebElement name= driver.findElement(By.id("apexir_SEARCH"));
	
	name.clear();
	
	name.sendKeys(Name);
	
	//click go
	
	WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(300);
	
	//Verify name
	
	String text1= driver.findElement(By.xpath("//tr[@class='even']//td[2]")).getText();
	
	Assert.assertEquals(text1, Name);
	
}

//*************************************** Method to edit contact group****************************************
public void Edit_ContactGroup() throws InterruptedException
{
    //click edit
	
	WebElement edit = driver.findElement(By.xpath("//tr[@class='even']//td//a//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(300);
	
	//change the contact name
	
	WebElement name= driver.findElement(By.id("P22_DCG_NAME"));
	
	name.clear();
	
	name.sendKeys(Name1);
	
	//Click apply changes
	
	WebElement go = driver.findElement(By.id("B8588102955396804"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();	
	
	Thread.sleep(300);
	
	//check success message
		
	Success_Msg();
 }


//****************************************** Method to verify edit *************************************
public void Verify_edit() throws InterruptedException
{
	Reset();

	//Enter contact name in the search box
	
	WebElement name= driver.findElement(By.id("apexir_SEARCH"));
	
	name.clear();
	
	name.sendKeys(Name1);
	
	//click go
	
	WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(300);
	
	//Verify name
	
	String text1= driver.findElement(By.xpath("//tr[@class='even']//td[2]")).getText();
	
	Reset();
	
	Assert.assertEquals(text1, Name1);
   
}

//******************************************* Method to delete contact group***********************************
public void delete_ContactGroup() throws InterruptedException
{
	  //click edit

	WebElement edit = driver.findElement(By.xpath("//tr[@class='even']//td//a//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(500);
	
	//Click delete
	
	WebElement delete = driver.findElement(By.id("B8588210011396804"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(delete).click().build().perform();	
	
	Thread.sleep(300);
		
	//Accept alert
	
	Alert alert= driver.switchTo().alert();
	
	alert.accept();
	
	Thread.sleep(300);

	//check success message
	
	Success_Msg();
	
}

//******************************************** Method to verify deleted contact ***************************
public void verifyDeleted() throws InterruptedException
{
	Reset();
	
	//Verify name
	
	String text1= driver.findElement(By.xpath("//tr[@class='even']//td[3]")).getText();
	
	Assert.assertNotEquals(text1, Name1);
}

}

