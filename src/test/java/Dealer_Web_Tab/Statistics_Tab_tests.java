package Dealer_Web_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Statistics_Tab_tests extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_DealersWebTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}
	
	@Test
	public void a_View_Statistics_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		view_StatisticsTab();
	}
	
	@Test
	public void b_Click_Statistics_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		click_StatisticsTab();
	}

	@Test
	public void c_View_NumberOfUsersPerDay() throws InterruptedException
	{
		Thread.sleep(300);
		View_NumberOfUsersPerDay();
	}
	
	@Test
	public void d_View_InactiveUsers() throws InterruptedException
	{
	
		Thread.sleep(300);
		View_InactiveUsers();
	}
	
	@Test
	public void e_View_UsagePerMonth() throws InterruptedException
	{
		Thread.sleep(300);
		View_UsagePerMonth();
	}
	
	@Test
	public void f_view_NumberOfSIMSwap_PerDay() throws InterruptedException
	{
		Thread.sleep(300);
		View_NumberOfSImSwapPerDay();
	}
	
	@Test
	public void g_View_NumberOfCSRPerDay() throws InterruptedException
	{
		Thread.sleep(300);
		View_NumberOfCSRPerDay();
	}
	
	@Test
	public void h_View_HitsPerPage() throws InterruptedException
	{
		Thread.sleep(300);
		View_NumberOfHitsPerPage();
		
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Statistics tab ******************************
public void view_StatisticsTab()
{
	WebElement Subscriber= driver.findElement(By.linkText("Statistics"));
	
	Boolean subscriber= Subscriber.isDisplayed();
	
	Assert.assertTrue(subscriber);
}


//********************************* Method to click Statistics tab ******************************
public void click_StatisticsTab() throws InterruptedException
{
	WebElement Subscriber= driver.findElement(By.linkText("Statistics"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Subscriber).click().build().perform();
	
	Thread.sleep(400);
	
	//	Verify report
	
	WebElement verify= driver.findElement(By.id("R13681422056949169"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
  }	

//******************************** Method to view number of users per day *********************************
public void View_NumberOfUsersPerDay() throws InterruptedException
{
	//Click Link to view Number of users per day
	
	WebElement link= driver.findElement(By.xpath("//a[contains(text(),'Number of Users per Day')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(link).click().build().perform();
	
	Thread.sleep(500);
	
	//Select start date	
	
	Select_StartDate();
	
	Thread.sleep(200);

	// Method to verify 
	
	String verify= driver.findElement(By.xpath("//div[@id='R13680916751949134']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']")).getText();
	
	Assert.assertEquals(verify, "Number of Users per Day");

}
//****************************************** Method to select start date*********************************
public void Select_StartDate() throws InterruptedException
{
	Thread.sleep(500);
  
	//Click calendar 
		
	WebElement startDate= driver.findElement(By.xpath("//td[2]//img[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(startDate).click().build().perform();
	
	Thread.sleep(200);
	
	//select date
	
	WebElement Date= driver.findElement(By.xpath("//tr[2]//td[1]//a[1]"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(Date).click().build().perform();
	
	Thread.sleep(200);
	
	//Click GO
	
	WebElement go= driver.findElement(By.id("B13692503662836913"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(200);
   }

//******************************** Method to view inactive users    *********************************
public void View_InactiveUsers() throws InterruptedException
{
	//Click Link to view inactive users
		
	WebElement link= driver.findElement(By.xpath("//a[contains(text(),'Inactive Users')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(link).click().build().perform();
	
	Thread.sleep(200);
	
	// Method to verify 
	
	String verify= driver.findElement(By.xpath("//div[@id='R13693814574918388']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']")).getText();
	
	Assert.assertEquals(verify, "Users by dealer that have been inactive for 30 days");

}

//******************************** Method to view usage per month  *********************************
public void View_UsagePerMonth() throws InterruptedException
{
	//Click Link to view usage per month
		
	WebElement link= driver.findElement(By.xpath("//a[contains(text(),'Usage per Month')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(link).click().build().perform();
	
	Thread.sleep(200);
			
	//Select start date	
	
	Select_StartDate();
	
	Thread.sleep(200);
		
	// Method to verify 
	
	String verify= driver.findElement(By.xpath("//div[@id='R13695522333005741']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']")).getText();
	
	Assert.assertEquals(verify, "Usage per Month");
	
}
//******************************** Method to view number of SIM Swap per day *********************************
public void View_NumberOfSImSwapPerDay() throws InterruptedException
{
	//Click Link to view Number of sim swap per day

	WebElement link= driver.findElement(By.xpath("//a[contains(text(),'Number of SIM Swaps per Day')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(link).click().build().perform();
	
	Thread.sleep(200);
		
	//Select start date	
	
	Select_StartDate();
	
	Thread.sleep(200);
		
	// Method to verify 
	
	String verify= driver.findElement(By.xpath("//div[@id='R13697123512091282']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']")).getText();
	
	Assert.assertEquals(verify, "Number of SIM Swaps per Day");

}

//******************************** Method to view number of CSR per day *********************************
public void View_NumberOfCSRPerDay() throws InterruptedException
{
	//Click Link to view Number of CSR per day
	
	WebElement link= driver.findElement(By.xpath("//a[contains(text(),\"Number of CSR's per Day\")]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(link).click().build().perform();
	
	Thread.sleep(200);
		
	//Select start date	
	
	Select_StartDate();
	
	Thread.sleep(200);
	
	// Method to verify 
	
	String verify= driver.findElement(By.xpath("//div[@id='R13698013300126195']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']")).getText();
	
	Assert.assertEquals(verify, "Number of CSR's per Day");

}

//******************************** Method to view number hits per page *********************************
public void View_NumberOfHitsPerPage() throws InterruptedException
{
	//Click Link to view Number of Hits Per page

	WebElement link= driver.findElement(By.xpath("//a[contains(text(),'Hits per Page')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(link).click().build().perform();
	
	Thread.sleep(200);
		
	//Select start date	
	
	Select_StartDate();
	
	Thread.sleep(200);
		
	// Method to verify 
	
	String verify= driver.findElement(By.xpath("//div[@id='R13700005083180520']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']")).getText();
	
	Assert.assertEquals(verify, "Hits per Page");
	
   }
}
