package Dealer_Web_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class home_tab_tests extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}

	
	@Test
	public void a_View_DealersWeb_Tab()
	{
		view_DealersWebTab();
	}

	@Test
	public void b_Click_DealersWeb_Tab()
	{
		click_DealersWebTab();
	}
	
	@Test
	public void c_View_Home_page() throws InterruptedException
	{
		view_homepage();
	}
	
	@Test
	public void d_View_EventCalendar() throws InterruptedException
	{
		view_EventCalendar();
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Dealers Web tab ******************************
public void view_DealersWebTab()
{
	WebElement Delear= driver.findElement(By.linkText("Dealer Web"));
	
	Boolean delear= Delear.isDisplayed();
	
	Assert.assertTrue(delear);
}

//************************************* Method to view home page *****************************
public void view_homepage() throws InterruptedException
{
	Thread.sleep(1000);

	//Verify report
	
	WebElement verify= driver.findElement(By.xpath("//body[@class='echo-page-two-level-tabs echo-layout-noSidebar ui-layout-container']/form[@id='wwvFlowForm']/div[@id='echo-page-content']/div[@id='echo-page-content-inner']/div[@id='echo-box-body']/b/b/table[1]"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);

}

//************************************* Method to view event Calendar *****************************
public void view_EventCalendar() throws InterruptedException
{
	Thread.sleep(1000);

	//Verify report
	
	WebElement verify= driver.findElement(By.id("R5938619841928702"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);

}

}
