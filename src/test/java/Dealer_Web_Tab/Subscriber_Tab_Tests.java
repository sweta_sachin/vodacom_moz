package Dealer_Web_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Subscriber_Tab_Tests extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_DealersWebTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}
	
	@Test
	public void a_ViewSubscriber_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		view_SubscriberTab();
	}
	
	@Test
	public void b_ClickSubscriber_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		click_SubscriberTab();
	}

	@Test
	public void c_Search_By_IMSI() throws InterruptedException
	{
		Thread.sleep(300);
		SEARCBY_IMSI();
		
	}

	@Test
	public void d_View_Subscriber_Details_SearchResultByIMSI() throws InterruptedException
	{
		Thread.sleep(300);
		View_SubscriberDetailsByIMSI();
	}
	
	@Test
	public void e_Search_By_ICCID() throws InterruptedException
	{
		Thread.sleep(300);
		SEARCBY_ICCID();
		
	}

	@Test
	public void f_View_Subscriber_Details_SearchResultByICCID() throws InterruptedException
	{
		Thread.sleep(300);
		View_SubscriberDetailsByICCID();
	}
	
	@Test
	public void g_Search_By_MSISDN() throws InterruptedException
	{
		Thread.sleep(300);
		SEARCBY_MSISDN();
		
	}

	@Test
	public void h_View_Subscriber_Details_SearchResultByMSISDN() throws InterruptedException
	{
		Thread.sleep(300);
		View_SubscriberDetailsByMSISDN();
	}
	
	@Test
	public void i_Create_CSR() throws InterruptedException
	{
		Thread.sleep(300);
		Create_CSR();
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Subscriber tab ******************************
public void view_SubscriberTab()
	{
		WebElement Subscriber= driver.findElement(By.linkText("Subscriber"));
		
		Boolean subscriber= Subscriber.isDisplayed();
		
		Assert.assertTrue(subscriber);
	}
	
//********************************* Method to click Subscriber tab ******************************
public void click_SubscriberTab() throws InterruptedException
	{
		WebElement Subscriber= driver.findElement(By.linkText("Subscriber"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(Subscriber).click().build().perform();
        
		Thread.sleep(400);

        //Verify report
		
		WebElement verify= driver.findElement(By.xpath("//a[@class='echo-current']"));
		
		Boolean Verify= verify.isDisplayed();
		
		Assert.assertTrue(Verify);
	}	
	
//*************************** Method to view Subscriber details by MSISDN *********************
public void View_SubscriberDetailsByMSISDN() throws InterruptedException
{
	  
	Thread.sleep(400);
	
	String MSISDN= driver.findElement(By.id("P2_SI_MSISDN")).getText();
	
	Thread.sleep(400);
	
	Assert.assertEquals(MSISDN, msisdn);
}

//*************************** Method to view Subscriber details by IMSI *********************

public void View_SubscriberDetailsByIMSI() throws InterruptedException

{
	Thread.sleep(400);
	
	String IMSI= driver.findElement(By.id("P2_NI_IMSI")).getText();
	
	Thread.sleep(400);
	
	Assert.assertEquals(IMSI, imsi);

}	
		
//*************************** Method to view Subscriber details by ICCID ******************************
public void View_SubscriberDetailsByICCID() throws InterruptedException
	{
     
	Thread.sleep(400);
	
	String ICCID= driver.findElement(By.id("P2_NI_ICCID")).getText();
	
	Thread.sleep(400);
	
	Assert.assertEquals(ICCID, iccid);
	}	
				
//************************** Method to create CSR **********************************************
public void Create_CSR()
	{
		
	//click CSR
	
	WebElement csr= driver.findElement(By.id("B7964920356505214"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(csr).click().build().perform();
		
	//select process
	
	Select process= new Select(driver.findElement(By.id("P18_CR_CRP_UID")));
	
	process.selectByIndex(2);
			
	//Select category
	
	Select category= new Select(driver.findElement(By.id("P18_CR_CRC_UID")));
	
	category.selectByIndex(2);

	//Select type
	
	Select type= new Select(driver.findElement(By.id("P18_CR_CRCT_UID")));
	
	type.selectByIndex(2);
	
	//Select item
	
	Select item= new Select(driver.findElement(By.id("P18_CR_CRCTI_UID")));
	
	item.selectByIndex(2);
	
	//Select source
	
	Select source= new Select(driver.findElement(By.id("P18_CR_CRS_UID")));
	
	source.selectByIndex(2);
		
	//Select Group
	
	Select group= new Select(driver.findElement(By.id("P18_CR_CRG_UID")));
	
	group.selectByIndex(2);
	
	//Enter Subject
	
	WebElement subject= driver.findElement(By.id("P18_CR_SUBJECT"));
	
	subject.clear();
	
	subject.sendKeys("test");
	
	//Enter Details
	
	WebElement details= driver.findElement(By.id("P18_CR_DETAIL"));
	
	details.clear();
	
	details.sendKeys("test");
			
	//Click create
	
	WebElement create= driver.findElement(By.id("B4952331102790742"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(create).click().build().perform();
			
	//Verify Created 
	
	WebElement verify= driver.findElement(By.xpath("add locator"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
	
	}
}
