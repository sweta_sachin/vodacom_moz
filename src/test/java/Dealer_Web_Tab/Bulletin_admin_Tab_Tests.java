package Dealer_Web_Tab;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Bulletin_admin_Tab_Tests extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_DealersWebTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}
	
	@Test
	public void a_ViewBulletinAdmin_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		view_BulletinAdminTab();
	}
	
	@Test
	public void b_ClickBulletinAdmin_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		click_BulletinAdminTab();
	}
	
	@Test
	public void c_Edit_Bulletin() throws InterruptedException
	{
		Thread.sleep(500);
		EditBulletine();
	}

	@Test
	public void d_Verify_Edited() throws InterruptedException
	{
		Thread.sleep(500); 
		Verify_Edited();
	}
	
	@Test
	public void e_DeleteBulletin() throws InterruptedException
	{
		Thread.sleep(500); 
		DeleteBulletine();
	}
	
	@Test
	public void g_CreateBulletin() throws InterruptedException
	{
		Thread.sleep(500);
		Create_bulletin();
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Bulletin Admin tab ******************************
public void view_BulletinAdminTab()
	{
		WebElement BulletinAdmin= driver.findElement(By.linkText("Bulletin Admin"));
		
		Boolean bulletinAdmin= BulletinAdmin.isDisplayed();
		
		Assert.assertTrue(bulletinAdmin);
	}

//********************************* Method to click Bulletin Admin tab ******************************
public void click_BulletinAdminTab() throws InterruptedException
	{
		WebElement BulletinAdmin= driver.findElement(By.linkText("Bulletin Admin"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(BulletinAdmin).click().build().perform();
        
		Thread.sleep(400);

        //Verify report
		
		WebElement verify= driver.findElement(By.xpath("//a[@class='echo-current']"));
		
		Boolean Verify= verify.isDisplayed();
		
		Assert.assertTrue(Verify);
}
	
//***************************** Method to create bulletin *****************************************
public void Create_bulletin()
	{
		//Click create

	WebElement create= driver.findElement(By.id("B6944133603214032"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(create).click().build().perform();
		
	//Select type
	
	Select type= new Select(driver.findElement(By.id("P47_TYPE")));
	
	type.selectByIndex(2);
	
	// Enter title
	
	WebElement title= driver.findElement(By.id("P47_IB_TITLE"));
	
	title.clear();
	
	title.sendKeys("test");
	
	//Enter Summary
	
	WebElement summary= driver.findElement(By.id("P47_IB_SUMMARY"));
	
	summary.clear();

	summary.sendKeys("test");
		
	//Enter details
	
	WebElement details= driver.findElement(By.id("P47_IB_DETAILS_DISPLAY"));
	
	details.clear();
	
	details.sendKeys("test");
		
	//Click create
	
	WebElement create1= driver.findElement(By.id("B1313321080782993"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(create1).click().build().perform();
	
	}
	

//****************************************** Method to edit bulletin***************************************
public void EditBulletine() throws InterruptedException
{
      
   //Click on title to edit

	WebElement title=  driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[1]/table[1]/tbody[1]/tr[2]/td[2]/a[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(title).click().build().perform();
	
	Thread.sleep(300);
   
	//Change title
    
	WebElement Title= driver.findElement(By.id("P47_IB_TITLE"));
    
	Title.clear();
    
	Title.sendKeys("test");
    
	Thread.sleep(300);
   
   //click apply changes
    
	WebElement apply=  driver.findElement(By.id("B1312923861782987"));
    
	Actions act1= new Actions(driver);
    
	act1.moveToElement(apply).click().build().perform();
   
}

//*********************************************** Method to verify edited bulletin *************************
public void Verify_Edited()
{
	String title1= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
	
	Assert.assertEquals(title1, "test");
}

//****************************************** Method to delete bulletin***************************************
public void DeleteBulletine() throws InterruptedException
{
 	 String title1= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
	
 	 System.out.println(title1);
	
	//Click on title to edit
	 
 	 WebElement title=  driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[1]/table[1]/tbody[1]/tr[2]/td[2]/a[1]"));
	 
 	 Actions act= new Actions(driver);
	 
 	 act.moveToElement(title).click().build().perform();
	 
 	 Thread.sleep(500);
  
 	 //click delete
	
 	 WebElement apply=  driver.findElement(By.id("B1313519041782993"));
	 
 	 Actions act1= new Actions(driver);
	 
 	 act1.moveToElement(apply).click().build().perform();
	 
 	 Thread.sleep(300);
 
  	 //accept alert
	 
 	 Alert alert= driver.switchTo().alert();
	 
 	 alert.accept();
	 
 	 String title2= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
	 
 	 System.out.println(title2);
	 
 	 Assert.assertNotEquals(title2, title1);
	}

}




