package Dealer_Web_Tab;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Delear_Contact_Tab_Tests extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_DealersWebTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		
	 Thread.sleep(1000);
	 driver.quit();
	 
	}

	@Test
	public void a_View_Delear_ContactTab()
	{
		view_Dealer_ContactTab();
	}

	@Test
	public void b_Click_Delear_ContactTab() throws InterruptedException
	{
		Thread.sleep(500);
		click_Dealer_ContactTab();
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(1000);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(3000); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(1500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void g_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","dealer_contacts.csv"));
	}
	
	@Test
	public void h_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
		
	}
		
	@Test
	public void i_reset_test() throws InterruptedException
	{
		Thread.sleep(500);
		reset();
	}
	

	@Test
	public void z_flashBackTest() throws InterruptedException
	{
		Thread.sleep(500);
		flashback_filter();
	}
	
   @Test
   public void k_Create_DealerContact() throws InterruptedException
   {
	   create_dealerContact();
   }
   
   @Test
   public void l_verify_CreatedContact() throws InterruptedException
   {
	   VerifyCreated();
   }
   
   @Test
   public void m_Edit_DealerContact() throws InterruptedException
   {
	   Edit_Contact();
   }
   
   @Test
   public void n_VerifyEdited() throws InterruptedException
   {
	   Verify_edit();
   }
   
   @Test
   public void o_DeleteContact() throws InterruptedException
   {
	   Thread.sleep(500);
	   delete_Contact();
   }
   
   @Test
   public void p_VerifyDeleted() throws InterruptedException
   {
	   Thread.sleep(500);
	   verifyDeleted();
   }
   
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Dealer Contact tab ******************************
public void view_Dealer_ContactTab()
{
	WebElement csr= driver.findElement(By.linkText("Dealer Contacts"));
	
	Boolean CSR= csr.isDisplayed();
	
	Assert.assertTrue(CSR);
}

//********************************* Method to click Dealer Contact tab ******************************
public void click_Dealer_ContactTab()
{
	WebElement CSR =driver.findElement(By.linkText("Dealer Contacts"));

	Actions act= new Actions(driver);
	
	act.moveToElement(CSR).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}	
//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	 col= "Contact Name";
	
	 expvalue= "test";
	
	 view_filtered_report();

// Verify report
	
	 Thread.sleep(300);
	
	 String verifyreport= driver.findElement(By.xpath("//div[@id='apexir_DATA_PANEL']//td[3]")).getText();
	
	 Thread.sleep(300);
	
	 Assert.assertEquals(verifyreport, expvalue);
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 
	
	save_report();
	
	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();	
	
	// verify 

	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
	{
	
	select_primary_report();
	
	Thread.sleep(300);
	
	try {
		
		WebElement primaryreport= driver.findElement(By.id("4946130720776080"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}catch(Exception e) 	{
	
		WebElement primaryreport= driver.findElement(By.id("8583020568831405"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
   }
	
}

//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
	{
		Reset();
		
		// Verify report

		WebElement primaryreport= driver.findElement(By.id("8583020568831405"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}

//**************************************** Method to create dealer contact *******************************
public void create_dealerContact() throws InterruptedException
{
      // Click create

	WebElement create = driver.findElement(By.id("B8583613228831410"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(create).click().build().perform();	
	
	Thread.sleep(500);
	
	//Select dealer
	
	Select dealer= new Select(driver.findElement(By.id("P15_DC_D_UID")));
	
	dealer.selectByIndex(3);
	
	Thread.sleep(200);
	
	// Enter name
	
	WebElement name= driver.findElement(By.id("P15_DC_NAME"));
	
	name.clear();
	
	name.sendKeys(Name);
	
	//Enter email
	
	WebElement email= driver.findElement(By.id("P15_DC_EMAIL"));
	
	email.clear();
	
	email.sendKeys("name@test.com");
	
	//click create
	
	WebElement create1 = driver.findElement(By.id("B8579728436831383"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(create1).click().build().perform();	
	
	Thread.sleep(500);
	
	//check success message
	
	Success_Msg();

}

//************************* Method to check success message *************************************************
public void Success_Msg()
{
	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
	
	String Expected_msg= "Action Processed.";
	
	Assert.assertEquals(Msg, Expected_msg);
}

//************************* Method to verify created contact ************************************************
public void VerifyCreated() throws InterruptedException
{
   //Enter contact name in the search box
	
	WebElement name= driver.findElement(By.id("apexir_SEARCH"));
	
	name.clear();
	
	name.sendKeys(Name);
	
	//click go
	
	WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(300);
	
	//Verify name
	
	String text1= driver.findElement(By.xpath("//tr[@class='even']//td[3]")).getText();
	
	reset();
	
	Assert.assertEquals(text1, Name);
	
}

//*************************************** Method to edit contact ****************************************
public void Edit_Contact() throws InterruptedException
{
    //click edit

	WebElement edit = driver.findElement(By.xpath("//tr[@class='even']//td//a//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(300);
	
	//change the contact name
	
	WebElement name= driver.findElement(By.id("P15_DC_NAME"));
	
	name.clear();
	
	name.sendKeys(Name1);
	
	//Click apply changes
	
	WebElement go = driver.findElement(By.id("B8579805668831383"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();	
	
	Thread.sleep(300);
	
	//check success message
	
	Success_Msg();
	 
 }

//****************************************** Method to verify edit *************************************
public void Verify_edit() throws InterruptedException
{
	//Enter contact name in the search box

	WebElement name= driver.findElement(By.id("apexir_SEARCH"));
	
	name.clear();
	
	name.sendKeys(Name1);
	
	//click go
	
	WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(300);
	
	//Verify name
	
	String text1= driver.findElement(By.xpath("//tr[@class='even']//td[3]")).getText();
	
	reset();
	
	Assert.assertEquals(text1, Name1);
   }

//******************************************* Method to delete contact ***********************************

public void delete_Contact() throws InterruptedException
{
	 //click edit

	WebElement edit = driver.findElement(By.xpath("//tr[@class='even']//td//a//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(500);
		
	//Click delete
	
	WebElement delete = driver.findElement(By.id("B8579919338831383"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(delete).click().build().perform();	
	
	Thread.sleep(300);
		
	//Accept alert
	
	Alert alert= driver.switchTo().alert();
	
	alert.accept();
	
	Thread.sleep(300);
	
	//check success message
	
	Success_Msg();
	
}

//******************************************** Method to verify deleted contact ***************************
public void verifyDeleted()
{
	//Verify name

	String text1= driver.findElement(By.xpath("//tr[@class='even']//td[3]")).getText();
	
	Assert.assertNotEquals(text1, Name1);
  }

}
