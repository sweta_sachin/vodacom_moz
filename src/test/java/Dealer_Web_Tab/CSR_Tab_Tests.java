package Dealer_Web_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CSR_Tab_Tests extends common_methods.CommonMethods{
	
	
		
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_DealersWebTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		
	 Thread.sleep(1000);
	 driver.quit();
	 
	}

	
	@Test
	public void a_View_CSRTab()
	{
		view_CSRTab();
	}

	@Test
	public void b_Click_CSRTab() throws InterruptedException
	{
		Thread.sleep(500);
		click_CSRTab();
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(1000);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void g_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","customer_service_requests.csv"));
	}
	
	@Test
	public void h_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
		
	}
		
	@Test
	public void i_reset_test() throws InterruptedException
	{
		Thread.sleep(500);
		reset();
	}
	

	@Test
	public void j_flashBackTest() throws InterruptedException
	{
		Thread.sleep(500);
		flashback_filter();
	}
	
	
	@Test
	public void k_View_CSRRequestReport() throws InterruptedException
	{
		Thread.sleep(500);
		View_CSR_Request_Report();
	}
	
	@Test
	public void l_edit_CSR_request() throws InterruptedException
	{
		Thread.sleep(500);
		edit_CSR_request();
		
	}
	

    @Test
    public void m_Create_CSR_Request() throws InterruptedException
    {
    	Thread.sleep(500);
    	Create_CSR();
    }
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view CSR tab ******************************
public void view_CSRTab()
{
	WebElement csr= driver.findElement(By.linkText("CSR"));
	
	Boolean CSR= csr.isDisplayed();
	
	Assert.assertTrue(CSR);
}

//********************************* Method to click CSR tab ******************************
public void click_CSRTab()
{
	WebElement CSR =driver.findElement(By.linkText("CSR"));

	Actions act= new Actions(driver);
	
	act.moveToElement(CSR).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();

	Assert.assertTrue(Verify);
}	

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	 col= "Type";

	 expvalue= "Complaints";
	 
	view_filtered_report();

	// Verify report
	
	Thread.sleep(300);
	
	String verifyreport= driver.findElement(By.xpath("//div[@id='apexir_DATA_PANEL']//tr[2]//td[7]")).getText();
	
	Thread.sleep(300);
	
	Assert.assertEquals(verifyreport, expvalue);
}

//************************************* Method to save report *************************************

public void Save_report() throws InterruptedException

{
	report= "2. Test report"; 
	
	save_report();

	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
}

//**************************************** Method to search by private report ****************************

public void search_by_private_report() throws InterruptedException

{
	Search_by_private_report();	
	
	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
}

//*********************************** Method to search primary report *********************

public void search_by_PrimaryReports() throws InterruptedException
{
	select_primary_report();
	
	Thread.sleep(300);
	
	try {
	
		WebElement primaryreport= driver.findElement(By.id("4946130720776080"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}catch(Exception e) 	{
	
		WebElement primaryreport= driver.findElement(By.id("4946130720776080"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
}

//************************ Method to test reset button on CIB *******************

public void reset() throws InterruptedException
{
	Reset();
		
		// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("4946130720776080"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
	
}

//**************************************** View CSR request report ********************************

public void View_CSR_Request_Report()

{
	// Verify report

	WebElement Report= driver.findElement(By.id("4946130720776080"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);

}
//****************************************** Method to edit CSR request **************************
public void edit_CSR_request()
{
   // Click Edit 
	
	WebElement edit= driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();
	
	//due to issue on click view, can not create further process
	// Verify report
	
	WebElement Report= driver.findElement(By.id("4946130720776080"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);

}

//************************** Method to create CSR **********************************************
public void Create_CSR()
	{
		//click CSR

	WebElement csr= driver.findElement(By.id("B7964920356505214"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(csr).click().build().perform();
		
	//select process
	
	Select process= new Select(driver.findElement(By.id("P18_CR_CRP_UID")));
	
	process.selectByIndex(2);
	
	//Select category
	
	Select category= new Select(driver.findElement(By.id("P18_CR_CRC_UID")));
	
	category.selectByIndex(2);

	//Select type
	
	Select type= new Select(driver.findElement(By.id("P18_CR_CRCT_UID")));
	
	type.selectByIndex(2);
	
	//Select item
	
	Select item= new Select(driver.findElement(By.id("P18_CR_CRCTI_UID")));
	
	item.selectByIndex(2);
	
	//Select source
	
	Select source= new Select(driver.findElement(By.id("P18_CR_CRS_UID")));
	
	source.selectByIndex(2);
	
	//Select Group
	
	Select group= new Select(driver.findElement(By.id("P18_CR_CRG_UID")));
	
	group.selectByIndex(2);
			
	//Enter Subject
	
	WebElement subject= driver.findElement(By.id("P18_CR_SUBJECT"));
	
	subject.clear();
	
	subject.sendKeys("test");
			
	//Enter Details
	
	WebElement details= driver.findElement(By.id("P18_CR_DETAIL"));
	
	details.clear();
	
	details.sendKeys("test");
			
	//Click create
	
	WebElement create= driver.findElement(By.id("B4952331102790742"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(create).click().build().perform();
	
	//Verify Created 
	
	WebElement verify= driver.findElement(By.xpath("add locator"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
	
	}
	
}
