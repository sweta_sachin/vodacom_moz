package Dealer_Web_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Email_Delears extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_DealersWebTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}
	
	@Test
	public void a_ViewEmailDelears_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		view_EmailDelearsTab();
	}
	
	@Test
	public void b_ClickEmailDelears_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		click_EmailDelearsTab();
	}
	
	@Test
	public void c_SendEmail() throws InterruptedException
	{
		Thread.sleep(500);
		SendEmail();
	}
	
	@Test
	public void d_Save_Email() throws InterruptedException
	{
		Thread.sleep(500);
		SaveEmail();
	}
	
	@Test
	public void e_Add_AttachmentToEmail() throws InterruptedException
	{
		Thread.sleep(500);
		AttachmentEmail();
	}
	
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Email Delears tab ******************************
public void view_EmailDelearsTab()
{
	WebElement EmailDelears= driver.findElement(By.linkText("Email Dealers"));
	
	Boolean emailDelears= EmailDelears.isDisplayed();
	
	Assert.assertTrue(emailDelears);
}

//********************************* Method to click Email Delears tab ******************************
public void click_EmailDelearsTab() throws InterruptedException
{
		WebElement EmailDelears= driver.findElement(By.linkText("Email Dealers"));

		Actions act= new Actions(driver);
		
		act.moveToElement(EmailDelears).click().build().perform();
        
		Thread.sleep(400);
		
		//Verify report
		
		WebElement verify= driver.findElement(By.id("B2463405263341921"));
		
		Boolean Verify= verify.isDisplayed();
		
		Assert.assertTrue(Verify);
}

//************************************* Method to compose email *************************************
public void composeEmail() throws InterruptedException
	{
		//click compose
			
			WebElement compose= driver.findElement(By.id("B2463405263341921"));
			
			Actions act= new Actions(driver);
			
			act.moveToElement(compose).click().build().perform();
			
			Thread.sleep(400);
        
        //Select send to option
			
			WebElement option= driver.findElement(By.id("P8_IB_IBT_UID_0"));
			
			Actions act1= new Actions(driver);
			
			act1.moveToElement(option).click().build().perform();
			
			Thread.sleep(400);
        
	   //Enter subject
	
			WebElement subject= driver.findElement(By.id("P8_IB_TITLE"));
			
			subject.clear();
			
			subject.sendKeys("test");
        
        //Enter message  
      
    //    WebElement message= driver.findElement(By.id("P8_IB_DETAILS_DISPLAY"));
    //    message.sendKeys("test");
        
     	}
	
//***************************************** Method to send email ********************************************
public void SendEmail() throws InterruptedException
	{

	Thread.sleep(400);
	
	composeEmail();
		
	//Click send
	
	WebElement send= driver.findElement(By.id("B2468012728583510"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(send).click().build().perform();
	
	Thread.sleep(400);
	
	//Verify sent
	
	WebElement verify= driver.findElement(By.xpath("add locator"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
	
	}
	
//***************************************** Method to save email ********************************************
public void SaveEmail() throws InterruptedException
	{

	Thread.sleep(400);
	
	composeEmail();
			
	//Click send
	
	WebElement save= driver.findElement(By.id("B4780523166990706"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(save).click().build().perform();
	
	Thread.sleep(400);
	
	//Verify saved email
	
	WebElement verify= driver.findElement(By.xpath("add locator"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
	
	}
	
//***************************************** Method to add attachment and verify  ********************************************
public void AttachmentEmail() throws InterruptedException
	{
		Thread.sleep(400);

		composeEmail();
			
		//Select file
			
		WebElement file= driver.findElement(By.id("B2487124137615082"));
		
		file.sendKeys("C:\\Users\\Sweta\\Desktop\\test doc.txt");
		
		Thread.sleep(500);
				
		//Click send
		
		WebElement send= driver.findElement(By.id("B2468012728583510"));
		
		Actions act1= new Actions(driver);
		
		act1.moveToElement(send).click().build().perform();
	    
		Thread.sleep(400);
		        
	     //Verify sent
	    
		WebElement verify= driver.findElement(By.xpath("add locator"));
		
		Boolean Verify= verify.isDisplayed();
		
		Assert.assertTrue(Verify);
	}

}
