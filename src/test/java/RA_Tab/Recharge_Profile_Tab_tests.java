package RA_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Recharge_Profile_Tab_tests extends common_methods.CommonMethods{
	
	
		
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_RATab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		
	 Thread.sleep(1000);
	 driver.quit();
	 
	}

	@Test
	public void a_View_RechargeProfile_Tab()
	{
		view_RechargeProfileTab();
	}

	@Test
	public void b_Click_RechargeProfileTab() throws InterruptedException
	{
		Thread.sleep(500);
		click_RechargeProfileTab();
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void g_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","exchange_rates.csv"));
	}
	
	@Test
	public void h_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
		
	}
		
	@Test
	public void i_flashBackTest() throws InterruptedException
	{
		Thread.sleep(500);
		flashback_filter();
	}
	
	@Test
	public void j_reset_test() throws InterruptedException
	{
		Thread.sleep(500);
		reset();
	}
	
	@Test
	public void k_AddNextMonthsRate() throws InterruptedException
	{
	
	}
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Recharge Profile tab ******************************
public void view_RechargeProfileTab()
 {
	WebElement RechargeProfile= driver.findElement(By.linkText("Recharge Profile"));
	
	Boolean rechargeProfile= RechargeProfile.isDisplayed();
	
	Assert.assertTrue(rechargeProfile);
}


//********************************* Method to click Exchange Rates tab ******************************
public void click_RechargeProfileTab() throws InterruptedException
{
	WebElement RechargeProfile= driver.findElement(By.linkText("Recharge Profile"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(RechargeProfile).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}


//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
 {
	col= "Denomination";
	 expvalue= "EUR";

	 view_filtered_report();
	
	// Verify report
	
	 Thread.sleep(600);
	
	 String verifyreport= driver.findElement(By.xpath("//div[@id='apexir_DATA_PANEL']//tr[2]//td[2]")).getText();
	
	 Thread.sleep(300);
	
	 Assert.assertEquals(verifyreport, expvalue);
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 

	save_report();

	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
}

//**************************************** Method to search by private report ****************************

public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();	
	
	// verify 

	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
}

//*********************************** Method to search primary report *********************

public void search_by_PrimaryReports() throws InterruptedException

{
	select_primary_report();

	Thread.sleep(300);
	
	try {

		WebElement primaryreport= driver.findElement(By.id("1790304738657687"));
	
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}catch(Exception e) 	{

		WebElement primaryreport= driver.findElement(By.id("1790304738657687"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}
}
//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
	{
		Reset();

	// Verify report

		WebElement primaryreport= driver.findElement(By.id("1790304738657687"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
}	
//************************** Method to add next month rate ***********************************
public void AddNextMonthRate() throws InterruptedException
{
	//Click Add next month rate button
		
	WebElement reset = driver.findElement(By.id("B1806132587006488"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(reset).click().build().perform();	
	
	Thread.sleep(500);
		
	//Enter Rate
	
	WebElement rate= driver.findElement(By.id(""));
	
	rate.clear();
	
	rate.sendKeys("0.045455");
	
	//Click Submit
	
	WebElement submit = driver.findElement(By.id("B1806132587006488"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(submit).click().build().perform();	
	
	Thread.sleep(500);
	
  }

}
