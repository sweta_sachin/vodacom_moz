package common_methods;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;

public class NotificationTests extends CommonMethods{
	
	
////////////////////////////////////////////////////////////////////////////////////////////////
//*************************** Method to click notification tab ********************************

public void click_notification() throws InterruptedException
{  
	try {
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
					.withTimeout(30, TimeUnit.SECONDS) 			
					.pollingEvery(5, TimeUnit.SECONDS) 			
					.ignoring(NoSuchElementException.class);
	
			WebElement notes= driver.findElement(By.id("B19744324048754180"));
			
			Boolean name= notes.isDisplayed();
			
			if(name==true)	{
				
				wait.equals(name);
				
				Actions act= new Actions(driver);
				
				act.moveToElement(notes).doubleClick().build().perform();
				
				Thread.sleep(1000);
			}
	
	}catch(NoSuchElementException e) {
	
		WebElement notes= driver.findElement(By.linkText("Notification"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
	}
}

//******************************** Method to enter end date ************************************
public void enter_endDate() throws InterruptedException
{
	
	WebElement enddate= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/table[1]/tbody[1]/tr[1]/td[4]/img[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(enddate).click().build().perform();
	
	Thread.sleep(300);
	
	WebElement date= driver.findElement(By.xpath("//a[contains(text(),'30')]"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(date).doubleClick().build().perform();
	
	//click close calendar

	WebElement close= driver.findElement(By.xpath("//button[@class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all']"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(close).click().build().perform();
}


//************************** Method to click create Notification **********************
public void click_createnote()
{
	try {

		driver.findElement(By.id("B12131301940768901")).click();
	
	}catch(NoSuchElementException e) {
	
		WebElement create=	driver.findElement(By.id("B12131301940768901"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(create).click().build().perform();
	}
	
}


//************************** Method to enter message **********************************
public void enter_message()
{
	WebElement mesg= driver.findElement(By.id("P65_CM_MESSAGE"));
	
	mesg.sendKeys("test message123");
}


//************************** Method to click create *********************************
public void click_create()
  {
	try {
	
		WebElement create= driver.findElement(By.id("B12132831571827550"));
		
		Actions act = new Actions(driver);
		
		act.moveToElement(create).click().build().perform();
	
	}catch(StaleElementReferenceException | NoSuchElementException e)	{
	
		WebElement create= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[1]/div[2]/a[1]"));
		
		Actions act = new Actions(driver);
		
		act.moveToElement(create).click().build().perform();
	}
  }

//************************* Method to check success message *************************
public void Success_Msg()
 {
	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
	
	String Expected_msg= "Action Processed.";
	
	Assert.assertEquals(Msg, Expected_msg);
}

//************************* Method to verify a created notification *******************
public void verify_note()
  {
	WebElement note= driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));
	
	Boolean note1= note.isDisplayed();
	
	Assert.assertTrue(note1);
}

//************************* Method to edit a created notification *******************
public void edit_note()
  {
	
	driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]")).click();
}
//************************ Method to change message of a notification ***************	
public void change_Msg()
{
	WebElement mesg= driver.findElement(By.id("P65_CM_MESSAGE"));
	
	mesg.clear();
	
	mesg.sendKeys("test message1235678");
}

//*********************** Method to click apply changes *****************************
public void Click_applyChanges()
{
	driver.findElement(By.linkText("Apply Changes")).click();
}
//*********************** Method to check edit success message **********************
public void Edit_success_msg()
{
	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
	
	String Expected_msg= "Action Processed.";
	
	Assert.assertEquals(Msg, Expected_msg);
}

//********************** Method to delete notification *****************************
public void deleteNotification() throws InterruptedException
{
	try {
	
		edit_note();
		
		Thread.sleep(1000);
		
		WebElement delete= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[1]/div[2]/a[2]"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(delete).doubleClick().build().perform();
		
		Thread.sleep(1500);
		
		Alert alert= driver.switchTo().alert();
		
		alert.accept();
	
	}catch(UnhandledAlertException | NoAlertPresentException e){
	
		Alert alert= driver.switchTo().alert();
		
		alert.accept();
	}
}
//************************* Method to verify a deleted notification *******************
public void deleteANDverify() throws InterruptedException
  {
		String note= driver.findElement(By.xpath("//tr[2]//td[5]")).getText();
		
		System.out.println(note);
		
		deleteNotification();
		
		String note1= driver.findElement(By.xpath("//tr[2]//td[5]")).getText();
		
		System.out.println(note1);
		
		Assert.assertNotEquals(note, note1);
	}	
}
