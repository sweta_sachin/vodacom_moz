package common_methods;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;



public class CommonMethods {
	
	public static WebDriver driver;
	
	public static String AcType= "Internal Account"; //used in multiple tests no need to change
	
	public static int Index= 1; // should be unique
	
	public static String verifyFilter= "CIB Value = 10000"; //used in multiple tests no need to change
	
	public static String Name= "q14"; // should be unique used in multiple tests to create 
	
	public static String Name1= "r14"; // should be unique used in multiple tests to edit 
	
	public static String ExchangeRate= "120"; // Should be unique used to create next months exchange rate
	
	public static String ESNname= "lmno1234"; // should be unique and must have 8 letter
	
	public static String ESNname1= "nopq1234"; // should be unique and must have 8 letter
	
	public static String Firstname= "gast"; // should be always unique and used to create user
	
	public static String surname= "verify"; // should be always unique and used to create user
		
	public static String logon= "gverify"; //this should be combination of first name and surname
	
	public static String Password= "gve@123"; // should be 1st 3 letters of log on name should be of 7 characters
		
	public static String MSISDN= "27650401697"; // should be always unique and used for MSISDN swap
	
	public static String ICCID= "8927071191401000075"; // should be always unique and used for SIM swap
	
	public static String AccName= "3AB LDA"; // Account name should be valid and available to search on credit control tab
	
	public static String CNumber= "23910069";  // contact number should be valid and available to search on credit control tab
		
	public static String BaseUrl = "http://10.201.239.97:11080/apexTest/f?p=UA:101"; // base url
	
	public static String username = "smaskelar";  // enter user name
	
	public static String password ="Sweta@123"; // valid password
	
	public static String AccNo = "V0129129"; // valid and active  test account number
	
	public static String msisdn = "258841033373"; // valid and active test msisdn 
	
	public static String toemail = "email@test.com"; // dummy email id for user create test and for send email report or subscription email
	
	public static String Time = "10"; // time for flash back test
	
	public static String flashback_msg= "10 minutes ago.   ";// number in this should be same as flash back time (now it is 10)
	
	public static String DATE ="01-Jan-2017"; //date should be used in multiple tests while creating 
	
	public static String imsi = "643041002362087"; // test IMSI number should be valid and active in the system
	
	public static String iccid = "8925804001002362087"; // test ICCIDI number should be valid and active in the system
	
	public static String InvoiceNumber = "258841033373"; // test Invoice number should be valid and active in the system
		
	public static String CircuitNumber = "258841033373"; // test Circuit number should be valid and active in the system
	
	public static String AccUID = "258841033373"; // test Account UID should be valid and active in the system
	
	public static String SubUID = "258841033373"; // test Subscriber UID should be valid and active in the system
	
	public static String mpesa_ott = "258841033373"; // test MPESA OTT number should be valid and active in the system
	
	public static String imei= "35705306272228"; // test IMEI should be valid and active in the system
		
	public static  String msisdn1= "258456784567"; // MSISDN for introduce numbers tests this should be starting of range
	
	public static String msisdn2= "258888888888"; // MSISDN for introduce numbers tests this should be end of range
	
	public static String subject= "test subject"; // this is commonly used string and no need to change every time
	
	public static String Editsubject= "Edited test subject"; // this is commonly used string and no need to change every time
	
	public static String Comment= "test comment"; // this is commonly used string and no need to change every time
	
	public static String Newname= "new name";  // this is commonly used string and no need to change every time
	
	public static String UID= "188044"; // UID to search on Customer care tab
	
	public static String name= "test";  // name to search on Customer care tab
	
	public  String expvalue ; // standard test parameters initialized for individual tests
	
	public  String report ;	// standard test parameters initialized for individual tests
	
	public  String col;	// standard test parameters initialized for individual tests
	
	public static String reportname= "Test report"; 	// standard test parameters initialized for individual tests
	
	public static String verifyreport= "Saved Report = \"Test report\"";	// standard test parameters initialized for individual tests
	
//****************************** Method to login to MVNX PROD*****************************************
	
	public void Login()
	{
		System.setProperty("webdriver.chrome.driver", "E:\\ChromeDriver\\chromedriver.exe");
		
		DesiredCapabilities caps = DesiredCapabilities.chrome();
	    
		LoggingPreferences logPrefs = new LoggingPreferences();
	    
		logPrefs.enable(LogType.BROWSER, Level.ALL);
	    
		caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
	    
		driver = new ChromeDriver(caps);
     	
		driver.get(BaseUrl);	
     	
		driver.manage().window().maximize();
     	
		driver.findElement(By.id("P101_USERNAME")).sendKeys(username);
    	
		driver.findElement(By.id("P101_PASSWORD")).sendKeys(password);
        
		driver.findElement(By.id("P101_LOGIN")).submit();
	}
	
//************************************** Method to click on CRM tab**************************************	
	public void clickCRMTab() 
    {		
		try {
				
			List<WebElement> anchors = driver.findElements(By.tagName("a"));
			
			java.util.Iterator<WebElement> i = anchors.iterator();
			
			while(i.hasNext()) {
			
				WebElement anchor = i.next();                 
				
				if(anchor.getText().contains("CRM"))  {
					
					System.out.println(anchor.getText());
					
					Actions act= new Actions(driver);
					
					act.moveToElement(anchor).doubleClick().build().perform();
					
					break;
					
				}else {
				
					System.out.println("Can not find CRM tab");
					
				}
				
			}
		}catch(StaleElementReferenceException  e){
			
			List<WebElement> anchors = driver.findElements(By.tagName("a"));
			
			java.util.Iterator<WebElement> i = anchors.iterator();
			
			while(i.hasNext()) {
			
				WebElement anchor = i.next();                 
				
				if(anchor.getText().contains("CRM")){
						
					System.out.println(anchor.getText());
					
					Actions act= new Actions(driver);
					
					act.moveToElement(anchor).doubleClick().build().perform();
					
					break;
					
				} else {
				
					System.out.println("Can not find CRM tab");
				}
			}
	     }
	}
	
//************************************* Method to click Credit control tab********************************
public void clickCreditControlTab() 
	{   
		try {

			List<WebElement> anchors = driver.findElements(By.tagName("a"));
			
			java.util.Iterator<WebElement> i = anchors.iterator();
			
			while(i.hasNext()) {
			
				WebElement anchor = i.next(); 
				
				if(anchor.getText().contains("Credit Control")) 	{
							
					System.out.println(anchor.getText());
					
					Actions act= new Actions(driver);
					
					act.moveToElement(anchor).doubleClick().build().perform();
					
					break;
					
				} else {
				
					System.out.println("Can not find credit control tab");
					
				}
				
			}
			
		}catch (StaleElementReferenceException  e) {
		
			List<WebElement> anchors = driver.findElements(By.tagName("a"));
			
			java.util.Iterator<WebElement> i = anchors.iterator();
			
			while(i.hasNext()) {
			
				WebElement anchor = i.next(); 
				
				if(anchor.getText().contains("Credit Control"))	{
				
					System.out.println(anchor.getText());
					
					Actions act= new Actions(driver);
					
					act.moveToElement(anchor).doubleClick().build().perform();
					
					break;
					
				} else {
				
					System.out.println("Can not find credit control tab");
				}
			}
		}
    }

//***************************************Method to search customer by account number*******************
  public void SearchCustomerByAccountNumber()
	{
	  Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			  .withTimeout(30, TimeUnit.SECONDS) 			
			  .pollingEvery(1, TimeUnit.SECONDS) 			
			  .ignoring(NoSuchElementException.class);
			
	  WebElement searchtype= driver.findElement(By.id("P0_SEARCH_TYPE"));
		
	  Boolean his= searchtype.isDisplayed();
		
	  wait.equals(his);
		
	  Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
		
	  dropdown.selectByVisibleText("Account Number");
		
	  driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
		
	  driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(AccNo);
		
	  try {
		
		  driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		 
		  driver.findElement(By.id("P0_SEARCH")).submit();
		 
	  }catch(NoSuchElementException| StaleElementReferenceException e){
		
		  driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		 
		  driver.findElement(By.id("P0_SEARCH")).submit();
		  
	  }
		
	}
//***************************************************Method to edit customer*************************
public void ClickEdit_Customer()
{
  try {
  		List<WebElement> anchors = driver.findElements(By.tagName("a"));

  		java.util.Iterator<WebElement> i = anchors.iterator();
		
  		while(i.hasNext()) {
		
  			WebElement anchor = i.next();                 
		  	
  			if(anchor.getText().contains("Edit Customer"))	{
		  	
  				WebElement editCust= driver.findElement(By.linkText("Edit Customer"));
		  		
  				Actions act= new Actions(driver);
		  		
  				act.moveToElement(editCust).doubleClick().build().perform();
		  		
  				break;
		  			}
		  		}
	  	}catch (NoSuchElementException| StaleElementReferenceException e)	{
	  			
	  		List<WebElement> anchors = driver.findElements(By.tagName("a"));
	  		
	  		java.util.Iterator<WebElement> i = anchors.iterator();
	  		
	  		while(i.hasNext()) {
	  		
	  			WebElement anchor = i.next();                 
	  			
	  			if(anchor.getText().contains("Edit Customer"))	{
	  			
	  				WebElement editCust= driver.findElement(By.linkText("Edit Customer"));
	  				
	  				Actions act= new Actions(driver);
	  				
	  				act.moveToElement(editCust).doubleClick().build().perform();
	  					
	  				break;
	   			}
	   		}
	      }
    	}
//******************************************Click GO button on CRM tab to search************************
public void Click_Go()
	{
	 try {
		 	driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
	
		 	WebElement search=driver.findElement(By.xpath("//a[@id='P0_SEARCH']"));
			
		 	Actions act= new Actions(driver);
			
		 	act.moveToElement(search).doubleClick().build().perform();
	 	  
	 }catch(NoSuchElementException| StaleElementReferenceException e) {
  	
		 WebElement search=driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/table[1]/tbody[1]/tr[1]/td[7]/a[1]"));
	  	
		 Actions act= new Actions(driver);
	  	
		 act.moveToElement(search).doubleClick().build().perform();
	 	 }
	}
	
//************************************Method to Click Create customer*****************************
public void ClickCreateCustomer()
{
	try {

		List<WebElement> anchors = driver.findElements(By.tagName("a"));
		
		java.util.Iterator<WebElement> i = anchors.iterator();
		
		while(i.hasNext()) {
		
			WebElement anchor = i.next();                 
			
			if(anchor.getText().contains("Create Customer")){
			
				Actions act= new Actions(driver);
				
				act.moveToElement(anchor).doubleClick().build().perform();
				
				break;
			}
		}
			
	}catch(StaleElementReferenceException |NoSuchElementException e)	{ 
					
		List<WebElement> anchors = driver.findElements(By.tagName("a"));
		
		java.util.Iterator<WebElement> i = anchors.iterator();
		
		while(i.hasNext()) {
		
			WebElement anchor = i.next();                 
			
			if(anchor.getText().contains("Create Customer")){
			
				Actions act= new Actions(driver);
				
				act.moveToElement(anchor).click().build().perform();
				
				break;
			}
		}
	}
}

//**********************Method to test Access credit subscriber ******************************************	
public void AccessCreateSubcriber() throws InterruptedException
    {
		try {
		
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
						.withTimeout(30, TimeUnit.SECONDS) 			
						.pollingEvery(1, TimeUnit.SECONDS) 			
						.ignoring(NoSuchElementException.class);
			
			WebElement Create_Subscriber= driver.findElement(By.id("B13989961185388418"));
			
			Boolean his= Create_Subscriber.isDisplayed();
			
			wait.equals(his);
			
			Actions act= new Actions(driver);
			
			act.moveToElement(Create_Subscriber).doubleClick().build().perform();
		 	
		}catch (NoSuchElementException e)   {
		
			WebElement Create_Subsciber= driver.findElement(By.id("B13989961185388418"));
		 	
			Actions act= new Actions(driver);
		 	
			act.moveToElement(Create_Subsciber).doubleClick().build().perform();  
		 	
		}catch (StaleElementReferenceException E) {
		 	
		 		
			WebElement Create_Subsciber= driver.findElement(By.id("B13989961185388418"));
		 		
		 	Actions act= new Actions(driver);
		 		
		 	act.moveToElement(Create_Subsciber).doubleClick().build().perform();
	       
		}
		
		Thread.sleep(1500);
	 }
//*********************** Method to search number by MSISDN ********************************
public void Search_by_MSISDNnumberCRMTab()
{    
	try {
	
		Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
		
		dropdown.selectByVisibleText("MSISDN");
		
		driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
		
		driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(msisdn);
		
		driver.findElement(By.id("P0_SEARCH")).submit();
		
		WebElement MSISDN= driver.findElement(By.id("P2_SI_MSISDN"));
		
		String MSIsdn= MSISDN.getText();
		
		System.out.println(MSIsdn);
		
		System.out.println(msisdn);
		
		Assert.assertEquals(MSIsdn, msisdn);
		
		driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
	  	
	}catch(StaleElementReferenceException E) {
	
		Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	  	
		dropdown.selectByVisibleText("MSISDN");
	  	
		driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
	  	
		driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(msisdn);
	  	
		driver.findElement(By.id("P0_SEARCH")).submit();
	  	
		WebElement MSISDN= driver.findElement(By.id("P2_SI_MSISDN"));
	  	
		String MSIsdn= MSISDN.getText();
	  	
		Assert.assertEquals(MSIsdn, msisdn);
	  	
		driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
	  }
	
   }


//*************************** Method to search by MSISDN ************************************
public void SearchByMSISDN(String msisdn)
  {
	 try {
		 	
		 Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
		 
		 dropdown.selectByVisibleText("MSISDN");
		 
		 driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
		 
		 driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(msisdn);
		 
		 driver.findElement(By.id("P0_SEARCH")).submit();
		 
	 }catch(StaleElementReferenceException E)  {
	
		 Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
		
		 dropdown.selectByVisibleText("MSISDN");
		
		 driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
		
		 driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(msisdn);
		
		 driver.findElement(By.id("P0_SEARCH")).submit();
	     
	 }
	}

//******************************** Method to select primary report *********************************
public void select_primary_report()
{
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText("1. Primary Report");
}

//******************************** Method to click action button ************************************
public void click_ActionButton()
{
	WebElement action= driver.findElement(By.id("apexir_ACTIONSMENUROOT"));

	Actions act= new Actions(driver);
	
	act.moveToElement(action).click().build().perform();
}

//************************************ Method to click apply ***************************************

public void Click_apply()

{

	WebElement apply= driver.findElement(By.id("apexir_btn_APPLY"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(apply).click().build().perform();
  }


//*********************************** Method to test download CSV ***************************
public boolean Download_CSV(String downloadPath, String fileName) throws InterruptedException
  {

	click_ActionButton();
	
	Thread.sleep(200);
	
	//Click download option
	
	WebElement reset = driver.findElement(By.xpath("//a[contains(text(),'Download')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(reset).click().build().perform();
	
	// Choose and click CSV report type to download 
	
	Thread.sleep(1000);
	
	WebElement csv = driver.findElement(By.xpath("//a[@id='apexir_dl_CSV']//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(csv).click().build().perform();
	
	Thread.sleep(3000);
	 	 
	File dir = new File(downloadPath);
	 	 
	File[] dirContents = dir.listFiles();
	
	for(int i = 0; i < dirContents.length; i++) {
	   
		if (dirContents[i].getName().equals(fileName)) {
	    
			// File has been found, it can now be deleted:
	    	
			Thread.sleep(1000);
	        
			dirContents[i].delete();
	        
			System.out.println("File is getting deleted");
	        
			return true;
	      }
		
	   }
	      return false;
	  }

//*********************************** Method to test download HTML ***************************
public boolean Download_HTML(String downloadPath, String fileName) throws InterruptedException
{
	Thread.sleep(200);
	
	// Choose and click HTML report type to download 
	
	Thread.sleep(500);
	
	WebElement html = driver.findElement(By.id("apexir_dl_HTML"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(html).click().build().perform();
	
	Thread.sleep(8000);
	
	File dir = new File(downloadPath);
	
	File[] dirContents = dir.listFiles();
	
	for (int i = 0; i < dirContents.length; i++) {
	
		if (dirContents[i].getName().equals(fileName)) {
	    
			// File has been found, it can now be deleted:
	    	
			Thread.sleep(1000);
	        
			dirContents[i].delete();
	        
			System.out.println("File is getting deleted");
	        
			return true;
	      }
       }

	return false;
}	


//*********************************** Method to send report via email *****************************
public void email_report() throws InterruptedException
  {
	 // Choose and click email report option 

	Thread.sleep(500);
	
	WebElement email = driver.findElement(By.id("apexir_dl_EMAIL"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(email).click().build().perform();
	
	// Enter email address of receiver	 
	
	WebElement TOemail= driver.findElement(By.id("apexir_EMAIL_TO"));
	
	TOemail.clear();
	
	TOemail.sendKeys(toemail);
	
	Thread.sleep(300);
	
	//Click send
	
	WebElement send = driver.findElement(By.id("apexir_btn_APPLY"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(send).click().build().perform();
  }


//********************************* Method to filter report using flash back option ****************
public void flashback_filter() throws InterruptedException
  {
	
	Thread.sleep(300);
	
	click_ActionButton();
	
	Thread.sleep(1000);
	
	//Click flash back option
	
	WebElement flashback= driver.findElement(By.xpath("//a[contains(text(),'Flashback')]"));
		
	Actions act= new Actions(driver);
		
	act.moveToElement(flashback).click().build().perform();
		
	// Enter flash back time
	
	Thread.sleep(800);
	
	WebElement time= driver.findElement(By.id("apexir_FLASHBACK_TIME"));
	
	time.clear();
	
	time.sendKeys(Time); 
	
	try{		
	
	//click apply
	
	Click_apply();
	
	Thread.sleep(5000);
	
	String text1= driver.findElement(By.xpath("//table[@id='apexir_CONTROL_PANEL_COMPLETE']")).getText(); 
	
	System.out.println(text1);
	
	Boolean successmsg= text1.endsWith(flashback_msg);
	
	Assert.assertTrue(successmsg);
	
	}catch(Exception e) {
	
	//click apply
	
	Click_apply();
		
	Thread.sleep(8000);
		
	String text1= driver.findElement(By.xpath("//table[@id='apexir_CONTROL_PANEL_COMPLETE']")).getText(); 
		
	System.out.println(text1);
		
	Boolean successmsg= text1.endsWith(flashback_msg);
		
	Assert.assertTrue(successmsg);
		
	}
}

//*************************** Method to click apply changes **************************
  public void applyChanges()
   {
	  WebElement applychanges = driver.findElement(By.id("B9584608921731998"));
	
	  Actions act= new Actions(driver);
	  
	  act.moveToElement(applychanges).click().build().perform();	
   }

//*********************************** Method to test download HTML ***************************
public boolean Download_PDF(String downloadPath, String fileName) throws InterruptedException
  {
	Thread.sleep(200);
	
	 // Choose and click PDF report type to download 
		
	Thread.sleep(500);
	
	WebElement pdf = driver.findElement(By.id("apexir_dl_PDF"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(pdf).click().build().perform();
	
	Thread.sleep(1500);
	
	File dir = new File(downloadPath);
	
	File[] dirContents = dir.listFiles();
	
	for (int i = 0; i < dirContents.length; i++) {
	
		if (dirContents[i].getName().equals(fileName)) {
	    
			// File has been found, it can now be deleted:
	    	
			Thread.sleep(1000);
	        
			dirContents[i].delete();
	        
			System.out.println("File is getting deleted");
	        
			return true;
	      }
	   }
	    
	return false;
	
  }	

//*********************************** Method to send report via email *****************************
public void get_subscription() throws InterruptedException
{
	WebElement chroller= driver.findElement(By.id("echo-page-header-toggler"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(chroller).click().build().perform();
	
	Thread.sleep(2500);
	
	click_ActionButton();
	
	Thread.sleep(500);
	
	 // Choose and click subscription option 
	
	Thread.sleep(500);
	
	WebElement email = driver.findElement(By.xpath("//a[contains(text(),'Subscription')]"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(email).click().build().perform();
	 
	 // Enter email address of receiver	 
	
	Thread.sleep(300);
	
	WebElement TOemail= driver.findElement(By.id("apexir_EMAIL_ADDRESS"));
	
	TOemail.clear();
	
	TOemail.sendKeys(toemail);
	 
	 //Click send
	
	WebElement send = driver.findElement(By.id("apexir_btn_APPLY"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(send).click().build().perform();
}
//************************************ Method to click User admin tab *************************
public void click_UserAdmin()
{
   	WebElement admin=  driver.findElement(By.linkText("User Admin"));
   	
	Actions act= new Actions(driver);
	
	act.moveToElement(admin).click().build().perform();
	
	}
	
//************************************* Method to search by MSISDN for sub tab of customer care tab	*******************
public void SEARCBY_MSISDN() throws InterruptedException
{
	//Select search option

	Select option= new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	
	option.selectByVisibleText("MSISDN");
	
	//Enter MSISDN 
	
	WebElement MSISDN= driver.findElement(By.id("P0_SEARCH_VALUE"));
	
	MSISDN.clear();
	
	MSISDN.sendKeys(msisdn);
						
	//click Go

	WebElement go = driver.findElement(By.id("P0_SEARCH"));
	
	Actions act1= new Actions(driver);

	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(600);
	
}

	
//************************************* Method to search by IMSI for sub tab of customer care tab	*******************
public void SEARCBY_IMSI() throws InterruptedException
{
	//Select search option

	Select option= new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	
	option.selectByVisibleText("IMSI");
	
	//Enter IMSI number 
	
	WebElement IMSI= driver.findElement(By.id("P0_SEARCH_VALUE"));
	
	IMSI.clear();
	
	IMSI.sendKeys(imsi);
		
	//click Go
	
	WebElement go = driver.findElement(By.id("P0_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(600);
	
}
		
//************************************* Method to search by ICCID for sub tab of customer care tab	*******************
public void SEARCBY_ICCID() throws InterruptedException
{
   	//Select search option

	Select option= new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	
	option.selectByVisibleText("ICCID");
	
	//Enter ICCID number 
	
	WebElement ICCID= driver.findElement(By.id("P0_SEARCH_VALUE"));
	
	ICCID.clear();
	
	ICCID.sendKeys(iccid);
							
	//click Go
	
	WebElement go = driver.findElement(By.id("P0_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(600);
	
}
		
		
//************************************* Method to search by Account Number for sub tab of customer care tab	*******************
public void SEARCBY_AccountNumber() throws InterruptedException
{
   	//Select search option

	Select option= new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	
	option.selectByVisibleText("Account Number");
	
	//Enter IAccount number 
	
	WebElement Account_number= driver.findElement(By.id("P0_SEARCH_VALUE"));
	
	Account_number.clear();
	
	Account_number.sendKeys(AccNo);
												
	//click Go
	
	WebElement go = driver.findElement(By.id("P0_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(600);
	
}

//************************************* Method to search by Invoice Number for sub tab of customer care tab	*******************
public void SEARCBY_InvoiceNumber() throws InterruptedException
{
	//Select search option

	Select option= new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	
	option.selectByVisibleText("Invoice Number");
											
	//Enter Invoice number 
	
	WebElement Invoice_number= driver.findElement(By.id("P0_SEARCH_VALUE"));
	
	Invoice_number.clear();
	
	Invoice_number.sendKeys(InvoiceNumber);
															
	//click Go
	
	WebElement go = driver.findElement(By.id("P0_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(600);
	
}
		
		
//************************************* Method to search by Circuit Number for sub tab of customer care tab	*******************
public void SEARCBY_CircuitNumber() throws InterruptedException
{
	//Select search option

	Select option= new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	
	option.selectByVisibleText("Circuit Number");
													
	//Enter Circuit number 
	
	WebElement Circuit_number= driver.findElement(By.id("P0_SEARCH_VALUE"));
	
	Circuit_number.clear();
	
	Circuit_number.sendKeys(CircuitNumber);
													
	//click Go
	
	WebElement go = driver.findElement(By.id("P0_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(600);
	
}
		
//************************************* Method to search by Account UID for sub tab of customer care tab	*******************
public void SEARCBY_AccountUID() throws InterruptedException
{
	//Select search option

	Select option= new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	
	option.selectByVisibleText("Account UID");
		
	//Enter Account UID number 
	
	WebElement AccountUID= driver.findElement(By.id("P0_SEARCH_VALUE"));
	
	AccountUID.clear();
	
	AccountUID.sendKeys(AccUID);
																		
	//click Go
	
	WebElement go = driver.findElement(By.id("P0_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(600);
	
}
		
		
//************************************* Method to search by Subscriber UID for sub tab of customer care tab	*******************
public void SEARCBY_SubscriberUID() throws InterruptedException
{
						
	//Select search option
	
	Select option= new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	
	option.selectByVisibleText("Subscriber UID");
															
	//Enter Subscriber UID number 

	WebElement SubscriberUID= driver.findElement(By.id("P0_SEARCH_VALUE"));
	
	SubscriberUID.clear();
	
	SubscriberUID.sendKeys(SubUID);
																	
	//click Go
	
	WebElement go = driver.findElement(By.id("P0_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(600);
	
}

//************************************* Method to search by MPESA OTT for sub tab of customer care tab	*******************
public void SEARCBY_MPESA_OTT() throws InterruptedException
{
	//Select search option

	Select option= new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	
	option.selectByVisibleText("MPESA OTT");
		
	//Enter MPESA OTT 
	
	WebElement MPESA_OTT= driver.findElement(By.id("P0_SEARCH_VALUE"));
	
	MPESA_OTT.clear();
	
	MPESA_OTT.sendKeys(mpesa_ott);
																		
	//click Go
	
	WebElement go = driver.findElement(By.id("P0_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(600);
	
}

//********************************* Method to click Dealer Web tab ******************************
public void click_DealersWebTab()
{
	WebElement WICC= driver.findElement(By.linkText("Dealer Web"));

	Actions act= new Actions(driver);
	
	act.moveToElement(WICC).click().build().perform();

	//Verify report
	
	WebElement verify= driver.findElement(By.xpath("//a[@class='echo-current']"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
	
}	
				
//********************************* Method to click Monitors tab ******************************
public void click_MonitorsTab() throws InterruptedException
{
	WebElement Monitors= driver.findElement(By.linkText("Monitors"));

	Actions act= new Actions(driver);
	
	act.moveToElement(Monitors).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement verify= driver.findElement(By.xpath("//a[@class='echo-current']"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
	
}
		
//********************************* Method to click ticketing tab ******************************
public void click_TicketingTab()
   	{

	WebElement Ticketing =driver.findElement(By.linkText("Ticketing"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Ticketing).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
	
   	}	
	
//********************************* Method to click FMS tab ******************************
public void click_FMSTab()
    {

	WebElement fms =driver.findElement(By.linkText("FMS"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(fms).click().build().perform();
	
  	}	
	
//********************************* Method to click RMS tab ******************************
public void click_RMSTab()
   {

	WebElement Rms =driver.findElement(By.linkText("RMS"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Rms).click().build().perform();
					
   }
	
//********************************* Method to click RA tab ******************************
public void click_RATab()
   {

	WebElement Ra =driver.findElement(By.linkText("RA"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Ra).click().build().perform();
							
   }
	
//*********************************** Method to test reset button on CIB ***********************************
public void Reset() throws InterruptedException
	{

	Thread.sleep(200);
	
	click_ActionButton();
	
	Thread.sleep(500);
	
	//Click reset option
	
	WebElement reset = driver.findElement(By.xpath("//a[contains(text(),'Reset')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(reset).click().build().perform();	
	
	Thread.sleep(500);

	//click apply
	
	WebElement apply = driver.findElement(By.id("apexir_btn_APPLY"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(apply).click().build().perform();	
	
	Thread.sleep(800);
	
	}

//**************************** Method to click Billing tab *****************************

public void click_billingTab()
{
	WebElement billing=  driver.findElement(By.linkText("Billing"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(billing).click().build().perform();
	
}
	  
//**************************** Method to click Reporting tab *****************************
 public void click_ReportingTab()
  {

	 WebElement reporting=  driver.findElement(By.linkText("Reporting"));
	
	 Actions act= new Actions(driver);
	
	 act.moveToElement(reporting).click().build().perform();
	 
  }
  
//**************************** Method to click workflow tab *****************************
 public void click_WorkflowTab()
  {
		WebElement Workflow=  driver.findElement(By.linkText("Workflow"));

		Actions act= new Actions(driver);
		
		act.moveToElement(Workflow).click().build().perform();
  }
 
	   
//************************* Method to check success message *************************
public void Success_Msg()
 {
	
	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
	
	String Expected_msg= "Action Processed.";
	
	Assert.assertEquals(Msg, Expected_msg);
}

//***************************************** Method to click Sales Tab **********************
public void click_SalesTab()
{
	
	WebElement Sales=  driver.findElement(By.linkText("Sales"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Sales).click().build().perform();
}

//************************************** Method to apply filter and view filtered report **********************
public void view_filtered_report() throws InterruptedException
{
	try {	
		// Click Action button
	
		click_ActionButton();
		
		Thread.sleep(300);

		//Click filter
		
		WebElement filter= driver.findElement(By.xpath("//a[@class='dhtmlSubMenuN'][contains(text(),'Filter')]"));
		
		Actions act = new Actions(driver);
		
		act.moveToElement(filter).click().build().perform();
		
		//Select Column
		
		Thread.sleep(300);
		
		Select column= new Select(driver.findElement(By.id("apexir_COLUMN_NAME")));
		
		column.selectByVisibleText(col);
		
		Thread.sleep(300);

		//select or enter expression
		
		WebElement expression= driver.findElement(By.id("apexir_EXPR"));
		
		expression.clear();
		
		expression.sendKeys(expvalue);
		
		//Click apply

		Thread.sleep(200);
		
		Click_apply();

		}catch(Exception e) {
		
			// Click Action button
			
			click_ActionButton();
			
			Thread.sleep(300);

			//Click filter
		
			WebElement filter= driver.findElement(By.xpath("//a[@class='dhtmlSubMenuN'][contains(text(),'Filter')]"));
		
			Actions act = new Actions(driver);
		
			act.moveToElement(filter).click().build().perform();

			//Select Column
			
			Thread.sleep(300);
		
			Select column= new Select(driver.findElement(By.id("apexir_COLUMN_NAME")));
		
			column.selectByVisibleText(col);
		
			Thread.sleep(300);

			//select or enter expression
		
			WebElement expression= driver.findElement(By.id("apexir_EXPR"));
		
			expression.clear();
		
			expression.sendKeys(expvalue);
		
			//Click apply
		
			Thread.sleep(200);
		
			Click_apply();

		}
}

//************************************* Method to save report *************************************
public void save_report() throws InterruptedException
{
	// Click Actions button
		
	click_ActionButton();
	
	// Click save report
	
	Thread.sleep(300);
	
	WebElement save= driver.findElement(By.xpath("//a[contains(text(),'Save Report')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(save).click().build().perform();
	
	
	 //enter name of the report
	
	Thread.sleep(300);
	
	WebElement name= driver.findElement(By.id("apexir_WORKSHEET_NAME"));
	
	name.clear();
	
	name.sendKeys(reportname);
	 	 
	 // enter description
	
	Thread.sleep(300);
	
	WebElement description= driver.findElement(By.id("apexir_DESCRIPTION"));
	
	description.clear();
	
	description.sendKeys(reportname); 
	 	
	 //click apply
	
	Click_apply();
	
}

//**************************************** Method to search by private report ****************************
public void Search_by_private_report() throws InterruptedException
{
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));

	dropdown.selectByVisibleText(report);

	}
		
}
 

