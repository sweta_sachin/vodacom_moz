package Customer_Care_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PGW_PaymentGateWay_Tab_Tests extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}

	
	@Test
	public void a_View_PGWTab()
	{
		view_PGWTab();
	}

	@Test
	public void b_Click_PGWTab()
	{
		click_PGWTab();
	}
	
	
	@Test
	public void c_ViewPGWTransaction_By_MSISDN() throws InterruptedException
	{
		ViewPGWTransaction_ByMSISDN();
	}
	
	@Test
	public void d_ViewPWG_SMSTransaction_By_MSISDN()
	{
		View_SMSTransaction_ByMSISDN();
	}
	
	@Test
	public void e_ViewPGWTransaction_By_IMSI() throws InterruptedException
	{
		ViewPGWTransaction_ByIMSI();
	}
	
	
	@Test
	public void f_ViewPWG_SMSTransaction_By_IMSI()
	{
		View_SMSTransaction_ByIMSI();
		
	}
	
	@Test
	public void g_ViewPGWTransaction_By_ICCID() throws InterruptedException
	{
		ViewPGWTransaction_ByICCID();
	}
	
	
	@Test
	public void h_ViewPWG_SMSTransaction_By_ICCID()
	{
		View_SMSTransaction_ByICCID();
		
	}
	
	@Test
	public void i_ViewPGWTransaction_By_AccountNumber() throws InterruptedException
	{
		ViewPGWTransaction_ByAccountNumber();
	}
	
	
	@Test
	public void j_ViewPWG_SMSTransaction_By_AccountNumber()
	{
		View_SMSTransaction_ByAccount_Number();
		
	}
	
	@Test
	public void k_ViewPGWTransaction_By_InvoiceNumber() throws InterruptedException
	{
		ViewPGWTransaction_ByInvoiceNumber();
	}
	
	
	@Test
	public void l_ViewPWG_SMSTransaction_By_InvoiceNumber()
	{
		View_SMSTransaction_ByInvoice_Number();
		
	}
	
	@Test
	public void m_ViewPGWTransaction_By_CircuitNumber() throws InterruptedException
	{
		ViewPGWTransaction_ByCircuitNumber();
	}
	
	
	@Test
	public void n_ViewPWG_SMSTransaction_By_CircuitNumber()
	{
		View_SMSTransaction_ByCircuitNumber();
		
	}

	
	@Test
	public void o_ViewPGWTransaction_By_AccountUID() throws InterruptedException
	{
		ViewPGWTransaction_ByAccountUID();
	}
	
	
	@Test
	public void p_ViewPWG_SMSTransaction_By_AccountUID()
	{
		View_SMSTransaction_ByAccountUID();
		
	}
	
	@Test
	public void q_ViewPGWTransaction_By_SubscriberUID() throws InterruptedException
	{
		ViewPGWTransaction_BySubscriberUID();
	}
	
	
	@Test
	public void r_ViewPWG_SMSTransaction_By_SubscriberUID()
	{
		View_SMSTransaction_BySubscriberUID();
		
	}
	
	@Test
	public void s_ViewPGWTransaction_By_MESAOTT() throws InterruptedException
	{
		ViewPGWTransaction_By_MPSA_OTT();
	}
	
	
	@Test
	public void t_ViewPWG_SMSTransaction_By_MESAOTT()
	{
		View_SMSTransaction_ByMPSA_OTT();
		
	}
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view PGW tab ******************************
public void view_PGWTab()
{
	WebElement pgw= driver.findElement(By.linkText("PGW"));
	
	Boolean PGW= pgw.isDisplayed();
	
	Assert.assertTrue(PGW);
}

//********************************* Method to click PGW tab ******************************
public void click_PGWTab()
{
	WebElement PGW= driver.findElement(By.linkText("PGW"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(PGW).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("P0_SEARCH_VALUE"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}	

//***************************************** Method to view PGW transaction by MSISDN number **********************
public void ViewPGWTransaction_ByMSISDN() throws InterruptedException
	{
		SEARCBY_MSISDN();
		
	// Verify report
	
		WebElement Report= driver.findElement(By.xpath("//div[@id='R5952731214380320']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
	}

//****************************************Method to view SMS transaction by MSISDN ************************************
public void View_SMSTransaction_ByMSISDN()
	{
		// Verify report

	WebElement Report= driver.findElement(By.xpath("//div[@id='R5960026908674777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	
	}

//***************************************** Method to view PGW transaction by IMSI number **********************
public void ViewPGWTransaction_ByIMSI() throws InterruptedException
	{
		SEARCBY_IMSI();
	
		WebElement Report= driver.findElement(By.xpath("//div[@id='R5952731214380320']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
	}
		
//****************************************Method to view SMS transaction by IMSI ************************************
public void View_SMSTransaction_ByIMSI()
	{
		// Verify report
		
	WebElement Report= driver.findElement(By.xpath("//div[@id='R5960026908674777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	
	}		

//***************************************** Method to view PGW transaction by ICCID number **********************
public void ViewPGWTransaction_ByICCID() throws InterruptedException
	{
		SEARCBY_ICCID();
				
		// Verify report
	
		WebElement Report= driver.findElement(By.xpath("//div[@id='R5952731214380320']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
	}

			
//****************************************Method to view SMS transaction by IMSI ************************************
public void View_SMSTransaction_ByICCID()
	{
			// Verify report

	WebElement Report= driver.findElement(By.xpath("//div[@id='R5960026908674777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	
	}		

//***************************************** Method to view PGW transaction by Account number **********************
 public void ViewPGWTransaction_ByAccountNumber() throws InterruptedException
	{
		SEARCBY_AccountNumber();
						
		// Verify report
	
		WebElement Report= driver.findElement(By.xpath("//div[@id='R5952731214380320']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
	}
 

//****************************************Method to view SMS transaction by Account Number ************************************
 public void View_SMSTransaction_ByAccount_Number()
	{
		// Verify report

	 WebElement Report= driver.findElement(By.xpath("//div[@id='R5960026908674777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	 Boolean report= Report.isDisplayed();
	
	 Assert.assertTrue(report);
	
	}	
		
//***************************************** Method to view PGW transaction by Invoice number **********************
public void ViewPGWTransaction_ByInvoiceNumber() throws InterruptedException
	{
	
	SEARCBY_InvoiceNumber();
	
	// Verify report
	
	WebElement Report= driver.findElement(By.xpath("//div[@id='R5952731214380320']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean report= Report.isDisplayed();
									
	Assert.assertTrue(report);
	
	}
			
//****************************************Method to view SMS transaction by Invoice number ************************************
public void View_SMSTransaction_ByInvoice_Number()
	{
		// Verify report

	WebElement Report= driver.findElement(By.xpath("//div[@id='R5960026908674777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean report= Report.isDisplayed();
							
	Assert.assertTrue(report);
		
	}		
			
//***************************************** Method to view PGW transaction by Circuit number **********************
public void ViewPGWTransaction_ByCircuitNumber() throws InterruptedException
	{
		SEARCBY_CircuitNumber();
										
		// Verify report
	
		WebElement Report= driver.findElement(By.xpath("//div[@id='R5952731214380320']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
										
	    
		Boolean report= Report.isDisplayed();
											
		
		Assert.assertTrue(report);
			
	}
			
//****************************************Method to view SMS transaction by Circuit Number ************************************
public void View_SMSTransaction_ByCircuitNumber()
	{
	
	// Verify report
	
	WebElement Report= driver.findElement(By.xpath("//div[@id='R5960026908674777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
							
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	
	}		
			
			
//***************************************** Method to view PGW transaction by Account UID **********************
public void ViewPGWTransaction_ByAccountUID() throws InterruptedException
	{
		SEARCBY_AccountUID();
										
		// Verify report
			 WebElement Report= driver.findElement(By.xpath("//div[@id='R5952731214380320']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
											
       		 Boolean report= Report.isDisplayed();
											
        	Assert.assertTrue(report);
	}
			
//****************************************Method to view SMS transaction by Account UID ************************************
public void View_SMSTransaction_ByAccountUID()
	{
		// Verify report

	WebElement Report= driver.findElement(By.xpath("//div[@id='R5960026908674777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
							
	Boolean report= Report.isDisplayed();
							
	Assert.assertTrue(report);
	
	}		
	
//***************************************** Method to view PGW transaction by Subscriber UID **********************
public void ViewPGWTransaction_BySubscriberUID() throws InterruptedException
	{
	SEARCBY_SubscriberUID();
											
	// Verify report
	
	WebElement Report= driver.findElement(By.xpath("//div[@id='R5952731214380320']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
												
	Boolean report= Report.isDisplayed();
												
	Assert.assertTrue(report);
	
	}
				
//****************************************Method to view SMS transaction by Subscriber UID ***********************************
public void View_SMSTransaction_BySubscriberUID()
 {
	// Verify report

	WebElement Report= driver.findElement(By.xpath("//div[@id='R5960026908674777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
							
	Boolean report= Report.isDisplayed();
							
	Assert.assertTrue(report);
	
 }		

//***************************************** Method to view PGW transaction by MPSA OTT **********************
public void ViewPGWTransaction_By_MPSA_OTT() throws InterruptedException
	{
		SEARCBY_MPESA_OTT();
										
		// Verify report
	
		WebElement Report= driver.findElement(By.xpath("//div[@id='R5952731214380320']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
														
		Boolean report= Report.isDisplayed();
														
		 Assert.assertTrue(report);
		
	}
//****************************************Method to view SMS transaction by MPSA OTT ************************************
public void View_SMSTransaction_ByMPSA_OTT()
{
	// Verify report

	WebElement Report= driver.findElement(By.xpath("//div[@id='R5960026908674777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
							
	Boolean report= Report.isDisplayed();
							
	Assert.assertTrue(report);
	
	}		
}
