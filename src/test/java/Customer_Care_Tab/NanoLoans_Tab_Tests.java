package Customer_Care_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class NanoLoans_Tab_Tests extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}

	
	@Test
	public void a_View_NanoLoansTab()
	{
		view_NanoLoansTab();
	}

	@Test
	public void b_Click_NanoLoansTab()
	{
		click_NanoLoansTab();
	}
	
	
	@Test
	public void c_View_CustomerDetails_By_MSISDN() throws InterruptedException
	{
		ViewcustomerDetails_ByMSISDN();
	}
	
	@Test
	public void d_View_TransactionDetails_By_MSISDN() throws InterruptedException
	{
		ViewTransactionDetails_ByMSISDN();
	}
	
	@Test
	public void e_View_RepaymentDetails_By_MSISDN() throws InterruptedException
	{
		ViewRepaymentDetails_ByMSISDN();
	}
	
		
	@Test
	public void c_View_CustomerDetails_By_IMSI() throws InterruptedException
	{
		ViewcustomerDetails_ByIMSI();
	}
	
	@Test
	public void c_View_TransactionDetails_By_IMSI() throws InterruptedException
	{
		ViewTransactionDetails_ByIMSI();
	}
	
	@Test
	public void c_View_RepaymentDetails_By_IMSI() throws InterruptedException
	{
		ViewRepaymentDetails_ByIMSI();
	}
	
	
	
		
	@Test
	public void c_View_CustomerDetails_By_ICCID() throws InterruptedException
	{
		ViewcustomerDetails_ByICCID();
	}
	
	@Test
	public void c_View_TransactionDetails_By_ICCID() throws InterruptedException
	{
		ViewTransactionDetails_ByICCID();
	}
	
	@Test
	public void c_View_RepaymentDetails_By_ICCID() throws InterruptedException
	{
		ViewRepaymentDetails_ByICCID();
	}
	
	

	@Test
	public void c_View_CustomerDetails_By_AccountNUmber() throws InterruptedException
	{
		ViewcustomerDetails_ByAccountNumber();
	}
	
	@Test
	public void c_View_TransactionDetails_By_AccountNUmber() throws InterruptedException
	{
		ViewTransactionDetails_ByAccountNumber();
	}
	
	@Test
	public void c_View_RepaymentDetails_By_AccountNUmber() throws InterruptedException
	{
		ViewRepaymentDetails_ByAccountNumber();
	}
	
	
		
	@Test
	public void c_View_CustomerDetails_By_InvoiceNUmber() throws InterruptedException
	{
		ViewcustomerDetails_ByInvoiceNumber();
	}
	
	@Test
	public void c_View_TransactionDetails_By_InvoiceNUmber() throws InterruptedException
	{
		ViewTransactionDetails_ByInvoiceNumber();
	}
	
	@Test
	public void c_View_RepaymentDetails_By_InvoiceNUmber() throws InterruptedException
	{
		ViewRepaymentDetails_ByInvoiceNumber();
	}
	
	@Test
	public void c_View_CustomerDetails_By_CircuitNUmber() throws InterruptedException
	{
		ViewcustomerDetails_ByCircuitNumber();
	}
	
	@Test
	public void c_View_TransactionDetails_By_CircuitNUmber() throws InterruptedException
	{
		ViewTransactionDetails_ByCircuitNumber();
	}
	
	@Test
	public void c_View_RepaymentDetails_By_CircuitNUmber() throws InterruptedException
	{
		ViewRepaymentDetails_ByCircuitNumber();
	}
	
	@Test
	public void c_View_CustomerDetails_By_AccountUID() throws InterruptedException
	{
		ViewcustomerDetails_ByAccountUID();
	}
	
	@Test
	public void c_View_TransactionDetails_By_AccountUID() throws InterruptedException
	{
		ViewTransactionDetails_ByAccountUID();
	}
	
	@Test
	public void c_View_RepaymentDetails_By_AccountUID() throws InterruptedException
	{
		ViewRepaymentDetails_ByAccountUID();
	}
	
	@Test
	public void c_View_CustomerDetails_By_SubscriberUID() throws InterruptedException
	{
		ViewcustomerDetails_BySubscriberUID();
	}
	
	@Test
	public void c_View_TransactionDetails_By_SubscriberUID() throws InterruptedException
	{
		ViewTransactionDetails_BySubscriberUID();
	}
	
	@Test
	public void c_View_RepaymentDetails_By_SubscriberUID() throws InterruptedException
	{
		ViewRepaymentDetails_BySubscriberUID();
	}
	
	
	@Test
	public void c_View_CustomerDetails_By_MPESAOTT() throws InterruptedException
	{
		ViewcustomerDetails_By_MPESA_OTT();
	}
	
	@Test
	public void c_View_TransactionDetails_By_MPESAOTT() throws InterruptedException
	{
		ViewTransactionDetails_ByMPESAOTT();
	}
	
	@Test
	public void c_View_RepaymentDetails_By_MPESAOTT() throws InterruptedException
	{
		ViewRepaymentDetails_MPESAOTT();
	}
	
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Nano Loans tab ******************************
public void view_NanoLoansTab()
	    {
			WebElement nano= driver.findElement(By.linkText("Nano Loans"));
			
			Boolean Nano= nano.isDisplayed();
			
			Assert.assertTrue(Nano);
       }             
	
//********************************* Method to click Nano Loans tab ******************************
public void click_NanoLoansTab()
	 {

	WebElement Nano= driver.findElement(By.linkText("Nano Loans"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Nano).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("P0_SEARCH_VALUE"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
	
	}	

//***************************************** Method to view customer details by MSISDN number **********************
public void ViewcustomerDetails_ByMSISDN() throws InterruptedException
	{
		SEARCBY_MSISDN();
	
		// Verify report

		WebElement Report= driver.findElement(By.xpath("//div[@id='R34402509334263955']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
	}

//******************************************* Method to view transaction details by MSISDN **********************
public void ViewTransactionDetails_ByMSISDN()
	{

	WebElement Report= driver.findElement(By.xpath("//div[@id='R14970832134883777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	
}
	
//******************************************* Method to view repayments details by MSISDN **********************
public void ViewRepaymentDetails_ByMSISDN()
	{

	WebElement Report= driver.findElement(By.xpath("//div[@id='R14971909671905605']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	
	}
		
//***************************************** Method to View customer Details by IMSI number **********************
public void ViewcustomerDetails_ByIMSI() throws InterruptedException
	{
		SEARCBY_IMSI();
		
		WebElement Report= driver.findElement(By.xpath("//div[@id='R14970832134883777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
	}
		
//******************************************* Method to view transaction details by IMSI **********************
public void ViewTransactionDetails_ByIMSI()
	{
		 WebElement Report= driver.findElement(By.xpath("//div[@id='R14970832134883777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		 Boolean report= Report.isDisplayed();
    	
		 Assert.assertTrue(report);
	}
		
//******************************************* Method to view repayments details by IMSI **********************
public void ViewRepaymentDetails_ByIMSI()
	{
		 WebElement Report= driver.findElement(By.xpath("//div[@id='R14971909671905605']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		 Boolean report= Report.isDisplayed();
		
		 Assert.assertTrue(report);
	}
		
//***************************************** Method to View customer Details by ICCID number **********************
public void ViewcustomerDetails_ByICCID() throws InterruptedException
	{
		SEARCBY_ICCID();

		// Verify report
				
		WebElement Report= driver.findElement(By.xpath("//div[@id='R34402509334263955']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
				
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
			
//******************************************* Method to view transaction details by ICCID **********************
public void ViewTransactionDetails_ByICCID()
	{
	
	WebElement Report= driver.findElement(By.xpath("//div[@id='R14970832134883777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	
	}
			
//******************************************* Method to view repayments details by ICCID **********************
public void ViewRepaymentDetails_ByICCID()
	{
	
	WebElement Report= driver.findElement(By.xpath("//div[@id='R14971909671905605']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	}
	
//***************************************** Method to View customer Details by Account number **********************
  public void ViewcustomerDetails_ByAccountNumber() throws InterruptedException
	{
		SEARCBY_AccountNumber();
			
		// Verify report
	
		WebElement Report= driver.findElement(By.xpath("//div[@id='R34402509334263955']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}

//******************************************* Method to view transaction details by Account Number **********************
public void ViewTransactionDetails_ByAccountNumber()
	{
	
	WebElement Report= driver.findElement(By.xpath("//div[@id='R34402509334263955']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	
	}
			
//******************************************* Method to view repayments details by AccountNumber **********************
public void ViewRepaymentDetails_ByAccountNumber()
	{
	
	WebElement Report= driver.findElement(By.xpath("//div[@id='R14971909671905605']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	
	}
//***************************************** Method to View customer Details by Invoice number **********************
public void ViewcustomerDetails_ByInvoiceNumber() throws InterruptedException
	{
	
		SEARCBY_InvoiceNumber();
		
		// Verify report
		
		WebElement Report= driver.findElement(By.xpath("//div[@id='R34402509334263955']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
	

//******************************************* Method to view transaction details by Invoice Number **********************
 public void ViewTransactionDetails_ByInvoiceNumber()
	{

	 WebElement Report= driver.findElement(By.xpath("//div[@id='R14970832134883777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	 Boolean report= Report.isDisplayed();
	
	 Assert.assertTrue(report);
	
	}
	
 
//******************************************* Method to view repayments details by Invoice Number **********************
 public void ViewRepaymentDetails_ByInvoiceNumber()
	{
		 WebElement Report= driver.findElement(By.xpath("//div[@id='R14971909671905605']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));

		 Boolean report= Report.isDisplayed();
			 
		 Assert.assertTrue(report);
	}
 
//***************************************** Method to View customer Details by Circuit number **********************
 public void ViewcustomerDetails_ByCircuitNumber() throws InterruptedException
 	{
			SEARCBY_CircuitNumber();
										
			// Verify report

			WebElement Report= driver.findElement(By.xpath("//div[@id='R34402509334263955']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	      	
			Boolean report= Report.isDisplayed();
	      	
			Assert.assertTrue(report);
	}
			
//******************************************* Method to view transaction details by Circuit Number **********************
 public void ViewTransactionDetails_ByCircuitNumber()
	{
	
	 WebElement Report= driver.findElement(By.xpath("//div[@id='R14970832134883777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	 Boolean report= Report.isDisplayed();
	
	 Assert.assertTrue(report);

	}
 
	
//******************************************* Method to view repayments details by Circuit Number **********************
 public void ViewRepaymentDetails_ByCircuitNumber()
	{
		
	 WebElement Report= driver.findElement(By.xpath("//div[@id='R14971909671905605']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	 Boolean report= Report.isDisplayed();
	
	 Assert.assertTrue(report);
	}
	
//***************************************** Method to View customer Details by Account UID **********************
 public void ViewcustomerDetails_ByAccountUID() throws InterruptedException
	{
		SEARCBY_AccountUID();
						
		// Verify report
	
		WebElement Report= driver.findElement(By.xpath("//div[@id='R34402509334263955']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}		
//******************************************* Method to view transaction details by Account UID **********************
 public void ViewTransactionDetails_ByAccountUID()
	{
		
	 WebElement Report= driver.findElement(By.xpath("//div[@id='R14970832134883777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	 Boolean report= Report.isDisplayed();
	
	 Assert.assertTrue(report);

	}
 
			
//******************************************* Method to view repayments details by Account UID **********************
public void ViewRepaymentDetails_ByAccountUID()
	{
		 WebElement Report= driver.findElement(By.xpath("//div[@id='R14971909671905605']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
		 Boolean report= Report.isDisplayed();
     	
		 Assert.assertTrue(report);
	}

//***************************************** Method to View customer Details by Subscriber UID **********************
public void ViewcustomerDetails_BySubscriberUID() throws InterruptedException
	{
		SEARCBY_SubscriberUID();
											
    	// Verify report
				
		WebElement Report= driver.findElement(By.xpath("//div[@id='R34402509334263955']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
			
//******************************************* Method to view transaction details by Subscriber UID **********************
public void ViewTransactionDetails_BySubscriberUID()
{
	
	WebElement Report= driver.findElement(By.xpath("//div[@id='R14970832134883777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	
	}
			
//******************************************* Method to view repayments details by Subscriber UID **********************
public void ViewRepaymentDetails_BySubscriberUID()
{
	WebElement Report= driver.findElement(By.xpath("//div[@id='R14971909671905605']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	
}			

//***************************************** Method to view View customer Details by MPSA OTT **********************
 public void ViewcustomerDetails_By_MPESA_OTT() throws InterruptedException
	{
		SEARCBY_MPESA_OTT();
										
		// Verify report

		WebElement Report= driver.findElement(By.xpath("//div[@id='R34402509334263955']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}

//******************************************* Method to view transaction details by MPSA OTT **********************
 public void ViewTransactionDetails_ByMPESAOTT()
	{
	 	WebElement Report= driver.findElement(By.xpath("//div[@id='R14970832134883777']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
	 	Boolean report= Report.isDisplayed();
		
	 	Assert.assertTrue(report);
		
	}
			
//******************************************* Method to view repayments details by MPSA OTT*****************
 public void ViewRepaymentDetails_MPESAOTT()
    {
	 	WebElement Report= driver.findElement(By.xpath("//div[@id='R14971909671905605']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
	 	Boolean report= Report.isDisplayed();
	    
	 	Assert.assertTrue(report);
		
    }
}
