package Customer_Care_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class NetControl_Tab_Tests extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}

	
	@Test
	public void a_View_NetControlTab()
	{
		view_NetControlTab();
	}

	@Test
	public void b_Click_NetControlTab()
	{
		click_NetControlTab();
	}
	
	
	@Test
	public void c_ViewNetControlTransaction_By_MSISDN() throws InterruptedException
	{
		ViewNetControlTransaction_ByMSISDN();
	}
	
		
	@Test
	public void e_ViewNetControlTransaction_By_IMSI() throws InterruptedException
	{
		ViewNetControlTransaction_ByIMSI();
	}
	
	@Test
	public void g_ViewNetControlTransaction_By_ICCID() throws InterruptedException
	{
		ViewNetControlTransaction_ByICCID();
	}
	
	@Test
	public void i_ViewNetControlTransaction_By_AccountNumber() throws InterruptedException
	{
		ViewNetControlTransaction_ByAccountNumber();
	}
	
	
	@Test
	public void k_ViewNetControlTransaction_By_InvoiceNumber() throws InterruptedException
	{
		ViewNetControlTransaction_ByInvoiceNumber();
	}
	
	@Test
	public void m_ViewNetControlTransaction_By_CircuitNumber() throws InterruptedException
	{
		ViewNetControlTransaction_ByCircuitNumber();
	}
	
	
	@Test
	public void o_ViewNetControlTransaction_By_AccountUID() throws InterruptedException
	{
		ViewNetControlTransaction_ByAccountUID();
	}
	
	@Test
	public void q_ViewNetControlTransaction_By_SubscriberUID() throws InterruptedException
	{
		ViewNetControlTransaction_BySubscriberUID();
	}
	
	@Test
	public void s_ViewNetControlTransaction_By_MESAOTT() throws InterruptedException
	{
		ViewNetControlTransaction_By_MPSA_OTT();
	}
	
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Net Control tab ******************************
public void view_NetControlTab()
{
	WebElement net= driver.findElement(By.linkText("NetControl"));
	
	Boolean Net= net.isDisplayed();
	
	Assert.assertTrue(Net);
}


//********************************* Method to click NetControl tab ******************************
public void click_NetControlTab()
{
	WebElement Net= driver.findElement(By.linkText("NetControl"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Net).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("P0_SEARCH_VALUE"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}	

//***************************************** Method to view NetControl transaction by MSISDN number **********************
public void ViewNetControlTransaction_ByMSISDN() throws InterruptedException
	{
		SEARCBY_MSISDN();
		
	// Verify report
	
		WebElement Report= driver.findElement(By.xpath("//div[@id='R8928923238287882']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
	}

	
//***************************************** Method to view NetControl transaction by IMSI number **********************
public void ViewNetControlTransaction_ByIMSI() throws InterruptedException
	{
		SEARCBY_IMSI();

		WebElement Report= driver.findElement(By.xpath("//div[@id='R8928923238287882']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
	}

//***************************************** Method to view NetControl transaction by ICCID number **********************
public void ViewNetControlTransaction_ByICCID() throws InterruptedException
	{
		SEARCBY_ICCID();
				
		// Verify report

		WebElement Report= driver.findElement(By.xpath("//div[@id='R8928923238287882']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
	}
			
//***************************************** Method to view NetControl transaction by Account number **********************
public void ViewNetControlTransaction_ByAccountNumber() throws InterruptedException
	{
		SEARCBY_AccountNumber();
						
		// Verify report
		
		WebElement Report= driver.findElement(By.xpath("//div[@id='R8928923238287882']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}

//***************************************** Method to view NetControl transaction by Invoice number **********************
public void ViewNetControlTransaction_ByInvoiceNumber() throws InterruptedException
	{
	
		SEARCBY_InvoiceNumber();
							
		// Verify report
		
		WebElement Report= driver.findElement(By.xpath("//div[@id='R8928923238287882']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
	}

//**************************************** Method to view NetControl transaction by Circuit number **********************
public void ViewNetControlTransaction_ByCircuitNumber() throws InterruptedException
	{
		SEARCBY_CircuitNumber();
										
			// Verify report

		WebElement Report= driver.findElement(By.xpath("//div[@id='R8928923238287882']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
			
//***************************************** Method to view NetControl transaction by Account UID **********************
  public void ViewNetControlTransaction_ByAccountUID() throws InterruptedException
	{
		SEARCBY_AccountUID();
										
			// Verify report
	
		WebElement Report= driver.findElement(By.xpath("//div[@id='R8928923238287882']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	    
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
			
//***************************************** Method to view NetControl transaction by Subscriber UID **********************
  public void ViewNetControlTransaction_BySubscriberUID() throws InterruptedException
	{
		SEARCBY_SubscriberUID();
											
			// Verify report
	
		WebElement Report= driver.findElement(By.xpath("//div[@id='R8928923238287882']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
	}
  
			
//***************************************** Method to view NetControl transaction by MPSA OTT **********************
public void ViewNetControlTransaction_By_MPSA_OTT() throws InterruptedException
	{
		SEARCBY_MPESA_OTT();
									
			// Verify report
	
		WebElement Report= driver.findElement(By.xpath("//div[@id='R8928923238287882']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
 }
