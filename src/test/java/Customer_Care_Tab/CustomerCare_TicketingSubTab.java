package Customer_Care_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CustomerCare_TicketingSubTab  extends common_methods.CommonMethods{
	
	public static String subject= "test subject";
	
	public static String Editsubject= "Edited test subject";
	
	public static String Comment= "test comment";
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		
	 Thread.sleep(1000);
	 driver.quit();
	 
	}

	
	@Test
	public void a_View_TicketingTab()
	{
		view_TicketingTab();
	}

	@Test
	public void b_Click_TicketingTab()
	{
		click_TicketingTab();
	}
	
	@Test
	public void c_ClickCreateButton() throws InterruptedException
	{
		Thread.sleep(300);
		click_create();
	}
	
	@Test
	public void d_CreateTicket() throws InterruptedException
	{
		Thread.sleep(800);
		Create_ticket();	
		
	}
	
	@Test
	public void e_VerifyCreatedTicket() throws InterruptedException
	{
		Thread.sleep(800);
		verify_Created_Ticket();
	}
	
	
	@Test
	public void f_EditTicket() throws InterruptedException
	{
		Thread.sleep(800);
		Edit_Ticket();
	}
	
	@Test
	public void g_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void h__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void i_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void j_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void k_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","tickets.csv"));
	}
	
	@Test
	public void l_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
		
	}
		
	@Test
	public void m_reset_test() throws InterruptedException
	{
		Thread.sleep(500);
		reset();
	}
	

	@Test
	public void n_flashBackTest() throws InterruptedException
	{
		Thread.sleep(500);
		flashback_filter();
	}
	
	@Test
	public void q_Click_Pending_Reminder() throws InterruptedException
	{
		Thread.sleep(500);
		click_Pending_Reminder();
	}
	
	@Test
	public void r_View_RemindersByAccount() throws InterruptedException
	{
		Thread.sleep(500);
		ReminderBy_Account();
	}
	
	@Test
	public void s_View_RemindersBySubscriber() throws InterruptedException
	{
		Thread.sleep(500);
		ReminderBy_Subscriber();
	}
	
	@Test
	public void t_View_RemindersByTickets() throws InterruptedException
	{
		Thread.sleep(500);
		ReminderBy_Tickets();
	}
	
	@Test
	public void u_ClickCancel_TOGoBack_ToTicketingPage() throws InterruptedException
	{
		click_cancel();
		
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view ticketing tab ******************************
public void view_TicketingTab()
{
	WebElement ticketing= driver.findElement(By.linkText("Ticketing"));
	
	Boolean Ticketing= ticketing.isDisplayed();
	
	Assert.assertTrue(Ticketing);
}

//********************************* Method to click ticketing tab ******************************
public void click_TicketingTab()
{
	WebElement Ticketing =driver.findElement(By.linkText("Ticketing"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Ticketing).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}	
//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	expvalue= "Open";
	col= "Status";
	
	view_filtered_report();

	// Verify report
	
	Thread.sleep(300);
	
	String verifyreport= driver.findElement(By.xpath("//div[@id='apexir_WORKSHEET']//div//td[8]")).getText();
	
	Thread.sleep(300);
	
	Assert.assertEquals(verifyreport, expvalue);
}


//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 

	save_report();

	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
	
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();		 

	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
	select_primary_report();
	
	Thread.sleep(300);
	
	try {
	
		WebElement primaryreport= driver.findElement(By.id("1201507579502749"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}catch(Exception e) 	{
	
		WebElement primaryreport= driver.findElement(By.id("1201507579502749"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}
}

//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
  {
	Reset();

	// Verify report

	WebElement primaryreport= driver.findElement(By.id("1201507579502749"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
}	


//******************************* Method to click create button *************************
public void click_create() throws InterruptedException
{
		
	WebElement create = driver.findElement(By.id("B1205630874502764"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(create).click().build().perform();	
	
	Thread.sleep(300);
}

//**************************** Method to create a ticket ********************************
public void Create_ticket() throws InterruptedException
{
	// Enter subject
	
	WebElement Subject= driver.findElement(By.id("P2_QN_SUBJECT"));
	
	Subject.clear();
	
	Subject.sendKeys(subject);
	
	// Enter Comment 
	
	WebElement comment= driver.findElement(By.id("P2_DETAIL"));
	
	comment.clear();
	
	comment.sendKeys(Comment);
	
	// Select Fault type
	
	Select fault= new Select(driver.findElement(By.id("P2_QN_FAULT_QNT_UID")));
	
	fault.selectByIndex(1);
	
	Thread.sleep(500);
	
	// Select reported fault code
	
	Select faultCode= new Select(driver.findElement(By.id("P2_QN_REPORTED_QNT_UID")));
	
	faultCode.selectByIndex(1);
	
	Thread.sleep(500);
	
	// Select Priority
	
	Select priority= new Select(driver.findElement(By.id("P2_QN_QNP_UID")));
	
	priority.selectByIndex(1);
	
	Thread.sleep(300);
	
	// Enter Reminder date
	
	WebElement calendar =driver.findElement(By.xpath("//img[@class='ui-datepicker-trigger']"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(calendar).click().build().perform();
	
	Thread.sleep(300);

	//Select Date
	
	WebElement date= driver.findElement(By.xpath("//a[contains(text(),'30')]"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(date).doubleClick().build().perform();
	
	Click_Create();
	
}

//***************************** Method to verify created ticket ***********************
public void verify_Created_Ticket() throws InterruptedException
{
	// Click on tickets tab on breadcrumbs
	
	WebElement tickets = driver.findElement(By.xpath("//ul[@class='echo-menu-breadcrumb ui-helper-clearfix']//a[contains(text(),'Tickets')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(tickets).click().build().perform();	
	
	Thread.sleep(500);
	
	String subject1= driver.findElement(By.xpath("//tr[@class='even']//td[16]")).getText();
	
	Assert.assertEquals(subject1, subject);
 }


//***************************** Method to click create ***********************************
public void Click_Create() throws InterruptedException
{
	// Click create

	WebElement create = driver.findElement(By.id("B1220104677510953"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(create).click().build().perform();	
	
	Thread.sleep(500);	
}

//*********************************** Method to edit ticket ************************
public void Edit_Ticket() throws InterruptedException
{
   // Click edit

	WebElement edit = driver.findElement(By.xpath("//tr[@class='even']//td//a//img"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(200);	

	// Enter new subject
	
	WebElement Subject= driver.findElement(By.id("P2_QN_SUBJECT"));
	
	Subject.clear();
	
	Subject.sendKeys(Editsubject);
	
	// Enter Comment 
	
	WebElement comment= driver.findElement(By.id("P2_DETAIL"));
	
	comment.clear();
	
	comment.sendKeys(Comment);
	
	
	// Click Apply changes
	
	WebElement apply = driver.findElement(By.id("B1219522299510953"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(apply).click().build().perform();	
	
	Thread.sleep(500);	
	
	// Verify Edit
	// Click on tickets tab on breadcrumbs
	
	WebElement tickets = driver.findElement(By.xpath("//ul[@class='echo-menu-breadcrumb ui-helper-clearfix']//a[contains(text(),'Tickets')]"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(tickets).click().build().perform();	
	
	Thread.sleep(500);
	
	String subject1= driver.findElement(By.xpath("//tr[@class='even']//td[16]")).getText();
	
	Assert.assertEquals(subject1, Editsubject);
 }

//****************************** Method to click pending reminder ****************************************
public void click_Pending_Reminder() throws InterruptedException
{
	WebElement click_pending = driver.findElement(By.id("B55772522265577589"));

	Actions act= new Actions(driver);
	
	act.moveToElement(click_pending).click().build().perform();	
	
	Thread.sleep(200);	
	
	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("B55791219849352676"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
	
}

// ************************************* Method to view Reminders by Account ******************************
public void ReminderBy_Account() throws InterruptedException
{
	WebElement ReminderByAccount = driver.findElement(By.id("P1_TYPE_0"));

	Actions act= new Actions(driver);
	
	act.moveToElement(ReminderByAccount).click().build().perform();	
	
	Thread.sleep(200);	
	
	
	// Verify report
	
	Boolean report= ReminderByAccount.isSelected();
	
	Assert.assertTrue(report);
	
	}

//************************************* Method to view Reminders by Subscriber ******************************
public void ReminderBy_Subscriber() throws InterruptedException
{
		WebElement ReminderBySubscriber = driver.findElement(By.id("P1_TYPE_1"));
	
		Actions act= new Actions(driver);
		
		act.moveToElement(ReminderBySubscriber).click().build().perform();	
		
		Thread.sleep(200);	
	
		// Verify report
		
		Boolean report= ReminderBySubscriber.isSelected();
		
		Assert.assertTrue(report);
	}


//************************************* Method to view Reminders by tickets ******************************
 public void ReminderBy_Tickets() throws InterruptedException
 {
	 WebElement ReminderByTickets = driver.findElement(By.id("P1_TYPE_2"));
	 
	 Actions act= new Actions(driver);
	 
	 act.moveToElement(ReminderByTickets).click().build().perform();	
	 
	 Thread.sleep(200);	
	
	// Verify report
	 
	 Boolean report= ReminderByTickets.isSelected();
	 
	 Assert.assertTrue(report);
	
 }
//*********************************** Method to click cancel ***********************************************
public void click_cancel() throws InterruptedException
{
		WebElement click_cancel = driver.findElement(By.id("B55791219849352676"));

		Actions act= new Actions(driver);
		
		act.moveToElement(click_cancel).click().build().perform();	
		
		Thread.sleep(200);	

	// Verify report
		
		WebElement primaryreport= driver.findElement(By.id("B1205630874502764"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);	
  }

}
