package Customer_Care_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AllAccountServicesTab_SubTab_CustomerCare  extends common_methods.CommonMethods{
	
   
		
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		Thread.sleep(1000);
	    driver.quit();			
	}

	@Test
	public void a_View_AllAccountServicesTab()
	{
		view_AllAccountServicesTab();
	}
	
	@Test
	public void b_Click_AllAccountServicesTab() throws InterruptedException
	{
		Thread.sleep(500);
		click_All_Account_Services_Tab();
	}
	
	@Test
	public void c_Search_By_PrimaryReport() throws InterruptedException
	{
		Thread.sleep(1000);
		search_by_PrimaryReports();
	}
	
	@Test
	public void d_Filter_report() throws InterruptedException
	{
		Thread.sleep(2000);
		View_filtered_report();
	}
	
	@Test
	public void e__Save_Report() throws InterruptedException
	{
		Thread.sleep(2000);
		Save_report();
	}
	
	@Test
	public void f_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(2000);
		search_by_private_report();
	}
			
	@Test
	public void g_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(1000);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","all_account_services.csv"));
	}
	
	@Test
	public void h_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(1000);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","all_account_services.htm"));
	}
	
	@Test
	public void i_emailReport() throws InterruptedException
	{
		Thread.sleep(1000);
		email_report();
	}
	
	
	@Test
	public void j_flashBackTest() throws InterruptedException
	{
		Thread.sleep(1500);
		flashback_filter();
	}
	
	
	@Test
	public void k_reset_test() throws InterruptedException
	{
		Thread.sleep(1500);
		reset();
	}
	
	
	@Test
	public void l_Search_By_AllInformation() throws InterruptedException
	{
		Thread.sleep(1000);
		Search_By_AllInfo();
	}
	
		
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to View all account Services tab ******************************
public void view_AllAccountServicesTab()
	{
		WebElement Aservices= driver.findElement(By.linkText("All Account Services"));
		
		Boolean services= Aservices.isDisplayed();
		
		Assert.assertTrue(services);
	}

//********************************* Method to click all account Services tab ******************************
public void click_All_Account_Services_Tab()
	{
		WebElement Aservices= driver.findElement(By.linkText("All Account Services"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(Aservices).click().build().perform();
		
		WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
		
		Boolean Verify= verify.isDisplayed();
		
		Assert.assertTrue(Verify);
	}	
	
//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
	{
		select_primary_report();
		
		Thread.sleep(2000);
		
		try {
					
			WebElement primaryreport= driver.findElement(By.id("32666720249290812"));
			
			Boolean report= primaryreport.isDisplayed();
			
			Assert.assertTrue(report);
			
		}catch(Exception e) 	{
		
			WebElement primaryreport= driver.findElement(By.id("32666720249290812"));
			
			Boolean report= primaryreport.isDisplayed();
			
			Assert.assertTrue(report);
			
		}
	}	
//*************************************** Method to search report by all information ************************
	public void Search_By_AllInfo() throws InterruptedException
		{
			Thread.sleep(300);
			
			Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
			
			dropdown.selectByVisibleText("2. All Information");
			
			Thread.sleep(2000);
			
			WebElement Report= driver.findElement(By.id("32666720249290812"));
			
			Boolean report= Report.isDisplayed();
			
			Assert.assertTrue(report);
		}
	
//*************************************** Method to search report by expiring in 30 days ************************
  public void Search_By_ExpiringIn30Days() throws InterruptedException
	{
		Thread.sleep(300);
		
		Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
		
		dropdown.selectByVisibleText("3. Expiring in the next 30 days");
		
		Thread.sleep(2000);
		
		String text1= driver.findElement(By.xpath("//table[@id='apexir_CONTROL_PANEL_COMPLETE']")).getText(); 
		
		System.out.println(text1);
		
		Boolean successmsg= text1.endsWith(" the next 30 days");
		
		Assert.assertTrue(successmsg);
	}

//*************************************** Method to search report by service not yet billed ************************
public void Search_By_Service_NotYet_Billed() throws InterruptedException
	{
		Thread.sleep(300);
		
		try {
		
			Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
			
			dropdown.selectByVisibleText("4. Services not yet Billed");
			
			Thread.sleep(2000);
			
			String text1= driver.findElement(By.xpath("//table[@id='apexir_CONTROL_PANEL_COMPLETE']")).getText(); 
			
			System.out.println(text1);
			
			Boolean successmsg= text1.endsWith(" UID is null");
			
			Assert.assertTrue(successmsg);
			
		}catch(Exception e) {
		
			Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
			
			dropdown.selectByVisibleText("4. Services not yet Billed");
			
			Thread.sleep(2000);
			
			String text1= driver.findElement(By.xpath("//table[@id='apexir_CONTROL_PANEL_COMPLETE']")).getText(); 
			
			System.out.println(text1);
			
			Boolean successmsg= text1.endsWith(" UID is null");
			
			Assert.assertTrue(successmsg);
			
		}
		
	}
		
//*************************************** Method to search report by UID only ************************
	public void Search_By_UID_Only() throws InterruptedException
		{
			Thread.sleep(300);
	
			Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
			
			dropdown.selectByVisibleText("5. UID Only");
			
			Thread.sleep(2000);
			
			WebElement Report= driver.findElement(By.id("apexir_BCD_CHILD_ACCT_UID"));
			
			Boolean report= Report.isDisplayed();
			
			Assert.assertTrue(report);
		}
		
//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
	{
			 col= "Statement Type";
			
			 expvalue= "C";
			 
			 view_filtered_report();

		// Verify report
				Thread.sleep(300);
			
				String verifyreport= driver.findElement(By.xpath("//tr[2]//td[14]")).getText();
				
				Thread.sleep(300);
				
				Assert.assertEquals(verifyreport, expvalue);
		}

//************************************* Method to save report *************************************
	public void Save_report() throws InterruptedException
		{
			report= "1. Test report"; 
			
			save_report();

					// Verify saved report
			
			Thread.sleep(500);
			
			Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
			
			dropdown.selectByVisibleText(report);
		}
	

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
 {
	Search_by_private_report();	
	
	// verify 

	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
	
 }

//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
	{
		Reset();

		// Verify report

		WebElement primaryreport= driver.findElement(By.id("32666720249290812"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}	
}

