package Customer_Care_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CustomerCare_TPHDSubTab  extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}

	
	@Test
	public void a_View_TPHDTab()
	{
		view_TPHDTab();
	}

	@Test
	public void b_Click_TPHDTab()
	{
		click_TPHDTab();
	}
	
	@Test
	public void f_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void g__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void h_Search_by_privateReport() throws InterruptedException
	{
		search_by_private_report();
	}
	
	@Test
	public void i_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
	@Test
	public void j_flashBackTest() throws InterruptedException
	{
		Thread.sleep(300);
		flashback_filter();
	}
	
	@Test
	public void k_reset_test() throws InterruptedException
	{
		Thread.sleep(300);
		reset();
	}
	
	@Test
	public void l_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","voucher_activate_requests.csv"));
	}
	
	@Test
	public void m_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","voucher_activate_requests.htm"));
	}
	
	@Test
	public void n_emailReport() throws InterruptedException
	{
		Thread.sleep(300);
		email_report();
	}
	
	@Test
	public void o_Click_View_ToGoTOVoucherActivateRequest() throws InterruptedException
	{
		Thread.sleep(300);
		Click_View_ToGoTOVoucherActivateRequest();
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view CIB tab ******************************
public void view_TPHDTab()
{
	WebElement tphd= driver.findElement(By.linkText("TPHD"));
	
	Boolean TPHD= tphd.isDisplayed();
	
	Assert.assertTrue(TPHD);
}


//********************************* Method to click CIB tab ******************************
public void click_TPHDTab()
{
	WebElement TPHD= driver.findElement(By.linkText("TPHD"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(TPHD).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}	

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
	{
		expvalue= "Processed";
		col= "Status";
	
		view_filtered_report();
		
		// Verify report
		
		Thread.sleep(300);
		
		String verifyreport= driver.findElement(By.xpath("//div[@id='apexir_WORKSHEET']//div//td[8]")).getText();
		
		Thread.sleep(300);
	
	// Remove filters
		
		WebElement removefilter= driver.findElement(By.xpath("//table[@id='apexir_CONTROL_PANEL_COMPLETE']//tbody//tr//td//a//img"));
		
		Actions act1= new Actions(driver);
		
		act1.moveToElement(removefilter).click().build().perform();
		
		Assert.assertEquals(verifyreport, expvalue);
	}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
	{
		report= "1. Test report"; 
		save_report();
				 	
		 // Verify saved report
		 	
		Thread.sleep(500);
		
		Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
		
		dropdown.selectByVisibleText(report);
		
	}
	
//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
	{
		Search_by_private_report();	
		
		// verify 

		Thread.sleep(300);
		
		String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
		
		System.out.println(text);
		
		Assert.assertEquals(text, verifyreport);
		
	}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
	{
		select_primary_report();
	
		Thread.sleep(300);
		
		WebElement primaryreport= driver.findElement(By.id("50338213272521601"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}


//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
	{
		Reset();
		 
	// Verify report

		WebElement primaryreport= driver.findElement(By.id("50338213272521601"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}	

	
//******************************** Method to click VIEW to go to voucher activate Request *****************
 public void Click_View_ToGoTOVoucherActivateRequest() throws InterruptedException
	{
		WebElement view= driver.findElement(By.xpath("//tr[2]//td[1]//a[1]"));

		Actions act= new Actions(driver);
		
		act.moveToElement(view).click().build().perform();
		
		Thread.sleep(300);
		
		WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
		
		Boolean Verify= verify.isDisplayed();
		
		Assert.assertTrue(Verify);
	}
 
}
