package Customer_Care_Tab;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common_methods.CommonMethods;

public class SearchOnCRM  extends CommonMethods{
	
		
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
	  driver.quit();
		
	}

	
	@Test
	public void a_viewCustomerCareTab() throws InterruptedException
	{
		Thread.sleep(400);
		View_CustomerCare();
	}
	
	@Test
	public void b_Click_CustomerCareTab() throws InterruptedException
	{
		Thread.sleep(200);
		click_CustomerCare();
	}
	
	@Test
	public void c_SearchByAccountNumber()
	{
		Search_by_AccountNumber();
	}
	
	@Test
	public void d_SearchByAccountName()
	{
		Search_by_AccountName();
	}
	
	@Test
	public void e_SearchByMSISDN() throws InterruptedException
	{
		Thread.sleep(200);
		Search_by_MSISDNnumberCRMTab();
	}
	
	@Test
	public void f_SearchBySubScribersFirst_Or_Surname()
	{
		Search_by_firstORLastName();
	}
	
	@Test
	public void g_SearchByAccountUID()
	{
		Search_by_AccountUID();
	}
	
	
//////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************* Method to view Customer care tab *****************************
public void View_CustomerCare()
	{
		WebElement CC=  driver.findElement(By.linkText("Customer Care"));

		Boolean cc_tab= CC.isDisplayed();
		
		Assert.assertTrue(cc_tab);
	}
	
//************************************ Method to click Customer care tab *************************
public void click_CustomerCare()
	{
		WebElement CC=  driver.findElement(By.linkText("Customer Care"));
	
		Actions act= new Actions(driver);
		
		act.moveToElement(CC).click().build().perform();
		
		WebElement searchbox= driver.findElement(By.id("P0_SEARCH_VALUE"));
			
		Boolean verify =searchbox.isDisplayed();
			
		Assert.assertTrue(verify);
	}
		
//*********************************** Method to search by account number on Customer Care tab ***********
public void Search_by_AccountNumber()
	{
			SearchCustomerByAccountNumber();
				
				// Add assertions when issue is resolved on unified
		
	
}
		
//*********************************** Method to search by account name on Customer Care tab ***********
public void Search_by_AccountName()
	{
		WebElement searchtype= driver.findElement(By.id("P0_SEARCH_TYPE"));
		
		Select dropdown = new Select(searchtype);
		
		dropdown.selectByVisibleText("Account Name");
		
		driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
		
		driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(AccName);
		
		try {
			driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		 	
			driver.findElement(By.id("P0_SEARCH")).submit();
		 	
		}catch(NoSuchElementException| StaleElementReferenceException e) {
		
			driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		 	
			driver.findElement(By.id("P0_SEARCH")).submit();
		 	
		}
		
		String verify = driver.findElement(By.xpath("//tr[@class='echo-report-row']//td[1]")).getText();
		
		System.out.println(verify);	
		
		Assert.assertEquals(verify, AccName);
		
	}
		
//************************************ Method to search by subscriber's first or last name *********************
public void Search_by_firstORLastName()
 {
		WebElement searchtype= driver.findElement(By.id("P0_SEARCH_TYPE"));
		
		Select dropdown = new Select(searchtype);
	    
		dropdown.selectByVisibleText("First or Surname (Subscriber)");
	 	
		driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
	 	
		driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(name);
	 	
		try {
	 		 	driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
	 		 	
	 			driver.findElement(By.id("P0_SEARCH")).submit();
	 			
	 	}catch(NoSuchElementException| StaleElementReferenceException e) {
	 	
	 		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
	 		
	 		driver.findElement(By.id("P0_SEARCH")).submit();
	  		
	 	}
	 	
		String verify = driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
	 	
		System.out.println(verify);	
	 	
		if(verify==name){
			
			System.out.println("name is " +verify);	
	 		
			Assert.assertEquals(verify, name);
	 		
		} else {
	 	
			String verify1 = driver.findElement(By.xpath("//tr[2]//td[3]")).getText();
	 		
			System.out.println("Surname is " +verify1);	
	 		
			Assert.assertEquals(verify1, name);
	 	  }
	 	}
	 
//*********************************** Method to search by account name on Customer Care tab ***********
 public void Search_by_AccountUID()
		{
			  WebElement searchtype= driver.findElement(By.id("P0_SEARCH_TYPE"));
			 
			  Select dropdown = new Select(searchtype);
			  
			  dropdown.selectByVisibleText("Account UID");
			  
			  driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
			  
			  driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(UID);
			  
			  try {
			 
				  driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			 		
				  driver.findElement(By.id("P0_SEARCH")).submit();
			 		
			  }catch(NoSuchElementException| StaleElementReferenceException e) {
			 
				  driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			 		
				  driver.findElement(By.id("P0_SEARCH")).submit();
			  		
			  }
			 	String verify = driver.findElement(By.xpath("//body[@class='echo-page-two-level-tabs-with-sidebar echo-layout-sidebar ui-layout-container']/form[@id='wwvFlowForm']/div[@id='echo-page-content']/div[@id='echo-page-content-inner']/div[@id='echo-box-body']/div[@id='R45615206611157019']/div[@id='R45615206611157019-tab-R13616970911260349']/div[@id='R13616970911260349']/div[1]")).getText();
			 
			 	System.out.println(verify);	
			 	
			 	Boolean uid= verify.endsWith(UID);
			 	
			 	Assert.assertTrue(uid);	 		
				
		}
}
