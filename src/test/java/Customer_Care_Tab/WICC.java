package Customer_Care_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WICC  extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}

	
	@Test
	public void a_View_WICC_Tab()
	{
		view_WICCTab();
	}

	@Test
	public void b_Click_WICC_Tab()
	{
		click_WICCTab();
	}
	
	@Test
	public void c_View_History_OF_SpecifiedMSISDN() throws InterruptedException
	{
		view_history();
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view WICC tab ******************************
public void view_WICCTab()
{
	WebElement WICC= driver.findElement(By.linkText("Neon"));
	
	Boolean wicc= WICC.isDisplayed();
	
	Assert.assertTrue(wicc);
}

//********************************* Method to click WICC tab ******************************
public void click_WICCTab()
{
	WebElement WICC= driver.findElement(By.linkText("WICC"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(WICC).click().build().perform();

	//Verify report
	
	WebElement verify= driver.findElement(By.id("P122_MSISDN"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}	

//************************************* Method to view history of specified MSISDN *****************************
public void view_history() throws InterruptedException
{
	// Enter MSISDN
	
	WebElement MSISDN= driver.findElement(By.id("P122_MSISDN"));
	
	MSISDN.clear();
	
	MSISDN.sendKeys(msisdn);
	
	Thread.sleep(300);
	
	//Click go
	
	WebElement go= driver.findElement(By.id("P122_GO"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();
	
	Thread.sleep(1000);
	
	//Verify report
	
	WebElement verify= driver.findElement(By.id(" add locator "));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
 }

}
