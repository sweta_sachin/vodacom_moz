package Customer_Care_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class OTA_Config_History_Tab extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}

	
	@Test
	public void a_View_OTA_Config_Tab()
	{
		view_OTA_ConfigTab();
	}

	@Test
	public void b_Click_OTA_Config_Tab()
	{
		click_OTA_ConfigTab();
	}
	
	@Test
	public void c_View_History_on_OTA_Config() throws InterruptedException
	{
		view_history();
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view OTA_Config tab ******************************
public void view_OTA_ConfigTab()
{
	WebElement ota= driver.findElement(By.linkText("OTA Configuration History"));
	
	Boolean OTA= ota.isDisplayed();
	
	Assert.assertTrue(OTA);
}

//********************************* Method to click OTA_Config tab ******************************
public void click_OTA_ConfigTab()
{
	WebElement OTA= driver.findElement(By.linkText("OTA Configuration History"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(OTA).click().build().perform();

//Verify report

	WebElement verify= driver.findElement(By.xpath("//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}	

//************************************* Method to view history on OTA config tab *****************************
public void view_history() throws InterruptedException
{
	Thread.sleep(300);

	//Verify report

	WebElement verify= driver.findElement(By.xpath("//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
	
  }

}
