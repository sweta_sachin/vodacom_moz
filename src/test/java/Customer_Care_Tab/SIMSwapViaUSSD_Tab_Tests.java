package Customer_Care_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SIMSwapViaUSSD_Tab_Tests extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}

	
	@Test
	public void a_View_SIMSwapViaUSSD_Tab()
	{
		view_SIMSwapViaUSSDTab();
	}

	@Test
	public void b_Click_SIMSwapViaUSSDTab()
	{
		click_SIMSwapViaUSSDTab();
	}
	

	@Test
	public void c__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	
	@Test
	public void d_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
	@Test
	public void g_flashBackTest() throws InterruptedException
	{
		Thread.sleep(300);
		flashback_filter();
	}
	
	@Test
	public void h_reset_test() throws InterruptedException
	{
		Thread.sleep(300);
		reset();
	}
	
	@Test
	public void i_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","sim_swap_requests_via_ussd.csv"));
	}
	
	@Test
	public void j_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","sim_swap_requests_via_ussd.htm"));
	}
	
	@Test
	public void k_emailReport() throws InterruptedException
	{
		Thread.sleep(300);
		email_report();
	}
	
				
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view SIM Swap Via USSD tab ******************************
public void view_SIMSwapViaUSSDTab()
	{
		WebElement sim= driver.findElement(By.linkText("SIM Swap via USSD"));
		
		Boolean SIM= sim.isDisplayed();
		
		Assert.assertTrue(SIM);
}

//********************************* Method to click SIM Swap Via USSD tab ******************************
public void click_SIMSwapViaUSSDTab()
	{
		WebElement sim= driver.findElement(By.linkText("SIM Swap via USSD"));

		Actions act= new Actions(driver);
		
		act.moveToElement(sim).click().build().perform();
		
		WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
		
		Boolean Verify= verify.isDisplayed();
		
		Assert.assertTrue(Verify);
}	

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
	{			
		expvalue= "HLRD";
		col= "Queue";
	
		view_filtered_report();
		
		// Verify report
		
		Thread.sleep(300);
		
		String verifyreport= driver.findElement(By.xpath("//div[@id='apexir_DATA_PANEL']//tr//tr[2]//td[1]")).getText();
		
		Thread.sleep(300);
		 
		 // Remove filters
		
		WebElement removefilter= driver.findElement(By.xpath("//table[@id='apexir_CONTROL_PANEL_COMPLETE']//tbody//tr//td//a//img"));
		
		Actions act1= new Actions(driver);
		
		act1.moveToElement(removefilter).click().build().perform();
		
		Assert.assertEquals(verifyreport, expvalue);
	}


//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
	{	
		report= "2. Test report"; 

		save_report();
		
		 // Verify saved report
		
		Thread.sleep(500);
		
		Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
		
		dropdown.selectByVisibleText(report);
		
	}
	
//**************************************** Method to search by private report ****************************

public void search_by_private_report() throws InterruptedException

{
	Search_by_private_report();	
	
	// verify 

	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);

	// Remove filters
	
	WebElement removefilter= driver.findElement(By.xpath("//table[@id='apexir_CONTROL_PANEL_COMPLETE']//tbody//tr//td//a//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(removefilter).click().build().perform();
	
	Assert.assertEquals(text, verifyreport);
		
	}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
	{
		select_primary_report();

		Thread.sleep(500);
		
		WebElement primaryreport= driver.findElement(By.id("15428519876724278"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}

//************************ Method to test reset button  *******************
public void reset() throws InterruptedException
	{
		Reset();
		 	
		// Verify report

		WebElement primaryreport= driver.findElement(By.id("15428519876724278"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}	

}
