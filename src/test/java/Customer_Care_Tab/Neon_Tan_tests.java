package Customer_Care_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Neon_Tan_tests extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}

	
	@Test
	public void a_View_NeonTab()
	{
		view_NeonTab();
	}

	@Test
	public void b_Click_NeonTab()
	{
		click_NeonTab();
	}
	
	
	@Test
	public void c_ViewNeonTransaction_By_MSISDN() throws InterruptedException
	{
		ViewNeonTransaction_ByMSISDN();
	}
	
		
	@Test
	public void e_ViewNeonTransaction_By_IMSI() throws InterruptedException
	{
		ViewNeonTransaction_ByIMSI();
	}
	
	
		
	@Test
	public void g_ViewNeonTransaction_By_ICCID() throws InterruptedException
	{
		ViewNeonTransaction_ByICCID();
	}
	

	@Test
	public void i_ViewNeonTransaction_By_AccountNumber() throws InterruptedException
	{
		ViewNeonTransaction_ByAccountNumber();
	}
	
		
	@Test
	public void k_ViewNeonTransaction_By_InvoiceNumber() throws InterruptedException
	{
		ViewNeonTransaction_ByInvoiceNumber();
	}
	
	
	@Test
	public void m_ViewNeonTransaction_By_CircuitNumber() throws InterruptedException
	{
		ViewNeonTransaction_ByCircuitNumber();
	}
	
		
	@Test
	public void o_ViewNeonTransaction_By_AccountUID() throws InterruptedException
	{
		ViewNeonTransaction_ByAccountUID();
	}
	
	@Test
	public void q_ViewNeonTransaction_By_SubscriberUID() throws InterruptedException
	{
		ViewNeonTransaction_BySubscriberUID();
	}
	
	@Test
	public void s_ViewNeonTransaction_By_MESAOTT() throws InterruptedException
	{
		ViewNeonTransaction_By_MPSA_OTT();
	}
	
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Neon tab ******************************
public void view_NeonTab()
{
	WebElement neon= driver.findElement(By.linkText("Neon"));
	
	Boolean Neon= neon.isDisplayed();
	
	Assert.assertTrue(Neon);
}

//********************************* Method to click Neon tab ******************************
public void click_NeonTab()
{
	WebElement Neon= driver.findElement(By.linkText("Neon"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Neon).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("P0_SEARCH_VALUE"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}	

//***************************************** Method to view Neon transaction by MSISDN number **********************
public void ViewNeonTransaction_ByMSISDN() throws InterruptedException
	{
		SEARCBY_MSISDN();
		
	// Verify report
	
		WebElement Report= driver.findElement(By.xpath("//div[@id='R66343914342571692']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
	}

	
//***************************************** Method to view Neon transaction by IMSI number **********************
 public void ViewNeonTransaction_ByIMSI() throws InterruptedException
	{
		SEARCBY_IMSI();

		WebElement Report= driver.findElement(By.xpath("//div[@id='R66343914342571692']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
	}
		
//***************************************** Method to view Neon transaction by ICCID number **********************
public void ViewNeonTransaction_ByICCID() throws InterruptedException
	{
		SEARCBY_ICCID();

		// Verify report
		
		WebElement Report= driver.findElement(By.xpath("//div[@id='R66343914342571692']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
			
		Boolean report= Report.isDisplayed();
			
		Assert.assertTrue(report);
			
	}
			
//***************************************** Method to view Neon transaction by Account number **********************
public void ViewNeonTransaction_ByAccountNumber() throws InterruptedException
	{
		SEARCBY_AccountNumber();
						
		// Verify report
		
		WebElement Report= driver.findElement(By.xpath("//div[@id='R66343914342571692']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	     	
		Boolean report= Report.isDisplayed();
			
		Assert.assertTrue(report);
	}

//***************************************** Method to view Neon transaction by Invoice number **********************
public void ViewNeonTransaction_ByInvoiceNumber() throws InterruptedException
	{
		SEARCBY_InvoiceNumber();
								
		// Verify report

		WebElement Report= driver.findElement(By.xpath("//div[@id='R66343914342571692']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
			
//***************************************** Method to view Neon transaction by Circuit number **********************
public void ViewNeonTransaction_ByCircuitNumber() throws InterruptedException
	{
		SEARCBY_CircuitNumber();
										
		// Verify report
		
		WebElement Report= driver.findElement(By.xpath("//div[@id='R66343914342571692']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
       	
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
			
//***************************************** Method to view Neon transaction by Account UID **********************
  public void ViewNeonTransaction_ByAccountUID() throws InterruptedException
	{
		SEARCBY_AccountUID();
									
		// Verify report
	
		WebElement Report= driver.findElement(By.xpath("//div[@id='R66343914342571692']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	    
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
			
//***************************************** Method to view Neon transaction by Subscriber UID **********************
 public void ViewNeonTransaction_BySubscriberUID() throws InterruptedException
	{
			SEARCBY_SubscriberUID();
											
			// Verify report
	
			WebElement Report= driver.findElement(By.xpath("//div[@id='R66343914342571692']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
			
			Boolean report= Report.isDisplayed();
			
			Assert.assertTrue(report);
		}
 
			
//***************************************** Method to view Neon transaction by MPSA OTT **********************
public void ViewNeonTransaction_By_MPSA_OTT() throws InterruptedException
	{
		SEARCBY_MPESA_OTT();
										
		// Verify report

		WebElement Report= driver.findElement(By.xpath("//div[@id='R66343914342571692']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
}
