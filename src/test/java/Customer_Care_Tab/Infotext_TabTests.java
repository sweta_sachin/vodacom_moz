package Customer_Care_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Infotext_TabTests extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}

	
	@Test
	public void a_View_InfoTextTab()
	{
		view_InfoTextTab();
	}

	@Test
	public void b_Click_InfoTextTab()
	{
		click_InfoTextTab();
	}
	
	
	@Test
	public void c_SearchReport_By_MSISDN() throws InterruptedException
	{
		Search_ByMSISDN();
	}
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	
	@Test
	public void e_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void f_Search_by_privateReport() throws InterruptedException
	{
		search_by_private_report();
	}
	
	@Test
	public void g_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
	@Test
	public void h_flashBackTest() throws InterruptedException
	{
		Thread.sleep(300);
		flashback_filter();
	}
	
	@Test
	public void i_reset_test() throws InterruptedException
	{
		Thread.sleep(300);
		reset();
	}
	
	@Test
	public void j_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","infotext_audit.csv"));
	}
	
	@Test
	public void k_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","infotext_audit.htm"));
	}
	
	@Test
	public void l_emailReport() throws InterruptedException
	{
		Thread.sleep(300);
		email_report();
	}
	
	@Test
	public void m_SearchReport_By_IMSI() throws InterruptedException
	{
		Thread.sleep(300);
		Search_ByIMSI();
	}

	@Test
	public void n_SearchReport_By_ICCID() throws InterruptedException
	{
		Thread.sleep(300);
		Search_ByICCID();
	}
	
	@Test
	public void o_SearchReport_By_AccountNumber() throws InterruptedException
	{
		Thread.sleep(300);
		Search_ByAccountNumber();
	}
	
	@Test
	public void p_SearchReport_By_InvoiceNumber() throws InterruptedException
	{
		Thread.sleep(300);
		Search_ByInvoiceNumber();
	}
	
	@Test
	public void q_SearchReport_By_CircuitNumber() throws InterruptedException
	{
		Thread.sleep(300);
		Search_ByCircuitNumber();
	}
	
	@Test
	public void r_SearchReport_By_AccountUID() throws InterruptedException
	{
		Thread.sleep(300);
		Search_ByAccountUID();
	}
	
	@Test
	public void s_SearchReport_By_SubscriberUID() throws InterruptedException
	{
		Thread.sleep(300);
		Search_BySubscriberUID();
	}
	
	@Test
	public void t_SearchReport_By_MesaOTT() throws InterruptedException
	{
		Thread.sleep(300);
		Search_By_MPSA_OTT();
	}
	
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view InfoText tab ******************************
public void view_InfoTextTab()
{
	WebElement tphd= driver.findElement(By.linkText("Infotext"));
	
	Boolean TPHD= tphd.isDisplayed();
	
	Assert.assertTrue(TPHD);
}

//********************************* Method to click InfoText tab ******************************
public void click_InfoTextTab()
{
	WebElement TPHD= driver.findElement(By.linkText("Infotext"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(TPHD).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}	


//************************************** Method to apply filter and view filtered report **********************
 public void View_filtered_report() throws InterruptedException
	{

	  col= "Channel";
	  expvalue= "Apex";

	  view_filtered_report();
				
		// Verify report
		
	  Thread.sleep(300);
		
	  String verifyreport= driver.findElement(By.xpath("//div[@id='apexir_WORKSHEET']//div//td[8]")).getText();
		
	  Thread.sleep(300);
		
	  // Remove filters
		
	  WebElement removefilter= driver.findElement(By.xpath("//table[@id='apexir_CONTROL_PANEL_COMPLETE']//tbody//tr//td//a//img"));
		
	  Actions act1= new Actions(driver);
		
	  act1.moveToElement(removefilter).click().build().perform();
		
	  Assert.assertEquals(verifyreport, expvalue);
	}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
	{
		report= "2. Test report"; 
		
		save_report();
		
		 // Verify saved report
		
		Thread.sleep(500);
		
		Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
		
		dropdown.selectByVisibleText(report);
		
	}
	
//**************************************** Method to search by private report ****************************
 public void search_by_private_report() throws InterruptedException
	{
	
	 Search_by_private_report();		

	 	// verify 
	
	 Thread.sleep(300);
	
	 String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	 System.out.println(text);

	 // Remove filters
	
	 WebElement removefilter= driver.findElement(By.xpath("//table[@id='apexir_CONTROL_PANEL_COMPLETE']//tbody//tr//td//a//img"));
	
	 Actions act1= new Actions(driver);
	
	 act1.moveToElement(removefilter).click().build().perform();
	
	 Assert.assertEquals(text, verifyreport);
	
	}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
	{
		select_primary_report();
		
		Thread.sleep(500);
		
		WebElement primaryreport= driver.findElement(By.id("43023706518229548"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}


//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
	{
		Reset();
		
		// Verify report
		
		WebElement primaryreport= driver.findElement(By.id("43023706518229548"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}	

//***************************************** Method to search a report by MSISDN number **********************
public void Search_ByMSISDN() throws InterruptedException
	{
		//Select search option

	Select option= new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	
	option.selectByVisibleText("MSISDN");
	
	//Enter MSISDN 
	
	WebElement MSISDN= driver.findElement(By.id("P0_SEARCH_VALUE"));
	
	MSISDN.clear();
	
	MSISDN.sendKeys(msisdn);
		
	//Enter Start date
	
	WebElement date= driver.findElement(By.id("P0_START_DATE"));
	
	date.clear();
	
	date.sendKeys(DATE);
					
	//click Go
	
	WebElement go = driver.findElement(By.id("P0_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(600);
		
	
	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("43023706518229548"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
	
	}
	
//********************************* Method to create infotext request *******************************
public void create_Infotext_Request() throws InterruptedException
{
	//Click Request
	
	WebElement request = driver.findElement(By.id("B82167624395050867"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(request).click().build().perform();	
	
	Thread.sleep(600);

	//Search by MSISDN 
	
	Select option= new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	
	option.selectByVisibleText("MSISDN");
		
	//Enter MSISDN 
	
	WebElement MSISDN= driver.findElement(By.id("P0_SEARCH_VALUE"));
	
	MSISDN.clear();
	
	MSISDN.sendKeys(msisdn);
	
	//click Go
	
	WebElement go = driver.findElement(By.id("P0_SEARCH"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();	
	
	Thread.sleep(600);
	
	//Select option to load
	
	WebElement option1 = driver.findElement(By.xpath("//option[contains(text(),'My Number One')]"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(option1).click().build().perform();	
	
	Thread.sleep(600);
		 
	// Click load parameters
	
	WebElement load = driver.findElement(By.id("B82172709017311332"));
	
	Actions action= new Actions(driver);
	
	action.moveToElement(load).click().build().perform();	
	
	Thread.sleep(600);
    
}

//***************************************** Method to search a report by IMSI number **********************
public void Search_ByIMSI() throws InterruptedException
{
	SEARCBY_IMSI();
		
	// Verify report

	WebElement Report= driver.findElement(By.id("43023706518229548"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	
}
		
//***************************************** Method to search a report by ICCID number **********************
public void Search_ByICCID() throws InterruptedException
{
	SEARCBY_ICCID();
			
	// Verify report

	WebElement Report= driver.findElement(By.id("43023706518229548"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	
}
		
//***************************************** Method to search a report by Account number **********************
public void Search_ByAccountNumber() throws InterruptedException
	{
		SEARCBY_AccountNumber();
	
		// Verify report

		WebElement Report= driver.findElement(By.id("43023706518229548"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
		
//***************************************** Method to search a report by Invoice number **********************
public void Search_ByInvoiceNumber() throws InterruptedException
{
	SEARCBY_InvoiceNumber();
							
	// Verify report

	WebElement Report= driver.findElement(By.id("43023706518229548"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	
}
		
//***************************************** Method to search a report by Circuit number **********************
public void Search_ByCircuitNumber() throws InterruptedException
 {
 	SEARCBY_CircuitNumber();
							
    	// Verify report
	
 	WebElement Report= driver.findElement(By.id("43023706518229548"));
	
 	Boolean report= Report.isDisplayed();
	
 	Assert.assertTrue(report);
	
 }
		
//***************************************** Method to search a report by Account UID **********************
public void Search_ByAccountUID() throws InterruptedException
 {
	SEARCBY_AccountUID();
									
		// Verify report
		
	WebElement Report= driver.findElement(By.id("43023706518229548"));
	
	Boolean report= Report.isDisplayed();
	
	Assert.assertTrue(report);
	
 }
		
//***************************************** Method to search a report by Subscriber UID **********************
public void Search_BySubscriberUID() throws InterruptedException
	{
		SEARCBY_SubscriberUID();
										
		// Verify report
		
		WebElement Report= driver.findElement(By.id("43023706518229548"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
		
//***************************************** Method to search a report by MPSA OTT **********************
public void Search_By_MPSA_OTT() throws InterruptedException
	{
		SEARCBY_MPESA_OTT();
									
		// Verify report

		WebElement Report= driver.findElement(By.id("43023706518229548"));
		
		Boolean report= Report.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
}
