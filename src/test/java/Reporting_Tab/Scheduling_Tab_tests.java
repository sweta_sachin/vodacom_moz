package Reporting_Tab;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Scheduling_Tab_tests extends common_methods.CommonMethods {
	
	
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_ReportingTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		
	 Thread.sleep(1000);
	 driver.quit();
	 
	}

	@Test
	public void a_View_Scheduling_Tab()
	{
		view_SchedulingTab();
	}

	@Test
	public void b_Click_SchedulingTab() throws InterruptedException
	{
		Thread.sleep(500);
		click_SchedulingTab();
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void g_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","scheduled_reports.csv"));
	}
	
	@Test
	public void h_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","scheduled_reports.htm"));
	}
	
	@Test
	public void i_emailReport() throws InterruptedException
	{
		Thread.sleep(300);
		email_report();
	}
	
	@Test
	public void y_flashBackTest() throws InterruptedException
	{
		Thread.sleep(2500);
		flashback_filter();
	}
	
	@Test
	public void x_reset_test() throws InterruptedException
	{
		Thread.sleep(3500);
		reset();
	} 
	
	@Test
	public void k_CreateSchedule() throws InterruptedException
	{
		Thread.sleep(500);
		Create_Schedule();
	}
	
	@Test
	public void l_Verify_CreatedSchedule() throws InterruptedException
	{
		Thread.sleep(500);
		Verify_created();
	}

    @Test
	public void m_Edit_Schedule() throws InterruptedException
	{
		Thread.sleep(500);
		EditSchedule();
	}
	
	@Test
	public void n_Verify_EditedSchedule() throws InterruptedException
	{
		Thread.sleep(800);
		Verify_edited();
	}
		
	@Test
	public void o_Delete_Schedule() throws InterruptedException
	{
		Thread.sleep(500);
		Delete_Schedule();
	}
	
	@Test
	public void p_Verify_DeleteSchedule() throws InterruptedException
	{
		Thread.sleep(500);
		Verify_Deletion();
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Scheduling tab ******************************
public void view_SchedulingTab()
{
	WebElement Scheduling= driver.findElement(By.linkText("Scheduling"));
	
	Boolean scheduling= Scheduling.isDisplayed();
	
	Assert.assertTrue(scheduling);
}


//********************************* Method to click Scheduling tab ******************************
public void click_SchedulingTab() throws InterruptedException
{
	WebElement Scheduling= driver.findElement(By.linkText("Scheduling"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Scheduling).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
    
}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	col= "Frequency";
	expvalue= "Daily";

	view_filtered_report();

	// Verify report
	
	Thread.sleep(600);
	
	String verifyreport= driver.findElement(By.xpath("//tr[2]//td[4]")).getText();
		
	Thread.sleep(300);
	
	Assert.assertEquals(verifyreport, expvalue);
}


//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 
	
	save_report();

	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();
	
	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
	select_primary_report();
	
	Thread.sleep(500);
	
	try {
		
		WebElement primaryreport= driver.findElement(By.id("35807524383751531"));
		
		Boolean report= primaryreport.isDisplayed();

		Assert.assertTrue(report);
	}catch(Exception e) 	{

		WebElement primaryreport= driver.findElement(By.id("35807524383751531"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}

}
//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
	{
		Reset();
		
		Thread.sleep(500);

	// Verify report
		
		WebElement primaryreport= driver.findElement(By.id("35807524383751531"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
}	

//******************************************* Method to create Schedule *****************************************
public void Create_Schedule() throws InterruptedException
{
	//Click Create
		
	WebElement CLICK= driver.findElement(By.id("B35809012249761515"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(CLICK).click().build().perform();
	
	Thread.sleep(300);
		
	//Select report
	
	Select report= new Select(driver.findElement(By.id("P24_REPS_REP_UID")));
	
	report.selectByIndex(1);
		
	//Enter description
	
	WebElement des = driver.findElement(By.id("P24_REPS_DESCRIPTION"));
	
	des.clear();
	
	des.sendKeys(Name);
	
	Thread.sleep(300);
	
	//Select frequency
	
	Select frequency= new Select(driver.findElement(By.id("P24_REPS_F_UID")));
	
	frequency.selectByIndex(1);
		
	//Enter hour
	
	WebElement hour = driver.findElement(By.id("P24_REPS_SCHEDULED_HOUR"));
	
	hour.clear();
	
	hour.sendKeys(Time);
	
	Thread.sleep(300);
		
	//Select schedule date from calendar 
	
	//click Calendar
	
	WebElement cal= driver.findElement(By.xpath("//img[@class='ui-datepicker-trigger']"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(cal).click().build().perform();
	
	Thread.sleep(500);
	
	//Select date from calendar
	
	WebElement date= driver.findElement(By.xpath("/html[1]/body[1]/div[4]/table[1]/tbody[1]/tr[2]/td[7]/a[1]"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(date).click().build().perform();
	
	Thread.sleep(800);
	
	// Select date into query date
	
	//click Calendar
	
	WebElement qDate= driver.findElement(By.xpath("//table[@id='f01_559_holder']//tbody//tr//td//a//img"));
	
	Actions act3= new Actions(driver);
	
	act3.moveToElement(qDate).click().build().perform();
	
	Thread.sleep(1000);
	
	//Select date from calendar
	
	String MainWindow=driver.getWindowHandle();		
    
	// To handle all new opened window.				
	
	Set<String> s1=driver.getWindowHandles();		
	
	Iterator<String> i1=s1.iterator();		
	        		
	while(i1.hasNext()) {
		
		String ChildWindow=i1.next();	
			            		
	    if(!MainWindow.equalsIgnoreCase(ChildWindow))   {
	    	                
	        // Switching to Child window
	       
	    	driver.switchTo().window(ChildWindow);	 
	        
	    	Thread.sleep(500);
	        
	    	WebElement QDate= driver.findElement(By.xpath("//a[contains(text(),'13')]"));	
	        
	    	Actions act00= new Actions(driver);
	        
	    	act00.moveToElement(QDate).click().build().perform();
	                                
				  }		
	        }		
	       
	// Switching to Parent window i.e Main Window.
	
	driver.switchTo().window(MainWindow);				
	    
	//click submit
	
	WebElement Create= driver.findElement(By.id("B36193129293409262"));
	
	Actions act0= new Actions(driver);
	
	act0.moveToElement(Create).click().build().perform();
	
	Thread.sleep(1300);
	
	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
	
	}
//*********************************** Method to verify created Schedule************************************
public void Verify_created() throws InterruptedException
{
		Reset();
		
		Thread.sleep(500);
		
	//enter name to search
		
		WebElement search= driver.findElement(By.id("apexir_SEARCH"));
		
		search.clear();
		
		search.sendKeys(Name);
		
		Thread.sleep(200);
		
	//Click Go
		
		WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
		
		Actions act2= new Actions(driver);
		
		act2.moveToElement(go).click().build().perform();
		
		Thread.sleep(400);
		
	//verify search
		
		String text1= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
		
		Assert.assertEquals(text1, Name);
}

//************************************ Method to edit Schedule **************************************
public void EditSchedule() throws InterruptedException
{
	//Click edit
		
	WebElement edit= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/a[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();
	
	Thread.sleep(300);
			
	//Change description
	
	WebElement name= driver.findElement(By.id("P24_REPS_DESCRIPTION"));
	
	name.clear();
	
	name.sendKeys(Name1);
		
	// Select date into query date
	
	//click Calendar
	
	WebElement qDate= driver.findElement(By.xpath("//table[@id='f01_559_holder']//tbody//tr//td//a//img"));
	
	Actions act3= new Actions(driver);
	
	act3.moveToElement(qDate).click().build().perform();
	
	Thread.sleep(1000);
		
	//Select date from calendar
	
	String MainWindow=driver.getWindowHandle();		
		
	// To handle all new opened window.				
	
	Set<String> s1=driver.getWindowHandles();		
	
	Iterator<String> i1=s1.iterator();		
	
	while(i1.hasNext()) {
		
	  String ChildWindow=i1.next();		
		 		
	  if(!MainWindow.equalsIgnoreCase(ChildWindow)) {    		
		                 
		  // Switching to Child window
		  
		  driver.switchTo().window(ChildWindow);	 
		  
		  Thread.sleep(500);
		  
		  WebElement QDate= driver.findElement(By.xpath("//a[contains(text(),'13')]"));	
		  
		  Actions act00= new Actions(driver);
		  
		  act00.moveToElement(QDate).click().build().perform();		                    		                                        
			
	  }		
		
	}		
	
	// Switching to Parent window i.e Main Window.
	
	driver.switchTo().window(MainWindow);	
		
	//Click submit
	
	WebElement submit= driver.findElement(By.id("B36193129293409262"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(submit).click().build().perform();
	
	Thread.sleep(1300);
		
	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
	
 }

//******************************************** Method to verify edited Schedule **********************************
public void Verify_edited() throws InterruptedException
{
	//reset all filters

	Reset();
	
	Thread.sleep(500);
	 	
	//enter name to search
	
	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(Name1);
	
	Thread.sleep(500);
		
	//Click Go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
		
	//verify search
	
	String text1= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
	
	Assert.assertEquals(text1, Name1);
}

//************************************************* Method to delete Schedule *******************************
public void Delete_Schedule() throws InterruptedException
{
	//Click edit
		
	WebElement edit= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/a[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();
	
	Thread.sleep(300);
	
	//Click delete
	
	WebElement delete= driver.findElement(By.id("B36220009413018411"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(delete).click().build().perform();
	
	Thread.sleep(300);
	
	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
	
}

//*********************************************** Verify Deletion *******************************
public void Verify_Deletion() throws InterruptedException
{
	
	//reset all filters

	Reset();
 	
	Thread.sleep(500);
	 	
	//enter name to search
	
	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(Name1);
	
	Thread.sleep(500);
		
	//Click Go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
			
	//verify search
	
	Boolean text1= driver.findElement(By.xpath("//span[@id='apexir_NO_DATA_FOUND_MSG']")).isDisplayed();
	
	Assert.assertTrue(text1);
  
	}

}
