package Reporting_Tab;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Configuration_Tab_tests extends common_methods.CommonMethods {
	
	
		
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_ReportingTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		
	 Thread.sleep(1000);
	 driver.quit();
	 
	}

	@Test
	public void a_View_Configuration_Tab()
	{
		view_ConfigurationTab();
	}

	@Test
	public void b_Click_ConfigurationTab() throws InterruptedException
	{
		Thread.sleep(500);
		click_ConfigurationTab();
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void g_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","reporting_config.csv"));
	}
	
	
	@Test
	public void i_flashBackTest() throws InterruptedException
	{
		Thread.sleep(500);
		flashback_filter();
	}
	
	@Test
	public void j_reset_test() throws InterruptedException
	{
		Thread.sleep(500);
		reset();
	} 
	
	@Test
	public void k_CreateReporting_Config() throws InterruptedException
	{
		Thread.sleep(500);
		Create_reporting_config();
	}
	
	@Test
	public void l_Verify_CreatedReportConfig() throws InterruptedException
	{
		Thread.sleep(500);
		Verify_created();
	}

	@Test
	public void m_Edit_ReportingConfig() throws InterruptedException
	{
		Thread.sleep(500);
		EditConfig();
	}
	
	@Test
	public void n_Verify_EditedConfig() throws InterruptedException
	{
		Thread.sleep(800);
		Verify_edited();
	}
	
	@Test
	public void o_Delete_reportingConfig() throws InterruptedException
	{
		Thread.sleep(1500);
		Delete_config();
	}
	
	@Test
	public void p_Verify_DeleteConfig() throws InterruptedException
	{
		Thread.sleep(1500);
		Verify_Deletion();
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Configuration tab ******************************
public void view_ConfigurationTab()
{
	WebElement Configuration= driver.findElement(By.linkText("Configuration"));
	
	Boolean configuration= Configuration.isDisplayed();
	
	Assert.assertTrue(configuration);
}


//********************************* Method to click Configuration tab ******************************
public void click_ConfigurationTab() throws InterruptedException
{
	WebElement Configuration= driver.findElement(By.linkText("Configuration"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Configuration).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
    
}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	 col= "Status";
	 expvalue= "Enabled";	

	 view_filtered_report();
	
	// Verify report
	
	 Thread.sleep(600);
	
	 String verifyreport= driver.findElement(By.xpath("//tr[2]//td[7]")).getText();
	
	 Thread.sleep(300);
	
	 Assert.assertEquals(verifyreport, expvalue);
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 
	
	save_report();
	
	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
}


//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();
	
	// verify 

	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
	select_primary_report();
	
	Thread.sleep(300);
	
	try {

		WebElement primaryreport= driver.findElement(By.id("13939702403200628"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	
	}catch(Exception e) 	{

		WebElement primaryreport= driver.findElement(By.id("13939702403200628"));
	
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}

}
//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
	{
		Reset();

	// Verify report
		
		WebElement primaryreport= driver.findElement(By.id("13939702403200628"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
}	

//******************************************* Method to create reporting Config *****************************************
public void Create_reporting_config() throws InterruptedException
{
	//Click Create
		
	WebElement CLICK= driver.findElement(By.id("B13940825746200649"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(CLICK).click().build().perform();
	
	Thread.sleep(300);

	//Enter name
	
	WebElement name = driver.findElement(By.id("P5_REP_NAME"));
	
	name.clear();
	
	name.sendKeys(Name);
	
	Thread.sleep(300);
		
	//Enter Code
	
	WebElement code = driver.findElement(By.id("P5_REP_CODE"));
	
	code.clear();
	
	code.sendKeys(Name);
	
	Thread.sleep(300);
		
	//Select is group option 
	
	WebElement group= driver.findElement(By.id("P5_REP_IS_GROUP_0"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(group).click().build().perform();
	
	//click create
	
	WebElement Create= driver.findElement(By.id("B13934422351200562"));
	
	Actions act0= new Actions(driver);
	
	act0.moveToElement(Create).click().build().perform();
	
	Thread.sleep(300);
	
	//Check Success msg
	
	Success_Msg();
	
}
//*********************************** Method to verify created reporting config ************************************
public void Verify_created() throws InterruptedException
{
	Reset();
	
	//enter name to search

	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(Name);
	
	Thread.sleep(200);
		
	//Click Go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
		
	//verify search
	
	String text1= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
	
	Assert.assertEquals(text1, Name);
}

//************************************ Method to edit reporting config **************************************
public void EditConfig() throws InterruptedException
{
	//Click edit
	
	WebElement edit= driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();
	
	Thread.sleep(300);
			
	//Change name
	
	WebElement name= driver.findElement(By.id("P5_REP_NAME"));
	
	name.clear();
	
	name.sendKeys(Name1);
			
	
	//Change Code
	
	WebElement code = driver.findElement(By.id("P5_REP_CODE"));
	
	code.clear();
	
	code.sendKeys(Name1);
	
	Thread.sleep(300);
		
	//Click Apply changes
	
	WebElement apply= driver.findElement(By.id("B13934512595200562"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(apply).click().build().perform();
	
	Thread.sleep(300);
		
	// Check success message
	
	Success_Msg();
			
}
//******************************************** Method to verify edited config**********************************
public void Verify_edited() throws InterruptedException
{
	// Click on bread crumbs to back to reporting tab
	 
	WebElement reporting = driver.findElement(By.xpath("//a[contains(text(),'Reports')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(reporting).click().build().perform();
	
	Thread.sleep(500);
	
	//reset all filters
	
	Reset();
	
	Thread.sleep(500);
	 	
	//enter name to search
	
	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(Name1);
	
	Thread.sleep(500);
		
	//Click Go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
	
	
	//verify search
	
	String text1= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
	
	Assert.assertEquals(text1, Name1);
}

//************************************************* Method to delete config *******************************

public void Delete_config() throws InterruptedException
{
	//Click edit

	WebElement edit= driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();
	
	Thread.sleep(300);
	
	//Click delete
	
	WebElement delete= driver.findElement(By.id("B13934619217200562"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(delete).click().build().perform();
	
	Thread.sleep(300);
		
	//Accept alert
	
	Alert alert= driver.switchTo().alert();
	
	alert.accept();

	// Check success message

	Success_Msg();
}

//*********************************************** Verify Deletion *******************************
public void Verify_Deletion() throws InterruptedException
{
	// Click on bread crumbs to back to reporting tab
	
	WebElement reporting = driver.findElement(By.xpath("//a[contains(text(),'Reports')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(reporting).click().build().perform();
	
	Thread.sleep(500);	
	
	//reset all filters
 	
	Reset();
 	
	Thread.sleep(500);
	
 	//enter name to search
	
	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(Name1);
	
	Thread.sleep(500);
			
	//Click Go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
	
	//verify search
	
	Boolean text1= driver.findElement(By.xpath("//span[@id='apexir_NO_DATA_FOUND_MSG']")).isDisplayed();
	
	Assert.assertTrue(text1);
  
}
		
}
