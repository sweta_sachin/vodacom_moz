package FMS_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CallData_Tab_tests extends common_methods.CommonMethods{
	
		
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_FMSTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		
	 Thread.sleep(1000);
	 driver.quit();
	 
	}

	
	@Test
	public void a_View_CallDataTab()
	{
		view_CallDataTab();
	}

	@Test
	public void b_Click_CallDataTab() throws InterruptedException
	{
		Thread.sleep(500);
		click_CallDataTab();
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void g_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","report_1.csv"));
	}
	
	@Test
	public void h_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
		
	}
		
	@Test
	public void i_reset_test() throws InterruptedException
	{
		Thread.sleep(500);
		reset();
	}
	
	
	@Test
	public void z_flashBackTest() throws InterruptedException
	{
		Thread.sleep(500);
		flashback_filter();
	}
	
	@Test
	public void k_View_CallData_ForSpecificMSISDNforSpecificTime() throws InterruptedException
	{
		Thread.sleep(500);
		View_CallData_ForSpecificMSISDNforSpecificTime();
		
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Call Data tab ******************************
public void view_CallDataTab()
 {
	WebElement CallData= driver.findElement(By.linkText("Call Data"));
	
	Boolean callData= CallData.isDisplayed();
	
	Assert.assertTrue(callData);
}

//********************************* Method to click Call Data tab ******************************
public void click_CallDataTab() throws InterruptedException
{
	WebElement profile= driver.findElement(By.linkText("Call Data"));

	Actions act= new Actions(driver);
	
	act.moveToElement(profile).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
 {
	 col= "Event Type";
	 expvalue= "Processed";
	
	 view_filtered_report();
	
	// Verify report
	
	 Thread.sleep(600);
	
	 String verifyreport= driver.findElement(By.xpath("//tr[2]//td[5]")).getText();
	
	 Thread.sleep(300);
	
	 Assert.assertEquals(verifyreport, expvalue);
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 
	
	save_report();
	
	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();	
	
	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
 {
	select_primary_report();

	Thread.sleep(300);
	
	try {

		WebElement primaryreport= driver.findElement(By.id("3820809179262221"));
	
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}catch(Exception e) 	{

		WebElement primaryreport= driver.findElement(By.id("3820809179262221"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}

}
//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
	{
		 Reset();

	// Verify report
		
		 WebElement primaryreport= driver.findElement(By.id("3820809179262221"));
		
		 Boolean report= primaryreport.isDisplayed();
		
		 Assert.assertTrue(report);
}	

//********************* Method to view call data report for specified number for a specified time span **********************
public void View_CallData_ForSpecificMSISDNforSpecificTime() throws InterruptedException
{
	//Enter MSISDN

	WebElement MSISDN= driver.findElement(By.id("P10_MSISDN"));
	
	MSISDN.clear();
	
	MSISDN.sendKeys(msisdn);
	
	Thread.sleep(300);
			
	// Click go
	
	WebElement go= driver.findElement(By.id("P10_GO"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
	
	//verify
	
	WebElement verify= driver.findElement(By.xpath("//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean report= verify.isDisplayed();
	
	Thread.sleep(300);
	
	Assert.assertTrue(report);
	
	}

}
