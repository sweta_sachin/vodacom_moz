package FMS_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FMS_Apex_Usage_Tab_tests extends common_methods.CommonMethods{
	
	
		
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_FMSTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		
	 Thread.sleep(1000);
	 driver.quit();
	 
	}

	@Test
	public void a_View_FMS_Apex_UsageTab()
	{
		view_FMS_Apex_UsageTab();
	}

	@Test
	public void b_Click_FMS_Apex_UsageTab() throws InterruptedException
	{
		Thread.sleep(500);
		click_FMS_Apex_UsageTab();
	}
	
	@Test
	public void ba_View_FMS_Apex_Usage_ForSpecificMSISDNforSpecificTime() throws InterruptedException
	{
		Thread.sleep(500);
		View_FMS_Apex_Usage_ForSpecificMSISDNforSpecificTime();
		
	}
	
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void g_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","fms_apex_usage.csv"));
	}
	

	@Test
	public void h_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","fms_apex_usage.htm"));
	}
	
	@Test
	public void z_emailReport() throws InterruptedException
	{
		Thread.sleep(300);
		email_report();
	}
				
	@Test
	public void i_reset_test() throws InterruptedException
	{
		Thread.sleep(2500);
		reset();
	}
	
	
	@Test
	public void j_flashBackTest() throws InterruptedException
	{
		Thread.sleep(2000);
		flashback_filter();
	}
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view FMS_Apex_Usage tab ******************************
public void view_FMS_Apex_UsageTab()
 {
	WebElement FMS= driver.findElement(By.linkText("FMS Apex Usage"));
	
	Boolean fms= FMS.isDisplayed();
	
	Assert.assertTrue(fms);
}


//********************************* Method to click FMS_Apex_Usage tab ******************************
public void click_FMS_Apex_UsageTab() throws InterruptedException
{
	WebElement profile= driver.findElement(By.linkText("FMS Apex Usage"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(profile).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
 {
	 col= "User Name";
	 expvalue= "A dmin";
	
	 view_filtered_report();

	// Verify report
	
	 Thread.sleep(600);
	
	 String verifyreport= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[2]")).getText();
	
	 Thread.sleep(300);
	
	 Assert.assertEquals(verifyreport, expvalue);
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{ 	
	report= "1. Test report"; 

	save_report();
	
	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();	
	
	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
 {
	select_primary_report();
	
	Thread.sleep(300);
	
	try {

		WebElement primaryreport= driver.findElement(By.id("80591022847188901"));
	
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	
	}catch(Exception e) 	{

		WebElement primaryreport= driver.findElement(By.id("80591022847188901"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}

}
//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
	{
		Reset();
		
		Thread.sleep(800);

	// Verify report
		
		WebElement primaryreport= driver.findElement(By.id("80591022847188901"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
}	
//********************* Method to view call data report for specified number for a specified time span **********************
public void View_FMS_Apex_Usage_ForSpecificMSISDNforSpecificTime() throws InterruptedException
{		
	//Enter MSISDN

	WebElement MSISDN= driver.findElement(By.id("P16_MSISDN"));
	
	MSISDN.clear();
	
	MSISDN.sendKeys(msisdn);
	
	Thread.sleep(300);
	
	// Click go
	
	WebElement go= driver.findElement(By.id("P16_GO"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
			
	//verify
	
	WebElement verify= driver.findElement(By.id("80591022847188901"));
	
	Boolean report= verify.isDisplayed();
	
	Thread.sleep(300);
	
	Assert.assertTrue(report);
	
}

}
