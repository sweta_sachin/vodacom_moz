package FMS_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ContinuAM_Handset_Profile_Tab_tests extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_FMSTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}
	
	@Test
	public void a_ViewContinuAM_Handset_Profile_Tab_tests_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		view_ContinuAM_Handset_ProfileTab();
	}
	
	@Test
	public void b_ClickContinuAM_Handset_Profile_Tab_tests_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		click_ContinuAM_Handset_Profile_Tab();
	}
 
	@Test
	public void c_SearchProfileBy_MSISDN() throws InterruptedException
	{
		Thread.sleep(300);
		SearchProfileByMSISDN();
	}
	
	@Test
	public void d_SearchProfileBy_IMSI() throws InterruptedException
	{
		Thread.sleep(300);
		SearchProfileByIMSI();
	}
	
	@Test
	public void e_SearchProfileBy_IMEI() throws InterruptedException
	{
		Thread.sleep(300);
		SearchProfileByIMEI();
		
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view ContinuAM Handset Profile tab ******************************
public void view_ContinuAM_Handset_ProfileTab()
{
	WebElement profile= driver.findElement(By.linkText("ContinuAM Handset Profile"));
	
	Boolean Profile= profile.isDisplayed();
	
	Assert.assertTrue(Profile);
}

//********************************* Method to click Bulletin Admin tab ******************************
public void click_ContinuAM_Handset_Profile_Tab() throws InterruptedException
{
	WebElement profile= driver.findElement(By.linkText("ContinuAM Handset Profile"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(profile).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement verify= driver.findElement(By.id("P2_SEARCH_VALUES"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
 }

//*************************************** Method to view profile by MSISDN *************************************
public void SearchProfileByMSISDN() throws InterruptedException
{
	//Select Search type
	
	WebElement searchType= driver.findElement(By.id("P2_SEARCH_TYPE_0"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(searchType).click().build().perform();
		
	//Enter MSISDN 
	
	WebElement SearchValue= driver.findElement(By.id("P2_SEARCH_VALUES"));
	
	SearchValue.clear();
	
	SearchValue.sendKeys(msisdn);
	
	Thread.sleep(500);
		
	//Click Go
	
	Click_Go();
		
	//Verify
	
	String MSISDN= driver.findElement(By.xpath("//tr[@class='echo-report-row']//td[1]")).getText();
	
	Thread.sleep(400);
	
	Assert.assertEquals(MSISDN, msisdn);
}


//*************************************** Method to view profile by IMSI *************************************
public void SearchProfileByIMSI() throws InterruptedException
{
	//Select Search type
	
	WebElement searchType= driver.findElement(By.id("P2_SEARCH_TYPE_1"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(searchType).click().build().perform();
		
	//Enter IMSI 
	
	WebElement SearchValue= driver.findElement(By.id("P2_SEARCH_VALUES"));
	
	SearchValue.clear();
	
	SearchValue.sendKeys(imsi);
	
	Thread.sleep(500);
		
	//Click Go
	
	Click_Go();
	
	//Verify
	
	String IMSI= driver.findElement(By.xpath("//tr[@class='echo-report-row']//td[2]")).getText();
	
	Thread.sleep(400);
	
	Assert.assertEquals(IMSI, imsi);
}

//*************************************** Method to view profile by IMEI *************************************
public void SearchProfileByIMEI() throws InterruptedException
{
	//Select Search type
	
	WebElement searchType= driver.findElement(By.id("P2_SEARCH_TYPE_2"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(searchType).click().build().perform();
		
	//Enter IMEI 
	
	WebElement SearchValue= driver.findElement(By.id("P2_SEARCH_VALUES"));
	
	SearchValue.clear();
	
	SearchValue.sendKeys(imei);
	
	Thread.sleep(500);
		
	//Click Go
	
	Click_Go();
		
	//Verify
	
	String IMEI= driver.findElement(By.xpath("//tr[@class='echo-report-row']//td[3]")).getText();
	
	Thread.sleep(400);
	
	Assert.assertEquals(IMEI, imei);
}

//****************************************** Method to click Go *******************************************
public void Click_Go()
{
	WebElement go= driver.findElement(By.id("P2_GO"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();

   }
}
