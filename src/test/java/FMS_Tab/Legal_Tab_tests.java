package FMS_Tab;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Legal_Tab_tests  extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_FMSTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		
	 Thread.sleep(1000);
	 driver.quit();
	 
	}

	
	@Test
	public void a_View_LegalTab()
	{
		view_LegalTab();
	}

	@Test
	public void b_Click_LegalTab() throws InterruptedException
	{
		Thread.sleep(500);
		click_LegalTab();
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void g_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  DownLoad_CSV("C:\\Users\\Sweta\\Downloads","legal_requests.csv"));
	}
	
	@Test
	public void h_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
		
	}
		
	@Test
	public void i_reset_test() throws InterruptedException
	{
		Thread.sleep(500);
		reset();
	}
	
	@Test
	public void j_Create_legalRequest() throws InterruptedException
	{
		Thread.sleep(500);
		Create_LegalRequest();
	}
	
	@Test
	public void k_Verify_created() throws InterruptedException
	{
		Thread.sleep(500);
		verifyCreated();
	}
	
	@Test
	public void z_flashBackTest() throws InterruptedException
	{
		Thread.sleep(500);
		flashback_filter();
	}
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Legal tab ******************************
public void view_LegalTab()
 {
	WebElement Legal= driver.findElement(By.linkText("Legal"));
	
	Boolean legal= Legal.isDisplayed();
	
	Assert.assertTrue(legal);
}


//********************************* Method to click Legal tab ******************************
public void click_LegalTab() throws InterruptedException
{
	WebElement profile= driver.findElement(By.linkText("Legal"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(profile).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
 {
	expvalue= "Processed";
	col= "Status";
	
	view_filtered_report();
	
	// Verify report
	
	Thread.sleep(600);
	
	String verifyreport= driver.findElement(By.xpath("//tr[2]//td[3]")).getText();
	
	Thread.sleep(300);
	
	Assert.assertEquals(verifyreport, expvalue);
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 
	
	save_report();
		
	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();	
	
	// verify 

	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
 {
	select_primary_report();
	
	Thread.sleep(300);
	
	try {
		WebElement primaryreport= driver.findElement(By.id("3820809179262221"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}catch(Exception e) 	{

		WebElement primaryreport= driver.findElement(By.id("3820809179262221"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}

}
//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
	{
		Reset();
		
	// Verify report
		
		WebElement primaryreport= driver.findElement(By.id("3820809179262221"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
}	

//*********************************** Method to create legal request ************************************
public void Create_LegalRequest() throws InterruptedException
{
     // Click Create 
		
	WebElement create= driver.findElement(By.id("B3830506486320464"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(create).click().build().perform();
	
	Thread.sleep(500);
			
	//Enter case number
	
	WebElement caseNo= driver.findElement(By.id("P13_CASE_NO"));
	
	caseNo.clear();
	
	caseNo.sendKeys(Name);
	
	Thread.sleep(600);
			
	//Click Next
	
	WebElement next= driver.findElement(By.id("B6322506401307279098"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(next).click().build().perform();
	
	Thread.sleep(300);
			
	//Click add row
	
	WebElement row1= driver.findElement(By.id("B6322509326964279111"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(row1).click().build().perform();
	
	Thread.sleep(500);
	
	//Enter MSISDN
	
	WebElement MSISDN= driver.findElement(By.id("f03_0001"));
	
	MSISDN.clear();
	
	MSISDN.sendKeys(msisdn);
	
	Thread.sleep(300);
			
	//Click Submit
	
	WebElement submit= driver.findElement(By.id("B6322509521487279111"));
	
	Actions act3= new Actions(driver);
	
	act3.moveToElement(submit).click().build().perform();
	
	Thread.sleep(300);
	
  }

//****************************************** Method to verify created request ******************************
public void verifyCreated() throws InterruptedException
{
	//Click on bread crumbs to go to requests
	/*		Thread.sleep(500);
	        WebElement request= driver.findElement(By.xpath("//a[contains(text(),'Legal Request')]"));
	        Actions act1= new Actions(driver);
	        act1.moveToElement(request).click().build().perform();
	        Thread.sleep(500); */
	
	//Search created request by case number

	WebElement caseNo= driver.findElement(By.id("apexir_SEARCH"));
	
	caseNo.clear();
	
	caseNo.sendKeys(Name);
			
	// Click go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
			
	//verify
	
	String text1= driver.findElement(By.xpath("//tr[2]//td[4]")).getText();
	
	Assert.assertEquals(text1, Name);
    
}

//**************************************** Method to view Results of a request ****************************
public void ViewResult() throws InterruptedException
{
	// Click view
	
	WebElement go= driver.findElement(By.xpath("//tr[@class='even']//td//a//img"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
	
	//verify
	
	WebElement verify= driver.findElement(By.xpath("//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean report= verify.isDisplayed();
			
	//Click on bread crumbs to go to requests
	
	WebElement request= driver.findElement(By.xpath("//a[contains(text(),'Legal Request')]"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(request).click().build().perform();
	
	Thread.sleep(500);
	
	reset();
	
	Assert.assertTrue(report);
    
}

//*********************************** Method to test download CSV ***************************

public boolean DownLoad_CSV(String downloadPath, String fileName) throws InterruptedException

{
	click_ActionButton();
	
	Thread.sleep(200);
	
	//Click download option

	WebElement reset = driver.findElement(By.xpath("//a[@class='dhtmlSubMenuN'][contains(text(),'Download')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(reset).click().build().perform();
	 
	// Choose and click CSV report type to download 
	
	Thread.sleep(1000);
	
	WebElement csv = driver.findElement(By.xpath("//a[@id='apexir_dl_CSV']//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(csv).click().build().perform();
	
	Thread.sleep(5000);
	
	File dir = new File(downloadPath);
	
	File[] dirContents = dir.listFiles();
	
	for (int i = 0; i < dirContents.length; i++) {
	
		if (dirContents[i].getName().equals(fileName)) {
	    
			// File has been found, it can now be deleted:
	    	
			Thread.sleep(2000);
	        
			dirContents[i].delete();
	        
			System.out.println("File is getting deleted");
	        
			return true;
	      }
		
	   }
	
	return false;
	
}

}




