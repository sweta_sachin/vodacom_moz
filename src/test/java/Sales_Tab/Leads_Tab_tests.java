package Sales_Tab;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Leads_Tab_tests extends common_methods.CommonMethods{
	
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_SalesTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		Thread.sleep(1000);
		driver.quit();			
	}
	
	@Test
	public void a_View_Leads_Tab()
	{
		view_LeadsTab();
	}

	@Test
	public void b_Click_Leads_Tab() throws InterruptedException
	{
		click_LeadsTab();
	}
	
	@Test
	public void c_GeneralSearch() throws InterruptedException
	{
		Thread.sleep(300);
		General_Search();
	}
	
	@Test
	public void ca_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
	@Test
	public void g_flashBackTest() throws InterruptedException
	{
		Thread.sleep(1000);
		flashback_filter();
		Thread.sleep(50000);
	}
	
	@Test
	public void h_reset_test() throws InterruptedException
	{
		Thread.sleep(5000);
		reset();
		Thread.sleep(50000);
	} 	
	@Test
	public void i_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(DownLoad_CSV());
	}
	
	@Test
	public void j_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(DownLoad_HTML());
	}
	
	@Test
	public void k_DownloadPDF() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(DownLoad_PDF());
	}
	
	@Test
	public void l_emailReport() throws InterruptedException
	{
		Thread.sleep(2500); 
		email_report();
	}
	
	
	@Test
	public void m_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(5000);
		get_subscription();
	}

	@Test
	public void n_SearchByAccountNumber() throws InterruptedException
	{
		Search_BY_AccountNumber();
	}
	
	@Test
	public void o_SearchByAccountName() throws InterruptedException
	{
		Search_BY_AccountName();
	}
	
	@Test
	public void p_SearchByContactNumber() throws InterruptedException
	{
		Search_BY_ContactNumber();
	}
	
	@Test
	public void q_SearchBySalesPerson() throws InterruptedException
	{
		Search_BY_SalesPerson();
	}
	
	@Test
	public void r_SearchByAccountType() throws InterruptedException
	{
		Search_BY_AccountType();
	}
	
	@Test
	public void s_SearchByLeadStatus_All() throws InterruptedException
	{
		Thread.sleep(200);
		SearchByLeadStatus_All();
	}
	@Test
	public void t_SearchByLeadStatus_Open() throws InterruptedException
	{
		Thread.sleep(200);
		SearchByLeadStatus_Open();
	}
	
	@Test
	public void u_SearchByLeadStatus_Partial() throws InterruptedException
	{
		Thread.sleep(200);
		SearchByLeadStatus_Partial();
	}
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Leads tab ******************************
public void view_LeadsTab()
{
	WebElement Leads= driver.findElement(By.linkText("Leads"));
	
	Boolean leads= Leads.isDisplayed();
	
	Assert.assertTrue(leads);
}


//********************************* Method to click Leads tab ******************************
public void click_LeadsTab() throws InterruptedException
{
	WebElement Leads= driver.findElement(By.linkText("Leads"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Leads).click().build().perform();
	
	Thread.sleep(400);

  //Verify report
	
	WebElement verify= driver.findElement(By.id("P1_ACC_NO"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}
	
//************************************** Method to click Search button *************************
public void Click_Search() throws InterruptedException
{
	WebElement search= driver.findElement(By.id("P1_SEARCH"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(search).click().build().perform();
	
	Thread.sleep(400);
}

//************************************ Method to perform General search ***********************
public void General_Search() throws InterruptedException
{
	Thread.sleep(300);
	
	Click_Search();
	
	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);

}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	 col= "Acc No";
	 expvalue= AccNo;
	
	 view_filtered_report();

	// Verify report

	 Thread.sleep(600);
	
	 String verifyreport= driver.findElement(By.xpath("//div[@id='apexir_DATA_PANEL']//tr[2]//td[2]")).getText();
	
	 Thread.sleep(300);
	
	 Assert.assertEquals(verifyreport, expvalue);
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 
	
	save_report();

	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();
	
	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
}


//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
	select_primary_report();
	
	Thread.sleep(300);
	
	try {

		WebElement primaryreport= driver.findElement(By.id("1790304738657687"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	
	}catch(Exception e) 	{

		WebElement primaryreport= driver.findElement(By.id("1790304738657687"));
	
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}

}
//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
	{
		Thread.sleep(200);
		
		Reset();

	// Verify report
		
		WebElement primaryreport= driver.findElement(By.id("1790304738657687"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
}	

//*********************************** Method to test download CSV ***************************
public boolean DownLoad_CSV() throws InterruptedException
{
	
	click_ActionButton();
	
	Thread.sleep(200);
	
	//Click download option
	
	WebElement reset = driver.findElement(By.xpath("//a[contains(text(),'Download')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(reset).click().build().perform();
	
	 // Choose and click CSV report type to download 
	
	Thread.sleep(1000);
	
	WebElement csv = driver.findElement(By.xpath("//a[@id='apexir_dl_CSV']//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(csv).click().build().perform();
	
	Thread.sleep(1500);
	
	String folderName = "C:\\Users\\Sweta\\Downloads"; // Give your folderName
	
	File[] listFiles = new File(folderName).listFiles();

		for (int i = 0; i < listFiles.length; i++) {

		    if (listFiles[i].isFile()) {
	
		    	String filename = listFiles[i].getName();
		        
		    	if (filename.startsWith("customeradmin_mozvc")
		        
		    			&& filename.endsWith(".csv")) {
		            
		    		System.out.println("found file" + " " + filename);
		            
		    		listFiles[i].delete();
		            
		    		return true;
		        }
	          }
		
	      	}
		
		return false;
}


//*********************************** Method to test download Html ***************************
public boolean DownLoad_HTML() throws InterruptedException
{
	click_ActionButton();
	
	Thread.sleep(200);
	
	//Click download option
		
	WebElement reset = driver.findElement(By.xpath("//a[contains(text(),'Download')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(reset).click().build().perform();
	 
	 // Choose and click CSV report type to download 
	
	Thread.sleep(1000);
	
	WebElement csv = driver.findElement(By.xpath("//a[@id='apexir_dl_HTML']//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(csv).click().build().perform();
	
	Thread.sleep(1500);
	
	String folderName = "C:\\Users\\Sweta\\Downloads"; // Give your folderName
	
	File[] listFiles = new File(folderName).listFiles();

		for (int i = 0; i < listFiles.length; i++) {

		    if (listFiles[i].isFile()) {
	
		    	String filename = listFiles[i].getName();
		        
		    	if (filename.startsWith("customeradmin_mozvc")
		        
		    			&& filename.endsWith(".htm")) {
		           
		    		System.out.println("found file" + " " + filename);
		            
		    		listFiles[i].delete();
		            
		    		return true;
		        }
	          }
		
	      	}
		return false;
}

//*********************************** Method to test download PDF ***************************
public boolean DownLoad_PDF() throws InterruptedException
{
	click_ActionButton();
	
	Thread.sleep(200);
	
	//Click download option
	
	WebElement reset = driver.findElement(By.xpath("//a[contains(text(),'Download')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(reset).click().build().perform();
	 
	 // Choose and click CSV report type to download 
	
	Thread.sleep(1000);
	
	WebElement csv = driver.findElement(By.xpath("//a[@id='apexir_dl_PDF']//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(csv).click().build().perform();
	
	Thread.sleep(1500);
	
	String folderName = "C:\\Users\\Sweta\\Downloads"; // Give your folderName
	
	File[] listFiles = new File(folderName).listFiles();

	for (int i = 0; i < listFiles.length; i++) {

	if (listFiles[i].isFile()) {
	
	String filename = listFiles[i].getName();
	
	if (filename.startsWith("customeradmin_mozvc")
	
			&& filename.endsWith(".pdf")) {
		    
		System.out.println("found file" + " " + filename);
		
		listFiles[i].delete();
		
		return true;
		
		}
	  }
		
	}
		return false;
}

//*********************************** Method to search leads by Account number ********************
public boolean Search_BY_AccountNumber() throws InterruptedException 
{
	// Enter account number
		
	WebElement AccountNumber= driver.findElement(By.id("P1_ACC_NO"));
	
	AccountNumber.clear();
	
	AccountNumber.sendKeys(AccNo);
	
	Thread.sleep(200);
	
	Click_Search();
	
	Thread.sleep(400);
			
	//Verify
	
	if(driver.findElement(By.id("apexir_NO_DATA_FOUND_MSG")).isDisplayed())	{
			
		driver.findElement(By.id("P1_ACC_NO")).clear();
		
		return true;
		
	}else{
	
		driver.findElement(By.id("P1_ACC_NO")).clear();
			
		String text= driver.findElement(By.id("")).getText();
		
		Assert.assertEquals(text, AccNo);
			
	}
		driver.findElement(By.id("P1_ACC_NO")).clear();
	
		return false;
	}


//*********************************** Method to search leads by Account name ********************
public boolean Search_BY_AccountName() throws InterruptedException 
{
	// Enter account name
		
	WebElement AccountName= driver.findElement(By.id("P1_ACC_NAME"));
	
	AccountName.clear();
	
	AccountName.sendKeys(AccName);
	
	Thread.sleep(200);
	
	Click_Search();
	
	Thread.sleep(400);
			
	//Verify
	
	if(driver.findElement(By.id("apexir_NO_DATA_FOUND_MSG")).isDisplayed())	{
			
		driver.findElement(By.id("P1_ACC_NAME")).clear();
		
		return true;
		
	}else{
	
		driver.findElement(By.id("P1_ACC_NAME")).clear();
		
		String text= driver.findElement(By.id("")).getText();
		
		Assert.assertEquals(text, AccName);
			
		}
		
	driver.findElement(By.id("P1_ACC_NAME")).clear();
	
	return false;
	
}

//*********************************** Method to search leads by Contact Number ********************
public boolean Search_BY_ContactNumber() throws InterruptedException 
{
	// Enter contact number
		
	WebElement ContactNumber= driver.findElement(By.id("P1_TELE_NO"));
	
	ContactNumber.clear();
	
	ContactNumber.sendKeys(CNumber);
	
	Thread.sleep(200);
	
	Click_Search();
	
	Thread.sleep(400);
			
	//Verify
	
	if(driver.findElement(By.id("apexir_NO_DATA_FOUND_MSG")).isDisplayed())	{
			
		driver.findElement(By.id("P1_TELE_NO")).clear();
		
		return true;
		
	}else{
	
		driver.findElement(By.id("P1_TELE_NO")).clear();
		
		String text= driver.findElement(By.id("")).getText();
		
		Assert.assertEquals(text,CNumber);
			
		}
		
	driver.findElement(By.id("P1_TELE_NO")).clear();
	
	return false;
	
}

//*********************************** Method to search leads by Sales person ********************
public boolean Search_BY_SalesPerson() throws InterruptedException 
{
	// Select sales person

	Select Sales= new Select(driver.findElement(By.id("P1_SAP_UID")));
	
	Sales.selectByIndex(2);
	
	Thread.sleep(200);
	
	WebElement text1= Sales.getFirstSelectedOption();
	
	String salesname =text1.getText();
	
	System.out.println(salesname);
	
	Click_Search();
	
	Thread.sleep(400);
	
	//Verify
	
	if(driver.findElement(By.id("apexir_NO_DATA_FOUND_MSG")).isDisplayed())	{
		
			Select Sale= new Select(driver.findElement(By.id("P1_SAP_UID")));
		
			Sale.selectByVisibleText("-- Select --");
			
			return true;
		
	}else{
	
		Select Sale= new Select(driver.findElement(By.id("P1_SAP_UID")));
		
		Sale.selectByVisibleText("-- Select --");
			
		String text= driver.findElement(By.id("")).getText();
		
		Assert.assertEquals(text,salesname);
			
	}

	Select Sale= new Select(driver.findElement(By.id("P1_SAP_UID")));
	
	Sale.selectByVisibleText("-- Select --");
		
	return false;
	
}

//*********************************** Method to search leads by Account Type ********************
public boolean Search_BY_AccountType() throws InterruptedException 
{
	// Select Account type
		
	Select Atype= new Select(driver.findElement(By.id("P1_ACC_TYPE")));
	
	Atype.selectByIndex(2);
	
	Thread.sleep(200);
	
	WebElement text1= Atype.getFirstSelectedOption();
	
	String salesname =text1.getText();
	
	System.out.println(salesname);
	
	Click_Search();
	
	Thread.sleep(400);
			
	//Verify
	
	if(driver.findElement(By.id("apexir_NO_DATA_FOUND_MSG")).isDisplayed())	{
		
			Select Sale= new Select(driver.findElement(By.id("P1_ACC_TYPE")));
		
			Sale.selectByVisibleText("-- Select --");
			
			return true;
			
		}else{
			
			Select Sale= new Select(driver.findElement(By.id("P1_ACC_TYPE")));
			
			Sale.selectByVisibleText("-- Select --");
			
			String text= driver.findElement(By.id("")).getText();
			
			Assert.assertEquals(text,salesname);
			
		}
		Select Sale= new Select(driver.findElement(By.id("P1_ACC_TYPE")));
		
		Sale.selectByVisibleText("-- Select --");
		
		return false;
	}

//************************************ Method to search by lead Status open **********************
public void SearchByLeadStatus_Open() throws InterruptedException
{
   WebElement open= driver.findElement(By.id("P1_STATUS_0"));
   
   Actions act= new Actions(driver);
   
   act.moveToElement(open).click().build().perform();
   
   Thread.sleep(200);

   //Verify
   
   Boolean Open= driver.findElement(By.id("P1_STATUS_0")).isSelected();
   
   Assert.assertTrue(Open);
 
}

//************************************ Method to search by lead Status All **********************
public void SearchByLeadStatus_All() throws InterruptedException
{
 
	WebElement all= driver.findElement(By.id("P1_STATUS_2"));
 
	Actions act= new Actions(driver);
 
	act.moveToElement(all).click().build().perform();
 
	Thread.sleep(200);

 //Verify
 
	Boolean All= driver.findElement(By.id("P1_STATUS_2")).isSelected();
 
	Assert.assertTrue(All);

}

//************************************ Method to search by lead Status All **********************
public void SearchByLeadStatus_Partial() throws InterruptedException
{

	WebElement partial= driver.findElement(By.id("P1_STATUS_1"));

	Actions act= new Actions(driver);

	act.moveToElement(partial).click().build().perform();

	Thread.sleep(200);

	//Verify

	Boolean Partial= driver.findElement(By.id("P1_STATUS_1")).isSelected();

	Assert.assertTrue(Partial);

}

}
