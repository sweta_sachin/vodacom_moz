package Sales_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WhiteLists_Tab_tests  extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_SalesTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	  driver.quit();			
	}
	
	@Test
	public void a_View_WhiteLists_Tab()
	{
		view_WhiteListsTab();
	}

	@Test
	public void b_Click_WhiteLists_Tab() throws InterruptedException
	{
		Thread.sleep(400);
		click_WhiteListsTab();
	}
	
	@Test
	public void c_View_Application_Service_report() throws InterruptedException
	{
		Thread.sleep(400);
		View_Application_service_report();
	}
	
	@Test
	public void ca_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void g_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","application_services.csv"));
	}
	
	@Test
	public void ga_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","application_services.htm"));
	}
	
	
	@Test
	public void gc_emailReport() throws InterruptedException
	{
		Thread.sleep(2500); 
		email_report();
	}
	
	
	@Test
	public void j_flashBackTest() throws InterruptedException
	{
		Thread.sleep(1000);
		flashback_filter();
	}
	
	@Test
	public void i_reset_test() throws InterruptedException
	{
		Thread.sleep(1500);
		reset();
	} 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view White list tab ******************************
public void view_WhiteListsTab()
{
	WebElement WhiteLists= driver.findElement(By.linkText("Whitelists"));
	
	Boolean WhiteList= WhiteLists.isDisplayed();
	
	Assert.assertTrue(WhiteList);
}


//********************************* Method to click WhiteLists tab ******************************
public void click_WhiteListsTab() throws InterruptedException
{
	WebElement WhiteLists= driver.findElement(By.linkText("Whitelists"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(WhiteLists).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}

//************************************** Method to click Search button *************************
public void Click_Search() throws InterruptedException
{
	WebElement search= driver.findElement(By.id("P1_SEARCH"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(search).click().build().perform();
	
	Thread.sleep(400);
	
}

//************************************ Method to perform General search ***********************
public void General_Search() throws InterruptedException
{
	Thread.sleep(300);

	Click_Search();
	
	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);

}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	col= "Type";
	expvalue= "Web Service";
	
	view_filtered_report();
	
	// 	Verify report
	
	Thread.sleep(600);
	
	String verifyreport= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[2]")).getText();
	
	Thread.sleep(300);
	
	Assert.assertEquals(verifyreport, expvalue);
	
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "2. Test report"; 

	save_report();
	

	//click apply
	
	Click_apply();
	

	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
	
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();
	
	// verify 
	
	Thread.sleep(300);

	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
	
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
	select_primary_report();

	Thread.sleep(300);
	
	try {
			WebElement primaryreport= driver.findElement(By.id("9105123250440250"));
	
			Boolean report= primaryreport.isDisplayed();
			
			Assert.assertTrue(report);
	
	}catch(Exception e) 	{
		
		WebElement primaryreport= driver.findElement(By.id("9105123250440250"));
	
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}
	
}
//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
{
	Thread.sleep(200);
	Reset();
	
	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("9105123250440250"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
	
}

//************************** Method to view application service report ***********************
public void View_Application_service_report()
{
	WebElement primaryreport= driver.findElement(By.id("9105123250440250"));

	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
	}
}
