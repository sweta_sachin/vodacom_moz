package Billing_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Categories_Tab_tests extends common_methods.CommonMethods{
	
	public static String ICCid= "1234567898765432145";
	
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		Thread.sleep(1000);
		click_billingTab();
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
		driver.quit();
	}
	
  @Test
  public void a_View_Categories_Tab()
  {
	  view_CategoriesTab();
  }
  
  @Test
  public void b_Click_Categories_Tab() throws InterruptedException
  {
	 Thread.sleep(1000);
	  click_CategoriesTab();
  }
  
   @Test
 	public void c_Filter_report() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		View_filtered_report();
 	}
 	
 	@Test
 	public void d_Save_Report() throws InterruptedException
 	{
 		Thread.sleep(1500); 
 		Save_report();
 	}
 	
 	@Test
 	public void e_Search_by_privateReport() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		search_by_private_report();
 	}
 	
 	@Test
 	public void f_search_By_Primary_report() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		search_by_PrimaryReports();
 	}
 	
 	@Test
 	public void g_flashBackTest() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		flashback_filter();
 	}
 	
 	@Test
 	public void h_reset_test() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		reset();
 	}
 	
 	@Test
 	public void i_DownloadCSV() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","number_plan_categories.csv"));
 	}
 	
	@Test
	public void j_Subscdription_email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
	}
	
	@Test
	public void k_Create_Category() throws InterruptedException
	{
		Thread.sleep(300);
		Create_category();
	}
	
	
	@Test
	public void l_VerifyCreated_Category() throws InterruptedException
	{
		Thread.sleep(300);
		Verify_created() ;
	}
	
	@Test
	public void m_Edit_Category() throws InterruptedException
	{
		Thread.sleep(300);
		Edit_Category();
	}
	
	@Test
	public void n_VerifyEditedCategory() throws InterruptedException
	{
		Thread.sleep(300);
		Verify_edited();
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//**************************** Method to view Categories tab *****************************
	public void view_CategoriesTab()
	{
		WebElement Categories=  driver.findElement(By.linkText("Categories"));
		
		Boolean Categories_tab= Categories.isDisplayed();
		
		Assert.assertTrue(Categories_tab);
	}

//**************************** Method to click Categories tab *****************************
	public void click_CategoriesTab()
	{
		WebElement Categories=  driver.findElement(By.linkText("Categories"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(Categories).click().build().perform();
	}
	
//************************************** Method to apply filter and view filtered report **********************
	public void View_filtered_report() throws InterruptedException
	{
		 expvalue= "No";
		 col= "Sim Swap";	
		
		 view_filtered_report();

		//Verify report
		
		 Thread.sleep(400);
		
		 String verifyreport= driver.findElement(By.xpath("//div[@id='apexir_DATA_PANEL']//tr[2]//td[7]")).getText();
		
		 Thread.sleep(300);
		
		 Assert.assertEquals(verifyreport, expvalue);
				
	}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
	{
		report= "1. Test report"; 
		save_report();
		 	
		 // Verify saved report
		
		Thread.sleep(500);
		
		Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
		
		dropdown.selectByVisibleText(report);
			
	}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
	{
		Search_by_private_report();	
		
		// verify 
	
		Thread.sleep(300);
		
		String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
		
		System.out.println(text);
		
		Assert.assertEquals(text, verifyreport);
	
		}

//**************************************** Method to search primary report ****************************
public void search_by_PrimaryReports() throws InterruptedException
	{
		try {
		
			select_primary_report();
			
			Thread.sleep(300);
			
			WebElement primaryreport= driver.findElement(By.id("3423601560145789"));
			
			Boolean report= primaryreport.isDisplayed();
			
			Assert.assertTrue(report);
			
		} catch(Exception e){
		
			select_primary_report();
			
			Thread.sleep(300);
			
			WebElement primaryreport= driver.findElement(By.id("3423601560145789"));
			
			Boolean report= primaryreport.isDisplayed();
			
			Assert.assertTrue(report);
		}
		
	}

//************************************ Method to test reset button on CIB ******************************
public void reset() throws InterruptedException
	{
		Thread.sleep(200);
		
		Reset();
		 
		// Verify report
		
		WebElement primaryreport= driver.findElement(By.id("3423601560145789"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}

//******************************************* Method to create category *************************************
public void Create_category() throws InterruptedException
{
	//Click create
	
	WebElement CLICK= driver.findElement(By.id("B3425231905145798"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(CLICK).click().build().perform();
	
	Thread.sleep(300);
		
	//Enter category name
	
	WebElement category= driver.findElement(By.id("P31_NA_CATEGORY"));
	
	category.clear();
	
	category.sendKeys(Name);
		
	//Enter Start ICCID
	
	WebElement ICCID= driver.findElement(By.id("P31_NA_START_ICCID"));
	
	ICCID.clear();
	
	ICCID.sendKeys(ICCid);
	
	//Select equipment group
	
	Select equipmentGroup= new Select(driver.findElement(By.id("P31_NA_EG_UID")));
	
	equipmentGroup.selectByIndex(1);
		
	//Select status
	
	Select status= new Select(driver.findElement(By.id("P31_NA_SS_UID")));
	
	status.selectByIndex(2);
		
	//Enter start MSISDN
	
	WebElement MSISDN1= driver.findElement(By.id("P31_NA_START_RANGE"));
	
	MSISDN1.clear();
	
	MSISDN1.sendKeys(msisdn1);
	
	//Enter End MSISDN
	
	WebElement MSISDN2= driver.findElement(By.id("P31_NA_END_RANGE"));
	
	MSISDN2.clear();
	
	MSISDN2.sendKeys(msisdn2);
	
	Thread.sleep(200);
		
	//Calculate total
	
	WebElement Calculate= driver.findElement(By.id("P31_CALCULATE"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(Calculate).click().build().perform();
	
	Thread.sleep(400);
	
	
   //Click create
	
	WebElement Create= driver.findElement(By.id("B3418706358145759"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(Create).click().build().perform();
	
	Thread.sleep(400);
	   
   // Check success message
	
	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
	
	String Expected_msg= "Category created successfully";
	
	Assert.assertEquals(Msg, Expected_msg);
 }


//******************************************** Method to verify created category**********************************
public void Verify_created() throws InterruptedException
{
	//enter name to search
	
	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(Name);
	
	Thread.sleep(200);
		
	//Click Go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
		
	//verify search
	
	String text1= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
	
	Assert.assertEquals(text1, Name);
 }
//***************************************** Method to edit category ********************************************
public void Edit_Category() throws InterruptedException
{
	//Click edit
		
	WebElement edit= driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();
	
	Thread.sleep(300);
		
	//Change name
	
	WebElement category= driver.findElement(By.id("P31_NA_CATEGORY"));
	
	category.clear();
	
	category.sendKeys(Name1);
	
	
	//Click Apply changes
	
	WebElement apply= driver.findElement(By.id("B3418826105145759"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(apply).click().build().perform();
	
	Thread.sleep(300);
	
	// Check success message
	
	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
	
	String Expected_msg= "Category updated successfully";
	
	Assert.assertEquals(Msg, Expected_msg);
	
  }
//******************************************** Method to verify edited category**********************************
public void Verify_edited() throws InterruptedException
{
	//reset all filters

	Reset();
	 	
	//enter name to search
	
	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(Name1);
	
	Thread.sleep(500);
		
	//Click Go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
		
	//verify search
	
	String text1= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[2]")).getText();
	
	Assert.assertEquals(text1, Name1);
}


}
