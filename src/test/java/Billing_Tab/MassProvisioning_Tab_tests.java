package Billing_Tab;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MassProvisioning_Tab_tests  extends common_methods.CommonMethods{
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		Thread.sleep(1000);
		click_billingTab();
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
		driver.quit();
	}
	
  @Test
  public void a_View_MassProvisioning_Tab()
  {
	  view_MassProvisioningTab();
  }
  
  @Test
  public void b_Click_MassProvisioning_Tab() throws InterruptedException
  {
	 Thread.sleep(1000);
	  click_MassProvisioningTab();
  }
  
  @Test
 	public void c_Filter_report() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		View_filtered_report();
 	}
 	
 	@Test
 	public void d_Save_Report() throws InterruptedException
 	{
 		Thread.sleep(1500); 
 		Save_report();
 	}
 	
 	@Test
 	public void e_Search_by_privateReport() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		search_by_private_report();
 	}
 	
 	@Test
 	public void f_search_By_Primary_report() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		search_by_PrimaryReports();
 	}
 	
 	@Test
 	public void g_flashBackTest() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		flashback_filter();
 	}
 	
 	@Test
 	public void h_reset_test() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		reset();
 	}
 	
 	@Test
 	public void i_DownloadCSV() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","mass_provisioning.csv"));
 	}
 	

 	@Test
	public void j_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","mass_provisioning.htm"));
	}
	
	@Test
	public void k_emailReport() throws InterruptedException
	{
		Thread.sleep(300);
		email_report();
	}
   
/*	@Test
	public void l_CreateMassProvisioningRecord()
	{
		//enter after adding more tests
	}
  
	@Test
	public void m_VerifyCreated()
	{
		
	}
	  */
	@Test
	public void n_Delete() throws InterruptedException
	{
		Thread.sleep(800); 
		Delete() ;
	}
	
/*	@Test
	public void o_VerifyDeleted()
	{
		
	}
	*/
	
	@Test
	public void p_ViewDetailsByClickingOnName() throws InterruptedException
	{
		Thread.sleep(800); 
		ViewDetails();
		
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//**************************** Method to view Mass Provisioning tab *****************************
public void view_MassProvisioningTab()
{
	WebElement MassProvisioning=  driver.findElement(By.linkText("Mass Provisioning"));
	
	Boolean MassProvisioning_tab= MassProvisioning.isDisplayed();
	
	Assert.assertTrue(MassProvisioning_tab);
}

//**************************** Method to click Mass Provisioning tab *****************************
public void click_MassProvisioningTab() throws InterruptedException
{
	WebElement MassProvisioning=  driver.findElement(By.linkText("Mass Provisioning"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(MassProvisioning).click().build().perform();
	
	Thread.sleep(400);

	//Verify
	
	Boolean verify= driver.findElement(By.id("apexir_SEARCH")).isDisplayed();
	
	Assert.assertTrue(verify);
	
}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	expvalue= "Processed";
	col= "Status";

	view_filtered_report();

	//Verify report
		
	Thread.sleep(400);
		
	String verifyreport= driver.findElement(By.xpath("//tr[2]//td[4]")).getText();
		
	Thread.sleep(300);
		
	Assert.assertEquals(verifyreport, expvalue);
				
	}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 

	save_report();
				 	
	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
		 	 
	
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();	
		
	// verify 

	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
			
}

//**************************************** Method to search primary report ****************************
public void search_by_PrimaryReports() throws InterruptedException
{
	try {

		select_primary_report();
		
		Thread.sleep(300);
		
		WebElement primaryreport= driver.findElement(By.id("87632913247425752"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	} catch(Exception e){
	
		select_primary_report();
		
		Thread.sleep(300);
		
		WebElement primaryreport= driver.findElement(By.id("87632913247425752"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
		
	}

//************************************ Method to test reset button on CIB ******************************
public void reset() throws InterruptedException
{
	Thread.sleep(200);

	Reset();
		 
	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("87632913247425752"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
	
}

//********************************* Method to create mass provisioning record *******************************
public void Create_Record() throws InterruptedException
{
	//Click create

	WebElement CLICK= driver.findElement(By.id("B87635306061425792"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(CLICK).click().build().perform();
	
	Thread.sleep(300);
	
	//Enter description
	
	WebElement description = driver.findElement(By.id("P94_MP_NAME"));
	
	description.clear();
	
	description.sendKeys("Test");
	
	Thread.sleep(300);
		
	//Select number plan
	
	Select numberPlan= new Select(driver.findElement(By.id("P94_MP_NA_UID")));
	
	numberPlan.selectByIndex(2);
	
	//Select rate plan
	
	Select ratePlan= new Select(driver.findElement(By.id("P94_MP_RP_UID")));
	
	ratePlan.selectByIndex(2);
	
	//Select provisioning profile
	
	Select profile= new Select(driver.findElement(By.id("P94_MP_RCP_UID")));
	
	profile.selectByIndex(2);
	
	//Select dealer
	
	Select dealer= new Select(driver.findElement(By.id("P94_MP_D_UID")));
	
	dealer.selectByIndex(2);
	
	//Enter start MSISDN
	
	WebElement MSISDN1= driver.findElement(By.id("P94_MP_START_MSISDN"));
	
	MSISDN1.clear();
	
	MSISDN1.sendKeys(msisdn1);
	
	//Enter End MSISDN
	
	WebElement MSISDN2= driver.findElement(By.id("P94_MP_END_MSISDN"));
	
	MSISDN2.clear();
	
	MSISDN2.sendKeys(msisdn2);
	
	Thread.sleep(200);
		
	//Click create
	
	WebElement Create= driver.findElement(By.id("B87625918013425671"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(Create).click().build().perform();
	
	Thread.sleep(300);
		
	
	//Need more information as fe showing message that this plan doesn't have basic telephony

}
//****************************************** Method to perform delete operation **************************
public void Delete() throws InterruptedException 
{
	//Search record by name 
	
	//enter name to search
	
	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys("test");
	
	Thread.sleep(200);
	
	//Click Go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
		
	//Select record to delete
	
	WebElement select= driver.findElement(By.xpath("//div[@id='apexir_WORKSHEET_REGION']//tr[2]//td[1]//input[1]"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(select).click().build().perform();
	
	Thread.sleep(300);
			
	//Click delete record button
	
	WebElement delete= driver.findElement(By.id("B7511504902537748"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(delete).click().build().perform();
	
	Thread.sleep(300);		
	
	//Accept alert
	
	Alert alert= driver.switchTo().alert();
	
	alert.accept();
	
	Thread.sleep(300);
	
	//Check success msg
	
	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
	
	String Expected_msg= "Record(s) Deleted";
	
	Assert.assertEquals(Msg, Expected_msg);

}

//******************************************* View details of record by clicking on the name *********************
public void ViewDetails() throws InterruptedException
{
    //Click on the name 
		
	WebElement name= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[3]/a[1]"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(name).click().build().perform();
	
	Thread.sleep(400);	
	
	//View details
	
	Boolean verify= driver.findElement(By.xpath("//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']")).isDisplayed();
	
	Assert.assertTrue(verify);	

	}
}
