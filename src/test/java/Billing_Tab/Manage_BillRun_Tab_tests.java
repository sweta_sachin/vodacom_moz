package Billing_Tab;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Manage_BillRun_Tab_tests extends  common_methods.CommonMethods{
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		Thread.sleep(1000);
		click_billingTab();
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
		driver.quit();
	}
	
  @Test
  public void a_ViewManage_BillRun_Tab()
  {
	  view_ManageBillRunTab();
  }
  
  @Test
  public void b_ClickManage_BillRun_Tab() throws InterruptedException
  {
	 Thread.sleep(1000);
	  click_ManageBillRunTab();
  }
  
   @Test
 	public void c_Filter_report() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		View_filtered_report();
 	}
 	
 	@Test
 	public void d_Save_Report() throws InterruptedException
 	{
 		Thread.sleep(1500); 
 		Save_report();
 	}
 	
 	@Test
 	public void e_Search_by_privateReport() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		search_by_private_report();
 	}
 	
 	@Test
 	public void f_search_By_Primary_report() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		search_by_PrimaryReports();
 	}
 	
 	@Test
 	public void g_flashBackTest() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		flashback_filter();
 	}
 	
 	@Test
 	public void h_reset_test() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		reset();
 	}
 	
 	@Test
 	public void i_DownloadCSV() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","bill_runs.csv"));
 	}
 	
 	@Test
	public void j_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","bill_runs.htm"));
	}
	
	@Test
	public void k_emailReport() throws InterruptedException
	{
		Thread.sleep(300);
		email_report();
	}
	
	 @Test
	  public void l_ViewAllTestBillRuns() throws InterruptedException
	  {
		 Thread.sleep(500); 
		 view_allTestBillRuns();
	  }
	  
	  @Test
	  public void m_ViewAllFullBillRuns() throws InterruptedException
	  {
		  Thread.sleep(500); 
		  view_allFullBillRuns();
	  }
	  
	  @Test
	  public void n_ViewAutoTestRefreshBillRuns() throws InterruptedException
	  {
		  Thread.sleep(500); 
		  view_autoTestRefreshBillRuns();
	  }
	  
	  
	  @Test
	  public void o_ViewInstantBill_AccountBillRun() throws InterruptedException
	  {
		  Thread.sleep(500); 
		  view_instantBillAccount_Billrun();
	  }
	  

	  @Test
	  public void p_ViewInstantBill_SubscriberBillRun() throws InterruptedException
	  {
		  Thread.sleep(500); 
		  view_instantBillSubscriber_Billrun();
	  }
	  
	 @Test
	  public void q_CreateTestBillRun() throws InterruptedException, ParseException
	  {
		 Thread.sleep(500); 
		 Create_Test_Billrun();
	  }
	  
	  	  
	  @Test
	  public void r_CreateAutoTestRefreshBillRun() throws InterruptedException, ParseException
	  {
		  Thread.sleep(500); 
		  Create_AutoTestRefresh_Billrun();
	  }
	
	@Test
	  public void s_CreateFullBillRun() throws InterruptedException, ParseException
	  {
		Thread.sleep(500);   
		Create_Full_Billrun();
	  }
	 
	  @Test
	  public void t_CreateInstantBillRun_Account() throws InterruptedException, ParseException
	  {
		  Thread.sleep(500); 
		  Create_InstantBillRun_Account();
	  }
	  
	  @Test
	  public void u_CreateInstantBillRun_Subscriber() throws InterruptedException, ParseException
	  {
		  Thread.sleep(500); 
		  Create_InstantBillRun_Subscriber();
	  }
	  
	
  
///////////////////////////////////////////////////////////////////////////////////////////////////

  
  //**************************** Method to view Manage Bill run tab *****************************
public void view_ManageBillRunTab()
{
	WebElement Manage=  driver.findElement(By.linkText("Manage Bill Runs"));
	
	Boolean ManageBillrun_tab= Manage.isDisplayed();
	
	Assert.assertTrue(ManageBillrun_tab);
}

//**************************** Method to click manage bill run tab *****************************
public void click_ManageBillRunTab()
{
	WebElement Manage=  driver.findElement(By.linkText("Manage Bill Runs"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Manage).click().build().perform();
}

//**************************** Method to view All Test Bill runs *****************************
public void view_allTestBillRuns() throws InterruptedException
{
	Select dropdown= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
	
	dropdown.selectByVisibleText("Test Bill Run");
	
	Thread.sleep(1200);
	
	WebElement type=  driver.findElement(By.xpath("//tr[2]//td[3]"));
	
	String Actual= type.getText();
	
	String Expected= "Test Bill Run";
	
	Assert.assertEquals(Actual, Expected);
}

//**************************** Method to view All Full Bill runs *****************************
public void view_allFullBillRuns() throws InterruptedException
{
	Select dropdown= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
	
	dropdown.selectByVisibleText("Full Bill Run");
	
	Thread.sleep(1200);
	
	WebElement type=  driver.findElement(By.xpath("//tr[2]//td[3]"));
	
	String Actual= type.getText();
	
	String Expected= "Full Bill Run";
	
	Assert.assertEquals(Actual, Expected);
}

//**************************** Method to view Auto test refresh Bill runs *****************************
public void view_autoTestRefreshBillRuns() throws InterruptedException
{
	Select dropdown= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
	
	dropdown.selectByVisibleText("Auto Test Refresh Bill Run");
	
	Thread.sleep(1200);
	
	WebElement type=  driver.findElement(By.xpath("//tr[2]//td[3]"));
	
	String Actual= type.getText();
	
	String Expected= "Auto Test Refresh Bill Run";
	
	Assert.assertEquals(Actual, Expected);
}

//**************************** Method to view Instant Bill- Account Bill runs *****************************
public void  view_instantBillAccount_Billrun() throws InterruptedException
{
	Select dropdown= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
	
	dropdown.selectByVisibleText("Instant Bill - Account");
	
	Thread.sleep(1200);
	
	WebElement type=  driver.findElement(By.xpath("//tr[2]//td[3]"));
	
	String Actual= type.getText();
	
	String Expected= "Instant Bill - Account";
	
	Assert.assertEquals(Actual, Expected);
}


//**************************** Method to view Instant Bill- subscriber Bill runs *****************************
public void  view_instantBillSubscriber_Billrun() throws InterruptedException
{
	Select dropdown= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
	
	dropdown.selectByVisibleText("Instant Bill - Subscriber");
	
	Thread.sleep(1200);
	
	WebElement type=  driver.findElement(By.xpath("//tr[2]//td[3]"));
	
	String Actual= type.getText();
	
	String Expected= "Instant Bill - Subscriber";
	
	Assert.assertEquals(Actual, Expected);
}

//**************************** Method to click create button *****************************
public void click_createButton()
{
	  WebElement create=  driver.findElement(By.linkText("Create"));
	
	  Actions act= new Actions(driver);
	  
	  act.moveToElement(create).click().build().perform();
}

//**************************** Method to create Test Bill run *****************************
public void Create_Test_Billrun() throws InterruptedException, ParseException
{
	click_createButton();
	
	Select dropdown= new Select(driver.findElement(By.id("P18_BILL_RUN_TYPE")));
	
	dropdown.selectByVisibleText("Test Bill Run");
	
	Thread.sleep(800);
	
	//Select Bill Cycle 
	
	Select billcycle= new Select(driver.findElement(By.id("P18_BBR_BBC_UID")));
	
	billcycle.selectByIndex(1);
	
	Thread.sleep(800);
		
	click_createButton();
	
	Thread.sleep(800);
		
	//Select bill run	
	
	Select dropdown1= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
	
	dropdown1.selectByVisibleText("Test Bill Run");
	
	Thread.sleep(800);
	
	//Get created date stamp	
	
	WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[12]"));
	
	String Date= datestamp.getText();
	
	System.out.println(Date);
	
	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	
	Date date = new Date();
	
	String date1= dateFormat.format(date);
	
	Date Date2 = dateFormat.parse(Date);
	
	String date2= dateFormat.format(Date2);
	
	System.out.println(date1);
	
	System.out.println(date2);
	
	Assert.assertEquals(date2, date1);
	
}
//**************************** Method to create Full Bill run *****************************
public void Create_Full_Billrun() throws InterruptedException, ParseException
{
	Thread.sleep(800);
	
	click_createButton();
	
	Select dropdown= new Select(driver.findElement(By.id("P18_BILL_RUN_TYPE")));
	
	dropdown.selectByVisibleText("Full Bill Run");
	
	Thread.sleep(800);
	
	//Select Bill Cycle 
	
	Select billcycle= new Select(driver.findElement(By.id("P18_BBR_BBC_UID")));
	
	billcycle.selectByIndex(2);
	
	Thread.sleep(800);
	  
	//Select Workflow
	
	Select workflow= new Select(driver.findElement(By.id("P18_WF_UID")));
	
	workflow.selectByVisibleText("Create Bill Run Workflow");
	
	click_createButton();
	
	Thread.sleep(800);
	  
	//Click Home
	
	WebElement home= driver.findElement(By.linkText("Home"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(home).click().build().perform();
	
	Thread.sleep(500);
	  
	//Click manage bill run tab
	
	click_ManageBillRunTab();
	
	Thread.sleep(300);
	
	Select dropdown1= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
	
	dropdown1.selectByVisibleText("Full Bill Run");
	
	Thread.sleep(800);
	
	WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[12]"));
	
	String Date= datestamp.getText();
	
	System.out.println(Date);
	
	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	
	Date date = new Date();
	
	String date1= dateFormat.format(date);
	
	Date Date2 = dateFormat.parse(Date);
	
	String date2= dateFormat.format(Date2);
	
	System.out.println(date1);
	
	System.out.println(date2);
	
	Assert.assertEquals(date2, date1);
	
}

//**************************** Method to create Auto Test Refresh Bill run *****************************
public void Create_AutoTestRefresh_Billrun() throws InterruptedException, ParseException
{
	click_createButton();

	Select dropdown= new Select(driver.findElement(By.id("P18_BILL_RUN_TYPE")));
	
	dropdown.selectByVisibleText("Auto Test Refresh Bill Run");
	
	Thread.sleep(800);
	
	//Select Bill Cycle 
	
	Select billcycle= new Select(driver.findElement(By.id("P18_BBR_BBC_UID")));
	
	billcycle.selectByIndex(1);
	
	Thread.sleep(800);
		
	click_createButton();
	
	Thread.sleep(800);
	  
	Select dropdown1= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
	
	dropdown1.selectByVisibleText("Auto Test Refresh Bill Run");
	
	Thread.sleep(800);
	  
	WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[12]"));
	
	String Date= datestamp.getText();
	
	System.out.println(Date);
	
	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	
	Date date = new Date();
	
	String date1= dateFormat.format(date);
	
	Date Date2 = dateFormat.parse(Date);
	
	String date2= dateFormat.format(Date2);
	
	System.out.println(date1);
	
	System.out.println(date2);
	
	Assert.assertEquals(date2, date1);
	
}


//**************************** Method to create Instant Bill - Account *****************************
public void Create_InstantBillRun_Account() throws InterruptedException, ParseException
{

	click_createButton();
	
	Select dropdown= new Select(driver.findElement(By.id("P18_BILL_RUN_TYPE")));
	
	dropdown.selectByVisibleText("Instant Bill - Account");
	
	Thread.sleep(800);
	
	//Click search

	WebElement search= driver.findElement(By.id("P18_SEARCH"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(search).click().build().perform();
	
	Thread.sleep(800);
		
	//Select the account to be added
	
	WebElement subscriber= driver.findElement(By.xpath("//div[@id='apexir_WORKSHEET_REGION']//tr[2]//td[1]//input[1]"));
	
	subscriber.click();
		
	//Click "Show selected account button"
	
	WebElement show_subscriber= driver.findElement(By.id("B43596132037641083"));
	
	act.moveToElement(show_subscriber).click().build().perform();
	
	Thread.sleep(800);
		
	click_createButton();
		
	Select dropdown1= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
	
	dropdown1.selectByVisibleText("Instant Bill - Account");
	
	WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[13]"));
	
	String Date= datestamp.getText();
	
	System.out.println(Date);
	
	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	
	Date date = new Date();
	
	String date1= dateFormat.format(date);
	
	Date Date2 = dateFormat.parse(Date);
	
	String date2= dateFormat.format(Date2);
	
	System.out.println(date1);
	
	System.out.println(date2);
	
	Assert.assertEquals(date2, date1);
	
}

//**************************** Method to create Instant Bill - Subscriber *****************************
public void Create_InstantBillRun_Subscriber() throws InterruptedException, ParseException
{
	click_createButton();

	Select dropdown= new Select(driver.findElement(By.id("P18_BILL_RUN_TYPE")));
	
	dropdown.selectByVisibleText("Instant Bill - Subscriber");
	
	Thread.sleep(800);
		 
	//Click search
	
	WebElement search= driver.findElement(By.id("P18_SEARCH"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(search).click().build().perform();
	
	Thread.sleep(800);
		
	//Select the subscriber to be added
	
	WebElement subscriber= driver.findElement(By.xpath("//div[@id='apexir_WORKSHEET_REGION']//tr[2]//td[1]//input[1]"));
	
	subscriber.click();
	
	//Click "Show selected subscriber button"
	
	WebElement show_subscriber= driver.findElement(By.id("B198686574560852010"));
	
	act.moveToElement(show_subscriber).click().build().perform();
	
	Thread.sleep(800);
	
	click_createButton();
	
	Select dropdown1= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
	
	dropdown1.selectByVisibleText("Instant Bill - Subscriber");
	
	WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[12]"));
	
	String Date= datestamp.getText();
	
	System.out.println(Date);
	
	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	
	Date date = new Date();
	
	String date1= dateFormat.format(date);
	
	Date Date2 = dateFormat.parse(Date);
	
	String date2= dateFormat.format(Date2);
	
	System.out.println(date1);
	
	System.out.println(date2);
	
	Assert.assertEquals(date2, date1);
	
}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	expvalue= "Full Bill Run";
	col= "Bill Run Type";
	
	view_filtered_report();
	
	//Verify report

	Thread.sleep(400);
	
	String verifyreport= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[3]")).getText();
	
	Thread.sleep(300);
	
	Assert.assertEquals(verifyreport, expvalue);
	
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 

	save_report();
	
	 // Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
		
	}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
		Search_by_private_report();
		
		// verify 
	
		Thread.sleep(300);
		
		String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
		
		System.out.println(text);
		
		Assert.assertEquals(text, verifyreport);
				
	}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
	try {
		
		select_primary_report();
		
		Thread.sleep(300);
		
		WebElement primaryreport= driver.findElement(By.id("97746306507379217"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	} catch(Exception e){
	
		select_primary_report();
		
		Thread.sleep(300);
		
		WebElement primaryreport= driver.findElement(By.id("97746306507379217"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}
	
}

//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
{
	Thread.sleep(200);
	
	Reset();
	 
	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("97746306507379217"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
	
}
}

