package Billing_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Sim_Receipt_Tab_tests extends common_methods.CommonMethods{
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		Thread.sleep(1000);
		click_billingTab();
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
		driver.quit();
	}
	
  @Test
  public void a_View_SIMReceipt_Tab()
  {
	  view_SIMReceiptTab();
  }
  
  @Test
  public void b_Click_SIMReceipt_Tab() throws InterruptedException
  {
	 Thread.sleep(1000);
	  click_SIMReceiptTab();
  }
  
   @Test
 	public void c_Filter_report() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		View_filtered_report();
 	}
 	
 	@Test
 	public void d_Save_Report() throws InterruptedException
 	{
 		Thread.sleep(1500); 
 		Save_report();
 	}
 	
 	@Test
 	public void e_Search_by_privateReport() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		search_by_private_report();
 	}
 	
 	@Test
 	public void f_search_By_Primary_report() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		search_by_PrimaryReports();
 	}
 	
 	@Test
 	public void g_flashBackTest() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		flashback_filter();
 	}
 	
 	@Test
 	public void h_reset_test() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		reset();
 	}
 	
 	@Test
 	public void i_DownloadCSV() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","sim_receipt.csv"));
 	}
 	
	@Test
	public void j_Subscdription_email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
	}
	
	@Test
	public void k_LoadFile() throws InterruptedException
	{
		Thread.sleep(300);	
		LoadFile();
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//**************************** Method to view SIM Receipt tab *****************************
	public void view_SIMReceiptTab()
	{
		WebElement SIMReceipt=  driver.findElement(By.linkText("SIM Receipt"));
		
		Boolean SIMReceipt_tab= SIMReceipt.isDisplayed();
		
		Assert.assertTrue(SIMReceipt_tab);
	}

//**************************** Method to click SIM Receipt tab *****************************
	public void click_SIMReceiptTab()
	{
		WebElement SIMReceipt=  driver.findElement(By.linkText("SIM Receipt"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(SIMReceipt).click().build().perform();
	}
	
//************************************** Method to apply filter and view filtered report **********************
	public void View_filtered_report() throws InterruptedException
	{
		expvalue= "Processed";
		col= "Status";
		
		view_filtered_report();

		//Verify report
		
		Thread.sleep(400);
		
		String verifyreport= driver.findElement(By.xpath("//div[@id='apexir_DATA_PANEL']//tr[2]//td[3]")).getText();
		
		Thread.sleep(300);
		
		Assert.assertEquals(verifyreport, expvalue);
		
	}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
	{   
		report= "1. Test report"; 
	
		save_report();
		
		// Verify saved report
		
		Thread.sleep(500);
		
		Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
		
		dropdown.selectByVisibleText(report);
		
	}

//**************************************** Method to search by private report ****************************

public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();	
	
	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);

	Assert.assertEquals(text, verifyreport);
					
	}

//**************************************** Method to search primary report ****************************
public void search_by_PrimaryReports() throws InterruptedException
{
	try {
		select_primary_report();
		
		Thread.sleep(300);
		
		WebElement primaryreport= driver.findElement(By.id("2599418561307676"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	} catch(Exception e){
	
		select_primary_report();
		
		Thread.sleep(300);
		
		WebElement primaryreport= driver.findElement(By.id("2599418561307676"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
		
	}

//************************************ Method to test reset button on CIB ******************************
public void reset() throws InterruptedException
	{
		Thread.sleep(200);
		
		Reset();
		 
		// Verify report
		
		WebElement primaryreport= driver.findElement(By.id("2599418561307676"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}

//************************************************ Method to load file *********************************
public void LoadFile()
{
	//Click Load file
	
	WebElement loadFile= driver.findElement(By.id("B2601503410332520"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(loadFile).click().build().perform();
		
	//Need file format and data for further options
	
	}

}
