package Billing_Tab;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ESN_Maintenance_Tab_tests extends common_methods.CommonMethods{
	

	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		Thread.sleep(1000);
		click_billingTab();
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() 
	{
		driver.quit();
	}
	
  @Test
  public void a_View_ESN_Maintenance_Tab()
  {
	  view_ESN_MaintenanceTab();
  }
  
  @Test
  public void b_Click_ESN_Maintenance_Tab() throws InterruptedException
  {
	 Thread.sleep(1000);
	  click_ESN_MaintenanceTab();
  }
  
   @Test
 	public void c_Filter_report() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		View_filtered_report();
 	}
 	
 	@Test
 	public void d_Save_Report() throws InterruptedException
 	{
 		Thread.sleep(1500); 
 		Save_report();
 	}
 	
 	@Test
 	public void e_Search_by_privateReport() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		search_by_private_report();
 	}
 	
 	@Test
 	public void f_search_By_Primary_report() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		search_by_PrimaryReports();
 	}
 	
 	@Test
 	public void g_flashBackTest() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		flashback_filter();
 	}
 	
 	@Test
 	public void h_reset_test() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		reset();
 	}
 	
 	@Test
 	public void i_DownloadCSV() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","all_esn's.csv"));
 	}
 	
	@Test
	public void j_Subscdription_email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
	}
	
	@Test
	public void k_Create_ESNRecord() throws InterruptedException
	{
		Thread.sleep(1300);
		Create_ESNRecord();
	}
	
	@Test
	public void l_verifyCreated_ESNRecord() throws InterruptedException
	{
		Thread.sleep(1300);
		Verify_created();
	}
	
	@Test
	public void m_Edit_ESNRecord() throws InterruptedException
	{
		Thread.sleep(1300);
		EditESNRecord();
	}
	
	@Test
	public void n_VerifyEdited_ESNRecord() throws InterruptedException
	{
		Thread.sleep(1300);
		 Verify_edited();
		
	}
	
	@Test
	public void o_Delete_ESNRecord() throws InterruptedException
	{
		Thread.sleep(1300);
		Delete_ESN();
	}
	
	@Test
	public void p_VerifyDeleted_ESNRecord() throws InterruptedException
	{
		Thread.sleep(1300);
		Verify_Deletion();
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//**************************** Method to view ESN Maintenance tab *****************************
	public void view_ESN_MaintenanceTab()
	{
		WebElement ESN_Maintenance=  driver.findElement(By.linkText("ESN Maintenance"));
		
		Boolean ESN_Maintenance_tab= ESN_Maintenance.isDisplayed();
		
		Assert.assertTrue(ESN_Maintenance_tab);
	}

//**************************** Method to click  ESN Maintenance tab *****************************
	public void click_ESN_MaintenanceTab()
	{
		WebElement ESN_Maintenance=  driver.findElement(By.linkText("ESN Maintenance"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(ESN_Maintenance).click().build().perform();
	}
	
//************************************** Method to apply filter and view filtered report **********************
	public void View_filtered_report() throws InterruptedException
	{
		expvalue= "Pending";
		col= "Status";
		
		view_filtered_report();

		//Verify report
		
		Thread.sleep(400);
		
		String verifyreport= driver.findElement(By.xpath("//tr[2]//td[3]")).getText();
		
		Thread.sleep(300);
		
		Assert.assertEquals(verifyreport, expvalue);
			
	}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
	{
		report= "1. Test report"; 
		
		save_report();
		 	
		 // Verify saved report
		
		Thread.sleep(500);
		
		Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
		
		dropdown.selectByVisibleText(report);
		 	 
		}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
	{
		Search_by_private_report();	
		
		// verify 
		
		Thread.sleep(300);
		
		String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
		
		System.out.println(text);
		
		Assert.assertEquals(text, verifyreport);
			
		}

//**************************************** Method to search primary report ****************************
public void search_by_PrimaryReports() throws InterruptedException
	{
		try {
			select_primary_report();
			
			Thread.sleep(300);
			
			WebElement primaryreport= driver.findElement(By.id("3707818791699916"));
			
			Boolean report= primaryreport.isDisplayed();
			
			Assert.assertTrue(report);
			
		} catch(Exception e){
		
			select_primary_report();
			
			Thread.sleep(300);
			
			WebElement primaryreport= driver.findElement(By.id("3707818791699916"));
			
			Boolean report= primaryreport.isDisplayed();
			
			Assert.assertTrue(report);
		}
		
	}

//************************************ Method to test reset button on CIB ******************************
public void reset() throws InterruptedException
	{
		Thread.sleep(200);
		
		Reset();
		 
		// Verify report
		
		WebElement primaryreport= driver.findElement(By.id("3707818791699916"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}

//******************************************* Method to create ESN Record ******************************
public void Create_ESNRecord() throws InterruptedException
{
	//click create
	
	WebElement CLICK= driver.findElement(By.id("B3708802094699920"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(CLICK).click().build().perform();
	
	Thread.sleep(300);
	
	//Enter ESN
	
	WebElement esn = driver.findElement(By.id("P3_EI_ESN"));
	
	esn.clear();
	
	esn.sendKeys(ESNname);
	
	Thread.sleep(300);
	
	//Click Create
	
	WebElement Create= driver.findElement(By.id("B3703211094699887"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(Create).click().build().perform();
	
	Thread.sleep(300);
		
	//Check success message
	
	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
	
	String Expected_msg= "Action Processed.";
	
	Assert.assertEquals(Msg, Expected_msg);
  }



//*********************************** Method to verify created ESN record ************************************
public void Verify_created() throws InterruptedException
{
	//enter name to search
	
	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(ESNname);
	
	Thread.sleep(200);
		
	//Click Go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
		
	//verify search
	
	String text1= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
	
	Assert.assertEquals(text1, ESNname);
 }

//************************************ Method to edit ESN record **************************************
public void EditESNRecord() throws InterruptedException
{
	//Click edit
	
	WebElement edit= driver.findElement(By.xpath("//tr[@class='even']//td//a//img"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();
	
	Thread.sleep(300);
			
	//Change name
	
	WebElement category= driver.findElement(By.id("P3_EI_ESN"));
	
	category.clear();
	
	category.sendKeys(ESNname1);
			
	//Click Apply changes
	
	WebElement apply= driver.findElement(By.id("B3703326488699887"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(apply).click().build().perform();
	
	Thread.sleep(300);
	
	
	// Check success message
	
	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
	
	
	String Expected_msg= "Action Processed.";
	
	Assert.assertEquals(Msg, Expected_msg);
	
   }

//******************************************** Method to verify edited ESN record**********************************
public void Verify_edited() throws InterruptedException
{
	//reset all filters

	Reset();
	
	Thread.sleep(500);
	 	
	//enter name to search
	
	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(ESNname1);
	
	Thread.sleep(500);
		
	//Click Go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
		
	//verify search
	
	String text1= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
	
	Assert.assertEquals(text1, ESNname1);
}

//************************************************* Method to delete ESN record *******************************
public void Delete_ESN() throws InterruptedException
{
	//Click edit
	
	WebElement edit= driver.findElement(By.xpath("//tr[@class='even']//td//a//img"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();
	
	Thread.sleep(500);
	
	//Click delete
	
	WebElement delete= driver.findElement(By.id("B3703426583699887"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(delete).click().build().perform();
	
	Thread.sleep(300);
		
	//Accept alert
	
	Alert alert= driver.switchTo().alert();
		
	alert.accept();
		
	// Check success message

	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
	
	String Expected_msg= "Action Processed.";
	
	Assert.assertEquals(Msg, Expected_msg);
 }

//*********************************************** Verify Deletion *******************************
public void Verify_Deletion() throws InterruptedException
{
	//enter name to search
	
	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(ESNname1);
	
	Thread.sleep(500);
			
	//Click Go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
	
	//verify search
	
	Boolean text1= driver.findElement(By.xpath("//span[@id='apexir_NO_DATA_FOUND_MSG']")).isDisplayed();
	
	Assert.assertTrue(text1);
    
}
}
