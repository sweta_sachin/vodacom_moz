package Credit_Control_Tab;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class Create_Customer  extends common_methods.CommonMethods {
	
	public static String VAT= "400065470";
	
	public static String firstname= "first name";
	
	public static String AccountName= "test name";
	
	public static String creditlimit= "200";
	
	public static String cnumber= "258222";
	
	public static String Cname= "new name";
	
	public static String date= "11-Jul-2019";
	
	@BeforeClass
	public void testCreateCustomer() throws InterruptedException
	{
		Login();
		Thread.sleep(1000);
		clickCreditControlTab();
		Thread.sleep(1000);
		ClickCreateCustomer();
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
		driver.manage().timeouts().implicitlyWait(2,TimeUnit.SECONDS);
		driver.quit();
	}
	
	@Test
	public void a_SelectAccount_Type()
	{
		SelectAccountType();
	}
	
	@Test
	public void b_EnterAccountName() throws InterruptedException
	{
		Thread.sleep(500);
		EnterAccountName();
		
	}
	
	@Test
	public void c_Enter_Company_Registration_Date()
	{
		Enter_companyRegistration_Date();
	}
	
	
	@Test
	public void d_SelectCreditController() throws InterruptedException
	{
		Thread.sleep(800);
		SelectCreditController();
	}
	
	@Test
	public void e_SelectTaxRate() throws InterruptedException
	{
		Thread.sleep(500);
		SelectTaxRate();
	}
	
	
	@Test
	public void f_SelectStatementType() throws InterruptedException
	{
		Thread.sleep(500);
		SelectStatementType();
	}
	
	@Test
	public void g_Enter_Credit_Limit()
	{
		Enter_credit_Limit();
	}
	

	@Test
	public void h_Enter_VATorRegNumber()
	{
		Enter_VAT_Number();
	}
	
	@Test
	public void i_Select_Region()
	{
		SelectRegion();
	}
		
	@Test
	public void j_SelectdeliveryAddress() throws InterruptedException
	{
		Thread.sleep(500);
		SelectdeliveryAddress();
	}
	
	@Test
	public void k_EnterAddress() throws InterruptedException
	{
		Thread.sleep(500);
		EnterAddress();
	}
		
	@Test
	public void l_SelectBillCycle() throws InterruptedException
	{
		Thread.sleep(500);
		SelectBillCycle();
	}
	
	@Test
	public void m_SelectAutoBarRules() throws InterruptedException
	{
		Thread.sleep(500);
		SelectAutoBarRules();
	}
	
	
	@Test
	public void n_SelectBillExtractCycle() throws InterruptedException
	{
		Thread.sleep(500);
		SelectBillExtractCycle();
	}
	
	
	@Test
	public void o_SelectTitile()
	{
	    Select dropdown= new Select(driver.findElement(By.id("P40_BCD_T_UID")));
	    dropdown.selectByIndex(2);
	    
	}
	
	@Test
	public void p_EnterContactName() throws InterruptedException
	{
		Thread.sleep(500);
		EnterContactName();
	}
	
		
	@Test
	public void q_EnterContactNumber() throws InterruptedException
	{
		Thread.sleep(500);
		EnterContactNumber();
	}
	
	@Test
	public void r_SelectBank() throws InterruptedException
	{
		Thread.sleep(500);
		SelectBank();
	}
		
	@Test
	public void s_Select_Provience()
	{
		SelectPronvince();
	}
	
	
	@Test
	public void t_Select_subAccountProfile()
	{
		SelectSubAccountProfile();
	}
	
	
	@Test
	public void u_EnterCompanyName() throws InterruptedException
	{
		Thread.sleep(500);
		
		WebElement Account_name= driver.findElement(By.id("P40_BCD_COMPANY_NAME"));
		Account_name.sendKeys("Company A");
	}
	
	
	@Test
	public void v_Select_District()
	{
		SelectDistrict();
	}
	
	@Test
	public void w_ClickApplyChanges() throws InterruptedException
	{ 
		Thread.sleep(500);
		ClickApplyChanges();
	}

	@Test
	public void x_success() throws InterruptedException
	{
		Thread.sleep(1000);
		success();
	} 
	
	@Test
	public void y_Verify_created_Customer()
	{
		verify_created_customer();
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

//************************** Method to enter account name ****************************
public void EnterAccountName()
{
	try {
			WebElement Account_name= driver.findElement(By.id("P40_BCD_NAME"));
	
			Account_name.sendKeys(AccountName);
		
	}catch (NoSuchElementException| StaleElementReferenceException e)	{
	
		WebElement Account_name= driver.findElement(By.id("P40_BCD_NAME"));
		
		Account_name.sendKeys(AccountName);
		
	}
}

//************************** Method to enter contact number ***************************
public void EnterContactNumber()
{
	try {
	
		WebElement Contact_number= driver.findElement(By.id("P40_BCD_TELE_NO1"));
		
		Contact_number.sendKeys(cnumber);
		
	}catch (NoSuchElementException| StaleElementReferenceException e)	{
	
		WebElement Contact_number= driver.findElement(By.id("P40_BCD_TELE_NO1"));
		
		Contact_number.sendKeys(cnumber);
		
	}
	}

//*************************** Method to select credit controller *******************
public void SelectCreditController()
{ 
	try {
			
		Select creditController= new Select (driver.findElement(By.id("P40_BCD_CRED_UID")));
		
		creditController.selectByIndex(2);
	
	}catch (NoSuchElementException| StaleElementReferenceException e){
			
		Select creditController= new Select (driver.findElement(By.id("P40_BCD_CRED_UID")));
		
		creditController.selectByIndex(2);
	}
}

//*************************** Method to select tax rate *******************************
public void SelectTaxRate()
{
	try {
		
		Select TaxRate= new Select (driver.findElement(By.id("P40_BCD_TR_UID")));
		
		TaxRate.selectByIndex(1);
		
	}catch (NoSuchElementException| StaleElementReferenceException e)	{
		
		Select TaxRate= new Select (driver.findElement(By.id("P40_BCD_TR_UID")));
		
		TaxRate.selectByIndex(1);
	}
}
//*************************** Method to select statement type ***************************	
public void SelectStatementType()
{   
	try {
			Select Statement= new Select (driver.findElement(By.id("P40_BCD_STATEMENT_TYPE")));
			
			Statement.selectByIndex(1);
		
	}catch(NoSuchElementException| StaleElementReferenceException e){
	
		Select Statement= new Select (driver.findElement(By.id("P40_BCD_STATEMENT_TYPE")));
		
		Statement.selectByIndex(1);
	}
}

//*************************** Method to select delivery address ***************************
public void SelectdeliveryAddress()
{
	try {
		
		driver.findElement(By.id("P40_DELIVERY_ADDRESS_0")).click();
    	
	}catch(NoSuchElementException| StaleElementReferenceException e) {
    
		driver.findElement(By.id("P40_DELIVERY_ADDRESS_0")).click();	
    	
	}
	}
	
//********************************* Method to enter address *********************************
public void EnterAddress()
{ 
	try {
		driver.findElement(By.id("P40_ADDRESS_LEFT_1")).sendKeys("xyz");
		
		driver.findElement(By.id("P40_ADDRESS_LEFT_2")).sendKeys("North Riding");
		
		driver.findElement(By.id("P40_ADDRESS_LEFT_3")).sendKeys("Johannesburg");
		
		driver.findElement(By.id("P40_ADDRESS_LEFT_4")).sendKeys("Gauteng");
		
		driver.findElement(By.id("P40_ADDRESS_LEFT_5")).sendKeys("2113");
	
	}catch(NoSuchElementException| StaleElementReferenceException e)	{
			
		driver.findElement(By.id("P40_ADDRESS_LEFT_1")).sendKeys("xyz");
		
		driver.findElement(By.id("P40_ADDRESS_LEFT_2")).sendKeys("North Riding");
		
		driver.findElement(By.id("P40_ADDRESS_LEFT_3")).sendKeys("Johannesburg");
		
		driver.findElement(By.id("P40_ADDRESS_LEFT_4")).sendKeys("Gauteng");
		
		driver.findElement(By.id("P40_ADDRESS_LEFT_5")).sendKeys("2113");
	}
	
}

//*************************** Method to select Bill cycle ***********************************
public void SelectBillCycle()
{
	try {
	
		Select BillCycle= new Select (driver.findElement(By.id("P40_BCD_BBC_UID")));
		
		BillCycle.selectByIndex(1);
		
	}catch(NoSuchElementException| StaleElementReferenceException e){
	
		Select BillCycle= new Select (driver.findElement(By.id("P40_BCD_BBC_UID")));
		
		BillCycle.selectByIndex(1);
	}
}

//*************************** Method to select Autobar rule ********************************
public void SelectAutoBarRules()
{
	try {
		
		Select AutoBar= new Select (driver.findElement(By.id("P40_BCD_ABR_UID")));
		
		AutoBar.selectByVisibleText("No Barring");
	
	}catch (NoSuchElementException| StaleElementReferenceException e) {
	
		Select AutoBar= new Select (driver.findElement(By.id("P40_BCD_ABR_UID")));
		
		AutoBar.selectByVisibleText("No Barring");
	}
}

//*************************** Method to select Bill extract cycle *****************************
public void SelectBillExtractCycle()
{
	try {
		
		Select BillExtractCycle= new Select (driver.findElement(By.id("P40_BCD_EXC_UID")));
		
		BillExtractCycle.selectByIndex(1);
		
	}catch (NoSuchElementException| StaleElementReferenceException e){
	
		Select BillExtractCycle= new Select (driver.findElement(By.id("P40_BCD_EXC_UID")));
		
		BillExtractCycle.selectByIndex(1);
	}
}

//*************************** Method to select Account type ************************************
public void SelectAccountType()
{  
	try {
		
		Select AccountType= new Select (driver.findElement(By.id("P40_BCD_ACCT_UID")));
		
		AccountType.selectByIndex(1);
	
	} catch (NoSuchElementException| StaleElementReferenceException e)	{
	
		Select AccountType= new Select (driver.findElement(By.id("P40_BCD_ACCT_UID")));
		
		AccountType.selectByIndex(1);
	}
	
}

//********************************** Method to enter contact name  ************************************
public void EnterContactName()
{
	try {
		
		driver.findElement(By.id("P40_BCD_CONTACT_NAME")).sendKeys(Cname);
		
	}catch (NoSuchElementException| StaleElementReferenceException e){
	
		driver.findElement(By.id("P40_BCD_CONTACT_NAME")).sendKeys(Cname);
	}
}

//********************************* Method to select Bank ****************************************
public void SelectBank()
{
	try {
		
		Select Bank= new Select (driver.findElement(By.id("P40_BCD_B_UID")));
		
		Bank.selectByVisibleText("CASH");
		
	}catch (NoSuchElementException| StaleElementReferenceException e)	{
	
		Select Bank= new Select (driver.findElement(By.id("P40_BCD_B_UID")));
		
		Bank.selectByVisibleText("CASH");
	}
}

//*************************** Method to click apply changes **********************************
public void ClickApplyChanges()
{ 
	try {

		driver.findElement(By.id("B13363654567673223")).click();
		
	}catch (NoSuchElementException| StaleElementReferenceException e){
	
		driver.findElement(By.id("B13363654567673223")).click();
		
	}
 }

//*************************** Method to check success message **********************************
public void success()
{
	WebElement Cdetails= driver.findElement(By.linkText("Customer Address Details"));
	
	Boolean CDetails= Cdetails.isDisplayed();
	
	Assert.assertTrue(CDetails);
}

//******************************** Method to enter company registration date ********************
public void Enter_companyRegistration_Date()
 {
	WebElement Cdate= driver.findElement(By.id("P40_BCD_COMPANY_REG_DATE"));
	
	Cdate.clear();
	
	Cdate.sendKeys(date);
	
 }


//******************************** Method to enter credit limit ********************
public void Enter_credit_Limit()
{
	WebElement Climit= driver.findElement(By.id("P40_BCD_CREDIT_LIMIT"));

	Climit.clear();
	
	Climit.sendKeys(creditlimit);
	
}

//******************************** Method to enter VAT/Registration number ********************
public void Enter_VAT_Number()
{
	WebElement vat= driver.findElement(By.id("P40_BCD_VAT_REG_NO"));
	
	vat.clear();
	
	vat.sendKeys(VAT);
 }

//********************************* Method to select region ****************************************
public void SelectRegion()
{
	try {
	
		Select region= new Select (driver.findElement(By.id("P40_BCD_CMR_UID1")));
		
		region.selectByIndex(2);
		
	}catch (NoSuchElementException| StaleElementReferenceException e)	{
	
		Select region= new Select (driver.findElement(By.id("P40_BCD_CMR_UID1")));
		
		region.selectByIndex(2);
		
	}
}

//********************************* Method to select Province  ****************************************
public void SelectPronvince()
{
	try	{		
		Select Prov= new Select (driver.findElement(By.id("P40_BCD_CMR_UID2")));
		
		Prov.selectByIndex(2);
		
	}catch (NoSuchElementException| StaleElementReferenceException e) {
		
		Select Prov= new Select (driver.findElement(By.id("P40_BCD_CMR_UID2")));
		
		Prov.selectByIndex(2);
	}
}

//********************************* Method to select sub account profile  ****************************************
public void SelectSubAccountProfile()
{
	try	{		
		
		Select Profile= new Select (driver.findElement(By.id("vx-f42-1121")));
		
		Profile.selectByIndex(2);
		
	}catch (NoSuchElementException| StaleElementReferenceException e)	{
		
		Select Profile= new Select (driver.findElement(By.id("vx-f42-1121")));
		
		Profile.selectByIndex(2);
	}
}

//********************************* Method to select district  ****************************************
public void SelectDistrict()
{
	try	{		
		
		Select district= new Select (driver.findElement(By.id("P40_BCD_CMR_UID3")));
		
		district.selectByIndex(2);
		
	}catch (NoSuchElementException| StaleElementReferenceException e)	{
		
		Select district= new Select (driver.findElement(By.id("P40_BCD_CMR_UID3")));
		
		district.selectByIndex(2);
	}
}

//********************************* Method to verify created customer  ****************************************
public void verify_created_customer()
{
	
	String CustomerName= driver.findElement(By.id("P2_BCD_NAME")).getText();
	
	Assert.assertEquals(CustomerName, AccountName);
  }
}



