package Credit_Control_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common_methods.CommonMethods;

public class Aging_tab extends CommonMethods{
	
	public static String AcName= "Selmec Lda";
	
	public static String Bname= "BCI";
	
	public static String controller= "FI";
		
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		clickCreditControlTab() ;	
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() 
	{
	    driver.quit();
	}
	
	@Test
	public void a_view_Aging_Tab()
	{
		view_agingTab();
	}
	
	@Test
	public void b_Click_Aging_Tab() throws InterruptedException
	{
		Thread.sleep(500);
		click_agingTab();
	}
	
	@Test
	public void c_Search_For_Primary_report() throws InterruptedException
	{
		Thread.sleep(500);
		Click_Search();
		Thread.sleep(400);
		Search_PrimaryReport();
	}
	@Test
	public void d_Search_By_AccountNumber_report() throws InterruptedException
	{
		Thread.sleep(500);
		Search_by_AccountNumber_Report();
	}
	
	@Test
	public void e_Search_By_AccountName_report() throws InterruptedException
	{
		Thread.sleep(500);
		Search_by_AccountName_Report();
	}
	
	@Test
	public void f_Search_By_AccountType() throws InterruptedException
	{
		Thread.sleep(500);
		Search_By_AccountType();
	}
	
	@Test
	public void g_Search_By_BankNAme() throws InterruptedException
	{
		Thread.sleep(500);
		Search_By_BankName();
		
	}
	
	@Test
	public void h_Search_By_CreditController() throws InterruptedException
	{
		Thread.sleep(500);
		Search_By_CreditController();		
	}
	
	
	@Test
	public void i_Search_OutstandingBalanceBy_AccountType_report() throws InterruptedException
	{
		
		Thread.sleep(400);
		Search_OutstandingBalanceByAccountType_Report();
	}
	
	@Test
	public void j_Search_OutstandingBalanceBy_CreditController_report() throws InterruptedException
	{
		
		Thread.sleep(400);
		Search_OutstandingBalanceByCreditController_Report();
	}
	
	@Test
	public void k_Search_OutstandingBalanceBy_Province_report() throws InterruptedException
	{
		
		Thread.sleep(400);
		Search_OutstandingBalanceByProvince_Report();
	}
	
	@Test
	public void l_Search_OutstandingBalanceBy_Region_report() throws InterruptedException
	{
		
		Thread.sleep(400);
		Search_OutstandingBalanceByRegion_Report();
		
	}

	@Test
	public void m__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void n_Search_by_privateReport() throws InterruptedException
	{
		search_by_private_report();
	}
	
	@Test
	public void o_reset_test() throws InterruptedException
	{
		Thread.sleep(300);
		reset();
		Search_PrimaryReport();
	}
	
	@Test
	public void p_Filter_report() throws InterruptedException
	{
		Thread.sleep(1500);
		View_filtered_report();
	}
	
	@Test
	public void q_flashBackTest() throws InterruptedException
	{
		Thread.sleep(5000);
		Flashback_filter();
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view aging tab ******************************
public void view_agingTab()
{
	WebElement aging= driver.findElement(By.linkText("Aging"));
	
	Boolean Aging= aging.isDisplayed();
	
	Assert.assertTrue(Aging);
}

//********************************* Method to click aging tab ******************************
public void click_agingTab()
{
	WebElement aging= driver.findElement(By.linkText("Aging"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(aging).click().build().perform();
	
	WebElement verify= driver.findElement(By.xpath("//div[@id='R20764225173546498']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
 }

//********************************* Method to click Search **********************************
public void Click_Search()
  {
	WebElement search= driver.findElement(By.id("B26930225421672037"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(search).click().build().perform();
}

//********************************* Method to search primary report on aging tab ****************
public void Search_PrimaryReport() throws InterruptedException
{
	Select dropdown = new Select (driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText("1. Primary Report");
	
	Thread.sleep(400);
	
	Boolean report= driver.findElement(By.id("apexir_DATA_PANEL")).isDisplayed();
	
	Assert.assertTrue(report);
	
}

//********************************* Method to search by account type on aging tab ****************
public void Search_By_AccountType() throws InterruptedException
{  
	Select dropdown = new Select (driver.findElement(By.id("P53_ACC_TYPE")));
	
	dropdown.selectByVisibleText(AcType);
	
	Thread.sleep(400);
	
	Click_Search();
	
	System.out.println("Data displayed");
	
	Boolean report= driver.findElement(By.id("apexir_DATA_PANEL")).isDisplayed();
	
	Select dropdown1 = new Select (driver.findElement(By.id("P53_ACC_TYPE")));
	
	dropdown1.selectByVisibleText("-- Select --");
	
	Assert.assertTrue(report);
 }

//********************************* Method to search Outstanding Balance by Account Type report on aging tab ****************
public void Search_OutstandingBalanceByAccountType_Report() throws InterruptedException
{
	Select dropdown = new Select (driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText("2. Outstanding Balance by Account Type");
	
	Thread.sleep(400);
	
	Boolean report= driver.findElement(By.id("apexir_CHART")).isDisplayed();
	
	Assert.assertTrue(report);
	
}

//********************************* Method to search by Account number report on aging tab ****************************
public void Search_by_AccountNumber_Report() throws InterruptedException
{
	Thread.sleep(800);
	
	WebElement accountNumber= driver.findElement(By.id("P53_ACC_NO"));
	
	accountNumber.clear();
	
	accountNumber.sendKeys(AccNo);
	
	Thread.sleep(300);
	
	Click_Search();
	
	Thread.sleep(300);
	
	WebElement verify= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[1]"));
	
	String number= verify.getText();
	
	System.out.println(number);
	
	driver.findElement(By.id("P53_ACC_NO")).clear();
	
	Assert.assertEquals(number, AccNo);
 }


//********************************* Method to search by Account name report on aging tab ******************************
public void Search_by_AccountName_Report() throws InterruptedException
{
	Thread.sleep(800);

	WebElement accountName= driver.findElement(By.id("P53_ACC_NAME"));
	
	accountName.clear();
	
	accountName.sendKeys(AcName);
	
	Click_Search();
	
	Thread.sleep(300);
	
	WebElement verify= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[3]"));
	
	String name= verify.getText();
	
	System.out.println(name);
	
	driver.findElement(By.id("P53_ACC_NAME")).clear();
	
	Assert.assertEquals(name, AcName);
}


//********************************* Method to search by bank name on aging tab ****************
public void Search_By_BankName() throws InterruptedException
{  
	Select dropdown = new Select (driver.findElement(By.id("P53_BD_NAME")));
	
	dropdown.selectByVisibleText(Bname);
	
	Thread.sleep(400);
	
	Click_Search();
	
	WebElement verify= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[15]"));
	
	String name= verify.getText();
	
	System.out.println(name);
	
	Select dropdown1 = new Select (driver.findElement(By.id("P53_BD_NAME")));
	
	dropdown1.selectByVisibleText("-- Select --");
	
	Assert.assertEquals(name, Bname);
	
}

//********************************* Method to search by credit controller on aging tab ****************
public void Search_By_CreditController() throws InterruptedException
{  
	Select dropdown = new Select (driver.findElement(By.id("P53_CRED_CODE")));

	dropdown.selectByVisibleText(controller);
	
	Thread.sleep(400);
	
	Click_Search();
	
	WebElement verify= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[5]"));
	
	String name= verify.getText();
	
	System.out.println(name);
	
	Select dropdown1 = new Select (driver.findElement(By.id("P53_CRED_CODE")));
	
	dropdown1.selectByVisibleText("-- Select --");
	
	Assert.assertEquals(name, controller);
	
}

//********************************* Method to search Outstanding Balance by Credit Controller report on aging tab ****************
public void Search_OutstandingBalanceByCreditController_Report() throws InterruptedException
{
	Select dropdown = new Select (driver.findElement(By.id("apexir_SAVED_REPORTS")));

	dropdown.selectByVisibleText("3. Outstanding Balance by Credit Controller");
	
	Thread.sleep(400);
	
	Boolean report= driver.findElement(By.id("apexir_CHART")).isDisplayed();
	
	Assert.assertTrue(report);
	
	}


//********************************* Method to search Outstanding Balance by Province report on aging tab ****************
public void Search_OutstandingBalanceByProvince_Report() throws InterruptedException
{
	Select dropdown = new Select (driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText("4. Outstanding Balance by Province");
	
	Thread.sleep(400);
	
	Boolean report= driver.findElement(By.id("apexir_CHART")).isDisplayed();
	
	Assert.assertTrue(report);
	
	}

//********************************* Method to search Outstanding Balance by Region report on aging tab ****************
public void Search_OutstandingBalanceByRegion_Report() throws InterruptedException
{
	Select dropdown = new Select (driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText("5. Outstanding Balance by Region");
	
	Thread.sleep(400);
	
	Boolean report= driver.findElement(By.id("apexir_CHART")).isDisplayed();
	
	Assert.assertTrue(report);
	
}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	col= "Account Type";
	expvalue= "Group";

	view_filtered_report();
	
	// Verify report
	
	Thread.sleep(1300);
	
	String verifyreport= driver.findElement(By.xpath("//a[contains(text(),\"Account Type = 'Group'\")]")).getText();
	
	Thread.sleep(300);
	
	// Remove filters
	
	WebElement removefilter= driver.findElement(By.xpath("//table[@id='apexir_CONTROL_PANEL_COMPLETE']//tbody//tr//td//a//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(removefilter).click().build().perform();
	
	System.out.println(verifyreport);
	
	Boolean report1= verifyreport.contains("Account Type = 'Group'");		 
	
	Assert.assertTrue(report1);		
	
}
	
//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
	{
		Reset();
	
		Thread.sleep(400);
		 
		// Verify report
		
		WebElement primaryreport= driver.findElement(By.id("apexir_CHART"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}


//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();	
		
	// verify 

	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
	
}
	
//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "2. Test report"; 

	save_report();
	
	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
	
}
	
//********************************* Method to filter report using flash back option ****************
public void Flashback_filter() throws InterruptedException
{
	click_ActionButton();

	Thread.sleep(400);
		
	//Click flash back option
		
	 WebElement flashback= driver.findElement(By.xpath("//a[contains(text(),'Flashback')]"));
	
	 Actions act= new Actions(driver);
	
	 act.moveToElement(flashback).click().build().perform();
		
	 try{
		 // Enter flash back time
	
		 Thread.sleep(300);
		 
		 WebElement time= driver.findElement(By.id("apexir_FLASHBACK_TIME"));
		 
		 time.clear();
		 
		 time.sendKeys(Time); 

		 //click apply
		
		 Click_apply();
		 
		 Thread.sleep(500);
		 
		 String text1= driver.findElement(By.xpath("//table[@id='apexir_CONTROL_PANEL_COMPLETE']")).getText(); 
	    
		 System.out.println(text1);
	   
		 Boolean successmsg= text1.contains("Report data as of 10 minutes ago.");
	    
		 Assert.assertTrue(successmsg);
		 
	 }catch(Exception e) {
			 
		 WebElement time= driver.findElement(By.id("apexir_FLASHBACK_TIME"));
		
		 time.clear();
		
		 time.sendKeys(Time); 
				
		 //click apply
		
		 Click_apply();
		
		 Thread.sleep(500);
			
		 String text1= driver.findElement(By.xpath("//td[contains(text(),'Report data as of 10 minutes ago.')]")).getText(); 
		
		 System.out.println(text1);
		 
		 Boolean successmsg= text1.contains("Report data as of 10 minutes ago.");
		 
		 Assert.assertTrue(successmsg);
	 	}
	}

}
