package Credit_Control_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common_methods.CommonMethods;

public class CIB_Tab extends CommonMethods{
	
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		clickCreditControlTab() ;	
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() 
	{
	    driver.quit();
	}
	
	@Test
	public void a_view_CIB_Tab()
	{
		view_CIBTab();
	}
	
	@Test
	public void b_Click_CIB_Tab() throws InterruptedException
	{
		Thread.sleep(1500);
		click_CIBTab();
		
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(2500);
		View_filtered_report();
	}
	
	@Test
	public void d_Save_Report() throws InterruptedException
	{
		Thread.sleep(5000); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(5000);
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(5000);
		search_by_PrimaryReports();
	}
	

	@Test
	public void i_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(5000);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","connection_incentive_bonus.csv"));
	}
	
	@Test
	public void j_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","connection_incentive_bonus.htm"));
	}
	
	@Test
	public void k_emailReport() throws InterruptedException
	{
		Thread.sleep(300);
		email_report();
	}
	
	@Test
	public void y_reset_test() throws InterruptedException
	{
		Thread.sleep(5000);
		reset();
	}
	
	
	@Test
	public void z_flashBackTest() throws InterruptedException
	{
		Thread.sleep(5000);
		flashback_filter();
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view CIB tab ******************************
public void view_CIBTab()
{
	WebElement aging= driver.findElement(By.linkText("CIB"));
	
	Boolean Aging= aging.isDisplayed();
	
	Assert.assertTrue(Aging);
}

//********************************* Method to click CIB tab ******************************
public void click_CIBTab()
{
	WebElement aging= driver.findElement(By.linkText("CIB"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(aging).click().build().perform();
	
	WebElement verify= driver.findElement(By.xpath("//div[@id='apexir_TOOLBAR_CLOSE']"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	 expvalue= "10000";
	 col= "CIB Value";
	
	 view_filtered_report();
	
	// Verify report
	
	 Thread.sleep(300);
	
	 String verifyreport= driver.findElement(By.xpath("//tr[3]//td[4]")).getText();
	
	 Assert.assertEquals(verifyreport, expvalue);
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "4. Test report"; 
	
	save_report();
	 	
	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
	
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();		

	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
	
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
	{
		select_primary_report();

		Thread.sleep(300);
		
		WebElement primaryreport= driver.findElement(By.id("446349003042891617"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}


//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
{
		Thread.sleep(200);

		Reset();
	 
		// Verify report
		
		WebElement primaryreport= driver.findElement(By.id("446349003042891617"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
  }

}

