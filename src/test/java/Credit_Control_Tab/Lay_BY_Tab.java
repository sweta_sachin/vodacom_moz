package Credit_Control_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common_methods.CommonMethods;

public class Lay_BY_Tab extends CommonMethods{
	
		
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		clickCreditControlTab() ;	
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() 
	{
	   driver.quit();
	}
	
	@Test
	public void a_view_LayBy_Tab()
	{
		view_LayByTab();
	}
	
	@Test
	public void b_Click_LayBy_Tab() throws InterruptedException
	{
		Thread.sleep(500);
		click_LayByTab();
	}
		
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
	@Test
	public void g_flashBackTest() throws InterruptedException
	{
		Thread.sleep(300);
		flashback_filter();
	}
	
	@Test
	public void h_reset_test() throws InterruptedException
	{
		Thread.sleep(300);
		reset();
	}
	
	@Test
	public void i_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","subscriber_laybys.csv"));
	}
	
	@Test
	public void j_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","subscriber_laybys.htm"));
	}
	
	@Test
	public void k_emailReport() throws InterruptedException
	{
		Thread.sleep(300);
		email_report();
	}
	
	@Test
	public void l_view_Audit_Data() throws InterruptedException
	{
		Thread.sleep(300);
		view_Audit_Data();
	}
	
	@Test
	public void m_Back_To_Main() throws InterruptedException
	{
		Thread.sleep(300);
		Go_back();
	}

	@Test
	public void n_view_Subacriber_LayBy_Data() throws InterruptedException
	{
		Thread.sleep(300);
		View_LayBy_Data();
	}
	
	
//////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view LayBy tab ******************************
public void view_LayByTab()
{
	WebElement LayBy= driver.findElement(By.linkText("LayBy"));
	Boolean LayBY= LayBy.isDisplayed();
	Assert.assertTrue(LayBY);
}

//********************************* Method to click LayBy tab ******************************
public void click_LayByTab()
{
	WebElement LayBy= driver.findElement(By.linkText("LayBy"));
	Actions act= new Actions(driver);
	act.moveToElement(LayBy).click().build().perform();
	WebElement verify= driver.findElement(By.id("R10421511190911008"));
	Boolean Verify= verify.isDisplayed();
	Assert.assertTrue(Verify);
}	

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	 expvalue= "Smart Kicka2";
	 col= "Product Name";
	view_filtered_report();
  
	  // Verify report
	   	Thread.sleep(300);
	   	String verifyreport= driver.findElement(By.xpath("//td[contains(text(),'Smart Kicka2')]")).getText();
	   	Thread.sleep(300);
	   	Assert.assertEquals(verifyreport, expvalue);
  
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 
	save_report();
	
	// Verify saved report
		Thread.sleep(500);
		Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
		dropdown.selectByVisibleText(report);
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();	
	// verify 
		Thread.sleep(300);
		String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
		System.out.println(text);
		Assert.assertEquals(text, verifyreport);
	}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
		select_primary_report();
		Thread.sleep(300);
		WebElement primaryreport= driver.findElement(By.id("10421701761911031"));
		Boolean report= primaryreport.isDisplayed();
		Assert.assertTrue(report);
	}

//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
{
	Reset();
	 
	// Verify report
		 WebElement primaryreport= driver.findElement(By.id("10421701761911031"));
		Boolean report= primaryreport.isDisplayed();
		Assert.assertTrue(report);
}

//************************************* Method to view audit data by clicking on reference number *********************
public void view_Audit_Data() throws InterruptedException
{
	// click on reference number
		WebElement RefNo = driver.findElement(By.xpath("//tr[@class='even']//td[2]"));
		Actions act1= new Actions(driver);
		act1.moveToElement(RefNo).click().build().perform();
		Thread.sleep(300);
	 
	// Verify report
		 WebElement primaryreport= driver.findElement(By.id("report_R10424516763911107"));
		Boolean report= primaryreport.isDisplayed();
		Assert.assertTrue(report);
	 }

//************************************** Method to go back to main Lay By page *******************************
public void Go_back() throws InterruptedException
{
	// click on back
		 WebElement back = driver.findElement(By.id("B10425709312911127"));
		 Actions act1= new Actions(driver);
		 act1.moveToElement(back).click().build().perform();
		 Thread.sleep(300);
		 
	// Verify report
		 WebElement primaryreport= driver.findElement(By.id("10421701761911031"));
		 Boolean report= primaryreport.isDisplayed();
		 Assert.assertTrue(report);
}

//************************************** Method to view subscriber's lay by data *******************************
public void View_LayBy_Data() throws InterruptedException
{
	// click on pencil icon to view data
		 WebElement view = driver.findElement(By.xpath("//tr[@class='even']//td[1]"));
		 Actions act1= new Actions(driver);
		 act1.moveToElement(view).click().build().perform();
		 Thread.sleep(300);
		 
	// Verify subscriber's Lay By data
		 WebElement primaryreport= driver.findElement(By.id("R10421511190911008"));
	     Boolean report= primaryreport.isDisplayed();
	     Assert.assertTrue(report);
  }
}
