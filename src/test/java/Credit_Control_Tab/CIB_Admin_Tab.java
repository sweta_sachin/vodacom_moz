package Credit_Control_Tab;

import java.text.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common_methods.CommonMethods;

	public class CIB_Admin_Tab extends CommonMethods{
	
	public static int index= 3;

	public static String Bonus= "400";
	
	public static String actiondate= "22-Nov-2019";
	
	public static String startDate= "22-Dec-2020";
	
	public static String endDate= "30-Dec-2021";
		
	public static String filename= "C:\\Users\\Sweta\\Desktop\\test doc.txt";
	
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		clickCreditControlTab() ;	
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() 
	{
	    driver.quit();
	}
	
	@Test
	public void a_view_CIBAdmin_Tab()
	{
		view_CIBAdminTab();
	}
	
	@Test
	public void b_Click_CIBAdmin_Tab() throws InterruptedException
	{
		Thread.sleep(500);
		click_CIBAdminTab();
		
	}
	
	@Test
	public void c_view_archieved_Rateplans() throws InterruptedException
	{
		Thread.sleep(300);
		view_archivedRatePlans();
	}
	
	@Test
	public void d_view_all_Rateplans() throws InterruptedException
	{
		Thread.sleep(300);
		view_allRatePlans();
	}
	
	@Test
	public void e_view_Current_Rateplans() throws InterruptedException
	{
		Thread.sleep(300);
		view_CurrentRatePlans();
	}
	
	@Test
	public void f_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void g__Save_Report() throws InterruptedException
	{
		Thread.sleep(2500); 
		Save_report();
	}
	
	@Test
	public void h_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(2500); 
		search_by_private_report();
	}
	
	@Test
	public void i_search_By_Primary_report() throws InterruptedException
	{
		
		Thread.sleep(2500); 
		reset();
		Thread.sleep(2500); 
		search_by_PrimaryReports();
	}
	
	@Test
	public void j_flashBackTest() throws InterruptedException
	{
		Thread.sleep(2500); 
		flashback_filter();
	}
	
	@Test
	public void k_reset_test() throws InterruptedException
	{
		Thread.sleep(5000); 
		reset();
	}
	
	@Test
	public void l_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(2500); 
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","rate_plan_attributes_report.csv"));
	}
	
	@Test
	public void m_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(2500); 
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","rate_plan_attributes_report.htm"));
	}
	
	@Test
	public void n_emailReport() throws InterruptedException
	{
		Thread.sleep(2500); 
		email_report();
	}
	
	@Test
	public void o_click_LoadData() throws InterruptedException, ParseException
	{
		Thread.sleep(2500); 
		LoadData();
	}
	
	@Test
	public void p_Click_Edit() throws InterruptedException
	{
		Thread.sleep(2500); 
		click_Edit();
	}
	
	@Test
	public void q_Search_Rateplans() throws InterruptedException
	{
		Thread.sleep(2500); 
		search_Rateplans();
	}
	

	@Test
	public void t_Add_New_CIB_Record() throws InterruptedException
	{
		Thread.sleep(2500); 
		 Add_newCIB_Record();
	} 
	
	
//////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view CIB tab ******************************
public void view_CIBAdminTab()
 {
	WebElement aging= driver.findElement(By.linkText("CIB Admin"));
	
	Boolean Aging= aging.isDisplayed();
	
	Assert.assertTrue(Aging);
}

//********************************* Method to click CIB tab ******************************
public void click_CIBAdminTab()
{
	WebElement aging= driver.findElement(By.linkText("CIB Admin"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(aging).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("B9482811031501387"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}	

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
		 col= "Rate Plan";
		 expvalue= "Empresa M";
	
	view_filtered_report();
	
	// Verify report
	
	Thread.sleep(1300);
	
	String verifyreport= driver.findElement(By.xpath("//div[@id='apexir_WORKSHEET']//div//td[2]")).getText();
	
	Thread.sleep(300);
	
	// Remove filters
	
	WebElement removefilter= driver.findElement(By.xpath("//table[@id='apexir_CONTROL_PANEL_COMPLETE']//tbody//tr//td//a//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(removefilter).click().build().perform();
	
	Assert.assertEquals(verifyreport, expvalue);
	
	}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 
	
	save_report();
	 	
	 // Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
	
}

//***************************** Method to view archived rate plans ****************************
public void view_archivedRatePlans() throws InterruptedException
{
	// Click archived option

	Thread.sleep(300);
	
	WebElement archived= driver.findElement(By.id("P99_ACTIVE_FILTER_1"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(archived).click().build().perform();
	
	Thread.sleep(300);
	
	Boolean selected = archived.isSelected();
	
	Assert.assertTrue(selected);
}

//***************************** Method to view all rate plans ****************************
public void view_allRatePlans() throws InterruptedException
{
	// Click all option

	Thread.sleep(300);
	
	WebElement all= driver.findElement(By.id("P99_ACTIVE_FILTER_2"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(all).click().build().perform();
	
	Thread.sleep(300);
	
	Boolean selected = all.isSelected();
	
	Assert.assertTrue(selected);
}

//***************************** Method to view all rate plans ****************************
public void view_CurrentRatePlans() throws InterruptedException
{
	// Click current option
	
	Thread.sleep(300);
	
	WebElement current= driver.findElement(By.id("P99_ACTIVE_FILTER_0"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(current).click().build().perform();
	
	Thread.sleep(300);
	
	Boolean selected = current.isSelected();
	
	Assert.assertTrue(selected);
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();	
	
	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
	
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{

	select_primary_report();
	
	Thread.sleep(300);
	
	WebElement primaryreport= driver.findElement(By.id("9480923296465926"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
}

//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
{
		Reset();
	 
		// Verify report
	
		WebElement primaryreport= driver.findElement(By.id("9480923296465926"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
}


//***************************************** Method to load data **************************************
public void LoadData() throws InterruptedException, ParseException
{
   // Click load data
		
	WebElement loadData = driver.findElement(By.id("B9482811031501387"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(loadData).click().build().perform();
	 
	 // Attach file
	
	Thread.sleep(200);
	
	WebElement file= driver.findElement(By.id("P102_BF_FILENAME"));
	
	file.sendKeys("C:\\Users\\Sweta\\Desktop\\test doc.txt");
	
	Thread.sleep(200);
	 
	 // Select file type
	
	Select dropdown = new Select(driver.findElement(By.id("P102_BF_BFT_UID")));
	
	dropdown.selectByIndex(1);
		
	//Click run button
	
	WebElement run = driver.findElement(By.id("B9484322618531960"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(run).click().build().perform();
	 
	 // Check success message
	
	String Msg= driver.findElement(By.id("echo-message")).getText();
	
	String Expected_msg= "Bulk file process started.";
	
	Assert.assertEquals(Msg, Expected_msg);
	
	Thread.sleep(400);
	
	//click cancel to go to CIB Admin page 
	
	WebElement cancel= driver.findElement(By.id("B9484110748531955"));
	
	Actions action= new Actions(driver);
	
	action.moveToElement(cancel).click().build().perform();
	
}
//****************************************** Method to click edit ****************************
public void click_Edit()
{
	 //Click edit

	WebElement run = driver.findElement(By.id("B9576309098651220"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(run).click().build().perform();
	 
	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("report_UPDATE_CIB"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
	
}

//**************************************** Method to search rate plan **************************
public void search_Rateplans() throws InterruptedException
{
	try {
	
		Select rateplan= new Select(driver.findElement(By.id("P104_RATE_PLAN_SEARCH")));
		
		rateplan.selectByIndex(index);
		
		WebElement option= rateplan.getFirstSelectedOption();
		
		String text1= option.getText();
		
		Thread.sleep(300);
		
		Select rateplan1= new Select(driver.findElement(By.id("P104_RPA_RP_UID")));
		
		WebElement option2= rateplan1.getFirstSelectedOption();
		
		String text2= option2.getText();

		
		//	verify selection
		
		Assert.assertEquals(text1, text2);
		
	}catch (Exception e)	{
				
		Select rateplan= new Select(driver.findElement(By.id("P104_RATE_PLAN_SEARCH")));
		
		rateplan.selectByIndex(3);
		
		WebElement option= rateplan.getFirstSelectedOption();
		
		String text1= option.getText();
		
		Thread.sleep(300);
		
		Select rateplan1= new Select(driver.findElement(By.id("P104_RPA_RP_UID")));
		
		WebElement option2= rateplan1.getFirstSelectedOption();
		
		String text2= option2.getText();
		
		 //	verify selection
		
		System.out.println(text1);
		
		System.out.println(text2);
		
		Assert.assertEquals(text1, text2);
		
	}
   }

//**************************************** Method to edit start date ***************************
public void edit_startDate() throws InterruptedException
	{
		WebElement startdate= driver.findElement(By.id("f04_0001"));
	
		startdate.clear();
		
		startdate.sendKeys(startDate);
		
		Thread.sleep(200);

		//Click Apply changes
		
		applyChanges();
	 
		//verify with success message
	 
		String Msg= driver.findElement(By.id("echo-message")).getText();
		
		String Expected_msg= "1 row(s) updated, 0 row(s) inserted.";
		
		Assert.assertEquals(Msg, Expected_msg);
		
		// verify changed start date
		
		Select rateplan= new Select(driver.findElement(By.id("P104_RATE_PLAN_SEARCH")));
		
		rateplan.selectByIndex(index);
		
		Thread.sleep(600);
		
		String verify = driver.findElement(By.id("f04_0001")).getText();
		
		System.out.println(verify);
	 
   }


//**************************************** Method to edit end date ***************************

public void edit_EndDate() throws InterruptedException
{
	 WebElement enddate= driver.findElement(By.id("f05_0001"));
	 
	 enddate.clear();
	 
	 enddate.sendKeys(endDate);
	 
	 Thread.sleep(200);
	 
	 //Click Apply changes
	 
	 applyChanges();
	 
	 //verify with success message
	 
	 String Msg= driver.findElement(By.id("echo-message")).getText();
		
	 String Expected_msg= "1 row(s) updated, 0 row(s) inserted.";
		
	 Assert.assertEquals(Msg, Expected_msg);
		
	 // verify changed end date
	
	 Select rateplan= new Select(driver.findElement(By.id("P104_RATE_PLAN_SEARCH")));
	
	 rateplan.selectByIndex(index);
		
	 Thread.sleep(600);
		
	 String verify = driver.findElement(By.id("f05_0001")).getText();
		
	 System.out.println(verify);
	
}

//***************************************** Method to add new CIB record ******************************
public void Add_newCIB_Record() throws InterruptedException
{
	//Enter Bonus Amount
	
	 WebElement bonus= driver.findElement(By.id("P104_RPA_NUMBER1"));
	 
	 bonus.clear();
	 
	 bonus.sendKeys(Bonus);
	 
	 Thread.sleep(200);
	 
	 // enter start date
	 
	 WebElement startdate= driver.findElement(By.id("P104_RPA_START_DATE"));
	 
	 startdate.clear();
	 
	 startdate.sendKeys(startDate);
	 
	 Thread.sleep(200);
	 
	 //enter end date
	 
	 WebElement enddate= driver.findElement(By.id("P104_RPA_END_DATE"));
	 
	 enddate.clear();
	 
	 enddate.sendKeys(endDate);
	 
	 Thread.sleep(200);
	 
	 //click create
	 
	 WebElement create = driver.findElement(By.id("B9590201650928166"));
	 
	 Actions act= new Actions(driver);
	 
	 act.moveToElement(create).click().build().perform();
	 
	 //Verify success message
	 
	 String Msg= driver.findElement(By.id("echo-message")).getText();
		
	 String Expected_msg= "Action Processed.";
		
	 Assert.assertEquals(Msg, Expected_msg);
		
	//Verify created record
		
	Select rateplan= new Select(driver.findElement(By.id("P104_RATE_PLAN_SEARCH")));
	
	rateplan.selectByIndex(index);
	
	String verify = driver.findElement(By.id("f03_0002")).getText();
	
	System.out.println(verify);
		
	// click cancel to go back to CIB Admin page
	
	WebElement cancel = driver.findElement(By.id("B9590413650928166"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(cancel).click().build().perform();
		
	}

}
