package Customer_Care_Subscriber_Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AccessAllSubscriberOptionsFromLeftHandMenu extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	  driver.quit();			
	}

	@Test
	public void a_AccessAttachmentFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_Attachment();
	}
	
	@Test
	public void b_Access_Communication_For_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_Communication();
	}
	
	@Test
	public void c_Access_Notification_For_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_Notification();
	}
	
	@Test
	public void d_Access_QuickNotes_For_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_QuickNotes();
	}
	
	@Test
	public void e_Access_SMSnotification_For_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_SMSNotification();
	}
	
	@Test
	public void f_Access_SubscriberDebugData_For_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_DebugData();
	}
	
	@Test 
	public void g_Access_EditSubcriber_For_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_EditSubscriber() ;
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************* Method to access attachment*****************************
public void Access_Attachment() throws InterruptedException
	{
		WebElement attachment= driver.findElement(By.xpath("//div[@id='R21887018202990915']//a[contains(text(),'Attachments')]"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(attachment).click().build().perform();
		
		Thread.sleep(400);
		
		Boolean verify= driver.findElement(By.id("B15126227968958324")).isDisplayed();
					
		Assert.assertTrue(verify);
}
	
//************************************* Method to access communication *****************************
public void Access_Communication() throws InterruptedException
{
	driver.navigate().back();
			
	Thread.sleep(400);
			
	WebElement com= driver.findElement(By.xpath("//div[@id='R21887018202990915']//a[contains(text(),'Communications')]"));
			
	Actions act= new Actions(driver);
			
	act.moveToElement(com).click().build().perform();
			
	Thread.sleep(1000);
			
	Boolean verify= driver.findElement(By.id("apexir_ACTIONSMENUROOT")).isDisplayed();
							
	Assert.assertTrue(verify);
	
}
		

//************************************* Method to access notification *****************************
public void Access_Notification() throws InterruptedException
{
	driver.navigate().back();
					
	Thread.sleep(400);
				
    WebElement notification= driver.findElement(By.xpath("//div[@id='R21887018202990915']//li[4]//a[1]"));
					
	Actions act= new Actions(driver);
					
	act.moveToElement(notification).click().build().perform();
					
	Thread.sleep(600);
					
	Boolean verify= driver.findElement(By.id("B12131301940768901")).isDisplayed();
									
	Assert.assertTrue(verify);
	
}
		

//************************************* Method to access quick note *****************************
public void Access_QuickNotes() throws InterruptedException
{
    	driver.navigate().back();
					
		Thread.sleep(400);
					
		WebElement QuickNote= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Quick Notes')]"));
					
		Actions act= new Actions(driver);
					
		act.moveToElement(QuickNote).click().build().perform();
					
		Thread.sleep(600);
					
		Boolean verify= driver.findElement(By.id("B13535157679207053")).isDisplayed();
									
		Assert.assertTrue(verify);
	
}

//************************************* Method to access SMS notification *****************************
public void Access_SMSNotification() throws InterruptedException
	{
		driver.navigate().back();
						
		Thread.sleep(400);
							
		WebElement SMSnotification= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'SMS Notifications')]"));
						
		Actions act= new Actions(driver);
							
		act.moveToElement(SMSnotification).click().build().perform();
							
		Thread.sleep(600);
							
		Boolean verify= driver.findElement(By.id("B12429021350488144")).isDisplayed();
											
		Assert.assertTrue(verify);

	}
				
		
//************************************* Method to access Subscriber's debug data *****************************
public void Access_DebugData() throws InterruptedException
  {
		driver.navigate().back();
								
		Thread.sleep(400);
									
		WebElement DD= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Subscriber Debug Data')]"));
								
		Actions act= new Actions(driver);
									
		act.moveToElement(DD).click().build().perform();
									
		Thread.sleep(600);
									
		Boolean verify= driver.findElement(By.xpath("//div[@id='R14454415541523068']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']")).isDisplayed();
													
		Assert.assertTrue(verify);
		
  }
		
//************************************* Method to access Edit subscriber *****************************
public void Access_EditSubscriber() throws InterruptedException
	{
		driver.navigate().back();
										
		Thread.sleep(400);
											
		WebElement edit= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Edit Subscriber')]"));
										
		Actions act= new Actions(driver);
											
		act.moveToElement(edit).click().build().perform();
											
		Thread.sleep(600);
											
		Boolean verify= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Edit Subscriber')]")).isDisplayed();
															
		Assert.assertTrue(verify);

	}
						
}
