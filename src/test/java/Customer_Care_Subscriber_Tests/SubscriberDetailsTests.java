package Customer_Care_Subscriber_Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SubscriberDetailsTests  extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	  driver.quit();			
	}
	
	@Test
	public void a_View_SubscriberDetailsTab() throws InterruptedException
	{
		ViewSubscriberDetailsTab();
		
	}
	
	@Test
	public void b_click_SubscriberDetailsTab() throws InterruptedException
	{
		ClickSubscriberDetailsTab();
		
	}
	
	@Test
	public void c_View_SubscriberDetails() throws InterruptedException
	{
		ViewSubscriberDetails();
		
	}
	@Test
	public void d_View_AssociatedCustomerDetailsTab() throws InterruptedException
	{
		 ViewCustomerDetailsTab();
	}
	
	@Test
	public void e_Click_AssociatedCustomerDetailsTab() throws InterruptedException
	{
		 ClickCustomerDetailsTab();
	}
	
	@Test
	public void f_View_AssociatedCustomerDetails() throws InterruptedException
	{
		ViewCustomerDetails();
	}
	
	@Test
	public void g_View_SubscriberServicesTab() throws InterruptedException
	{
		View_SubscriberServices_Tab();
	}
	
	@Test
	public void h_click_SubscriberServicesTab() throws InterruptedException
	{
		Click_SubscriberServices_Tab();
	}
	
	
	@Test
	public void i_View_SubscriberServices() throws InterruptedException
	{
		ViewSubscriberServices();
	}
	@Test
	public void j_View_SubscriberAddressDetailsTab() throws InterruptedException
	{
		View_SubscriberAddressDetails_Tab();
	}
	
	@Test
	public void k_click_SubscriberAddressDetailsTab() throws InterruptedException
	{
		Click_SubscriberAddressDetails_Tab();
	}
	
	@Test
	public void l_View_SubscriberAddressDetails() throws InterruptedException
	{
		ViewSubscriberAddressDetails();
	}
	
	@Test
	public void m_View_CustomerAddressDetailsTab() throws InterruptedException
	{
		View_CustomerAddressDetails_Tab();
	}
	
	@Test
	public void n_click_CustomerAddressDetailsTab() throws InterruptedException
	{
		Click_CustomerAddressDetails_Tab();
	}

	@Test
	public void o_View_CustomerAddressDetails() throws InterruptedException
	{
		ViewCustomerAddressDetails();
	}
	
	
////////////////////////////////////////////////////////////////////////////////////////////////
//********************************** Method to view subscriber details tab **********************
public void ViewSubscriberDetailsTab() throws InterruptedException
{
	WebElement subscriberDetails= driver.findElement(By.partialLinkText("Subscriber Details I"));
		
	Thread.sleep(300);
		
	Boolean view= subscriberDetails.isDisplayed();
		
	Assert.assertTrue(view);
}
	
//********************************** Method to click subscriber details tab **********************
public void ClickSubscriberDetailsTab() throws InterruptedException
{
	WebElement subscriberDetails= driver.findElement(By.partialLinkText("Subscriber Details I"));
			
	Actions act= new Actions(driver);
			
	act.moveToElement(subscriberDetails).click().build().perform();
			
	Thread.sleep(300);
			
	Boolean view= driver.findElement(By.id("B13536255917213352")).isDisplayed();
			
	Assert.assertTrue(view);

}
		
//********************************** Method to view subscriber details **********************
public void ViewSubscriberDetails() throws InterruptedException
{
	WebElement subscriberDetails= driver.findElement(By.id("apex_layout_13637578239366544"));
			
	Thread.sleep(300);
			
	Boolean view= subscriberDetails.isDisplayed();
			
	Assert.assertTrue(view);

}
		
//********************************** Method to view Customer details tab **********************
public void ViewCustomerDetailsTab() throws InterruptedException
{
	WebElement customerDetails= driver.findElement(By.partialLinkText("Customer Details ID"));
			
	Thread.sleep(300);
			
	Boolean view= customerDetails.isDisplayed();
			
	Assert.assertTrue(view);

}
		
//********************************** Method to click customer details tab **********************
public void ClickCustomerDetailsTab() throws InterruptedException
{
	WebElement customerDetails= driver.findElement(By.partialLinkText("Customer Details ID"));
			
	Actions act= new Actions(driver);
				
	act.moveToElement(customerDetails).click().build().perform();
				
	Thread.sleep(300);
				
	Boolean view= driver.findElement(By.id("B13528763358177637")).isDisplayed();
				
	Assert.assertTrue(view);

}
			
//********************************** Method to view customer details **********************
public void ViewCustomerDetails() throws InterruptedException
{
	WebElement customerDetails= driver.findElement(By.id("apex_layout_13616970911260349"));
				
	Thread.sleep(300);
				
	Boolean view= customerDetails.isDisplayed();
				
	Assert.assertTrue(view);

}
			
//********************************** Method to view Subscriber services tab **********************
public void View_SubscriberServices_Tab() throws InterruptedException
{
	WebElement SubscriberServices= driver.findElement(By.xpath("//a[contains(text(),'Subscriber Services')]"));
				
	Thread.sleep(300);
				
	Boolean view= SubscriberServices.isDisplayed();
				
	Assert.assertTrue(view);

}
			
//********************************** Method to Subscriber services tab **********************
public void Click_SubscriberServices_Tab() throws InterruptedException
{
	WebElement SubscriberServices= driver.findElement(By.xpath("//a[contains(text(),'Subscriber Services')]"));
					
	Actions act= new Actions(driver);
					
	act.moveToElement(SubscriberServices).click().build().perform();
				
	Thread.sleep(300);
					
	Boolean view= driver.findElement(By.id("B29011503255168026")).isDisplayed();
					
	Assert.assertTrue(view);

}
				
//********************************** Method to view Subscriber Services details **********************
public void ViewSubscriberServices() throws InterruptedException
{
	WebElement SubscriberServices= driver.findElement(By.id("report_28357401521988031_catch"));
					
	Thread.sleep(300);
					
	Boolean view= SubscriberServices.isDisplayed();
					
	Assert.assertTrue(view);

}
		
//********************************** Method to view Subscriber Address details tab **********************
public void View_SubscriberAddressDetails_Tab() throws InterruptedException
{
	WebElement SubscriberAddressDetails= driver.findElement(By.xpath("//a[contains(text(),'Subscriber Address Details')]"));
						
	Thread.sleep(300);
						
	Boolean view= SubscriberAddressDetails.isDisplayed();
						
	Assert.assertTrue(view);

}
					
//********************************** Method to click Subscriber Address details tab **********************
public void Click_SubscriberAddressDetails_Tab() throws InterruptedException
{
	WebElement SubscriberAddressDetails= driver.findElement(By.xpath("//a[contains(text(),'Subscriber Address Details')]"));
							
	Actions act= new Actions(driver);
							
	act.moveToElement(SubscriberAddressDetails).click().build().perform();
							
	Thread.sleep(300);
							
	Boolean view= driver.findElement(By.id("R13418073259115295")).isDisplayed();
							
	Assert.assertTrue(view);
	
}
						
//********************************** Method to view Subscriber Address details **********************
public void ViewSubscriberAddressDetails() throws InterruptedException
{
	WebElement SubscriberAddressDetails= driver.findElement(By.id("R13418073259115295"));
							
	Thread.sleep(300);
							
	Boolean view= SubscriberAddressDetails.isDisplayed();
							
	Assert.assertTrue(view);

}
				
//********************************** Method to view Customer Address details tab **********************
public void View_CustomerAddressDetails_Tab() throws InterruptedException
{
	WebElement CustomerAddressDetails= driver.findElement(By.xpath("//a[contains(text(),'Customer Address Details')]"));
								
	Thread.sleep(300);
								
	Boolean view= CustomerAddressDetails.isDisplayed();
							
	Assert.assertTrue(view);

}
							
//********************************** Method to click Customer Address details tab **********************
public void Click_CustomerAddressDetails_Tab() throws InterruptedException
{
	WebElement CustomerAddressDetails= driver.findElement(By.xpath("//a[contains(text(),'Customer Address Details')]"));
									
	Actions act= new Actions(driver);
									
	act.moveToElement(CustomerAddressDetails).click().build().perform();
									
	Thread.sleep(300);
									
	Boolean view= driver.findElement(By.id("R45615206611157019-tab-R16581551875833684")).isDisplayed();
									
	Assert.assertTrue(view);

}
							
//********************************** Method to view Customer Address details **********************
public void ViewCustomerAddressDetails() throws InterruptedException
 {
	WebElement CustomerAddressDetails= driver.findElement(By.id("R16581551875833684"));
									
	Thread.sleep(300);
									
	Boolean view= CustomerAddressDetails.isDisplayed();
									
	Assert.assertTrue(view);

 }								

}
