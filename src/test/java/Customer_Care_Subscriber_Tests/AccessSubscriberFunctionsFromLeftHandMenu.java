package Customer_Care_Subscriber_Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AccessSubscriberFunctionsFromLeftHandMenu extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	  driver.quit();			
	}
	
	@Test
	public void a_AccessAccountSwapFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		 Access_AccountSwap();
	}
	
	@Test
	public void b_AccessAddorRemoveServicesFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_AddRemoveServices();
	}
	
	@Test
	public void c_AccessAutoBarHistoryFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_AutoBarHistory();
	}
	
	@Test
	public void d_AccessAverageSpendFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_AverageSpend();
	}
	
	@Test
	public void e_BillShockFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_BillShock();
	}
	
	@Test
	public void f_AccessContractUpgradeFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_ContractUpgrade();
	}
	
	@Test
	public void g_AccessCreditTabFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_CreditTab();
	}
	
	@Test
	public void h_AccessCSRFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_CSRTab();
	}
	
	@Test
	public void i_AccessDiscountsFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_DiscountsTab();
	}
	
	@Test
	public void j_AccessExternalSpendFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		 Access_ExternalSpendTab();
	}
	
	@Test
	public void k_AccessFinancialAdjustmentFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_FinancialAdjustmentTab();
	}
	
	@Test
	public void l_AccessFraudMSISDNFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_FraudMSISDNTab();
	}
	
	@Test
	public void m_AccessHandsetProfileFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_HandsetProfileTab();
	}
	
	@Test
	public void n_AccessHardLockFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_HardLockTab();
	}
	
	@Test
	public void o_AccessInstantBillFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_InstantBillSubscriberTab();
	}
	
	@Test
	public void p_AccessMigrateFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_Migrate_Tab();
	}
	
	@Test
	public void q_AccessMSISDNSwapFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_MSISDNswap_Tab();
	}
	
	@Test
	public void r_AccessOTAFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_OTA_Tab();
	}
	
	@Test
	public void s_AccessPenaltySimulationFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_PenaltySimulation_Tab();
	}
	
	@Test
	public void zz_AccessQuickCallsFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_QuickCalls_Tab();
	}
	
	@Test
	public void u_AccessRCSFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_RCS_Tab();
	}
	
	@Test
	public void v_AccessSIMSwapFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_SimSwap_Tab();
	}
	
	@Test
	public void w_AccessSponserFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		 Access_Sponsor_Tab();
	}
	
	@Test
	public void wa_AccessTransactionHistoryFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_TransactionHistory_Tab();
	}
	
	@Test
	public void x_AccessVASLimitFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_VASLimit_Tab();
	}
	
	@Test
	public void xa_AccessViewInvoicesFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_ViewInvoice_Tab();
	}
	
	@Test
	public void y_AccessVoiceMailFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		Access_VoiceMail_Tab();
	}
	
	@Test
	public void z_AccessLayByFor_SubscriberFromLeftHandMenu() throws InterruptedException
	{
		 Access_LayBy_Tab();
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************* Method to access account swap*****************************
public void Access_AccountSwap() throws InterruptedException
{
	
	WebElement account_swap= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Account Swap')]"));

	Actions act= new Actions(driver);

	act.moveToElement(account_swap).click().build().perform();

	Thread.sleep(400);

	Boolean verify= driver.findElement(By.id("P43_NEW_ACCOUNT")).isDisplayed();

	Assert.assertTrue(verify);
}


//************************************* Method to access add/remove services *****************************
public void Access_AddRemoveServices() throws InterruptedException
{
	driver.navigate().back();
			
	Thread.sleep(400);
			
	WebElement addRemove= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Add/Remove Services')]"));
			
	Actions act= new Actions(driver);
			
	act.moveToElement(addRemove).click().build().perform();
			
	Thread.sleep(1000);
			
	Boolean verify= driver.findElement(By.id("SRV_NAME")).isDisplayed();
							
	Assert.assertTrue(verify);

}
		

//************************************* Method to access autobar history *****************************
public void Access_AutoBarHistory() throws InterruptedException
{
	driver.navigate().back();
					
	Thread.sleep(400);
					
	WebElement AutoBarHistory= driver.findElement(By.xpath("//a[contains(text(),'AutoBarr History')]"));
					
	Actions act= new Actions(driver);
					
	act.moveToElement(AutoBarHistory).click().build().perform();
					
	Thread.sleep(1000);
					
	Boolean verify= driver.findElement(By.id("SRV_NAME")).isDisplayed();
									
	Assert.assertTrue(verify);
	
}
		

//************************************* Method to access average Spend *****************************
public void Access_AverageSpend() throws InterruptedException
{
	driver.navigate().back();
							
	Thread.sleep(400);
							
	WebElement AvgSpend= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Average Spend')]"));
							
	Actions act= new Actions(driver);
							
	act.moveToElement(AvgSpend).click().build().perform();
							
	Thread.sleep(1000);
							
	Boolean verify= driver.findElement(By.id("P75_PERIODS")).isDisplayed();
											
	Assert.assertTrue(verify);

}
		
//************************************* Method to access Bill shock *****************************
public void Access_BillShock() throws InterruptedException
{
	driver.navigate().back();
									
	Thread.sleep(400);
									
	WebElement BillShock= driver.findElement(By.xpath("//a[contains(text(),'Bill Shock')]"));
									
	Actions act= new Actions(driver);
									
	act.moveToElement(BillShock).click().build().perform();
									
	Thread.sleep(1000);
									
	Boolean verify= driver.findElement(By.id("apexir_SUBRA_SUBR_UID")).isDisplayed();
													
	Assert.assertTrue(verify);

}
		
		
//************************************* Method to access Contract Upgrade *****************************
public void Access_ContractUpgrade() throws InterruptedException
{
	driver.navigate().back();
									
	Thread.sleep(400);
									
	WebElement ContractUpgrade= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Contract Upgrade')]"));
									
	Actions act= new Actions(driver);
									
	act.moveToElement(ContractUpgrade).click().build().perform();
									
	Thread.sleep(1000);
									
	Boolean verify= driver.findElement(By.id("P103_IMEI")).isDisplayed();
													
	Assert.assertTrue(verify);
	
}
		
//************************************* Method to access Credit tab *****************************
public void Access_CreditTab() throws InterruptedException
{
	driver.navigate().back();
											
	Thread.sleep(400);
											
	WebElement CreditTab= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Credit Tab')]"));
											
	Actions act= new Actions(driver);
											
	act.moveToElement(CreditTab).click().build().perform();
											
	Thread.sleep(1000);
											
	Boolean verify= driver.findElement(By.id("apexir_SEARCH")).isDisplayed();
															
	Assert.assertTrue(verify);

}
		
//************************************* Method to access CSR *****************************
public void Access_CSRTab() throws InterruptedException
{
	driver.navigate().back();
											
	Thread.sleep(400);
											
	WebElement CSR= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'CSR')]"));
											
	Actions act= new Actions(driver);
											
	act.moveToElement(CSR).click().build().perform();
											
	Thread.sleep(1000);
											
	Boolean verify= driver.findElement(By.id("B17028347195396813")).isDisplayed();
															
	Assert.assertTrue(verify);

}
		
		
//************************************* Method to access Discounts *****************************
public void Access_DiscountsTab() throws InterruptedException
{
	driver.navigate().back();
											
	Thread.sleep(400);
											
	WebElement discount= driver.findElement(By.xpath("//div[@id='R21887513485999063']//a[contains(text(),'Discounts')]"));
											
	Actions act= new Actions(driver);
											
	act.moveToElement(discount).click().build().perform();
											
	Thread.sleep(1000);
											
	Boolean verify= driver.findElement(By.id("B5025316912188034")).isDisplayed();
															
	Assert.assertTrue(verify);

}
		
		
//************************************* Method to access External Spend *****************************
public void Access_ExternalSpendTab() throws InterruptedException
{
	driver.navigate().back();
											
	Thread.sleep(400);
											
	WebElement Espend= driver.findElement(By.xpath("//a[contains(text(),'External Spend')]"));
											
	Actions act= new Actions(driver);
											
	act.moveToElement(Espend).click().build().perform();
											
	Thread.sleep(1000);
											
	Boolean verify= driver.findElement(By.id("B5025316912188034")).isDisplayed();
															
	Assert.assertTrue(verify);

}
		
//************************************* Method to access Financial Adjustment *****************************
public void Access_FinancialAdjustmentTab() throws InterruptedException
 {
		driver.navigate().back();
													
		Thread.sleep(400);
													
		WebElement adjustment= driver.findElement(By.xpath("//div[@id='R21887513485999063']//a[contains(text(),'Financial Adjustment')]"));
												
		Actions act= new Actions(driver);
													
		act.moveToElement(adjustment).click().build().perform();
													
		Thread.sleep(1000);
													
		Boolean verify= driver.findElement(By.id("B5025316912188034")).isDisplayed();
																	
		Assert.assertTrue(verify);

 }
		
//************************************* Method to access fraud MSISDN *****************************
public void Access_FraudMSISDNTab() throws InterruptedException
	{
		driver.navigate().back();
															
		Thread.sleep(400);
															
		WebElement FraudMSISDN= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Fraud MSISDN')]"));
														
		Actions act= new Actions(driver);
															
		act.moveToElement(FraudMSISDN).click().build().perform();
															
		Thread.sleep(1000);
															
		Boolean verify= driver.findElement(By.id("P66_FM_PID_NOT_ALLOWED")).isDisplayed();
																			
		Assert.assertTrue(verify);

	}
		
//************************************* Method to access Handset profile *****************************
public void Access_HandsetProfileTab() throws InterruptedException
{
	driver.navigate().back();
																	
	Thread.sleep(400);
																	
	WebElement handset= driver.findElement(By.xpath("//a[contains(text(),'Handset Profile')]"));
																
	Actions act= new Actions(driver);
																	
	act.moveToElement(handset).click().build().perform();
																	
	Thread.sleep(1000);
																	
	Boolean verify= driver.findElement(By.id("P66_FM_PID_NOT_ALLOWED")).isDisplayed();
																					
	Assert.assertTrue(verify);

}
		
		
//************************************* Method to access Hard lock *****************************
public void Access_HardLockTab() throws InterruptedException
{
	driver.navigate().back();
																	
	Thread.sleep(400);
																	
	WebElement hardlock= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Hard Lock')]"));
																
	Actions act= new Actions(driver);
																	
	act.moveToElement(hardlock).click().build().perform();
																	
	Thread.sleep(1000);
																	
	Boolean verify= driver.findElement(By.id("P117_MSISDN_2")).isDisplayed();
																					
	Assert.assertTrue(verify);

}
		
//************************************* Method to access instant bill subscriber *****************************
public void Access_InstantBillSubscriberTab() throws InterruptedException
{
	driver.navigate().back();
																	
	Thread.sleep(400);
																	
	WebElement instantBill= driver.findElement(By.xpath("//a[contains(text(),'Instant Bill Subscriber')]"));
																
	Actions act= new Actions(driver);
																	
	act.moveToElement(instantBill).click().build().perform();
																	
	Thread.sleep(1000);
																	
	Boolean verify= driver.findElement(By.id("P18_BILL_RUN_TYPE")).isDisplayed();
																					
	Assert.assertTrue(verify);

}
		
//************************************* Method to access Migrate *****************************
public void Access_Migrate_Tab() throws InterruptedException
	{
		driver.navigate().back();
																	
		Thread.sleep(400);
																	
		WebElement migrate= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Migrate')]"));
																
		Actions act= new Actions(driver);
																	
		act.moveToElement(migrate).click().build().perform();
																	
		Thread.sleep(1000);
																	
		Boolean verify= driver.findElement(By.id("P37_FALE_MAIS_DEPOSIT")).isDisplayed();
																					
		Assert.assertTrue(verify);
		
	}
		
		
//************************************* Method to access MSISDN Swap *****************************
public void Access_MSISDNswap_Tab() throws InterruptedException
 {
		driver.navigate().back();
																			
		Thread.sleep(400);
																			
		WebElement swap= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'MSISDN Swap')]"));
																		
		Actions act= new Actions(driver);
																			
		act.moveToElement(swap).click().build().perform();
																			
		Thread.sleep(1000);
																			
		Boolean verify= driver.findElement(By.id("P48_RESERVE_NO")).isDisplayed();
																							
		Assert.assertTrue(verify);

 }
		
		
//************************************* Method to access OTA *****************************
public void Access_OTA_Tab() throws InterruptedException
{
		driver.navigate().back();
																			
		Thread.sleep(400);
																			
		WebElement OTA= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'OTA')]"));
																		
		Actions act= new Actions(driver);
																			
		act.moveToElement(OTA).click().build().perform();
																			
		Thread.sleep(1000);
																			
		Boolean verify= driver.findElement(By.id("P124_IMEI")).isDisplayed();
																						
		Assert.assertTrue(verify);

}
		
//************************************* Method to access Penalty Simulation *****************************
public void Access_PenaltySimulation_Tab() throws InterruptedException
{
		driver.navigate().back();
																				
		Thread.sleep(400);
																					
		WebElement penalty= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Penalty Simulation')]"));
																				
		Actions act= new Actions(driver);
																					
		act.moveToElement(penalty).click().build().perform();
																					
		Thread.sleep(1000);
																					
		Boolean verify= driver.findElement(By.id("B79593208758301843")).isDisplayed();
																									
		Assert.assertTrue(verify);
	
	}
		
//************************************* Method to access Quick Calls *****************************
public void Access_QuickCalls_Tab() throws InterruptedException
	{
		driver.navigate().back();
																						
		Thread.sleep(400);
																							
		WebElement quickCalls= driver.findElement(By.xpath("//a[@class='echo-current'][contains(text(),'Quick Calls')]"));
																						
		Actions act= new Actions(driver);
																							
		act.moveToElement(quickCalls).click().build().perform();
																							
		Thread.sleep(1000);
																							
		Boolean verify= driver.findElement(By.id("B79593208758301843")).isDisplayed();
																											
		Assert.assertTrue(verify);
			
	}
		
//************************************* Method to access RCS tab *****************************
public void Access_RCS_Tab() throws InterruptedException
	{
		driver.navigate().back();
																								
		Thread.sleep(400);
																									
		WebElement RCS= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'RCS')]"));
																								
		Actions act= new Actions(driver);
																									
		act.moveToElement(RCS).click().build().perform();
																									
		Thread.sleep(1000);
																									
		Boolean verify= driver.findElement(By.id("report_R16612863528389370")).isDisplayed();
																													
		Assert.assertTrue(verify);
					
	}
		
//************************************* Method to access SIM swap tab *****************************
public void Access_SimSwap_Tab() throws InterruptedException
	{
		driver.navigate().back();
																										
		Thread.sleep(400);
																											
		WebElement SIM= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'SIM Swap')]"));
																										
		Actions act= new Actions(driver);
																											
		act.moveToElement(SIM).click().build().perform();
																											
		Thread.sleep(1000);
																											
		Boolean verify= driver.findElement(By.id("P46_ICCID")).isDisplayed();
																															
		Assert.assertTrue(verify);
							
	}
 	
//************************************* Method to access Sponsor tab *****************************
public void Access_Sponsor_Tab() throws InterruptedException
	{
   		driver.navigate().back();
 																											
		Thread.sleep(400);
 																												
		WebElement Sponsor= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Sponsor')]"));
 																											
       	Actions act= new Actions(driver);
 																												
		act.moveToElement(Sponsor).click().build().perform();
 																												
		Thread.sleep(1000);
 																												
		Boolean verify= driver.findElement(By.id("B16541864605415761")).isDisplayed();
 																																
		Assert.assertTrue(verify);
 								
	}
 	
//************************************* Method to access Transaction History tab *****************************
public void Access_TransactionHistory_Tab() throws InterruptedException
{
		driver.navigate().back();
 																											
 		Thread.sleep(400);
 																												
 		WebElement THistory= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Transaction History')]"));
 																											
 	    Actions act= new Actions(driver);
 																												
 		act.moveToElement(THistory).click().build().perform();
 																												
 		Thread.sleep(1000);
 																												
 		Boolean verify= driver.findElement(By.id("apexir_ACTIONSMENUROOT")).isDisplayed();
 																																
 		Assert.assertTrue(verify);
 								
 	}
 	
//************************************* Method to access VAS Limit tab *****************************
public void Access_VASLimit_Tab() throws InterruptedException
{
		driver.navigate().back();
 	 																											
		Thread.sleep(400);
 	 																												
		WebElement vas= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'VAS Limit')]"));
 	 																											
       	Actions act= new Actions(driver);
 	 																												
		act.moveToElement(vas).click().build().perform();
 	 																												
		Thread.sleep(1000);
 	 																												
		Boolean verify= driver.findElement(By.id("B16801717287009647")).isDisplayed();
 	 																																
		Assert.assertTrue(verify);
 	 								
	}
 	
//************************************* Method to access View Invoice tab *****************************
public void Access_ViewInvoice_Tab() throws InterruptedException
	{
   		driver.navigate().back();
 	 																											
		Thread.sleep(400);
 	 																												
		WebElement invoice= driver.findElement(By.xpath("//div[@id='R21887513485999063']//a[contains(text(),'View Invoices')]"));
 	 																											
       	Actions act= new Actions(driver);
 	 																												
		act.moveToElement(invoice).click().build().perform();
 	 																												
		Thread.sleep(1000);
 	 																												
		Boolean verify= driver.findElement(By.id("B16366916284812711")).isDisplayed();
 	 																																
		Assert.assertTrue(verify);
 	 								
	}
 	
//************************************* Method to access Voice mail tab *****************************
public void Access_VoiceMail_Tab() throws InterruptedException
	{
   		driver.navigate().back();
 	 																											
		Thread.sleep(400);
 	 																												
		WebElement voice= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Voice Mail')]"));
 	 																											
       	Actions act= new Actions(driver);
 	 																												
		act.moveToElement(voice).click().build().perform();
 	 																												
		Thread.sleep(1000);
 	 																												
		Boolean verify= driver.findElement(By.id("R81845117277937330")).isDisplayed();
 	 																																
		Assert.assertTrue(verify);
 	 								
	}
 	
//************************************* Method to access Lay By tab *****************************
public void Access_LayBy_Tab() throws InterruptedException
	{
   		driver.navigate().back();
 	 																											
		Thread.sleep(400);
 	 																												
		WebElement LayBy= driver.findElement(By.xpath("//a[contains(text(),'LayBy')]"));
 	 																											
       	Actions act= new Actions(driver);
 	 																												
		act.moveToElement(LayBy).click().build().perform();
 	 																												
		Thread.sleep(1000);
 	 																												
		Boolean verify= driver.findElement(By.id("apexir_ACTIONSMENUROOT")).isDisplayed();
 	 																																
		Assert.assertTrue(verify);
 	 								
	}
}
