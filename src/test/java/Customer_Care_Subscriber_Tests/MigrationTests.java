package Customer_Care_Subscriber_Tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common_methods.CommonMethods;

public class MigrationTests extends CommonMethods{
	

	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	}
	
	@AfterClass(alwaysRun=true)
	public void end() throws InterruptedException
	{
		Thread.sleep(1000);
		driver.close();
	}
	
	@Test
	public void a_Click_Migrate_ForSubscriber() throws InterruptedException
	{
		Thread.sleep(1000);
		Click_migrate();
	}
	
	@Test
	public void b_select_tariffType()
	{
		SelectTariffType();
	}
	
	@Test
	public void c_Click_ratePlanOption()
	{
		clickRatePlan();
	}
	
	@Test
	public void d_Select_NewRatePlan()
	{
		selectnewRateplan();
	}
	
	@Test
	public void e_Click_Next()
	{
		CLickNext();
	}
	
	@Test
	public void f_selectTrespass() throws InterruptedException
	{
		Thread.sleep(500);
		select_Trespass();
	}
	
	@Test
	public void g_Click_submit() throws InterruptedException
	{
		Thread.sleep(500);
		click_submit();
	}
	
	@Test
	public void y_Success_Msg()
	{
		success_msg();
	}
	
	@Test
	public void z_CheckRCSStatus() throws InterruptedException
	{
		Thread.sleep(800);
		RCSStatus_ForMigration();
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////////
//******************************** Method to click bill limit history ***************************
public void Click_migrate() throws InterruptedException
{		
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
	
	WebElement attachment= driver.findElement(By.id("B13991474607411197"));
	
	Boolean name= attachment.isDisplayed();
	
	if(name==true)    {
		   
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(attachment).doubleClick().build().perform();
		
		Thread.sleep(1500);
		
		}
	}
//****************************** Method to click rate plan ************************************
public void clickRatePlan()
{
	WebElement rateplan= driver.findElement(By.id("P37_RP_DEAL_0"));

	Actions act= new Actions(driver);
	
	act.moveToElement(rateplan).click().build().perform();
	
}
//****************************** Method to select new rate plan from dropdown *****************
public void selectnewRateplan()
{
	Select dropdown= new Select(driver.findElement(By.id("P37_NEW_RP_UID")));

	dropdown.selectByIndex(2);
	
}

//****************************** Method to select TreePass ******************************
public void select_Trespass()
{
	driver.findElement(By.id("P37_TRESPASS_0")).click();
		
}
	
//**************************** Method to click submit button *****************************
public void click_submit()
{
	try {
			WebElement submit= driver.findElement(By.id("B13452161266335346"));
			
			Actions act= new Actions(driver);
			
			act.moveToElement(submit).doubleClick().build().perform();
	
	}catch(NoSuchElementException |StaleElementReferenceException e){
	
		WebElement submit= driver.findElement(By.id("B13452161266335346"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(submit).click().build().perform();
		
	}
	
}
//*************************** Method to check success message **************************
public void success_msg()
{
	String Msg= driver.findElement(By.id("echo-message")).getText();

	Boolean msg= Msg.endsWith("Successful");
	
	Assert.assertTrue(msg);
	
}
	
//******************************* Method to check success status on RCS after migration  ********************
public void RCSStatus_ForMigration() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement RCS= driver.findElement(By.id("B16614757006394351"));
	
	Boolean name= RCS.isDisplayed();
	
	if(name==true)  {
		
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(RCS).click().build().perform();
		
		Thread.sleep(1500);
		
		String Status= driver.findElement(By.xpath("//tr[2]//td[10]")).getText();
		
		Assert.assertEquals(Status, "SUCCESS");
		
	} 
	
}
//************************************** Method to click next ***********************************************
public void CLickNext()
{
	WebElement next= driver.findElement(By.id("B10098706477630094"));

	Actions act= new Actions(driver);
	
	act.moveToElement(next).click().build().perform();
			
}

//********************************* Method to select tariff type **********************************
public void SelectTariffType()
{
	WebElement tariff= driver.findElement(By.id("P37_TARIFF_TYPE_0"));

	Actions act= new Actions(driver);
	
	act.moveToElement(tariff).click().build().perform();
	
}

}
