package Customer_Care_Subscriber_Tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common_methods.NotificationTests;

public class SubscriberNotificationTests extends NotificationTests{
	@BeforeClass()
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
	   driver.quit();
		
	}
	
	@Test
	public void a_Click_Notification() throws InterruptedException
	{
		Thread.sleep(1500);
		click_notification();
	}
	
	@Test
	public void b_Click_CreateNote() throws InterruptedException
	{

		Thread.sleep(500);
		click_createnote();
	}
	
	@Test
	public void c_selectEndDate() throws InterruptedException
	{
		Thread.sleep(500);
		enter_endDate();
	}
	@Test
	public void e_Enter_Message() throws InterruptedException
	{

		Thread.sleep(500);
		enter_message();
	}
	
	@Test
	public void g_Click_Create() throws InterruptedException
	{

		Thread.sleep(1500); 
		click_create();	
	}
	
	@Test
	public void h_Check_Success_Message() throws InterruptedException
	{

		Thread.sleep(1500);
		Success_Msg();
	}
	
	@Test
	public void i_ClickEdit_notification() throws InterruptedException 
	{
		Thread.sleep(1000);
		edit_note();
		
	}
	
	@Test
	public void j_ChangeMessage() throws InterruptedException 
	{
		Thread.sleep(1000);
		change_Msg();
	}
	
	@Test
	public void k_clickApply_changes() throws InterruptedException 
	{
		Thread.sleep(1000);
		Click_applyChanges();
		
	}
	
	@Test
	public void l_checkEdit_SuccessMessage() throws InterruptedException 
	{
		Thread.sleep(1000);
		Edit_success_msg();
		
	}
	
	@Test
	public void la_VerifyCreatedNotes() throws InterruptedException 
	{
		Thread.sleep(1000);
		verify_note();
		
	}
	
	@Test
	public void m_Delete_notification() throws InterruptedException 
	{
		Thread.sleep(1200);
   	  deleteANDverify();
		
	}
	
	@Test
	public void n_Delete_note_Successmsg() throws InterruptedException 
	{
		Thread.sleep(1000);
		
		Success_Msg();
	}

}
