package Customer_Care_Subscriber_Tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common_methods.AttachmentTests;

public class SubscriberAttachmentTests extends AttachmentTests
{
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	    ClickAttachment();
	    Thread.sleep(1000);
	  	
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}
	

	@Test
	public void b_Select_Document_Type() throws InterruptedException
	{
		Select_document_Type();
	}
	
	@Test
	public void c_Enter_name()
	{
		Enter_Name();
	}
	
	@Test
	public void d_Select_File() throws InterruptedException
	{
		
		Select_file();
	}
	
	@Test
	public void e_Click_Attach()
	{
		Click_attach();
	}
	
	@Test
	public void f_View_Attachments()
	{
		view_attachment();
	}
	
		
	@Test
	public void g_ClickEdit_attachment() throws InterruptedException
	{
		click_edit();
	} 
	
	@Test
	public void h_Edit_attach_new_File() throws InterruptedException
	{
		Attach_edit();
	}  
	
	
	@Test
	public void j_delete_attachment() throws InterruptedException
	{
	Delete_Attachment();
	}
	
	@Test
	public void i_Canceldelettion() throws InterruptedException
	{
		cancel_deletion_attachment();
	}
	

}
	
	


