package Customer_Care_Subscriber_Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AccessAccountFunctionsFromLeftHandMenu  extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	  driver.quit();			
	}

	@Test
	public void a_Access_AccountDiscount() throws InterruptedException
	{
		Access_AccountDiscount();
	}
	
	@Test
	public void b_Access_AccountServices() throws InterruptedException
	{
		Access_AccountServices();
	}
	
	@Test
	public void c_Access_AccountSubscription() throws InterruptedException
	{
		Access_AccountSubscription();
	}
	
	@Test
	public void d_Access_AdHocChanrges() throws InterruptedException
	{
		Access_AdHocCharges();
	}
	
	@Test
	public void e_Access_AutoBarHistory() throws InterruptedException
	{
		Access_AutoBarHistory();
	}
	
	@Test
	public void f_Access_DeactivateAccount() throws InterruptedException
	{
		Access_DeactivateAccount();
	}
	
	@Test
	public void g_Access_FinancialAdjustment() throws InterruptedException
	{
		Access_FinancialAdjustment();
	}
	
	@Test
	public void h_Access_InstantBillCustomer() throws InterruptedException
	{
		Access_InstantBillCustomer();
	}
	
	@Test
	public void i_Access_ViewInvoices() throws InterruptedException
	{
		Access_ViewInvoices();
	}
	
	@Test
	public void j_Access_ViewPayments() throws InterruptedException
	{
		Access_ViewPayments();
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************* Method to access account discount *****************************
public void Access_AccountDiscount() throws InterruptedException
{
	WebElement discount= driver.findElement(By.xpath("//a[contains(text(),'Account Discounts')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(discount).click().build().perform();
	
	Thread.sleep(400);
	
	Boolean verify= driver.findElement(By.id("")).isDisplayed();
	
	Assert.assertTrue(verify);
}

//************************************* Method to access account services *****************************
public void Access_AccountServices() throws InterruptedException
{
	driver.navigate().back();
	
	Thread.sleep(400);
	
	WebElement services= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Account Services')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(services).click().build().perform();
	
	Thread.sleep(1000);
	
	Boolean verify= driver.findElement(By.id("B2799109162334156")).isDisplayed();
	
	Assert.assertTrue(verify);
}

//************************************* Method to access account subscription *****************************
public void Access_AccountSubscription() throws InterruptedException
{
	driver.navigate().back();
	
	Thread.sleep(400);
	
	WebElement subscription= driver.findElement(By.xpath("//a[contains(text(),'Account Subscriptions')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(subscription).click().build().perform();
	
	Thread.sleep(1000);
	
	Boolean verify= driver.findElement(By.id("P115_SUBRE_REQUEST")).isDisplayed();
	
	Assert.assertTrue(verify);
}	

//************************************* Method to access adHoc Charges *****************************
public void Access_AdHocCharges() throws InterruptedException
{
	driver.navigate().back();
	
	Thread.sleep(400);
	
	WebElement adHoc= driver.findElement(By.xpath("//a[contains(text(),'Ad-hoc Charges View')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(adHoc).click().build().perform();
	
	Thread.sleep(1000);
	
	Boolean verify= driver.findElement(By.id("apexir_ACTIONSMENUROOT")).isDisplayed();
	
	Assert.assertTrue(verify);
}

//************************************* Method to access AutoBar History *****************************
public void Access_AutoBarHistory() throws InterruptedException
{
	driver.navigate().back();
	
	Thread.sleep(400);
	
	WebElement autoBar= driver.findElement(By.xpath("//a[contains(text(),'Autobarr History')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(autoBar).click().build().perform();
	
	Thread.sleep(1000);
	
	Boolean verify= driver.findElement(By.id("  ")).isDisplayed();
	
	Assert.assertTrue(verify);
}

//************************************* Method to access Deactivate Account *****************************
public void Access_DeactivateAccount() throws InterruptedException
{
	driver.navigate().back();
	
	Thread.sleep(400);
	
	WebElement deactivate= driver.findElement(By.xpath("//a[contains(text(),'Deactivate Account')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(deactivate).click().build().perform();
	
	Thread.sleep(1000);
	
	Boolean verify= driver.findElement(By.id("P82_ACCOUNT_2")).isDisplayed();
	
	Assert.assertTrue(verify);
}

//************************************* Method to access financial adjustment *****************************
public void Access_FinancialAdjustment() throws InterruptedException
{
	driver.navigate().back();

	Thread.sleep(400);
	
	WebElement adjustment= driver.findElement(By.xpath("//div[@id='R21887513485999063']//a[contains(text(),'Financial Adjustment')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(adjustment).click().build().perform();
	
	Thread.sleep(1000);
	
	Boolean verify= driver.findElement(By.id("    ")).isDisplayed();
	
	Assert.assertTrue(verify);
}

//************************************* Method to access instantBillCustomer *****************************
public void Access_InstantBillCustomer() throws InterruptedException
{
	driver.navigate().back();

	Thread.sleep(400);
	
	WebElement instantBill= driver.findElement(By.xpath("//a[contains(text(),'Instant Bill Customer')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(instantBill).click().build().perform();
	
	Thread.sleep(1000);
	
	Boolean verify= driver.findElement(By.id("P18_BILL_RUN_TYPE")).isDisplayed();
	
	Assert.assertTrue(verify);
}

//************************************* Method to access view invoices *****************************
public void Access_ViewInvoices() throws InterruptedException
{
	driver.navigate().back();

	Thread.sleep(400);
	
	WebElement ViewInvoices= driver.findElement(By.xpath("//div[@id='R20187428462554920']//a[contains(text(),'View Invoices')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(ViewInvoices).click().build().perform();
	
	Thread.sleep(1000);
	
	Boolean verify= driver.findElement(By.id("B16366916284812711")).isDisplayed();
	
	Assert.assertTrue(verify);
}

//************************************* Method to access view payments *****************************
public void Access_ViewPayments() throws InterruptedException
{
	driver.navigate().back();
	
	Thread.sleep(400);
	
	WebElement ViewPayment= driver.findElement(By.xpath("//a[contains(text(),'View Payments')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(ViewPayment).click().build().perform();
	
	Thread.sleep(1000);
	
	Boolean verify= driver.findElement(By.id("B16696307083201909")).isDisplayed();
	
	Assert.assertTrue(verify);
  
  }
}
