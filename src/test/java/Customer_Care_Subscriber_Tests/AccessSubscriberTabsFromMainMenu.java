package Customer_Care_Subscriber_Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AccessSubscriberTabsFromMainMenu  extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		Thread.sleep(1000);
	    driver.quit();			
	}
	
	@Test
	public void y_Access_EditSubscriber() throws InterruptedException
	{
		Access_EditSubscriber() ;
	}
	
	@Test
	public void ab_Access_QuickNotes() throws InterruptedException
	{
		Access_QuickNotes();
	}
	
	@Test
	public void c_Access_Attachments() throws InterruptedException
	{
		Access_Attachment();
	}
	
	@Test
	public void d_Access_SMSNotification() throws InterruptedException
	{
		Access_SMSNotification();
	}
	
	@Test
	public void da_Access_BillLimitHistory() throws InterruptedException
	{
		Access_BillLimitHistory();
	}
	
	@Test
	public void e_Access_Communication() throws InterruptedException
	{
		Access_Communication();
	}
	
	@Test
	public void ea_Access_Notification() throws InterruptedException
	{
		Access_Notification();
	}
	
	@Test
	public void f_Access_SubscriberDebugData() throws InterruptedException
	{
		Access_DebugData();
	}
	
	@Test
	public void fa_Access_Migrate() throws InterruptedException
	{
		Access_Migrate_Tab();
	}
	
	@Test
	public void g_Access_AddRemoveServices() throws InterruptedException
	{
		Access_AddRemoveServices();	
	}

	@Test
	public void ga_Access_SIMSwap() throws InterruptedException
	{
		Access_SimSwap_Tab();
	}
	
	@Test
	public void h_Access_MSISDNSwap() throws InterruptedException
	{
		Access_MSISDNswap_Tab();
	}
	
	@Test
	public void ha_Access_TransactionHistory() throws InterruptedException
	{
		Access_TransactionHistory_Tab();
	}
	
	@Test
	public void i_Access_ViewInvoices() throws InterruptedException
	{
		Access_ViewInvoice_Tab();	
	}
	
	@Test
	public void z_Access_QuickCalls() throws InterruptedException
	{
		Access_QuickCalls_Tab();
	}
	
	@Test
	public void j_Access_Sponsers() throws InterruptedException
	{
		Access_Sponsor_Tab();
	}
	
	@Test
	public void ja_Access_RCS() throws InterruptedException
	{
		Access_RCS_Tab();
	}
	
	@Test
	public void k_Access_CSR() throws InterruptedException
	{
		Access_CSRTab();
	}
	
	@Test
	public void ka_Access_AccountSwap() throws InterruptedException
	{
		Access_AccountSwap();
	}
	
	@Test
	public void l_Access_Discounts() throws InterruptedException
	{
		Access_DiscountsTab();
	}
	
	@Test
	public void la_Access_FraudMSISDN() throws InterruptedException
	{
		Access_FraudMSISDNTab();
	}
	
	@Test
	public void m_Access_AverageSpend() throws InterruptedException
	{
		Access_AverageSpend();
	}
	
	@Test
	public void n_Access_InstantBill() throws InterruptedException
	{
		Access_InstantBillSubscriberTab();
	}
	
	@Test
	public void o_Access_CreditTab() throws InterruptedException
	{
		Access_CreditTab();
	}
	
	@Test
	public void p_Access_Infotext() throws InterruptedException
	{
		Access_Infotext();
	}
	
	@Test
	public void q_Access_ThirdParty() throws InterruptedException
	{
		Access_ThirdParty();
	}
	
	@Test
	public void r_Access_ContractUpgrade() throws InterruptedException
	{
		Access_ContractUpgrade();
	}
	
	@Test
	public void s_Access_NotificationExclusion() throws InterruptedException
	{
		Access_NotificationExclusion() ;
	}
	
	@Test
	public void t_Access_HardLock() throws InterruptedException
	{
		 Access_HardLockTab();
	}
	
	@Test
	public void u_Access_PenaltySimulation() throws InterruptedException
	{
		Access_PenaltySimulation_Tab();
	}
	
	@Test
	public void v_Access_OTA() throws InterruptedException
	{
		Access_OTA_Tab();
	}
	
	@Test
	public void w_Access_VoiceMail() throws InterruptedException
	{
		Access_VoiceMail_Tab();
	}
	
	@Test
	public void wa_Access_VASLimit() throws InterruptedException
	{
		Access_VASLimit_Tab();
	}
	
	@Test
	public void x_Access_LayBy() throws InterruptedException
	{
		Access_LayBy_Tab();
	}
	
	@Test
	public void xa_Access_TestInvoice() throws InterruptedException
	{
		Access_TestInvoice();
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************* Method to access Edit subscriber *****************************
public void Access_EditSubscriber() throws InterruptedException
	{
		Thread.sleep(400);
												
		WebElement edit= driver.findElement(By.id("B13990173998392109"));
											
		Actions act= new Actions(driver);
												
		act.moveToElement(edit).click().build().perform();
												
		Thread.sleep(600);
												
		Boolean verify= driver.findElement(By.id("P34_BSD_NAME")).isDisplayed();
																
		Assert.assertTrue(verify);
	}
			
//************************************* Method to access quick note *****************************
 public void Access_QuickNotes() throws InterruptedException
 {
											
	Thread.sleep(400);
								
	WebElement QuickNote= driver.findElement(By.id("B13536255917213352"));
								
	Actions act= new Actions(driver);
								
	act.moveToElement(QuickNote).click().build().perform();
								
	Thread.sleep(600);
								
	Boolean verify= driver.findElement(By.id("B13535157679207053")).isDisplayed();
												
	Assert.assertTrue(verify);

 }

 //************************************* Method to access attachment*****************************
 public void Access_Attachment() throws InterruptedException
 {
    driver.navigate().back();
				
    Thread.sleep(400);
   			
   	WebElement attachment= driver.findElement(By.id("B8982416461177513"));
    			
    Actions act= new Actions(driver);
    			
    act.moveToElement(attachment).click().build().perform();
    			
    Thread.sleep(600);
    			
    Boolean verify= driver.findElement(By.id("B15126227968958324")).isDisplayed();
    						
    Assert.assertTrue(verify);
    
 }
   		
//************************************* Method to access SMS notification *****************************
public void Access_SMSNotification() throws InterruptedException
{
	driver.navigate().back();
   							
	Thread.sleep(400);
   								
	WebElement SMSnotification= driver.findElement(By.id("B12429217325496444"));
   							
	Actions act= new Actions(driver);
   								
	act.moveToElement(SMSnotification).click().build().perform();
   								
	Thread.sleep(600);
  								
	Boolean verify= driver.findElement(By.id("B12429021350488144")).isDisplayed();
   												
	Assert.assertTrue(verify);

}
		
//************************************* Method to access Bill Limit History *****************************
public void Access_BillLimitHistory() throws InterruptedException
{

	driver.navigate().back();
   							
	Thread.sleep(400);
   								
	WebElement history= driver.findElement(By.id("B43732225229299245"));
   							
	Actions act= new Actions(driver);
   								
	act.moveToElement(history).click().build().perform();
   								
	Thread.sleep(600);
   								
	Boolean verify= driver.findElement(By.id("P0_SEARCH_VALUE")).isDisplayed();
   												
	Assert.assertTrue(verify);
	
}

//************************************* Method to access communication *****************************
public void Access_Communication() throws InterruptedException
{
	driver.navigate().back();
					
	Thread.sleep(400);
					
	WebElement com= driver.findElement(By.id("B43693221539123111"));
					
	Actions act= new Actions(driver);
					
	act.moveToElement(com).click().build().perform();
					
	Thread.sleep(1000);
					
	Boolean verify= driver.findElement(By.id("apexir_ACTIONSMENUROOT")).isDisplayed();
									
	Assert.assertTrue(verify);
	
}
		
//************************************* Method to access notification *****************************
public void Access_Notification() throws InterruptedException
{

	driver.navigate().back();
							
	Thread.sleep(400);
						
	WebElement notification= driver.findElement(By.id("B19744324048754180"));
							
	Actions act= new Actions(driver);
							
	act.moveToElement(notification).click().build().perform();
							
	Thread.sleep(600);
							
	Boolean verify= driver.findElement(By.id("B12131301940768901")).isDisplayed();
											
	Assert.assertTrue(verify);
	
}	
		
//************************************* Method to access Subscriber's debug data *****************************
public void Access_DebugData() throws InterruptedException
{
	driver.navigate().back();
	
	Thread.sleep(400);
											
	WebElement DD= driver.findElement(By.id("B32342205552931356"));
										
	Actions act= new Actions(driver);
											
	act.moveToElement(DD).click().build().perform();
											
	Thread.sleep(600);
											
	Boolean verify= driver.findElement(By.xpath("//div[@id='R14454415541523068']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']")).isDisplayed();
															
	Assert.assertTrue(verify);

	}
		
//************************************* Method to access Migrate *****************************
public void Access_Migrate_Tab() throws InterruptedException
{
	driver.navigate().back();
																			
	Thread.sleep(400);
																			
	WebElement migrate= driver.findElement(By.id("B13991474607411197"));
																		
	Actions act= new Actions(driver);
																			
	act.moveToElement(migrate).click().build().perform();
																			
	Thread.sleep(1000);
																			
	Boolean verify= driver.findElement(By.id("P37_FALE_MAIS_DEPOSIT")).isDisplayed();
																							
	Assert.assertTrue(verify);

}
		
//************************************* Method to access add/remove services *****************************
public void Access_AddRemoveServices() throws InterruptedException
{
	driver.navigate().back();
					
	Thread.sleep(400);
					
	WebElement addRemove= driver.findElement(By.id("B13991652920414396"));
					
	Actions act= new Actions(driver);
					
	act.moveToElement(addRemove).click().build().perform();
					
	Thread.sleep(1000);
					
	Boolean verify= driver.findElement(By.id("SRV_NAME")).isDisplayed();
									
    Assert.assertTrue(verify);

}
		
//************************************* Method to access SIM swap tab *****************************
public void Access_SimSwap_Tab() throws InterruptedException
{
	driver.navigate().back();
																											
	Thread.sleep(400);
																												
	WebElement SIM= driver.findElement(By.id("B13992068504418949"));
																											
	Actions act= new Actions(driver);
																												
	act.moveToElement(SIM).click().build().perform();
																												
	Thread.sleep(1000);
																												
	Boolean verify= driver.findElement(By.id("P46_ICCID")).isDisplayed();
																																
	Assert.assertTrue(verify);
								
}
		
//************************************* Method to access MSISDN Swap *****************************
public void Access_MSISDNswap_Tab() throws InterruptedException
{
	driver.navigate().back();
																			
	Thread.sleep(400);
																			
	WebElement swap= driver.findElement(By.id("B13992278893421950"));
																		
	Actions act= new Actions(driver);
																			
	act.moveToElement(swap).click().build().perform();
																			
	Thread.sleep(1000);
																			
	Boolean verify= driver.findElement(By.id("P48_RESERVE_NO")).isDisplayed();
																							
	Assert.assertTrue(verify);

}

//************************************* Method to access Transaction History tab *****************************
public void Access_TransactionHistory_Tab() throws InterruptedException
{
	driver.navigate().back();
		 																											
	Thread.sleep(400);
		 																												
	WebElement THistory= driver.findElement(By.id("B14244878221549173"));
		 																											
   	Actions act= new Actions(driver);
		 																												
	act.moveToElement(THistory).click().build().perform();
		 																												
	Thread.sleep(1000);
		 																												
	Boolean verify= driver.findElement(By.id("apexir_ACTIONSMENUROOT")).isDisplayed();
		 																																
	Assert.assertTrue(verify);
		 								
}	
	 	
//************************************* Method to access View Invoice tab *****************************
public void Access_ViewInvoice_Tab() throws InterruptedException
{
	driver.navigate().back();
	 	 	 																											
	Thread.sleep(400);
	 	 	 																												
	WebElement invoice= driver.findElement(By.id("B13486076915059018"));
	 	 	 																											
   	Actions act= new Actions(driver);
	 	 	 																												
	act.moveToElement(invoice).click().build().perform();
	 	 	 																												
	Thread.sleep(1000);
	 	 	 																												
	Boolean verify= driver.findElement(By.id("B16366916284812711")).isDisplayed();
	 	 	 																																
	Assert.assertTrue(verify);
	 	 	 								
	}	
	 	
//************************************* Method to access Quick Calls *****************************
 public void Access_QuickCalls_Tab() throws InterruptedException
 {
	 driver.navigate().back();
	 	 																					
	 Thread.sleep(400);
	 																								
	 WebElement quickCalls= driver.findElement(By.id("B14401853182260880"));
	 																							
	 Actions act= new Actions(driver);
	 																								
	 act.moveToElement(quickCalls).click().build().perform();
	 																								
	 Thread.sleep(1000);
	 																								
	 Boolean verify= driver.findElement(By.id(" ")).isDisplayed();
	 																												
	 Assert.assertTrue(verify);
	 				
}	 	

//************************************* Method to access Sponsor tab *****************************
public void Access_Sponsor_Tab() throws InterruptedException
{
	driver.navigate().back();
     																											
	Thread.sleep(400);
     																												
	WebElement Sponsor= driver.findElement(By.id("B16533076530529722"));
     																											
   	Actions act= new Actions(driver);
     																												
	act.moveToElement(Sponsor).click().build().perform();
     																												
	Thread.sleep(1000);
     																												
	Boolean verify= driver.findElement(By.id("B16541864605415761")).isDisplayed();
     																																
	Assert.assertTrue(verify);
     								
	}
     	
//************************************* Method to access RCS tab *****************************
public void Access_RCS_Tab() throws InterruptedException
{
	driver.navigate().back();
     																									
	Thread.sleep(400);
     																										
	WebElement RCS= driver.findElement(By.id("B16614757006394351"));
     																									
   	Actions act= new Actions(driver);
     																										
   	act.moveToElement(RCS).click().build().perform();
     																										
   	Thread.sleep(1000);
     																										
   	Boolean verify= driver.findElement(By.id("report_R16612863528389370")).isDisplayed();
     																														
   	Assert.assertTrue(verify);
   	
}
	
//************************************* Method to access CSR *****************************
public void Access_CSRTab() throws InterruptedException
{
	driver.navigate().back();
													
	Thread.sleep(400);
													
	WebElement CSR= driver.findElement(By.id("B7965529669555217"));
													
	Actions act= new Actions(driver);
													
	act.moveToElement(CSR).click().build().perform();
													
	Thread.sleep(1000);
													
	Boolean verify= driver.findElement(By.id("B17028347195396813")).isDisplayed();
																	
	Assert.assertTrue(verify);

}
		
//************************************* Method to access account swap*****************************
public void Access_AccountSwap() throws InterruptedException
 {
	driver.navigate().back();
		
	Thread.sleep(400);
		
	WebElement account_swap= driver.findElement(By.id("B2316527810975023"));

	Actions act= new Actions(driver);

	act.moveToElement(account_swap).click().build().perform();

	Thread.sleep(400);

	Boolean verify= driver.findElement(By.id("P43_NEW_ACCOUNT")).isDisplayed();

	Assert.assertTrue(verify);

 }

//************************************* Method to access Discounts *****************************
public void Access_DiscountsTab() throws InterruptedException
{
	driver.navigate().back();
												
	Thread.sleep(400);
												
	WebElement discount= driver.findElement(By.id("B5018405026976458"));
												
	Actions act= new Actions(driver);
												
	act.moveToElement(discount).click().build().perform();
												
	Thread.sleep(1000);
												
	Boolean verify= driver.findElement(By.id("B5025316912188034")).isDisplayed();
																
	Assert.assertTrue(verify);

}
	
//************************************* Method to access fraud MSISDN *****************************
public void Access_FraudMSISDNTab() throws InterruptedException
{
	driver.navigate().back();
																
	Thread.sleep(400);
																
	WebElement FraudMSISDN= driver.findElement(By.id("B12895521417762806"));
															
	Actions act= new Actions(driver);
																
	act.moveToElement(FraudMSISDN).click().build().perform();
																
	Thread.sleep(1000);
																
	Boolean verify= driver.findElement(By.id("P66_FM_PID_NOT_ALLOWED")).isDisplayed();
																				
	Assert.assertTrue(verify);

	}
	
//************************************* Method to access average Spend *****************************
public void Access_AverageSpend() throws InterruptedException
{
	driver.navigate().back();
							
	Thread.sleep(400);
								
	WebElement AvgSpend= driver.findElement(By.id("B45587703047243714"));
								
	Actions act= new Actions(driver);
								
	act.moveToElement(AvgSpend).click().build().perform();
								
	Thread.sleep(1000);
							
	Boolean verify= driver.findElement(By.id("P75_PERIODS")).isDisplayed();
												
	Assert.assertTrue(verify);

}

//************************************* Method to access instant bill subscriber *****************************
public void Access_InstantBillSubscriberTab() throws InterruptedException
{
	driver.navigate().back();
																		
	Thread.sleep(400);
																		
	WebElement instantBill= driver.findElement(By.id("B43514900944699522"));
																	
	Actions act= new Actions(driver);
																		
	act.moveToElement(instantBill).click().build().perform();
																		
	Thread.sleep(1000);
																		
	Boolean verify= driver.findElement(By.id("P18_BILL_RUN_TYPE")).isDisplayed();
																						
	Assert.assertTrue(verify);

}

//************************************* Method to access Credit tab *****************************
public void Access_CreditTab() throws InterruptedException
{
	driver.navigate().back();
													
	Thread.sleep(400);
													
	WebElement CreditTab= driver.findElement(By.id("B49052106786312688"));
													
	Actions act= new Actions(driver);
													
	act.moveToElement(CreditTab).click().build().perform();
													
	Thread.sleep(1000);
													
	Boolean verify= driver.findElement(By.id("apexir_SEARCH")).isDisplayed();
																	
	Assert.assertTrue(verify);

	}
		
//************************************* Method to access Contract Upgrade *****************************
public void Access_ContractUpgrade() throws InterruptedException
{
	driver.navigate().back();
									
	Thread.sleep(400);
									
	WebElement ContractUpgrade= driver.findElement(By.id("B74726229437391028"));
									
	Actions act= new Actions(driver);
									
	act.moveToElement(ContractUpgrade).click().build().perform();
									
	Thread.sleep(1000);
									
	Boolean verify= driver.findElement(By.id("P103_IMEI")).isDisplayed();
													
	Assert.assertTrue(verify);

}

//************************************* Method to access Penalty Simulation *****************************
public void Access_PenaltySimulation_Tab() throws InterruptedException
{
	driver.navigate().back();
																						
	Thread.sleep(400);
																							
	WebElement penalty= driver.findElement(By.id("B79653426781031469"));
																						
    Actions act= new Actions(driver);
																							
	act.moveToElement(penalty).click().build().perform();
																							
	Thread.sleep(1000);
																							
	Boolean verify= driver.findElement(By.id("B79593208758301843")).isDisplayed();
																											
	Assert.assertTrue(verify);
			
	}
		
//************************************* Method to access Lay By tab *****************************
public void Access_LayBy_Tab() throws InterruptedException
{
	driver.navigate().back();
	 	 																											
	Thread.sleep(400);
	 	 																												
	WebElement LayBy= driver.findElement(By.id("B17263106883918432"));
	 	 																											
   	Actions act= new Actions(driver);
	 	 																												
	act.moveToElement(LayBy).click().build().perform();
	 	 																												
	Thread.sleep(1000);
	 	 																												
	Boolean verify= driver.findElement(By.id("apexir_ACTIONSMENUROOT")).isDisplayed();
	 	 																																
	Assert.assertTrue(verify);
	 	 								
	}		

//************************************* Method to access VAS Limit tab *****************************
public void Access_VASLimit_Tab() throws InterruptedException
{
	driver.navigate().back();
	 	 																											
	Thread.sleep(400);
	 	 																												
	WebElement vas= driver.findElement(By.id("B16850008507405094"));
	 	 																											
   	Actions act= new Actions(driver);
	 	 																												
   	act.moveToElement(vas).click().build().perform();
	 	 																												
	Thread.sleep(1000);
	 	 																												
	Boolean verify= driver.findElement(By.id("B16801717287009647")).isDisplayed();
	 	 																																
	Assert.assertTrue(verify);
	 	 								
	}

//************************************* Method to access Voice mail tab *****************************
public void Access_VoiceMail_Tab() throws InterruptedException
{
	driver.navigate().back();
	 	 																											
	Thread.sleep(400);
	 	 																												
	WebElement voice= driver.findElement(By.id("B13132206782557482"));
	 	 																											
   	Actions act= new Actions(driver);
	 	 																												
	act.moveToElement(voice).click().build().perform();
	 	 																												
	Thread.sleep(1000);
	 	 																												
	Boolean verify= driver.findElement(By.id("R81845117277937330")).isDisplayed();
	 	 																																
	Assert.assertTrue(verify);
	 	 								
	}
	 	
//************************************* Method to access OTA *****************************
public void Access_OTA_Tab() throws InterruptedException
{
	driver.navigate().back();
	 																				
	Thread.sleep(400);
	 																				
	WebElement OTA= driver.findElement(By.id("B79933315687274062"));
	 																			
	Actions act= new Actions(driver);
	 																				
	act.moveToElement(OTA).click().build().perform();
	 																				
	Thread.sleep(1000);
	 																				
	Boolean verify= driver.findElement(By.id("P124_IMEI")).isDisplayed();
	 																								
	Assert.assertTrue(verify);

}	 		 	

//************************************* Method to access Hard lock *****************************
public void Access_HardLockTab() throws InterruptedException
{
	driver.navigate().back();
																			
	Thread.sleep(400);
																			
	WebElement hardlock= driver.findElement(By.id("B21672303344232757"));
																		
	Actions act= new Actions(driver);
																			
	act.moveToElement(hardlock).click().build().perform();
																			
	Thread.sleep(1000);
																			
	Boolean verify= driver.findElement(By.id("P117_MSISDN_2")).isDisplayed();
																							
	Assert.assertTrue(verify);

	}
		
//************************************* Method to access infotext *****************************
public void Access_Infotext() throws InterruptedException
{
	driver.navigate().back();
																				
	Thread.sleep(400);
																					
	WebElement infotext= driver.findElement(By.id("B51253426838653728"));
																				
	Actions act= new Actions(driver);
																					
	act.moveToElement(infotext).click().build().perform();
																					
	Thread.sleep(1000);
																					
	Boolean verify= driver.findElement(By.id("P0_SEARCH_VALUE")).isDisplayed();
																									
	Assert.assertTrue(verify);

	}	
		
//************************************* Method to access Third party *****************************
public void Access_ThirdParty() throws InterruptedException
{
	driver.navigate().back();
		
	Thread.sleep(400);
																							
	WebElement thirdparty= driver.findElement(By.id("B7183131325094008"));
																						
	Actions act= new Actions(driver);
																							
	act.moveToElement(thirdparty).click().build().perform();
																							
	Thread.sleep(1000);
																							
	Boolean verify= driver.findElement(By.id("P0_SEARCH_VALUE")).isDisplayed();
																											
	Assert.assertTrue(verify);

	}			
		
//************************************* Method to access Notification Exclusion *****************************
public void Access_NotificationExclusion() throws InterruptedException
	{
		driver.navigate().back();
																								
		Thread.sleep(400);
																									
		WebElement Nexclusion= driver.findElement(By.id("B9435403671951520"));
																								
		Actions act= new Actions(driver);
																									
		act.moveToElement(Nexclusion).click().build().perform();
																									
		Thread.sleep(1000);
																									
		Boolean verify= driver.findElement(By.id("P112_TTNE_TT_UID")).isDisplayed();
																													
		Assert.assertTrue(verify);

		}	
		
//************************************* Method to access Test Invoice  *****************************
public void Access_TestInvoice() throws InterruptedException
{
	driver.navigate().back();
																										
	Thread.sleep(400);
																											
	WebElement TInvoice= driver.findElement(By.id("B9015609450742425"));
																										
	Actions act= new Actions(driver);
																											
	act.moveToElement(TInvoice).click().build().perform();
																											
	Thread.sleep(1000);
																											
    driver.switchTo().activeElement();

  }	
		
}
