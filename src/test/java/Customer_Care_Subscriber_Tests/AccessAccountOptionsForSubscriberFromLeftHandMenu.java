package Customer_Care_Subscriber_Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AccessAccountOptionsForSubscriberFromLeftHandMenu extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	  driver.quit();			
	}
   
	
	@Test
	public void a_AccessAttachment_forAccount() throws InterruptedException
	{
		 Access_Attachment();
	}
	
	@Test
	public void b_AccessCommunication_forAccount() throws InterruptedException
	{
		Access_Communication();
	}
	
	@Test
	public void z_AccessCreateSubscriber_forAccount() throws InterruptedException
	{
		Access_CreateSubscriber();
	}
	
	@Test
	public void d_AccessCustomerDebugData_forAccount() throws InterruptedException
	{
		Access_Customer_DebugData();
	}
	
	@Test
	public void e_AccessDDReceipt_forAccount() throws InterruptedException
	{
		Access_DDReceipt();
	}
	
	@Test
	public void f_AccessEditCustomer_forAccount() throws InterruptedException
	{
		Access_EditCustomer();
	}
	
	@Test
	public void g_AccessNotes_forAccount() throws InterruptedException
	{
		Access_Notes();
	}
	
	@Test
	public void h_AccessNotification_forAccount() throws InterruptedException
	{
		Access_Notification();
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************* Method to access attachment*****************************
public void Access_Attachment() throws InterruptedException
{
	WebElement attachment= driver.findElement(By.xpath("//div[@id='R20182213890323685']//a[contains(text(),'Attachments')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(attachment).click().build().perform();
	
	Thread.sleep(400);
	
	Boolean verify= driver.findElement(By.id("P59_FILE")).isDisplayed();
	
	Assert.assertTrue(verify);
}

//************************************* Method to access communication *****************************
public void Access_Communication() throws InterruptedException
{
	driver.navigate().back();
	
	Thread.sleep(400);
	
	WebElement com= driver.findElement(By.xpath("//div[@id='R20182213890323685']//a[contains(text(),'Communications')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(com).click().build().perform();
	
	Thread.sleep(1000);
	
	Boolean verify= driver.findElement(By.id("apexir_ACTIONSMENUROOT")).isDisplayed();
	
	Assert.assertTrue(verify);
}

//************************************* Method to access create subscriber *****************************
public void Access_CreateSubscriber() throws InterruptedException
{
	driver.navigate().back();
	
	Thread.sleep(400);
	
	WebElement sub= driver.findElement(By.xpath("//a[contains(text(),'Create Subscriber')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(sub).click().build().perform();
	
	Thread.sleep(1000);
	
	Boolean verify= driver.findElement(By.id("Surname")).isDisplayed();
	
	Assert.assertTrue(verify);
}

//************************************* Method to access customer debug data *****************************
public void Access_Customer_DebugData() throws InterruptedException
{
	driver.navigate().back();
	
	Thread.sleep(400);
	
	WebElement debugData= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Customer Debug Data')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(debugData).click().build().perform();
	
	Thread.sleep(1000);
	
	Boolean verify= driver.findElement(By.id("R14491121614688658")).isDisplayed();
	
	Assert.assertTrue(verify);
}

//************************************* Method to access DD Receipt *****************************
public void Access_DDReceipt() throws InterruptedException
{
	driver.navigate().back();
	
	Thread.sleep(400);
	
	WebElement DD= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'DD Receipt')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(DD).click().build().perform();
	
	Thread.sleep(1000);
	
	Boolean verify= driver.findElement(By.id("R12654904342264893")).isDisplayed();
	
	Assert.assertTrue(verify);
}

//************************************* Method to access Edit Customer *****************************
public void Access_EditCustomer() throws InterruptedException
{
	driver.navigate().back();
	
	Thread.sleep(400);
	
	WebElement edit= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Edit Customer')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();
	
	Thread.sleep(1000);
	
	Boolean verify= driver.findElement(By.id("P40_BCD_ACCT_UID")).isDisplayed();
	
	Assert.assertTrue(verify);
}

//************************************* Method to access notification *****************************
public void Access_Notification() throws InterruptedException
{
	driver.navigate().back();
	
	Thread.sleep(400);
	
	WebElement notification= driver.findElement(By.xpath("//div[@id='R20182213890323685']//a[contains(text(),'Notifications')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(notification).click().build().perform();
	
	Thread.sleep(600);
	
	Boolean verify= driver.findElement(By.id("B12131301940768901")).isDisplayed();
	
	Assert.assertTrue(verify);
}

//************************************* Method to access notes *****************************
public void Access_Notes() throws InterruptedException
{
	driver.navigate().back();
	
	Thread.sleep(400);
	
	WebElement Note= driver.findElement(By.xpath("//div[@id='R20182213890323685']//a[contains(text(),'Notes')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Note).click().build().perform();
	
	Thread.sleep(600);
	
	Boolean verify= driver.findElement(By.id("B13535157679207053")).isDisplayed();
	
	Assert.assertTrue(verify);
 }


}
