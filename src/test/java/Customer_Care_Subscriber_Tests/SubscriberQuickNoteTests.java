package Customer_Care_Subscriber_Tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common_methods.quickNoteTests;

public class SubscriberQuickNoteTests extends quickNoteTests{
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
        Thread.sleep(1000);
		driver.quit();
	}
	
	@Test
	public void a_Click_QuickNotesFor_Subscriber() throws InterruptedException
	{
		click_QuickNotes();		
	}
	
	@Test
	public void b_Click_Create_Quicknote() throws InterruptedException
	{
		
		ClickCreateQuickNote();
		Thread.sleep(1000);
	}
	
	@Test
	public void c_Select_type() throws InterruptedException
	{
		Thread.sleep(1000);
		Select_Type();
	}
	
	@Test
	public void d_Enter_Subject()
	{
		Enter_subject();
	}
	
	@Test
	public void e_Enter_Note()
	{
		
		Enter_note();
	}
	
	@Test
	public void f_Select_Status()
	{
		Select_status();
	}
	
	@Test
	public void g_Click_Create()
	{
		Click_create();
	}
	
	@Test
	public void h_check_success_message()
	{
		suceess_msg();
	}
	
	@Test
	public void i_VerifyCreatedNote() throws InterruptedException 
	{
		Thread.sleep(500);
		Verify_note();
		
	}
	
	@Test
	public void j_ClickEditQuickNote() throws InterruptedException 
	{
		Thread.sleep(500);
		Click_edit();
		
	}
	
	@Test
	public void k_EditSubject() throws InterruptedException 
	{
		Thread.sleep(500);
		EditSubject();
		
	}

	@Test
	public void l_Edit_successMsg()
	{
		
		suceess_msg();
	}
	
	@Test
	public void m_VerifyEdit()
	{
		verifyEdit();
	}
}
