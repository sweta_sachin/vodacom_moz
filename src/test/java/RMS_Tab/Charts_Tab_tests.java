package RMS_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Charts_Tab_tests extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_RMSTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		
	 Thread.sleep(1000);
	 driver.quit();
	 
	}

	@Test
	public void a_View_Charts_Tab()
	{
		view_ChartsTab();
	}

	@Test
	public void b_Click_ChartsTab() throws InterruptedException
	{
		Thread.sleep(500);
		click_ChartsTab();
	}
	
	@Test
	public void c_View_ChartsForspecificFilters() throws InterruptedException
	{
		View_ChartsForspecificFilters();
	}
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Charts tab ******************************
public void view_ChartsTab()
{
	WebElement Charts= driver.findElement(By.linkText("Charts"));
	Boolean charts= Charts.isDisplayed();
	Assert.assertTrue(charts);
}


//********************************* Method to click Charts tab ******************************
public void click_ChartsTab() throws InterruptedException
{
	WebElement Charts= driver.findElement(By.linkText("Charts"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Charts).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
		WebElement verify= driver.findElement(By.id("P0_N_NAME"));
	
		Boolean Verify= verify.isDisplayed();
		
		Assert.assertTrue(Verify);
    }

//******************************** Method to view reconciliation summary *********************************
public void View_ChartsForspecificFilters() throws InterruptedException
{
	//Select network
		
	Select network= new Select( driver.findElement(By.id("P0_N_UID")));
	
	network.selectByIndex(3);
	
	Thread.sleep(300);
		
	//Click go
	
	WebElement go= driver.findElement(By.id("P0_GO2"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();
	
	Thread.sleep(600);

	//Verify report
	
	WebElement report= driver.findElement(By.xpath("//div[@id='R1977915595533257']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean Report = report.isDisplayed();
	
	Assert.assertTrue(Report);

}


}
