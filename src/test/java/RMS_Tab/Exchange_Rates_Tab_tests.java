package RMS_Tab;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Exchange_Rates_Tab_tests extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_RMSTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		
	 Thread.sleep(1000);
	 driver.quit();
	 
	}

	@Test
	public void a_View_ExchangeRates_Tab()
	{
		view_ExchangeRatesTab();
	}

	@Test
	public void b_Click_ExchangeRatesTab() throws InterruptedException
	{
		Thread.sleep(500);
		click_ExchangeRatesTab();
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void g_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","exchange_rates.csv"));
	}
	
	@Test
	public void h_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
		
	}
		
	@Test
	public void i_flashBackTest() throws InterruptedException
	{
		Thread.sleep(500);
		flashback_filter();
	}
	
	@Test
	public void j_reset_test() throws InterruptedException
	{
		Thread.sleep(500);
		reset();
	} 
	
	@Test
	public void k_AddNextMonthsRate() throws InterruptedException
	{
		Thread.sleep(500);
		AddNextMonthRate();
	}

	@Test
	public void l_VerifyAddedNewRate() throws InterruptedException
	{
		Thread.sleep(500);
		Verify_ExchangeRate();
	}
	
	@Test
	public void m_DeleteRateRecord() throws InterruptedException
	{
		Thread.sleep(500);
		delete_record();
	}
	
	@Test
	public void o_Edit_ExchangeRate() throws InterruptedException
	{
		Thread.sleep(500);
		Edit_ExchangeRate();
	}
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Exchange Rates tab ******************************
public void view_ExchangeRatesTab()
 {
	WebElement ExchangeRates= driver.findElement(By.linkText("Exchange Rates"));
	
	Boolean exchangeRates= ExchangeRates.isDisplayed();
	
	Assert.assertTrue(exchangeRates);
}

//********************************* Method to click Exchange Rates tab ******************************
public void click_ExchangeRatesTab() throws InterruptedException
{
	WebElement ExchangeRates= driver.findElement(By.linkText("Exchange Rates"));

	Actions act= new Actions(driver);
	
	act.moveToElement(ExchangeRates).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
 {
	 col= "From</br>Currency";
	 expvalue= "EUR";
	
	 view_filtered_report();

	// Verify report
	
	 Thread.sleep(600);
	
	 String verifyreport= driver.findElement(By.xpath("//div[@id='apexir_DATA_PANEL']//tr[2]//td[2]")).getText();
	
	 Thread.sleep(300);
	
	 Assert.assertEquals(verifyreport, expvalue);
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 
	
	save_report();
	
	// Verify saved report

	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();
	
	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
 {
	select_primary_report();
	
	Thread.sleep(300);

	try {

		WebElement primaryreport= driver.findElement(By.id("1790304738657687"));
	
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	
	}catch(Exception e) 	{

		WebElement primaryreport= driver.findElement(By.id("1790304738657687"));
	
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}

}
//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
	{
		Reset();

	// Verify report
		
		WebElement primaryreport= driver.findElement(By.id("1790304738657687"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
}	
//************************** Method to add next month rate ***********************************
public void AddNextMonthRate() throws InterruptedException
{
	//Click Add next month rate button
		
	WebElement reset = driver.findElement(By.id("B1806132587006488"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(reset).click().build().perform();	
	
	Thread.sleep(500);
		
	//Enter Rate
	
	WebElement rate= driver.findElement(By.id(ExchangeRate));
	
	rate.clear();
	
	rate.sendKeys();
	
	Thread.sleep(500);
	
	//Click Submit
	
	WebElement submit = driver.findElement(By.id("B1806132587006488"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(submit).click().build().perform();	
	
	Thread.sleep(500);
  }

//*********************************** Method to verify added  exchange rate ******************************
public void Verify_ExchangeRate()
{
	//Verify with the created date
		
	WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[7]"));
	
	String Date= datestamp.getText();
	
	System.out.println(Date);
	
	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	
	Date date = new Date();
	
	String date1= dateFormat.format(date);
	
	System.out.println(date1);
	
	Assert.assertEquals(Date, date1);
   
  }

//************************************* Method to edit Exchange rate ********************************
public void Edit_ExchangeRate() throws InterruptedException
{
	//Save exchange rate before edit
	
	String rate1= driver.findElement(By.xpath("//tr[2]//td[4]")).getText();
		
	//Click edit
	
	WebElement edit = driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(500);
		
    //Change rate 
	
	WebElement rate= driver.findElement(By.id("P12_CURE_RATE"));
	
	rate.clear();
	
	rate.sendKeys("0.090455");
		
    // Click apply changes
	
	WebElement apply= driver.findElement(By.id("B1796800286837007"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(apply).click().build().perform();

	//Save exchange rate after edit
	
	String rate2= driver.findElement(By.xpath("//tr[2]//td[4]")).getText();
		
	Assert.assertNotEquals(rate2, rate1);

}

//************************************ Method to delete record ***********************************
public void delete_record() throws InterruptedException
{
	//Save info before delete
	
	String rate1= driver.findElement(By.xpath("//tr[2]")).getText();
	
	//Click edit
	
	WebElement edit = driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(500);
			
	//Click delete
	
	WebElement delete = driver.findElement(By.id("B1821309861597897"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(delete).click().build().perform();	
	
	Thread.sleep(500);
	
	// Accept alert
	
	Alert alert= driver.switchTo().alert();
	
	alert.accept();
	
	Thread.sleep(500);
			
	//Save info before delete
	
	String rate2= driver.findElement(By.xpath("//tr[2]")).getText();
	
	Assert.assertNotEquals(rate2, rate1);
		
  }
}
