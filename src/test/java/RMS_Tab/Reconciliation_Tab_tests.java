package RMS_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Reconciliation_Tab_tests  extends common_methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_RMSTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		
	 Thread.sleep(1000);
	 driver.quit();
	 
	}

	@Test
	public void a_View_Reconciliation_Tab()
	{
		view_ReconciliationTab();
	}

	@Test
	public void b_Click_ReconciliationTab() throws InterruptedException
	{
		Thread.sleep(500);
		click_ReconciliationTab();
	}
	
	@Test
	public void c_View_Reconciliation_Summary() throws InterruptedException
	{
		View_ReconciliationSummary();
	}
	
	@Test
	public void d_View_Missing_TAPin_Files() throws InterruptedException
	{
		View_Missing_TAPin_Files();
	}
	
	@Test
	public void e_View_Reconciliation_Detail() throws InterruptedException
	{
		View_Reconciliation_Detail();
	}
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Reconciliation tab ******************************
public void view_ReconciliationTab()
{
	WebElement Reconciliation= driver.findElement(By.linkText("Reconciliation"));

	Boolean reconciliation= Reconciliation.isDisplayed();
	
	Assert.assertTrue(reconciliation);
}


//********************************* Method to click Reconciliation tab ******************************
public void click_ReconciliationTab() throws InterruptedException
{
	WebElement Reconciliation= driver.findElement(By.linkText("Reconciliation"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Reconciliation).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement verify= driver.findElement(By.id("P0_N_NAME"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
    
}

//******************************** Method to view reconciliation summary *********************************
public void View_ReconciliationSummary() throws InterruptedException
{
	//Select network

	Select network= new Select( driver.findElement(By.id("P0_N_UID")));
	
	network.selectByIndex(3);
	
	Thread.sleep(300);
	
	//Click go
	
	WebElement go= driver.findElement(By.id("P0_GO2"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement report= driver.findElement(By.xpath("//div[@id='R1931505646524278']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean Report = report.isDisplayed();
	
	Assert.assertTrue(Report);

}

//******************************** Method to view missing tap in files *********************************
public void View_Missing_TAPin_Files() throws InterruptedException
{
	//Verify report
			
	WebElement report= driver.findElement(By.xpath("//div[@id='R1932519414547104']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean Report = report.isDisplayed();
	
	Assert.assertTrue(Report);
}

//******************************** Method to view Reconciliation Detail *********************************
public void View_Reconciliation_Detail() throws InterruptedException
{
	//Verify report
	
	WebElement report= driver.findElement(By.xpath("//div[@id='R1929409970487640']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
	
	Boolean Report = report.isDisplayed();
	
	Assert.assertTrue(Report);
}
}
