package UserAdminTab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common_methods.CommonMethods;

public class Create_User extends CommonMethods{
	
		
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
	  driver.quit();
		
	}
	
	@Test
	public void a_View_UserAdmin_tab() throws InterruptedException
	{
		View_UserAdmin();
	}
	
	@Test
	public void b_Click_UserAdmin_tab() throws InterruptedException
	{
		Thread.sleep(500);
		click_UserAdmin();
	}
	

	@Test
	public void c_Click_createPendingButton() throws InterruptedException
	{
		Thread.sleep(500);
		Click_Create();
	}
	
	@Test
	public void d_EnterFirstName() throws InterruptedException
	{
		Thread.sleep(500);
		enter_firstName();
	}
	
	@Test
	public void e_Enter_SurName() throws InterruptedException
	{
		Thread.sleep(500);
		enter_surName();
	}
	
	@Test
	public void f_Enter_LogonName() throws InterruptedException
	{
		Thread.sleep(500);
    	enter_logonname();
	}
	
	@Test
	public void g_Enter_password() throws InterruptedException
	{
		Thread.sleep(500);
		enter_password();
	}
	
	@Test
	public void g_Re_enter_password() throws InterruptedException
	{
		Thread.sleep(500);
		Re_enter_password();
	}
	
	@Test
	public void h_SelectForce_PasswordChangeStatus() throws InterruptedException
	{
		Thread.sleep(500);
		SelectForcePasswor_status();
	}
	
	@Test
	public void i_Select_LockOut_Status() throws InterruptedException
	{
		Thread.sleep(500);
		SelectLockOutstatus();
	}
	
	@Test
	public void j_Enter_MSISDN() throws InterruptedException
	{
		Thread.sleep(500);
		EnterMSISDN();
	}
	
	@Test
	public void k_Enter_email() throws InterruptedException
	{
		Thread.sleep(500);
		EnterEmail();
	}
	
	@Test
	public void l_Select_DefaultPage() throws InterruptedException
	{
		Thread.sleep(500);
		SelectDeafult_Page();
	}
	
	@Test
	public void m_Click_Create() throws InterruptedException
	{
		Thread.sleep(500);
		click_create();
	}
	
	@Test
	public void n_Check_SuccessMsg() throws InterruptedException
	{
		Thread.sleep(500);
		check_SuccessMsg();
	}
	
	@Test
	public void o_Verify_CreatedUser() throws InterruptedException
	{
		Thread.sleep(500);
		Verify_User();
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************ Method to view User admin tab **********************************
public void View_UserAdmin()
	{
		WebElement admin=  driver.findElement(By.linkText("User Admin"));
		
		Boolean Uadmin_tab= admin.isDisplayed();
		
		Assert.assertTrue(Uadmin_tab);
	}
	
//************************************ Method to click User admin tab *************************
 public void click_UserAdmin()
		{
			WebElement admin=  driver.findElement(By.linkText("User Admin"));
		
			Actions act= new Actions(driver);
			
			act.moveToElement(admin).click().build().perform();
		}
		
//************************************ Method to click User admin tab *************************
public void Click_Create() throws InterruptedException
		{
			WebElement pending=  driver.findElement(By.linkText("Create"));
			
			Actions act= new Actions(driver);
			
			act.moveToElement(pending).click().build().perform();
			
			Thread.sleep(400);
			
			WebElement name=  driver.findElement(By.id("P11_U_FIRST_NAME"));
			
			Boolean NAME= name.isDisplayed();
			
			Assert.assertTrue(NAME);
		}
		
//************************************ Method to enter first name ************************************
public void enter_firstName()
		{
			WebElement name=  driver.findElement(By.id("P11_U_FIRST_NAME"));
			
			name.clear();
			
			name.sendKeys(Firstname);
		
		}
//************************************ Method to enter surname ************************************
public void enter_surName()
    	{
			WebElement name=  driver.findElement(By.id("P11_U_SURNAME"));
			
			name.clear();
			
			name.sendKeys(surname);
		
		}
		
//************************************ Method to enter logon name ************************************
public void enter_logonname()
   		{
			WebElement name=  driver.findElement(By.id("P11_U_LOGON_NAME"));
			
			name.clear();
			
			name.sendKeys(logon);
		}
		
//************************************ Method to enter password ************************************
public void enter_password()
		{
			WebElement name=  driver.findElement(By.id("P11_U_PASSWORD"));
			
			name.clear();
			
			name.sendKeys(Password);
		}

//************************************ Method to Re-enter password ************************************
public void Re_enter_password()
		{
			WebElement name=  driver.findElement(By.id("P11_U_PASSWORD_2"));
			
			name.clear();
			
			name.sendKeys(Password);
		}

//************************************ Method to select force password status *************************
public void SelectForcePasswor_status()
		{
			WebElement pswd=  driver.findElement(By.id("P11_U_RESET_PASSWORD_1"));
			
			Actions act= new Actions(driver);
			
			act.moveToElement(pswd).click().build().perform();
		}
		
//************************************ Method to select lock out status *************************
public void SelectLockOutstatus()
		{
		    WebElement lockOut=  driver.findElement(By.id("P11_U_ISLOCKEDOUT_1"));
			
		    Actions act= new Actions(driver);
			
		    act.moveToElement(lockOut).click().build().perform();
		}
		
//****************************************** Method to enter MSISDN **********************************
public void EnterMSISDN()
		{
			WebElement name=  driver.findElement(By.id("P11_U_DEFAULT_MSISDN"));
			
			name.clear();
			
			name.sendKeys(MSISDN);
		}
		
//****************************************** Method to enter Email **********************************
public void EnterEmail()
		{
		  WebElement name=  driver.findElement(By.id("P11_U_EMAIL"));
		  
		  name.clear();
		  
		  name.sendKeys(toemail);
		}
		
//****************************************** Method to select default page **********************************
public void SelectDeafult_Page()
	{
		 Select dropdown= new Select(driver.findElement(By.id("P11_U_APEX_PAGE_ID")));
		 
		 dropdown.selectByVisibleText("2 - Customer Details");
	}

//************************************ Method to click create *************************
public void click_create()
	{
	    WebElement create=  driver.findElement(By.id("B4079223177667937"));
		
	    Actions act= new Actions(driver);
		
	    act.moveToElement(create).click().build().perform();
	}
		
//************************************ Method to check success message *************************
public void check_SuccessMsg()
	{
		String Msg= driver.findElement(By.id("echo-message")).getText();
		
		String Expected_msg= "Action Processed.";
		
		Assert.assertEquals(Msg, Expected_msg);
	}
		
//************************************ Method to verify created account *************************
public void Verify_User() throws InterruptedException
	{
		WebElement user=  driver.findElement(By.id("apexir_SEARCH"));
		
		user.clear();
		
		user.sendKeys(Firstname);

		//Click GO 
		
		WebElement GO=  driver.findElement(By.id("apexir_btn_SEARCH"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(GO).click().build().perform();
		
		Thread.sleep(1000);
		
		String name= driver.findElement(By.xpath("//tr[2]//td[3]")).getText();
		
		Assert.assertEquals(name, Firstname);
		
	}
}
