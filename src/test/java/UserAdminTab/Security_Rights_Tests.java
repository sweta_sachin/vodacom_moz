package UserAdminTab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common_methods.CommonMethods;

public class Security_Rights_Tests  extends CommonMethods{
	
	   @BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_UserAdmin();	
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() 
	{
	    driver.quit();
	}
	
	@Test
	public void a_view_SecurityRight_Tab()
	{
		view_securityRightsTab();
	}
	
	@Test
	public void b_Click_securityRight_Tab() throws InterruptedException
	{
		Thread.sleep(500);
		click_securityRightsTab();
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void d_Search_For_Primary_report() throws InterruptedException
	{
		Thread.sleep(500);
		
		search_by_PrimaryReports();
	}
	
	@Test
	public void e__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void f_Search_by_privateReport() throws InterruptedException
	{
		search_by_private_report();
	}
	
	@Test
	public void g_reset_test() throws InterruptedException
	{
		Thread.sleep(300);
		reset();
		
	}
		
	@Test
	public void j_Edit_SecurityRights() throws InterruptedException
	{
		Edit_SecurityRight();
	}
	
	@Test
	public void k_VerifyEdited_SecurityRights() throws InterruptedException
	{
		Verify_Edited_securityRight();
	}
	

	@Test
	public void l_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","security_rights.csv"));
	}

	@Test
	public void m_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
		
	}
	@Test
	public void n_flashBackTest() throws InterruptedException
	{
		Thread.sleep(300);
		flashback_filter();
	}
	

	
	
	
//////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Security Group tab ******************************
public void view_securityRightsTab()
{
	WebElement aging= driver.findElement(By.linkText("Security Rights"));
	
	Boolean Aging= aging.isDisplayed();
	
	Assert.assertTrue(Aging);
}

//********************************* Method to click security Group tab ******************************
public void click_securityRightsTab()
{
	WebElement aging= driver.findElement(By.linkText("Security Rights"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(aging).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{

	expvalue= "new name";
	col= "Name";
	
	view_filtered_report();

	try {
	
		// Verify report
		
		Thread.sleep(300);
		
		String verifyreport= driver.findElement(By.xpath("//tr[3]//td[3]")).getText();
		
		Assert.assertEquals(verifyreport, expvalue);
		
	}catch(Exception e)	{
	
		// Verify report
		
		Thread.sleep(300);
		
		String verifyreport= driver.findElement(By.xpath("//tr[3]//td[3]")).getText();
		
		Assert.assertEquals(verifyreport, expvalue);	
	
	}
	
	//Remove extra filters
	
	WebElement filters= driver.findElement(By.xpath("//tr[2]//td[4]//a[1]//img[1]"));
	
	Actions act1 = new Actions(driver);
	
	act1.moveToElement(filters).click().build().perform();
	
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 

	save_report();

	//Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
	
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();

	//verify 

	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
	select_primary_report();
	
	Thread.sleep(300);
	
	WebElement primaryreport= driver.findElement(By.id("4058700480667914"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
}

//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
{
	Reset();

	//Verify report
	
	WebElement primaryreport= driver.findElement(By.id("4058700480667914"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
}
//***************************************** Method to click create *******************************
public void Click_create() throws InterruptedException
{
	//Click create option
	
	WebElement create = driver.findElement(By.id("B4069629286667926"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(create).click().build().perform();	
	
	Thread.sleep(500);
			
}
//***************************************** Method to edit security group *******************************
public void Edit_SecurityRight() throws InterruptedException
{
	Thread.sleep(500);
	
	WebElement edit = driver.findElement(By.xpath("//tr[@class='even']//td//a//img"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(500);
	
	WebElement name= driver.findElement(By.id("P7_SR_NAME"));
	
	name.clear();
	
	name.sendKeys(Newname);
	
	//Click create
	
	WebElement create = driver.findElement(By.id("B4060131355667915"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(create).click().build().perform();	
	
	Thread.sleep(500);
	
}

//**************************************** Method to verify edited security group ***********************

public void Verify_Edited_securityRight() throws InterruptedException
{
	Thread.sleep(500);

	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(Newname);
	
	//Click Go
	
	WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();	
	
	Thread.sleep(500);
	
	String text= driver.findElement(By.xpath("//div[@id='apexir_WORKSHEET']//div//td[3]")).getText();
	
	Assert.assertEquals(text, Newname);
  }

}

