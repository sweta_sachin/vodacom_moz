package UserAdminTab;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common_methods.CommonMethods;

public class Limits_Tab_Tests extends CommonMethods{
	
    public static String NewLimit= "800";
	
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_UserAdmin();	
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() 
	{
	   driver.quit();
	}
	
	@Test
	public void a_view_Limits_Tab()
	{
		view_LimitsTab();
	}
	
	@Test
	public void b_Click_Limits_Tab() throws InterruptedException
	{
		Thread.sleep(500);
		click_LimitsTab();
	}
	
	
	@Test
	public void c_Search_For_Primary_report() throws InterruptedException
	{
		Thread.sleep(3000);
		
     	search_by_PrimaryReports();
	}
	
	@Test
	public void d_Filter_report() throws InterruptedException
	{
		Thread.sleep(3000);
		View_filtered_report();
	}
	@Test
	public void da__Save_Report() throws InterruptedException
	{
		Thread.sleep(5000); 
		Save_report();
	}
	
	@Test
	public void db_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(3000); 
		search_by_private_report();
	}
	
		
	@Test
	public void de_flashBackTest() throws InterruptedException
	{
		Thread.sleep(1000);
		flashback_filter();
	}
	
	@Test
	public void df_reset_test() throws InterruptedException
	{
		Thread.sleep(3000); 
		reset();
		
	}
	
	@Test
	public void e_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(3000);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","user_audit_limit.csv"));
	}
 
	@Test
	public void f_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(1000);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","user_audit_limit.htm"));
	}
 
	@Test
	public void g_Email_Report() throws InterruptedException
	{
		Thread.sleep(1000);
		email_report();
	}
	
	
	
	@Test
	public void l_Edit_record() throws InterruptedException
	{
		Thread.sleep(500);
		Edit_Record();
	}
	
	@Test
	public void m_Verify_Edited() throws InterruptedException
	{
		Thread.sleep(1500);
		VerifyEdited();
	
	}
	
	@Test
	public void n_View_InactiveRecord() throws InterruptedException
	{
		
		Thread.sleep(3000);
		ViewInactive();
	}
	
	@Test
	public void o_Create_Record() throws InterruptedException
	{
		Thread.sleep(5000);
		Create_Record();
	}
	
	@Test
	public void p_Verify_Created() throws InterruptedException
	{
		Thread.sleep(3500);
		Verify_Created();
		
	}

//////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Limits tab ******************************
public void view_LimitsTab()
{
	WebElement aging= driver.findElement(By.linkText("Limits"));
	
	Boolean Aging= aging.isDisplayed();
	
	Assert.assertTrue(Aging);
}

//********************************* Method to click Limits tab ******************************
public void click_LimitsTab()
{
	WebElement aging= driver.findElement(By.linkText("Limits"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(aging).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	expvalue= "Report";
    col= "Type";
	
    view_filtered_report();
	
    try {
    	// Verify report
	
    	Thread.sleep(300);
	
    	String verifyreport= driver.findElement(By.xpath("//tr[3]//td[7]")).getText();
	
    	Assert.assertEquals(verifyreport, expvalue);
	
    }catch(Exception e) {
	
    	Thread.sleep(300);
		
    	String verifyreport= driver.findElement(By.xpath("//tr[3]//td[7]")).getText();
		
    	Assert.assertEquals(verifyreport, expvalue);
	}
  }

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 
	
	save_report();

	//Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
	
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();
	
	//verify 

	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);

}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
	select_primary_report();

	Thread.sleep(1000);
	
	WebElement primaryreport= driver.findElement(By.id("88912216548783344"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
}

//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
{
	Reset();

	//Verify report
	
	WebElement primaryreport= driver.findElement(By.id("88912216548783344"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
}
//************************************* Method to create record ********************************
public void Create_Record() throws InterruptedException
{
   // Click create 
	
	WebElement create = driver.findElement(By.id("B88914305168783347"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(create).click().build().perform();	
	
	Thread.sleep(400);

	// Select user audit type
	
	Select userAuditType= new Select (driver.findElement(By.id("P41_UAPL_UAT_UID")));
	
	userAuditType.selectByIndex(1);
	
	//select type
	
	WebElement type = driver.findElement(By.id("P41_UAPL_TYPE_2"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(type).click().build().perform();	
	
	Thread.sleep(400);
	
	//Select node value
	
	Select node= new Select (driver.findElement(By.id("P41_UAPL_ASPT_UID")));
	
	node.selectByIndex(Index);
	
	//Enter value limit
	
	WebElement ValueLimit= driver.findElement(By.id("P41_UAPL_VALUE_LIMIT"));
	
	ValueLimit.clear();
	
	ValueLimit.sendKeys("55");
	
	// Enter start date
	
	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
	
	Date date = new Date();
	
	String date1= dateFormat.format(date);
	
	WebElement DATE= driver.findElement(By.id("P41_UAPL_START_DATE"));
	
	DATE.clear();
	
	DATE.sendKeys(date1);
	
	//Enter Current limit
	
	WebElement CurrentLimit= driver.findElement(By.id("P41_UAPL_COUNT_LIMIT"));
	
	CurrentLimit.clear();
	
	CurrentLimit.sendKeys("100");
		
	 //Click Create
	
	WebElement Create = driver.findElement(By.id("B88901917411714688"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(Create).click().build().perform();	
	
	Thread.sleep(400);
		
	// Check success msg
	
	Check_SuccessMsg();
	
}

//***************************** Method to check success message after adjustment gets created ***********
public void Check_SuccessMsg()
{
	  String Msg= driver.findElement(By.id("echo-message")).getText();
	
	  String Expected_msg= "Successfully Created Record";
	  
	  Assert.assertEquals(Msg, Expected_msg);
}

//***************************** Method to verify created record ********************************************
public void Verify_Created() throws InterruptedException
{
	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys("Report");
		
	// Click go
	
	WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();	
	
	Thread.sleep(400);
	
	try {	
	
		// arrange date column in descending order
		
		WebElement column = driver.findElement(By.id("apexir_UAPL_CREATED_ON"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(column).click().build().perform();	
		
		Thread.sleep(400);
				
		//click descending arrow
		
		WebElement arrow = driver.findElement(By.id("//span[@id='apexir_sortdown']//img"));
		
		Actions act1= new Actions(driver);
		
		act1.moveToElement(arrow).click().build().perform();	
		
		Thread.sleep(400);	
				
		// verify date
		
		WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[13]"));
		
		String Date= datestamp.getText();
		
		System.out.println(Date);
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		
		Date date = new Date();
		
		String date1= dateFormat.format(date);
		
		System.out.println(date1);
		
		Assert.assertEquals(Date, date1);
		
	}catch(Exception e) {
	
		// arrange date column in descending order
		
		WebElement column = driver.findElement(By.id("apexir_UAPL_CREATED_ON"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(column).click().build().perform();	
		
		Thread.sleep(600);
		
		//click descending arrow
		
		WebElement arrow = driver.findElement(By.xpath("//span[@id='apexir_sortdown']//img"));
		
		Actions act1= new Actions(driver);
		
		act1.moveToElement(arrow).click().build().perform();	
		
		Thread.sleep(400);	
		
		// verify date
		
		WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[13]"));
		
		String Date= datestamp.getText();
		
		System.out.println(Date);
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		
		Date date = new Date();
		
		String date1= dateFormat.format(date);
		
		System.out.println(date1);
		
		Assert.assertEquals(Date, date1);
		
	}
}
//**************************************** Method to edit record ***********************************
public void Edit_Record() throws InterruptedException
{
	// click edit
	
	WebElement edit = driver.findElement(By.xpath("//tr[@class='even']//td[1]//a[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(500);
		
	//Enter name
	
	WebElement limit= driver.findElement(By.id("P41_UAPL_VALUE_LIMIT"));
	
	limit.clear();
	
	limit.sendKeys(NewLimit);
	
	// Click create

	WebElement Create = driver.findElement(By.id("B88901719584714688"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(Create).click().build().perform();	
	
	Thread.sleep(500);	
		
	//Check success msg
	
	String Msg= driver.findElement(By.id("echo-message")).getText();
	
	String Expected_msg= "Successfully Updated Record";
	
	Assert.assertEquals(Msg, Expected_msg);
    
}

//******************************************** Method to verify edited record *****************************
public void VerifyEdited() throws InterruptedException
{
	Thread.sleep(500);

	String edit = driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[8]")).getText();
	
	Assert.assertEquals(edit, NewLimit);
}


//***************************************** Method to view inactive records *******************************
public void ViewInactive() throws InterruptedException
{
   //Click inactive record button

	WebElement inactive = driver.findElement(By.id("B90054419925875732"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(inactive).click().build().perform();	
	
	Thread.sleep(500);
		
	//view reports
	
	WebElement Report= driver.findElement(By.id("report_R90051121479838286"));
	
	Boolean report= Report.isDisplayed();
	
	Thread.sleep(500);
	
	driver.navigate().back();
	
	Assert.assertTrue(report);
	
   }

}
