package UserAdminTab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common_methods.CommonMethods;

public class UserAdminTests extends CommonMethods{
	
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		Thread.sleep(400);
		
		click_UserAdmin();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
	  driver.quit();
		
	}
	
	@Test
	public void a_reset() throws InterruptedException
	{
		Thread.sleep(300);
		reset();
	}
	@Test
	public void b_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void c__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void d_Search_by_privateReport() throws InterruptedException
	{
		search_by_private_report();
	}
	
	@Test
	public void e_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
	@Test
	public void ea_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","users.csv"));
	}
	
	@Test
	public void eb_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
		
	}
	@Test
	public void f_Click_EditUser() throws InterruptedException
	{
		Thread.sleep(500);
		Click_EditUser();
	}
	
	@Test
	public void g_EditName() throws InterruptedException
	{
		Thread.sleep(500);
		Edit_name();
	}
	
	@Test
	public void h_EditUser_SuccessMsg() throws InterruptedException
	{
		Thread.sleep(500);
		check_SuccessMsg();
	}
	@Test
	public void i_Verify_Edit_User() throws InterruptedException
	{
		Thread.sleep(1000);
		Verify_EditUser();
	}
	
	

///////////////////////////////////////////////////////////////////////////////////////////
//************************************ Method to check success message *************************
public void check_SuccessMsg()
	{
		String Msg= driver.findElement(By.id("echo-message")).getText();
		
		String Expected_msg= "Action Processed.";
		
		Assert.assertEquals(Msg, Expected_msg);
	}
	
	
//************************************ Method to click edit user *********************************
public void Click_EditUser()
	{
	    WebElement edit=  driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));
		
	    Actions act= new Actions(driver);
		
	    act.moveToElement(edit).click().build().perform();
	}
	
//************************************ Method to edit name *********************************
public void Edit_name()
	{
		WebElement name=  driver.findElement(By.id("P11_U_FIRST_NAME"));
		
		name.clear();
		
		name.sendKeys(Newname);
		
	/*	WebElement msisdn=  driver.findElement(By.id("P11_U_FIRST_NAME"));
		msisdn.clear();
		msisdn.sendKeys(Newname);
		
		
		WebElement email=  driver.findElement(By.id("P11_U_FIRST_NAME"));
		email.clear();
		email.sendKeys(Newname);
		*/
		//Click Apply changes
		
		WebElement changes=  driver.findElement(By.id("B4079131738667937"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(changes).click().build().perform();
		
	}
	
//************************************ Method to verify created account *************************
	public void Verify_EditUser() throws InterruptedException
		{
		   	Thread.sleep(400);
		
		   	Reset();
			
		   	Thread.sleep(800);
		   	
		   	WebElement user=  driver.findElement(By.id("apexir_SEARCH"));
			
		   	user.clear();
			
		   	user.sendKeys(Newname);
			
			//Click GO 
			
		   	WebElement GO=  driver.findElement(By.id("apexir_btn_SEARCH"));
			
		   	Actions act= new Actions(driver);
			
		   	act.moveToElement(GO).click().build().perform();
			
		   	Thread.sleep(1200);
			
		   	String name= driver.findElement(By.xpath("//tr[2]//td[3]")).getText();
			
		   	Assert.assertEquals(name, Newname);
		}
	
//************************ Method to test reset button on user tab *******************
public void reset() throws InterruptedException
	{
		Reset();
		 
		// Verify report
			
		WebElement primaryreport= driver.findElement(By.id("4075614418667933"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}
	
//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
	{
	 	expvalue= "test";
	 	col= "First Name";	

	 	view_filtered_report();

		//Verify report
		
	 	Thread.sleep(400);
		
	 	String verifyreport= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[3]")).getText();
		
	 	Thread.sleep(300);
		
	 	Assert.assertEquals(verifyreport, expvalue);
			
	}
	
//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
	{
		report= "1. Test report"; 
		
		save_report();
		 	
		 // Verify saved report
		
		Thread.sleep(500);
		
		Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
		
		dropdown.selectByVisibleText(report);
	
	}
	
//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
	{
		Search_by_private_report();
	
		// verify 
	
		Thread.sleep(300);
		
		String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
		
		System.out.println(text);
		
		Assert.assertEquals(text, verifyreport);
		
	}
	
//*********************************** Method to search primary report *********************
	public void search_by_PrimaryReports() throws InterruptedException
	{
		try {
	
			select_primary_report();
			
			Thread.sleep(300);
			
			WebElement primaryreport= driver.findElement(By.id("32713711641861233"));
			
			Boolean report= primaryreport.isDisplayed();
			
			Assert.assertTrue(report);
			
		} catch(Exception e){
		
			select_primary_report();
			
			Thread.sleep(300);
			
			WebElement primaryreport= driver.findElement(By.id("4075614418667933"));
			
			Boolean report= primaryreport.isDisplayed();
			
			Assert.assertTrue(report);
		}
		
	}
}
