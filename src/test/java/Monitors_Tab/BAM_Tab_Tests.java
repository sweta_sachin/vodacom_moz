package Monitors_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class BAM_Tab_Tests extends common_methods.CommonMethods {
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_MonitorsTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}
	
	@Test
	public void a_View_BAM_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		view_BAMTab();
	}
	
	@Test
	public void b_Click_BAM_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		click_BAMTab();
	}
	
	@Test
	public void c_ViewAllProcesses() throws InterruptedException
	{
		Thread.sleep(300);
		View_All_Processes();
	}
	
	@Test
	public void d_ViewProcessesByStatus() throws InterruptedException
	{
		Thread.sleep(300);
		View_Processes_By_Status();
		
	}
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//****************************************** Method to view BAM tab *************************************
public void view_BAMTab()
{
	WebElement BAM= driver.findElement(By.linkText("BAM"));
	
	Boolean bam= BAM.isDisplayed();
	
	Assert.assertTrue(bam);
}

//***************************************** Method to click BAM tab ***********************************
public void click_BAMTab() throws InterruptedException
{
	WebElement BAM= driver.findElement(By.linkText("BAM"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(BAM).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement verify= driver.findElement(By.xpath("//a[@class='echo-current']"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
 }

//****************************** Method to view all processes on selected date *******************************
public void View_All_Processes() throws InterruptedException
{
	//click calendar to select date
		
	WebElement calendar= driver.findElement(By.xpath("//img[@class='ui-datepicker-trigger']"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(calendar).click().build().perform();
	
	Thread.sleep(300);
		
	//select date
	
	WebElement date= driver.findElement(By.xpath("//a[contains(text(),'11')]"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(date).click().build().perform();
	
	Thread.sleep(300);
		
	//verify report
	
	WebElement verify= driver.findElement(By.id("R3489617877627204_chart"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
		
   }

//************************************* Method to view processes by status ***********************************
public void View_Processes_By_Status()
{
	//Select filter
	
	Select filter= new Select(driver.findElement(By.id("P100_PROCESS")));
	
	filter.selectByIndex(0);
	
	//verify report
	
	WebElement verify= driver.findElement(By.id("R3727500004677343_chart"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
   }


}

