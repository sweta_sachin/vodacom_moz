 package Monitors_Tab;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Integrity_Monitor_Tab_Tests extends common_methods.CommonMethods {
	
	
		
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_DealersWebTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}
	
	@Test
	public void a_View_Monitors_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		view_MonitorsTab();
	}
	
	@Test
	public void b_Click_Monitors_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		click_MonitorsTab();
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(1000);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void g_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","integrity_monitors.csv"));
	}
	
	@Test
	public void h_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
		
	}
	
	@Test
	public void ha_flashBackTest() throws InterruptedException
	{
		Thread.sleep(500);
		flashback_filter();
	}	
	
	@Test
	public void i_reset_test() throws InterruptedException
	{
		Thread.sleep(500);
		reset();
	}
	  
	@Test
	public void j_Create_Monitor() throws InterruptedException
	{
		Thread.sleep(300);
		Create_Monitor();
	}
	
	@Test
	public void k_Verify_CreatedMonitor() throws InterruptedException
	{
		Thread.sleep(3000);
		VerifyCreated();
	} 
	
	@Test
	public void l_Edit_Monitor() throws InterruptedException
	{
		Thread.sleep(300);
		Edit_Monitor() ;
	}
	
	@Test
	public void m_Verify_EditedMonitor() throws InterruptedException
	{
		Thread.sleep(3000);
		Verify_edit();
	}
  
	@Test
	public void n_Create_Checks() throws InterruptedException
	{
		Thread.sleep(2500);
		Create_checks();
	}
	
	@Test
	public void o_Verify_CreatedChecks() throws InterruptedException
	{
		Thread.sleep(3000);
		VerifyCreatedChecks();
	}
	
	@Test
	public void p_Edit_Checks() throws InterruptedException
	{
		Thread.sleep(1300);
		Edit_Checks();
	}
	
	@Test
	public void q_Verify_EditChecks() throws InterruptedException
	{
		Thread.sleep(3000);
		Verify_editChecks();
	}
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Monitors tab ******************************
  public void view_MonitorsTab()
	{
		WebElement Monitors= driver.findElement(By.linkText("Monitors"));
		
		Boolean Monitor= Monitors.isDisplayed();
	
		Assert.assertTrue(Monitor);
	}
		
//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
  {
	col= "Type";
	expvalue= "Generic";
	
	view_filtered_report();
	
	// Verify report
	
	Thread.sleep(300);
	
	String verifyreport= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[3]")).getText();
	
	Thread.sleep(300);
	
	Assert.assertEquals(verifyreport, expvalue);
	}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
	{
		report= "2. Test report"; 
	
		save_report();

	// Verify saved report
		
		Thread.sleep(500);
		
		Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
		
		dropdown.selectByVisibleText(report);
	}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
	{
		Search_by_private_report();	
		
		// verify 
		
		Thread.sleep(300);
		
		String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
		
		System.out.println(text);
		
		Assert.assertEquals(text, verifyreport);
		
	}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
	{
		select_primary_report();
	
		Thread.sleep(300);
		
		try {
		
			WebElement primaryreport= driver.findElement(By.id("4946130720776080"));
			
			Boolean report= primaryreport.isDisplayed();
			
			Assert.assertTrue(report);
			
		}catch(Exception e) 	{
		
			WebElement primaryreport= driver.findElement(By.id("8104625009989019"));
			
			Boolean report= primaryreport.isDisplayed();
			
			Assert.assertTrue(report);
			
		}
	}

//*********************************** Method to test reset button on CIB ***********************************

public void reset() throws InterruptedException
	{

	Reset();
	
	Thread.sleep(800);

	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("8104625009989019"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
	
	}

//*********************************************** Method to create Monitor ************************************

public void Create_Monitor() throws InterruptedException

{
	//click create
	
	WebElement create = driver.findElement(By.id("B8105224146989022"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(create).click().build().perform();	
	
	Thread.sleep(800);
	
	//Enter name
	
	WebElement name= driver.findElement(By.id("P15_IM_NAME"));
	
	name.clear();
	
	name.sendKeys(Name);
	
	//click create
	
	WebElement create1 = driver.findElement(By.id("B8100616499988988"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(create1).click().build().perform();	
	
	Thread.sleep(300);
	
	//click cancel to cancel adding checks
	
	WebElement cancel = driver.findElement(By.id("B8112115915209505"));

	Actions act2= new Actions(driver);
	
	act2.moveToElement(cancel).click().build().perform();	
	
	Thread.sleep(300);
		
	//click cancel to go to home page
	
	WebElement Cancel = driver.findElement(By.id("B8119414342267651"));
	
	Actions act3= new Actions(driver);
	
	act3.moveToElement(Cancel).click().build().perform();	
	
	Thread.sleep(500);
		
}

//************************* Method to verify created Monitor ************************************************
public void VerifyCreated() throws InterruptedException
{
	Reset();

	Thread.sleep(2500);
	
	//Enter contact name in the search box
	
	WebElement name= driver.findElement(By.id("apexir_SEARCH"));
	
	name.clear();
	
	name.sendKeys(Name);
		
	//click go
	
	WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(300);
		
	//Verify name
		
	String text1= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
	
	Assert.assertEquals(text1, Name);
		
 }

//*************************************** Method to edit Monitor ****************************************
public void Edit_Monitor() throws InterruptedException
{
  //click edit

	WebElement edit = driver.findElement(By.xpath("//tr[@class='even']//td//a//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(300);
		
	//change the Monitor name
	
	WebElement name= driver.findElement(By.id("P15_IM_NAME"));
	
	name.clear();
	
	name.sendKeys(Name1);
		
	//Click apply changes
	
	WebElement go = driver.findElement(By.id("B8100724272988988"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();	
	
	Thread.sleep(300);
	
	 }

//****************************************** Method to verify edit *************************************
public void Verify_edit() throws InterruptedException
{
	Reset();
	
	Thread.sleep(2500);
	
	//Enter contact name in the search box
	
	WebElement name= driver.findElement(By.id("apexir_SEARCH"));
	
	name.clear();
	
	name.sendKeys(Name1);
		
	//click go
	
	WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(300);
		
	//Verify name
	
	String text1= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
	
	Assert.assertEquals(text1, Name1);
	
 }

//*********************************************** Method to create checks ************************************
public void Create_checks() throws InterruptedException
{
	Thread.sleep(500);
 
	//click checks

	WebElement checks= driver.findElement(By.xpath("//tr[2]//td[11]//a[1]"));
	
	Actions act0= new Actions(driver);
	
	act0.moveToElement(checks).click().build().perform();
	
	Thread.sleep(400);
		
	//click create
	
	WebElement create = driver.findElement(By.id("B8117617471209523"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(create).click().build().perform();	
	
	Thread.sleep(800);
		
	//Enter name
	
	WebElement name= driver.findElement(By.id("P25_IMC_NAME"));
	
	name.clear();
	
	name.sendKeys(Name);
	
	//Enter Description
	
	WebElement description= driver.findElement(By.id("P25_IMC_DESCRIPTION"));

	description.clear();
	
	description.sendKeys(Name);
		
	//click create
	
	WebElement create1 = driver.findElement(By.id("B8111718771209505"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(create1).click().build().perform();	
	
	Thread.sleep(300);
	
}

//************************* Method to verify created checks ************************************************

public void VerifyCreatedChecks() throws InterruptedException
{
	Reset();

	Thread.sleep(2500);
		
	//Enter contact name in the search box
	
	WebElement name= driver.findElement(By.id("apexir_SEARCH"));
	
	name.clear();
	
	name.sendKeys(Name);
		
	//click go
	
	WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(300);
		
	//Verify name
	
	String text1= driver.findElement(By.xpath("//tr[2]//td[3]")).getText();
	
	Assert.assertEquals(text1, Name);
	
}

//*************************************** Method to edit checks ****************************************
public void Edit_Checks() throws InterruptedException
{
	//click edit
	
	WebElement edit = driver.findElement(By.xpath("//tr[@class='even']//td//a//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(300);
	
	//change the check name
	
	WebElement name= driver.findElement(By.id("P25_IMC_NAME"));
	
	name.clear();
	
	name.sendKeys(Name1);
	
	Thread.sleep(300);
		
	//Click apply changes
	
	WebElement go = driver.findElement(By.id("B8111801623209505"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();	
	
	Thread.sleep(300);
	
}

//****************************************** Method to verify edit *************************************

public void Verify_editChecks() throws InterruptedException
{
		
	Reset();

	Thread.sleep(2500);
		
	//Enter contact name in the search box
	
	WebElement name= driver.findElement(By.id("apexir_SEARCH"));
	
	name.clear();
	
	name.sendKeys(Name1);
		
	//click go
	
	WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(300);
		
	//Verify name
	
	String text1= driver.findElement(By.xpath("//tr[@class='even']//td[2]")).getText();
	
	Assert.assertEquals(text1, Name1);

}

//*************************************** Method to delete checks ****************************************
public void Delete_Checks() throws InterruptedException
{
	//click edit
		
	WebElement edit = driver.findElement(By.xpath("//tr[@class='even']//td//a//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(500);
	
	//Click delete
	
	WebElement delete = driver.findElement(By.id("B8111905614209505"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(delete).click().build().perform();	
	
	Thread.sleep(500);
	
	Alert alert= driver.switchTo().alert();
	
	alert.accept();
 }

//****************************************** Method to verify delete *************************************

public void Verify_delete_Checks() throws InterruptedException
{
	Reset();

	Thread.sleep(2500);
		
	//Enter contact name in the search box
		
	WebElement name= driver.findElement(By.id("apexir_SEARCH"));
		
	name.clear();
		
	name.sendKeys(Name1);
		
	//click go
	
	WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(300);
	
	//Verify name
	
	String text1= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
	
	Assert.assertNotEquals(text1, Name1);
}

//*********************************************** Method to create schedule ************************************
public void Create_schedule() throws InterruptedException
{
	Thread.sleep(500); 

	driver.navigate().back();
	
	Thread.sleep(500);
	
	driver.navigate().back();
	
	//Click schedule
	
	WebElement schedule= driver.findElement(By.xpath("//tr[2]//td[12]//a[1]"));
	
	Actions act00= new Actions(driver);
	
	act00.moveToElement(schedule).click().build().perform();
	
	Thread.sleep(500);
	
	//click create
	
	WebElement create = driver.findElement(By.id("B8127532435401429"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(create).click().build().perform();	
	
	Thread.sleep(800);
	
	//Enter name
	
	WebElement name= driver.findElement(By.id("P29_IMS_NAME"));
	
	name.clear();
	
	name.sendKeys(Name);
	
	//click calendar 
	
	WebElement Date= driver.findElement(By.xpath("//img[@class='ui-datepicker-trigger']"));
	
	Actions act12= new Actions(driver);
	
	act12.moveToElement(Date).click().build().perform();
	
	Thread.sleep(200);
	
	//	Select date

	WebElement date= driver.findElement(By.xpath("//div[@id='ui-datepicker-div']//tr[2]//td[1]//a[1]"));
	
	Actions act11= new Actions(driver);
	
	act11.moveToElement(date).click().build().perform();
		
	//click close calendar
	
	WebElement close= driver.findElement(By.xpath("//button[@class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all']"));
	
	Actions act10= new Actions(driver);
	
	act10.moveToElement(close).click().build().perform();
		
	//click create
	
	WebElement create1 = driver.findElement(By.id("B8123109162401408"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(create1).click().build().perform();	
	
	Thread.sleep(300);
	
}

//************************* Method to verify created schedule ************************************************
public void VerifyCreatedSchedule() throws InterruptedException
	{
		
		Reset();
		
		Thread.sleep(2500);
		
	//Enter contact name in the search box
		
		WebElement name= driver.findElement(By.id("apexir_SEARCH"));
		
		name.clear();
		
		name.sendKeys(Name);
	
	//click go

		WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
		
		Actions act1= new Actions(driver);
		
		act1.moveToElement(go).click().build().perform();	
		
		Thread.sleep(300);
	
	//Verify name

		String text1= driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
		
		Assert.assertEquals(text1, Name);
	
}

//*************************************** Method to edit schedule ****************************************
public void Edit_Schedule() throws InterruptedException
{
	//click edit
		
	WebElement edit = driver.findElement(By.xpath("//tr[@class='even']//td//a//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(300);
	
	//change the check name
	
	WebElement name= driver.findElement(By.id("P29_IMS_NAME"));
	
	name.clear();
	
	name.sendKeys(Name1);
	
	Thread.sleep(300);
		
	//Click apply changes
	
	WebElement go = driver.findElement(By.id("B8123226779401408"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();	
	
	Thread.sleep(300);
		
}

//****************************************** Method to verify edit *************************************
public void Verify_editSchedule() throws InterruptedException
{
	Reset();

	Thread.sleep(2500);
				
	//Enter contact name in the search box
	
	WebElement name= driver.findElement(By.id("apexir_SEARCH"));
	
	name.clear();
	
	name.sendKeys(Name1);
	
	//click go
	
	WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(go).click().build().perform();	
	
	Thread.sleep(300);
		
	//Verify name
	
	String text1= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[2]")).getText();
	
	Boolean verify= text1.endsWith(Name1);
	
	Assert.assertTrue(verify);
			
  }

}
