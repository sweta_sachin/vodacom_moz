package Monitors_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ChurnList_Tab_Tests extends common_methods.CommonMethods {
	
		
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_MonitorsTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}
	
	@Test
	public void a_View_ChurnList_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		view_ChurnListTab();
	}
	
	@Test
	public void b_Click_ChurnList_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		click_ChurnListTab();
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(1000);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void h_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
		
	}
		
	@Test
	public void i_reset_test() throws InterruptedException
	{
		Thread.sleep(500);
		reset();
	}
	
	@Test
	public void z_flashBackTest() throws InterruptedException
	{
		Thread.sleep(500);
		flashback_filter();
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Churn List tab ******************************
  public void view_ChurnListTab()
	{

	  WebElement Monitors= driver.findElement(By.linkText("Churn List"));
		
	  Boolean Monitor= Monitors.isDisplayed();
		
	  Assert.assertTrue(Monitor);
	}

//********************************* Method to click Churn List tab ******************************
public void click_ChurnListTab() throws InterruptedException
	{
		WebElement Monitors= driver.findElement(By.linkText("Churn List"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(Monitors).click().build().perform();
        
		Thread.sleep(400);

     //Verify report
		
		WebElement verify= driver.findElement(By.xpath("//a[@class='echo-current']"));
		
		Boolean Verify= verify.isDisplayed();
		
		Assert.assertTrue(Verify);
	}
		
//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
  {
	 col= "Type";
	 expvalue= "Generic";
	 
	view_filtered_report();

	// Verify report
		
	Thread.sleep(300);
	
	String verifyreport= driver.findElement(By.xpath("//tr[2]//td[3]")).getText();
	
	Thread.sleep(300);
	
	Assert.assertEquals(verifyreport, expvalue);
	
  }

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
	{
		report= "2. Test report"; 
	
		save_report();
   	 
	// Verify saved report
		
		Thread.sleep(500);
		
		Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
		
		dropdown.selectByVisibleText(report);
	}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
	{
		Search_by_private_report();
		
		// verify 
		
		Thread.sleep(300);
		
		String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
		
		System.out.println(text);
		
		Assert.assertEquals(text, verifyreport);
	
	}

//*********************************** Method to search primary report *********************

public void search_by_PrimaryReports() throws InterruptedException

{

	select_primary_report();
	
	Thread.sleep(300);
	
	try {
		
		WebElement primaryreport= driver.findElement(By.id("4946130720776080"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}catch(Exception e) 	{
	
		WebElement primaryreport= driver.findElement(By.id("8104625009989019"));
		
		Boolean report= primaryreport.isDisplayed();

		Assert.assertTrue(report);
		
	}
	
}

//*********************************** Method to test reset button on CIB ***********************************
public void reset() throws InterruptedException
	{
	
	Reset();

	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("8104625009989019"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
	
	}
}

