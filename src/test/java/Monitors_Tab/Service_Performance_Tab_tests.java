package Monitors_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Service_Performance_Tab_tests  extends common_methods.CommonMethods {
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_MonitorsTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}
	
	@Test
	public void a_View_ServicePerformance_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		view_ServicePerformanceTab();
	}
	
	@Test
	public void b_Click_ServicePerformance_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		click_ServicePerformanceTab();
	}
	
	@Test
	public void c_ViewSuccessRateForSpecificDate() throws InterruptedException
	{
		Thread.sleep(300);
		ViewSuccessRateForSpecificDate();
	}
	
	@Test
	public void d_ViewAverageDuration() throws InterruptedException
	{
		Thread.sleep(300);
		View_AverageDuration();
		
	}
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//****************************************** Method to view Service Performance tab *************************************
public void view_ServicePerformanceTab()
{
	WebElement SP= driver.findElement(By.linkText("Service Performance"));
	
	Boolean sp= SP.isDisplayed();
	
	Assert.assertTrue(sp);
}

//***************************************** Method to click Service Performance tab ***********************************
public void click_ServicePerformanceTab() throws InterruptedException
{
	WebElement SP= driver.findElement(By.linkText("Service Performance"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(SP).click().build().perform();
	
	Thread.sleep(400);

//Verify report
	
	WebElement verify= driver.findElement(By.xpath("//a[@class='echo-current']"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
 }

//****************************** Method to View Success Rate For Specific Date *******************************
public void ViewSuccessRateForSpecificDate() throws InterruptedException
{
	//click calendar to select date
	
	WebElement calendar= driver.findElement(By.xpath("//img[@class='ui-datepicker-trigger']"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(calendar).click().build().perform();
	
	Thread.sleep(300);
	
	//select date
	
	WebElement date= driver.findElement(By.xpath("//a[contains(text(),'11')]"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(date).click().build().perform();
	
	Thread.sleep(300);
	
	//verify report
	
	WebElement verify= driver.findElement(By.id("R3946711870177988_chart"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
		
   }

//************************************* Method to view Average Duration in seconds  ***********************************
public void View_AverageDuration()
{
	//verify report
	
	WebElement verify= driver.findElement(By.id("R3963732266170947_chart"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
	
   }

}
