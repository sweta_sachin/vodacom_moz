package Monitors_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ScheduledChecks_Tab_tests  extends common_methods.CommonMethods{
	
	
		
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_MonitorsTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}

	
	@Test
	public void a_View_ScheduledChecks_Tab()
	{
		view_ScheduledChecksTab();
	}

	@Test
	public void b_Click_ScheduledChecksTab()
	{
		click_ScheduledChecksTab();
	}
	

	@Test
	public void c__Save_Report() throws InterruptedException
	{
		Thread.sleep(900); 
		Save_report();
	}
	
	
	@Test
	public void d_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
	@Test
	public void g_flashBackTest() throws InterruptedException
	{
		Thread.sleep(500);
		flashback_filter();
	}
	
	@Test
	public void h_reset_test() throws InterruptedException
	{
		Thread.sleep(300);
		reset();
	}
	
	@Test
	public void i_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","open_scheduled_checks_workflows.csv"));
	}
	
	@Test
	public void j_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","open_scheduled_checks_workflows.htm"));
	}
	
	@Test
	public void k_emailReport() throws InterruptedException
	{
		Thread.sleep(300);
		email_report();
	}
	
	
	@Test
	public void l_ViewCompleteWorkFlow() throws InterruptedException
	{
		Thread.sleep(300);
		View_Complete_Workflow();
	}
		
	@Test
	public void m_ViewStepsForTheWorkflow() throws InterruptedException
	{
		Thread.sleep(300);
		 View_Steps_for_Workflow();
		
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Scheduled Checks tab ******************************
public void view_ScheduledChecksTab()
{
	WebElement sc= driver.findElement(By.linkText("Scheduled Checks"));
	
	Boolean SC= sc.isDisplayed();
	
	Assert.assertTrue(SC);
}

//********************************* Method to click Scheduled Checks tab ******************************
public void click_ScheduledChecksTab()
{
	WebElement SC= driver.findElement(By.linkText("Scheduled Checks"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(SC).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}	

//************************************** Method to apply filter and view filtered report **********************
 public void View_filtered_report() throws InterruptedException
	{
	 	col= "Status";
		expvalue= "Open";
	
		view_filtered_report();	
		
		// Verify report
	 	
		Thread.sleep(500);
	 	
		String verifyreport= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[3]")).getText();
	 	
		Thread.sleep(300);
	 	
		Assert.assertEquals(verifyreport, expvalue);
	}
  
//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
	{
		report= "2. Test report"; 

		save_report();
		 	
		 // Verify saved report
		
		Thread.sleep(500);
		
		Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
		dropdown.selectByVisibleText(report);
		
	}
	
//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
	{
		Search_by_private_report();
		
		// verify 
		
		Thread.sleep(300);
		
		String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
		
		System.out.println(text);
	}


//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
	{
		select_primary_report();
		
		Thread.sleep(500);
		
		WebElement primaryreport= driver.findElement(By.id("51244400291043480"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}


//************************ Method to test reset button  *******************
public void reset() throws InterruptedException
	{
		Reset();
		
		// Verify report
		
		WebElement primaryreport= driver.findElement(By.id("51244400291043480"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}	

//************************************** Method to view Complete workflow ********************************

public void View_Complete_Workflow() throws InterruptedException
{
	//Click view complete workflow button

	WebElement workflow = driver.findElement(By.id("B55639718139315757"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(workflow).click().build().perform();	
	
	Thread.sleep(1500);
		
	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("B55640117231322270"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Thread.sleep(300);
	
	driver.navigate().back();
	
	Assert.assertTrue(report);
	
}

//***************************************** Method to view steps for the worflow *****************************

public void View_Steps_for_Workflow() throws InterruptedException
{
	//click Steps link

	WebElement workflow = driver.findElement(By.xpath("//tr[2]//td[5]//a[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(workflow).click().build().perform();	
	
	Thread.sleep(1500);
		
	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("report_R51247203704083155"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Thread.sleep(300);
	
	driver.navigate().back();
	
	Assert.assertTrue(report);
   	
	}

}
