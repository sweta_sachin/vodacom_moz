package Monitors_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Provisiong_Tab_tests extends common_methods.CommonMethods {
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_MonitorsTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	 driver.quit();			
	}
	
	@Test
	public void a_View_Provisioning_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		view_ProvisioningTab();
	}
	
	@Test
	public void b_Click_Provisioning_Tab() throws InterruptedException
	{
		Thread.sleep(300);
		click_ProvisioningTab();
	}
	
	@Test
	public void c_ViewStatisticsReportForSpecifiedData() throws InterruptedException
	{
		Thread.sleep(300);
		 ViewStatisticsReportForSpecifiedData();
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Provisioning tab ******************************
public void view_ProvisioningTab()
{
	WebElement Monitors= driver.findElement(By.linkText("Provisioning"));
	
	Boolean Monitor= Monitors.isDisplayed();
	
	Assert.assertTrue(Monitor);
}

//********************************* Method to click Provisioning tab ******************************
public void click_ProvisioningTab() throws InterruptedException
{
	WebElement Monitors= driver.findElement(By.linkText("Provisioning"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Monitors).click().build().perform();
	
	Thread.sleep(400);

//Verify report
	
	WebElement verify= driver.findElement(By.xpath("//a[@class='echo-current']"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
 }
//********************************* Method to view statistics report for specified data **********************
public void ViewStatisticsReportForSpecifiedData() throws InterruptedException
{
	// click calendar
	
	WebElement calendar= driver.findElement(By.xpath("//tr[1]//td[2]//img[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(calendar).click().build().perform();
	
	Thread.sleep(300);
	
	// Enter date
	
	WebElement date= driver.findElement(By.xpath("//tr[2]//td[2]//a[1]"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(date).click().build().perform();
	
	Thread.sleep(500);
	
	//Click Apply
	
	WebElement apply= driver.findElement(By.id("P20_APPLY2"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(apply).click().build().perform();
	
	Thread.sleep(300);
	
	//Verify
	
	WebElement verify= driver.findElement(By.xpath(" add report locator "));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);

  }

}

